package com.app.fol;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.app.data.repository.DataClassesRealmModule;
import com.app.domain.model.DomainClassesRealmModule;
import com.app.fol.di.application.ApplicationComponent;
import com.app.fol.di.application.ApplicationModule;
import com.app.fol.di.application.DaggerApplicationComponent;
import com.app.fol.di.network.NetworkModule;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.orhanobut.hawk.HawkBuilder;
import com.orhanobut.hawk.Parser;

import java.lang.reflect.Type;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class FosApplication extends MultiDexApplication {
    public static Context applicationContext;
    public static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = this;
        initRealm();
        rebuildApplicationComponent();

        initHawk(applicationComponent.gson());
        initCrashlytics();
        initCallygraphy();
    }

    public void rebuildApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule((Application) applicationContext))
                .networkModule(new NetworkModule(getString(R.string.base_url)))
                .build();
    }

    private void initCrashlytics() {
        if (BuildConfig.CRASHLYTICS_ENABLED)
            Fabric.with(this, new Crashlytics());
    }

    private void initCallygraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void initHawk(Gson gson) {
        new HawkBuilder(applicationContext).setParser(new Parser() {
            @Override
            public <T> T fromJson(String content, Type type) throws Exception {
                return gson.fromJson(content, type);
            }

            @Override
            public String toJson(Object body) {
                return gson.toJson(body);
            }
        }).build();
    }

    private Realm initRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(String.format("%s.realm", getString(R.string.app_name)))
                .modules(new DataClassesRealmModule(), new DomainClassesRealmModule())
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        return Realm.getDefaultInstance();
    }

}
