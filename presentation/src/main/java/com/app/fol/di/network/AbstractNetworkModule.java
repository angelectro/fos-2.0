package com.app.fol.di.network;


import com.app.domain.preferences.internal.ApplicationPreferences;
import com.google.gson.Gson;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public abstract class AbstractNetworkModule {

    private final String baseUrl;

    public AbstractNetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(@Named("logging") HttpLoggingInterceptor loggingInterceptor,
                                     Interceptor interceptor, ApplicationPreferences preferences) {
        final long REQUEST_TIMEOUT = 30;

        OkHttpClient okhttpClient = new OkHttpClient.Builder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .followRedirects(false)
                .followSslRedirects(false)
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .addInterceptor(interceptor)
                .addNetworkInterceptor(loggingInterceptor) //since network interceptor comes after application ones
                .addNetworkInterceptor(chain -> {
                    Request.Builder builder = chain.request().newBuilder();
                    builder.addHeader("Content-Type", "application/json;charset=utf-8")
                            .addHeader("Accept", "application/json")
                            .removeHeader("Accept-Encoding");
                    if (preferences.authCredentialsModel().getOptional().isPresent()) {
                        builder.addHeader("X-PASW", preferences.authCredentialsModel().get().getPassword());
                    }
                    return chain.proceed(builder.build());
                })
                .build();
        return okhttpClient;
    }

    @Provides
    @Singleton
    Retrofit provideRestAdapter(Gson gson, OkHttpClient okhttp) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okhttp)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    @Provides
    @Named("logging")
    HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }
}
