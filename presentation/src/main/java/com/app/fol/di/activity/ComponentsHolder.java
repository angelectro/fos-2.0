package com.app.fol.di.activity;


public interface ComponentsHolder {
    <T> T getComponent(Class<T> aClass);
}
