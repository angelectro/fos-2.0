package com.app.fol.di.application;

import android.app.Application;
import android.content.Context;

import com.app.data.repository.AuthorizationRepositoryImpl;
import com.app.data.repository.BreakRepositoryImpl;
import com.app.data.repository.CityRepositoryImpl;
import com.app.data.repository.ContactRepositoryImpl;
import com.app.data.repository.FileRepositoryImpl;
import com.app.data.repository.FinishFieldsRepositoryImpl;
import com.app.data.repository.NewsRepositoryImpl;
import com.app.data.repository.PreparationFieldsRepositoryImpl;
import com.app.data.repository.PreparationRepositoryImpl;
import com.app.data.repository.ProjectRepositoryImpl;
import com.app.data.repository.PromoterRepositoryImpl;
import com.app.data.repository.QuestionnaireRepositoryImpl;
import com.app.data.repository.ScheduleRepositoryImpl;
import com.app.data.repository.StepsRepositoryImpl;
import com.app.data.repository.SupervisorRepositoryImpl;
import com.app.data.repository.ViolationRepositoryImpl;
import com.app.data.repository.WorkRepositoryImpl;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.ServiceState;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.AuthorizationRepository;
import com.app.domain.repository.BreakRepository;
import com.app.domain.repository.CityRepository;
import com.app.domain.repository.ContactRepository;
import com.app.domain.repository.FileRepository;
import com.app.domain.repository.FinishStepFieldsRepository;
import com.app.domain.repository.InitStepFieldsRepository;
import com.app.domain.repository.NewsRepository;
import com.app.domain.repository.PreparationRepository;
import com.app.domain.repository.ProjectRepository;
import com.app.domain.repository.PromoterRepository;
import com.app.domain.repository.QuestionnaireRepository;
import com.app.domain.repository.ScheduleRepository;
import com.app.domain.repository.StepsRepository;
import com.app.domain.repository.SupervisorRepository;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.repository.WorkRepository;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.realm.Realm;
import retrofit2.Retrofit;

@Module
public class ApplicationModule {
    public static final String SYNC_SERVICE_STATE = "SYNC_SERVICE_STATE";
    public static final String PRELOADING_SERVICE_STATE = "PRELOADING_SERVICE_STATE";
    private final Application application;


    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    FolService provideAuthService(Retrofit retrofit) {
        return retrofit.create(FolService.class);
    }

    @Provides
    @Singleton
    Scheduler provideTasksScheduler() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    RealmProvider provideRealmProvider() {
        return Realm::getDefaultInstance;
    }

    @Provides
    @Singleton
    ProjectRepository projectRepository(ProjectRepositoryImpl projectRepository) {
        return projectRepository;
    }

    @Provides
    @Singleton
    AuthorizationRepository authorizationRepository(AuthorizationRepositoryImpl authorizationRepository) {
        return authorizationRepository;
    }

    @Provides
    @Singleton
    CityRepository cityRepository(CityRepositoryImpl cityRepository) {
        return cityRepository;
    }

    @Provides
    @Singleton
    SupervisorRepository supervisorRepository(SupervisorRepositoryImpl supervisorRepository) {
        return supervisorRepository;
    }

    @Provides
    @Singleton
    PromoterRepository promoterRepository(PromoterRepositoryImpl promouterRepository) {
        return promouterRepository;
    }

    @Provides
    @Singleton
    ScheduleRepository scheduleRepository(ScheduleRepositoryImpl scheduleRepository) {
        return scheduleRepository;
    }

    @Provides
    @Singleton
    PreparationRepository preparationRepository(PreparationRepositoryImpl preparationRepository) {
        return preparationRepository;
    }

    @Provides
    @Singleton
    StepsRepository stepRepository(StepsRepositoryImpl stepsRepository) {
        return stepsRepository;
    }

    @Provides
    @Singleton
    FileRepository fileRepository(FileRepositoryImpl fileRepository) {
        return fileRepository;
    }

    @Provides
    @Singleton
    InitStepFieldsRepository initStepFieldsRepository(PreparationFieldsRepositoryImpl repository) {
        return repository;
    }

    @Provides
    @Singleton
    ContactRepository contactRepository(ContactRepositoryImpl contactRepository) {
        return contactRepository;
    }

    @Provides
    @Singleton
    ViolationRepository violationRepository(ViolationRepositoryImpl violationRepository) {
        return violationRepository;
    }

    @Provides
    @Singleton
    BreakRepository breakRepository(BreakRepositoryImpl breakRepository) {
        return breakRepository;
    }

    @Provides
    @Singleton
    NewsRepository newsRepository(NewsRepositoryImpl newsRepository) {
        return newsRepository;
    }

    @Provides
    @Singleton
    WorkRepository workRepository(WorkRepositoryImpl workRepository) {
        return workRepository;
    }

    @Provides
    @Singleton
    QuestionnaireRepository questionnaireRepository(QuestionnaireRepositoryImpl questionnaireRepository) {
        return questionnaireRepository;
    }


    @Provides
    @Singleton
    FinishStepFieldsRepository finishStepFieldsRepository(FinishFieldsRepositoryImpl finishFieldsRepository) {
        return finishFieldsRepository;
    }

    @Provides
    @Named("projectId")
    int projectId(ApplicationPreferences preferences) {
        return preferences.authCredentialsModel().get().getProjectModel().getId();
    }

    @Provides
    @Singleton
    @Named(SYNC_SERVICE_STATE)
    BehaviorSubject<ServiceState> syncServiceState() {
        return BehaviorSubject.createDefault(ServiceState.NO_RUNNING);
    }


    @Provides
    @Singleton
    @Named(PRELOADING_SERVICE_STATE)
    BehaviorSubject<ServiceState> preloadingServiceState() {
        return BehaviorSubject.createDefault(ServiceState.NO_RUNNING);
    }
}
