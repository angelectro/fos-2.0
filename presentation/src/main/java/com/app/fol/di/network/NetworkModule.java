package com.app.fol.di.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.app.data.utils.RealmInteger;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmList;
import io.realm.RealmObject;
import okhttp3.Interceptor;

@Module
public class NetworkModule extends AbstractNetworkModule {


    public NetworkModule(String baseUrl) {
        super(baseUrl);
    }

    @Provides
    Gson provideGson() {
        //2017-04-21 14:47:11
        return new GsonBuilder()
                .setDateFormat(("yyyy-MM-dd HH:mm:ss"))
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .registerTypeAdapter(new TypeToken<RealmList<RealmInteger>>() {
                }.getType(), RealmInteger.GSON_TYPE_ADAPTER)
                .create();
    }


    @NonNull
    @Provides
    Interceptor authorizationInterceptor(Context context, ApplicationPreferences applicationPreferences, Gson gson) {
        return chain -> {
            return chain.proceed(chain.request());//// FIXME: 17.07.2017
        };/*new AuthorizationInterceptor(applicationPreferences, context, gson);*/
    }

}