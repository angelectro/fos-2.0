package com.app.fol.di.fos;



import com.app.fol.di.activity.ActivityModule;
import com.app.fol.di.activity.PerActivity;
import com.app.fol.di.application.ApplicationComponent;
import com.app.fol.feature.go_work.fragment.GoWorkFragment;
import com.app.fol.feature.auth.fragment.SelectProjectFragment;
import com.app.fol.feature.auth.fragment.StaffSelectionFragment;
import com.app.fol.feature.drawer.DrawerNavigationPresenterImpl;
import com.app.fol.feature.help.fragment.HelpFragment;
import com.app.fol.feature.multimedia.fragment.MultimediaFragment;
import com.app.fol.feature.news.presenter.NewsPresenter;
import com.app.fol.feature.preparationwork.fragment.PreparationStepFragment;
import com.app.fol.feature.questionnaire.presenter.QuestionnairePresenter;
import com.app.fol.feature.selectionplace.fragment.PlaceSelectionFragment;
import com.app.fol.feature.steps.presenter.StepPresenter;
import com.app.fol.feature.targetaudience.fragment.TargetAudienceFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = {ApplicationComponent.class},
        modules = { ActivityModule.class,HomeModule.class})
public interface HomeComponent {

    void inject(SelectProjectFragment selectProjectFragment);
    void inject(StaffSelectionFragment staffSelectionFragment);
    void inject(PlaceSelectionFragment placeSelectionFragment);
    void inject(DrawerNavigationPresenterImpl drawerNavigationPresenter);
    void inject(PreparationStepFragment preparationStepFragment);
    void inject(GoWorkFragment goWorkFragment);
    void inject(MultimediaFragment multimediaFragment);
    void inject(HelpFragment helpFragment);
    void inject(TargetAudienceFragment targetAudienceFragment);
    void inject(StepPresenter stepPresenter);
    void inject(NewsPresenter newsPresenter);
    void inject(QuestionnairePresenter questionnairePresenter);
}
