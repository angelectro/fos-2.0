package com.app.fol.di.settings;



import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.preferences.internal.ApplicationPreferencesImpl;

import dagger.Module;
import dagger.Provides;
import com.app.domain.model.AuthCredentialsModel;

import javax.inject.Singleton;


@Module
public class SettingsModule {
    @Provides
    @Singleton
    ApplicationPreferences provideApplicationPreferences() {
        return new ApplicationPreferencesImpl();
    }

    @Provides
    @Singleton
    AuthCredentialsModel authCredentialsModel(ApplicationPreferences applicationPreferences) {
        return applicationPreferences.authCredentialsModel().get();
    }
}
