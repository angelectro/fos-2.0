package com.app.fol.di.fos;


import com.app.domain.preferences.internal.ApplicationPreferences;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;


@Module
public class HomeModule {

    @Provides
    @Named("projectId")
    public int projectId(ApplicationPreferences preferences){
        return preferences.authCredentialsModel().get().getProjectModel().getId();
    }
}
