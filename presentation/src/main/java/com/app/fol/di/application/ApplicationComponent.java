package com.app.fol.di.application;

import android.content.Context;

import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.ServiceState;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.AuthorizationRepository;
import com.app.domain.repository.BreakRepository;
import com.app.domain.repository.CityRepository;
import com.app.domain.repository.ContactRepository;
import com.app.domain.repository.FileRepository;
import com.app.domain.repository.FinishStepFieldsRepository;
import com.app.domain.repository.InitStepFieldsRepository;
import com.app.domain.repository.NewsRepository;
import com.app.domain.repository.PreparationRepository;
import com.app.domain.repository.ProjectRepository;
import com.app.domain.repository.PromoterRepository;
import com.app.domain.repository.QuestionnaireRepository;
import com.app.domain.repository.ScheduleRepository;
import com.app.domain.repository.StepsRepository;
import com.app.domain.repository.SupervisorRepository;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.repository.WorkRepository;
import com.app.fol.di.network.NetworkModule;
import com.app.fol.di.settings.SettingsModule;
import com.app.fol.feature.steps.presenter.StepPresenter;
import com.app.fol.feature.sync.PreLoadingService;
import com.app.fol.feature.sync.SyncService;
import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;
import io.reactivex.subjects.BehaviorSubject;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, SettingsModule.class})
public interface ApplicationComponent {

    Context context();
    Scheduler tasksScheduler();
    FolService fosService();
    ApplicationPreferences applicationPreferences();
    Gson gson();
    Retrofit retrofit();
    RealmProvider realmProvider();
    ProjectRepository projectRepository();
    AuthorizationRepository authorizationRepository();
    CityRepository cityRepository();
    PromoterRepository promoterRepository();
    SupervisorRepository supervisorRepository();
    ScheduleRepository scheduleRepository();
    PreparationRepository preparationRepository();
    StepsRepository stepRepository();
    FileRepository fileRepository();
    InitStepFieldsRepository initStepFieldsRepository();
    ContactRepository contactRepository();
    ViolationRepository violationRepository();
    BreakRepository breakRepository();
    WorkRepository workRepository();
    NewsRepository newsRepository();
    FinishStepFieldsRepository finishStepFieldsRepository();
    QuestionnaireRepository questionnaireRepository();
    @Named(ApplicationModule.SYNC_SERVICE_STATE)
    BehaviorSubject<ServiceState> syncServiceStateBehaviorSubject();
    @Named(ApplicationModule.PRELOADING_SERVICE_STATE)
    BehaviorSubject<ServiceState> preloadingServiceStateBehaviorSubject();
    void inject(SyncService syncService);
    void inject(PreLoadingService preloadingService);
    void inject(StepPresenter stepPresenter);
}
