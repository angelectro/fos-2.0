package com.app.fol.feature.pdfviewer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.app.fol.R;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PdfViewerActivity extends AppCompatActivity {
    private static final String BUNDLE_URL = "URL";
    private static final String BUNDLE_PATH = "PATH";
    @BindView(R.id.pdfView)
    PDFView mPdfView;
    @BindView(R.id.toolbarTitle)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static void createViewerFromFile(Context context, String pathFile) {
        Intent intent = new Intent(context, PdfViewerActivity.class);
        intent.putExtra(BUNDLE_PATH, pathFile);
        context.startActivity(intent);
    }

    public static void createViewerFromUrl(Context context, String url) {
        Intent intent = new Intent(context, PdfViewerActivity.class);
        intent.putExtra(BUNDLE_URL, url);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);
        setSupportActionBar(mToolbar);
        ButterKnife.bind(this);
        load();
    }

    private void load() {
        PDFView.Configurator configurator = null;
        if (getIntent().hasExtra(BUNDLE_URL))
            configurator = mPdfView.fromUri(Uri.parse(getIntent().getStringExtra(BUNDLE_URL)));
        else if (getIntent().hasExtra(BUNDLE_PATH))
            configurator = mPdfView.fromFile(new File(getIntent().getStringExtra(BUNDLE_PATH)));
        else
            throw new NullPointerException();
        configurator
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                .enableAnnotationRendering(false)
                .password(null)
                .onRender((nbPages, pageWidth, pageHeight) -> mPdfView.fitToWidth())
                .scrollHandle(null)
                .enableAntialiasing(true)
                .spacing(0)
                .load();
    }

    @OnClick(R.id.button_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
