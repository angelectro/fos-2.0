package com.app.fol.feature.steps.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.Switch;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.domain.model.steps.ChoiceModel;
import com.app.domain.model.steps.TypeAttachment;
import com.app.fol.R;
import com.app.fol.base.progress.AlertUtils;
import com.app.fol.base.progress.ProgressView;
import com.app.fol.base.progress.StepProgressView;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.home.activity.HomeActivity;
import com.app.fol.feature.multimedia.fragment.MultimediaFragment;
import com.app.fol.feature.news.fragment.NewsFragment;
import com.app.fol.feature.steps.ButtonsBuilder;
import com.app.fol.feature.steps.DialogModel;
import com.app.fol.feature.steps.SpaceBuilder;
import com.app.fol.feature.steps.presenter.FieldsPresenter;
import com.app.fol.feature.steps.presenter.StepPresenter;
import com.app.fol.feature.steps.view.ButtonColor;
import com.app.fol.feature.steps.view.StepViewNew;
import com.app.fol.utils.DialogUtils;
import com.app.fol.utils.IntentUtils;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.arellomobile.mvp.presenter.ProvidePresenterTag;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.functions.Action0;


public class StepFragmentNew extends BaseStepFragment implements StepViewNew {
    private static final String ARGS_STEP = "ARGS_STEP";
    @InjectPresenter(type = PresenterType.LOCAL)
    StepPresenter mPresenter;
    @BindView(R.id.buttonRegistrationContact)
    Button mButtonRegistrationContact;
    @BindView(R.id.grid_layout)
    GridLayout mGridLayout;
    @BindView(R.id.button_promo_materials)
    Button mButtonPromoMaterials;
    @BindView(R.id.switch_go_break)
    Switch mSwitchGoBreak;
    @BindView(R.id.switch_working_online)
    Switch mSwitchWorkingOnline;
    @BindView(R.id.progressContainer)
    ProgressView mProgressContainer;
    Unbinder unbinder;
    private MaterialDialog mMaterialDialog;
    private List<Button> mButtonList;
    private StepProgressView progressViewDialog;

    public static Fragment create(int step) {
        StepFragmentNew stepFragment = new StepFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putInt(ARGS_STEP, step);
        stepFragment.setArguments(bundle);
        return stepFragment;
    }


    @Override
    protected String toolbarTitle() {
        return getString(R.string.title_steps);
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    StepPresenter providePresenter() {
        int step = getArguments().getInt(ARGS_STEP, -1);
        HomeComponent homeComponent = ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class);
        return new StepPresenter(step, homeComponent);
    }

    @ProvidePresenterTag(presenterClass = StepPresenter.class, type = PresenterType.LOCAL)
    String provideRepositoryPresenterTag() {
        return String.valueOf(getArguments().getInt(ARGS_STEP, -1));
    }


    @Override
    protected int provideLayout() {
        return R.layout.fragment_step;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        assert rootView != null;
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void setSizeGridLayout(int rowCount, int columnCount) {
        mGridLayout.setRowCount(rowCount);
        mGridLayout.setColumnCount(columnCount);
    }

    @Override
    public void buildButton(List<ChoiceModel> choiceModels, boolean enabledButtons) {
        mButtonList = new ButtonsBuilder(getActivity())
                .setButtonsEnabled(enabledButtons)
                .setChoiceModels(choiceModels)
                .setItemMargin(getActivity().getResources().getDimensionPixelSize(R.dimen.margin_5))
                .build();
        Stream.of(mButtonList)
                .peek(button -> button.setOnClickListener(v -> mPresenter.clickedChoiceButton(button.getId())))
                .forEach(mGridLayout::addView);
    }

    @Override
    public void buildSpaces(int rowCount, int columnCount) {
        mGridLayout.post(() -> {
            if (mGridLayout != null) {
                Stream.of(new SpaceBuilder(getActivity())
                        .columnCount(columnCount)
                        .rowCount(rowCount)
                        .setOrientation(SpaceBuilder.Orientation.Horizontal)
                        .itemSize((mGridLayout.getWidth() / columnCount))
                        .build())
                        .forEach(mGridLayout::addView);

                Stream.of(new SpaceBuilder(getActivity())
                        .columnCount(columnCount)
                        .rowCount(rowCount)
                        .setOrientation(SpaceBuilder.Orientation.Vertical)
                        .itemSize((mGridLayout.getWidth() / columnCount))
                        .build())
                        .forEach(mGridLayout::addView);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        Optional.ofNullable(progressViewDialog)
                .ifPresent(StepProgressView::dismiss);
    }

    @Override
    public void segueNextStep(int stepId) {
        replaceFragment(StepFragmentNew.create(stepId), true);
    }

    @Override
    public void setEnabledRegistrationButton(boolean enabled) {
        mButtonRegistrationContact.setEnabled(enabled);
    }

    @Override
    public void setVisibleRegistrationButton(boolean isVisible) {
        mButtonRegistrationContact.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setEnabledChoiceButtons(boolean enabled) {
        Stream.range(0, mGridLayout.getChildCount())
                .map(position -> mGridLayout.getChildAt(position))
                .filter(view -> view instanceof ButtonColor)
                .map(view -> (ButtonColor) view)
                .forEach(buttonColor -> buttonColor.setEnabled(enabled));
    }

    @Override
    public void showNews() {
        replaceFragment(NewsFragment.create(), true);
    }

    @Override
    public void showMediaFiles() {
        replaceFragment(MultimediaFragment.create(), true);
    }

    @Override
    public void setVisibleAttachmentButton(boolean visible) {
        mButtonPromoMaterials.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showViolationDialog(DialogModel dialogModel) {
        mMaterialDialog = DialogUtils.buildViolation(getActivity(),
                dialogModel.getText(),
                () -> mPresenter.dialogPositiveButtonClicked(dialogModel))
                .show();
    }

    @Override
    public void showDialog(DialogModel dialogModel) {
        switch (dialogModel.getType()) {
            case Violation:
                showViolationDialog(dialogModel);
                break;
            case Warning:
                showWarningDialog(dialogModel);
                break;
            case Confirmation:
                showConfirmationDialog(dialogModel);
                break;
        }
    }

    @Override
    public void closeDialog() {
        Optional.ofNullable(mMaterialDialog)
                .ifPresent(MaterialDialog::dismiss);
    }

    @Override
    public void showWarningDialog(DialogModel dialogModel) {
        mMaterialDialog = DialogUtils.buildWarning(getActivity(), dialogModel.getText(),
                () -> mPresenter.dialogPositiveButtonClicked(dialogModel),
                () -> mPresenter.dialogNegativeButtonClicked(dialogModel))
                .show();
    }

    @Override
    public void showConfirmationDialog(DialogModel dialogModel) {
        mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .content(dialogModel.getText())
                .cancelable(false)
                .positiveText(R.string.yes)
                .negativeText(R.string.cancel)
                .onPositive((dialog, which) -> mPresenter.dialogPositiveButtonClicked(dialogModel))
                .onNegative((dialog, which) -> mPresenter.dialogNegativeButtonClicked(dialogModel))
                .show();
    }

    @Override
    public void showInfoDialog(DialogModel dialogModel) {
        mMaterialDialog = AlertUtils.showSimpleDialog(getActivity(), dialogModel.getText(),
                () -> mPresenter.dialogPositiveButtonClicked(dialogModel));
    }

    @Override
    public void setStateSwitchBreak(Boolean isChecked) {
        mSwitchGoBreak.setOnCheckedChangeListener(null);
        mSwitchGoBreak.setChecked(isChecked);
        mSwitchGoBreak.setOnCheckedChangeListener((buttonView, isChecked1) -> mPresenter.changedBreak(isChecked1));
        if (mButtonList != null) {
            if (isChecked) {
                Stream.of(mButtonList)
                        .forEach(button -> button.setEnabled(false));
                mButtonRegistrationContact.setEnabled(false);
            } else {
                if (mButtonRegistrationContact.getVisibility() == View.GONE) {
                    Stream.of(mButtonList)
                            .forEach(button -> button.setEnabled(true));
                } else {
                  mPresenter.setEnabledRegistrationButton(true);
                }
            }
        }
    }

    @Override
    public void setStateSwitchOnline(Boolean isChecked) {
        mSwitchWorkingOnline.setOnCheckedChangeListener(null);
        mSwitchWorkingOnline.setChecked(isChecked);
        mSwitchWorkingOnline.setOnCheckedChangeListener((buttonView, isChecked1) -> mPresenter.changedOnline(isChecked1));
    }

    @Override
    public void showProgress() {
        mProgressContainer.showProgressBar();
    }

    @Override
    public void hideProgress() {
        mProgressContainer.hideProgressBar();
    }

    @Override
    public void updateToolbarTitle(String name) {
        updateTitle(name);
    }

    @Override
    public void showAttachment(TypeAttachment type, File mFileForProject, String url) {
        IntentUtils.showAttachment(getActivity(), type, mFileForProject, url);
    }

    @Override
    public void recreateActivity() {
        Optional.ofNullable(progressViewDialog)
                .ifPresent(StepProgressView::dismiss);
        getMvpDelegate().onDetach();
        getMvpDelegate().onDestroyView();
        getMvpDelegate().onDestroy();
        HomeActivity.create(getActivity());
    }

    @Override
    public void hideProgressDialog() {
        progressViewDialog.hideProgressBar();
    }


    @Override
    public void showError(String message, StepPresenter.ActionType actionType) {
        if (progressViewDialog == null)
            progressViewDialog = new StepProgressView(getActivity());
        Action0 actionPositive = null;
        Action0 actionNegative = null;
        switch (actionType) {
            case GETTING_STEP:
                actionPositive = () -> mPresenter.repeatGetStep();
                break;
            case END_DAY:
                actionPositive = () -> mPresenter.repeatEndDay();
                break;
            case GET_ATTACHMENT:
                actionPositive = () -> mPresenter.repeatDownloadAttachment();
                actionNegative = () -> mPresenter.cancelDownloadAttachment();
                break;
            case CHECK_QUESTIONNAIRE_AVAILABLE:
                actionPositive = () -> mPresenter.checkQuestionnaireAvailable();
                break;
        }
        progressViewDialog.showError(message, actionPositive, actionNegative);
    }

    @Override
    public void closeError() {
        progressViewDialog.closeError();
    }

    @Override
    public void showProgressDialog(@StringRes int text, StepPresenter.ActionType actionType) {
        Optional.ofNullable(progressViewDialog)
                .ifPresent(StepProgressView::dismiss);
        switch (actionType) {
            case END_DAY:
                progressViewDialog = new StepProgressView(getActivity());
                break;
            case GET_ATTACHMENT:
                progressViewDialog = new StepProgressView(getActivity(), () -> mPresenter.cancelDownloadAttachment());
                break;
        }
        progressViewDialog.showProgressBar(text);
    }

    @Override
    public void showSuccessFinishDialog() {
        AlertUtils.showSimpleDialog(getActivity(), getActivity().getString(R.string.text_thanks), this::recreateActivity)
                .setCancelable(false);
    }

    @OnClick({R.id.buttonRegistrationContact, R.id.button_news, R.id.button_media, R.id.button_send_contacts, R.id.button_end_workday, R.id.button_promo_materials})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonRegistrationContact:
                mPresenter.clickedRegistrationButton();
                break;
            case R.id.button_news:
                mPresenter.clickedNewsButton();
                break;
            case R.id.button_media:
                mPresenter.clickedMediaButton();
                break;
            case R.id.button_send_contacts:
                mPresenter.clickedSendContactsButton();
                break;
            case R.id.button_end_workday:
                mPresenter.clickedEndWorkDayButton();
                break;
            case R.id.button_promo_materials:
                mPresenter.clickedPromoButton();
                break;
        }
    }

    @Override
    protected FieldsPresenter getPresenter() {
        return mPresenter;
    }
}
