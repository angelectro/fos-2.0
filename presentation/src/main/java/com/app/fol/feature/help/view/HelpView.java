package com.app.fol.feature.help.view;

public interface HelpView {
    void populate(String s);
}
