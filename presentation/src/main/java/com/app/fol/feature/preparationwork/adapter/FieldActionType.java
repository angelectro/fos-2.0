package com.app.fol.feature.preparationwork.adapter;

public enum FieldActionType {
    TAKE_PHOTO,
    TAKE_VIDEO,
    TAKE_AUDIO,
    SCAN_QR,
    ADD_SIGN,
    REMOVE,
    PLAY_VIDEO,
    PLAY_AUDIO
}
