package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class GroupCheckboxTypeViewImpl extends BaseItemFieldViewImpl implements GroupCheckboxTypeView {
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.root_checkbox_container)
    LinearLayout mLinearLayout;
    @BindView(R.id.text_description)
    TextView mTextDescription;

    public GroupCheckboxTypeViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mTextTitle.setText(entity.getName());
        mLinearLayout.removeAllViews();
        mTextDescription.setText(entity.getDescription());
        CompoundButton.OnCheckedChangeListener listener = (buttonView, isChecked) -> {
            List<String> ids = Optional.ofNullable(entity.getResult())
                    .map(this::stringToArray)
                    .orElse(new ArrayList<>());
            if (isChecked) {
                ids.add(buttonView.getText().toString());
                entity.setEmptyField(false);
                setActivated(false);
            } else
                ids.remove(buttonView.getText().toString());
            entity.setResult(arrayToString(ids));
        };

        Stream.of(entity.getListPreparationSubFields())
                .map(subField -> {
                    CheckBox checkBox = (CheckBox) LayoutInflater.from(getContext()).inflate(R.layout.item_checkbox_subfieid, null);
                    checkBox.setText(subField.getData());
                    checkBox.setId(subField.getId());
                    Optional.ofNullable(entity.getResult())
                            .map(this::stringToArray)
                            .map(value -> Stream.of(value).filter(text -> TextUtils.equals(text, checkBox.getText())).count())
                            .ifPresent(integer -> checkBox.setChecked(integer != 0));
                    checkBox.setOnCheckedChangeListener(listener);
                    return checkBox;
                }).forEach(mLinearLayout::addView);
    }

    private String arrayToString(List<String> strings) {
        boolean isFirst = true;
        StringBuffer stringBuffer = new StringBuffer();
        for (String string : strings) {
            if (!isFirst)
                stringBuffer.append(",");
            isFirst = false;
            stringBuffer.append(string);
        }
        return stringBuffer.toString();
    }

    private List<String> stringToArray(String s) {
        List<String> strings = new ArrayList<>();
        for (String s1 : s.split(",")) {
            strings.add(s1);
        }
        return strings;
    }
}