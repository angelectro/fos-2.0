package com.app.fol.feature.news.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.domain.model.news.NewsModel;
import com.app.fol.R;
import com.app.fol.base.adapter.GenericAdapter;
import com.app.fol.base.fragment.ToolbarBaseFragment;
import com.app.fol.base.progress.ProgressView;
import com.app.fol.base.view.NavigationView;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.news.presenter.NewsPresenter;
import com.app.fol.feature.news.view.NewsView;
import com.app.fol.utils.view.RecyclerItemSpacingDecoration;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NewsFragment extends ToolbarBaseFragment implements NewsView {

    @InjectPresenter
    NewsPresenter mNewsPresenter;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.progressContainer)
    ProgressView mProgressContainer;
    @BindView(R.id.navigationView)
    NavigationView mNavigationView;
    private Unbinder unbinder;

    public static Fragment create() {
        return new NewsFragment();
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    NewsPresenter providePresenter() {
        HomeComponent homeComponent = ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class);
        return new NewsPresenter(homeComponent);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        mNavigationView.setVisibleUpdateButton(false);
        mNavigationView.actionButtonBack(() -> getActivity().onBackPressed());
        return rootView;
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.news);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_news;
    }

    @Override
    public void showProgress() {
        mProgressContainer.showProgressBar();
    }

    @Override
    public void hideProgress() {
        mProgressContainer.hideProgressBar();
    }

    @Override
    public void showError(String message) {
        mProgressContainer.showError(message);
    }

    @Override
    public void showNews(List<NewsModel> newsModels) {
        mRecyclerView.addItemDecoration(new RecyclerItemSpacingDecoration(getActivity(), R.dimen.margin_10, R.dimen.margin_10, R.dimen.margin_10));
        mRecyclerView.setAdapter(new GenericAdapter<>(newsModels, R.layout.item_news));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
