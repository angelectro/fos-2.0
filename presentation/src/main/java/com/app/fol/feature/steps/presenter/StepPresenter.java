package com.app.fol.feature.steps.presenter;


import android.annotation.SuppressLint;

import com.app.domain.interactors.StepsInteractor;
import com.app.domain.model.FieldModel;
import com.app.domain.model.ProgressState;
import com.app.domain.model.ServiceState;
import com.app.domain.model.steps.ActionResult;
import com.app.domain.model.steps.ChoiceModel;
import com.app.domain.model.steps.StepModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.usecase.GetStepUseCase;
import com.app.domain.utils.FileUtils;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.di.application.ApplicationModule;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.steps.ActionHandler;
import com.app.fol.feature.steps.DialogModel;
import com.app.fol.feature.steps.view.StepViewNew;
import com.app.fol.feature.sync.PreLoadingService;
import com.app.fol.feature.sync.SyncService;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

import static com.app.domain.model.steps.ActionResult.Event.BREAK_IN_FIRST_HOUR_WARNING;
import static com.app.domain.model.steps.ActionResult.Event.BREAK_IN_LAST_HOUR_WARNING;
import static com.app.domain.model.steps.ActionResult.Event.MORE_ON_BREAK_PER_HOUR_WARNING;
import static com.app.domain.model.steps.ActionResult.Event.PREMATURE_TERMINATION_VIOLATION;


@InjectViewState
public class StepPresenter extends FieldsPresenter<StepViewNew> {

    private static int FIRST_STEP = -1;
    @Inject
    GetStepUseCase mGetStepUseCase;
    @Inject
    ApplicationPreferences mPreferences;
    @Inject
    StepsInteractor mStepsInteractor;

    @Inject
    @Named(ApplicationModule.SYNC_SERVICE_STATE)
    BehaviorSubject<ServiceState> mStateBehaviorSubject;

    @Inject
    @Named(ApplicationModule.PRELOADING_SERVICE_STATE)
    BehaviorSubject<ServiceState> mPreloadingStateBehaviorSubject;

    private int mStep;
    private StepModel mStepModel;
    private Disposable mSubscribeSyncServiceState;
    private Disposable mSubscribePreloadingServiceState;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public StepPresenter(int step, HomeComponent homeComponent) {
        mStep = step;
        homeComponent.inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadStep();
        mPreferences.progressState().getOptional()
                .filter(value -> value == ProgressState.COMPLETE_FINISH_QUESTIONNAIRE)
                .executeIfPresent(progressState -> completeEndDay())
                .executeIfAbsent(() -> {
                    if (mStep == FIRST_STEP) {
                        Disposable disposable = mStepsInteractor.checkFalsifications()
                                .subscribe(actionResult -> {
                                    if (actionResult.isContainsEvent())
                                        handleAction(actionResult);
                                });
                        compositeDisposable.add(disposable);
                    }
                });
        mPreferences.progressState().getOptional()
                .filter(value -> value == ProgressState.CHECK_QUESTIONNAIRE_AVAILABLE)
                .ifPresent(progressState -> checkQuestionnaireAvailable());

        mPreferences.notificationSubscribe().getOptional()
                .executeIfAbsent(() -> {
                    @SuppressLint("DefaultLocale") String topic = String.format("project_%d", mPreferences.authCredentialsModel().get().getProjectModel().getId());
                    FirebaseMessaging.getInstance().subscribeToTopic(topic);
                    mPreferences.notificationSubscribe().set(topic);
                });

    }

    private void loadStep() {
        mGetStepUseCase.setStepId(mStep)
                .buildUseCaseObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doOnTerminate(getViewState()::hideProgress)
                .subscribe(FosDefaultSubscriber.onPositiveNegative(null, (stepModel) -> {
                    mStepModel = stepModel;
                    getViewState().updateToolbarTitle(stepModel.getDescription());
                    getViewState().setSizeGridLayout(stepModel.getRows(), stepModel.getCols());
                    setVisibilityRegistrationButton(stepModel.isVisibleRegistrationButton());
                    getViewState().setVisibleAttachmentButton(mStepModel.getAttachment() != null);
                    getViewState().buildButton(stepModel.getChoices(), !stepModel.isVisibleRegistrationButton());
                    getViewState().buildSpaces(stepModel.getRows(), stepModel.getCols());
                    getViewState().setStateSwitchOnline(mPreferences.onlineState().get());
                    getViewState().setStateSwitchBreak(mPreferences.breakState().get());
                    List<FieldModel> fields = stepModel.getFields();
                    if (fields != null && !fields.isEmpty())
                        getViewState().showFields(fields);
                }, errorModel -> {
                    getViewState().showError(errorModel.getMessage(), ActionType.GETTING_STEP);
                    errorModel.getThrowable().printStackTrace();
                }));
    }

    private void setVisibilityRegistrationButton(boolean isVisible) {
        getViewState().setVisibleRegistrationButton(isVisible);
        if (isVisible) {
            setEnabledRegistrationButton(true);
        }
    }

    public void setEnabledRegistrationButton(boolean isEnabled) {
        if (isEnabled) {
            mPreferences.lastDateClickedRegistrationButton().getOptional()
                    .executeIfAbsent(() -> getViewState().setEnabledRegistrationButton(true))
                    .executeIfPresent(date -> {
                        getViewState().setEnabledRegistrationButton(false);
                        Disposable disposable = Observable.interval(0, 500, TimeUnit.MILLISECONDS)
                                .filter(__ -> !isTimeout(date))
                                .take(1)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(__ -> getViewState().setEnabledRegistrationButton(true));
                        compositeDisposable.add(disposable);
                    });
        } else {
            getViewState().setEnabledRegistrationButton(false);
        }
    }

    private boolean isTimeout(Date date) {
        Integer timeout = mPreferences.authCredentialsModel().get().getProjectModel().getContactRegistrationTimeout();
        return (new Date().getTime() - date.getTime()) / 1000 < timeout;
    }

    public void clickedChoiceButton(int id) {
        if (mStepsInteractor.checkRequiredFields(mStepModel.getFields())) {
            getViewState().displayMarkErrorFields();
        } else {
            ChoiceModel choiceModel = Stream.of(mStepModel.getChoices())
                    .filter(value -> value.getId().equals(id)).single();
            nextStepOrCreateContact(choiceModel);
        }
    }

    private void nextStepOrCreateContact(ChoiceModel choiceModel) {
        Disposable disposable = mStepsInteractor.saveStep(mStepModel, choiceModel)
                .subscribe(isLast -> {
                    if (isLast) {
                        if (mPreferences.onlineState().get()) {
                            SyncService.startService(mContext, SyncService.SyncType.SYNC_CONTACTS);
                        }
                        getViewState().recreateActivity();
                    } else {
                        getViewState().segueNextStep(choiceModel.getNextStepId());
                    }
                });
        compositeDisposable.add(disposable);
    }

    public void clickedRegistrationButton() {
        mPreferences.lastDateClickedRegistrationButton().set(new Date());
        getViewState().setEnabledRegistrationButton(false);
        getViewState().setEnabledChoiceButtons(true);
    }

    public void clickedNewsButton() {
        getViewState().showNews();
    }

    public void clickedMediaButton() {
        getViewState().showMediaFiles();
    }

    public void clickedSendContactsButton() {
        SyncService.startService(mContext, SyncService.SyncType.SYNC_CONTACTS);
    }

    public void clickedEndWorkDayButton() {
        Disposable disposable = mStepsInteractor.endWorkDay()
                .subscribe(this::handleAction);
        compositeDisposable.add(disposable);
    }

    public void changedBreak(boolean checked) {
        Disposable disposable;
        if (checked) {
            disposable = mStepsInteractor.goBreak()
                    .subscribe(actionResult -> {
                        if (actionResult.isContainsEvent()) {
                            handleAction(actionResult);
                        } else {
                            SyncService.startService(mContext, SyncService.SyncType.SYNC_WORK);
                        }
                    });
        } else {
            disposable = mStepsInteractor.endBreak()
                    .subscribe(actionResult -> {
                        SyncService.startService(mContext, SyncService.SyncType.SYNC_WORK);
                        if (actionResult.isContainsEvent()) {
                            handleAction(actionResult);
                        }
                    });
        }
        getViewState().setStateSwitchBreak(checked);
        compositeDisposable.add(disposable);
    }

    public void changedOnline(boolean checked) {
        mStepsInteractor.changedStateOnline(checked);
        if (!checked) {
            getViewState().showInfoDialog(DialogModel.create(DialogModel.Type.Info, mContext.getString(R.string.text_online_not_checked)));
        }
        getViewState().setStateSwitchOnline(checked);
    }

    public void clickedPromoButton() {
        showAttachment();
    }

    private void showAttachment() {
        String url = mStepModel.getAttachment().getPath();
        File mFileForProject = FileUtils.getFileForProject(mContext, String.valueOf(projectId), FileUtils.getFileNameFromUrl(url));
        if (mFileForProject.exists()) {
            getViewState().showAttachment(mStepModel.getAttachment().getType(), mFileForProject, url);
        } else {
            startDownloadAttachment();
        }
    }

    private void startDownloadAttachment() {
        mPreloadingStateBehaviorSubject.onNext(ServiceState.RUNNING);
        PreLoadingService.startService(mContext);
        Optional.ofNullable(mSubscribePreloadingServiceState)
                .ifPresent(Disposable::dispose);
        mSubscribePreloadingServiceState = mPreloadingStateBehaviorSubject
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgressDialog(R.string.text_progress_download_file, ActionType.GET_ATTACHMENT))
                .doOnDispose(() -> getViewState().hideProgressDialog())
                .flatMap(serviceState -> {
                    if (serviceState == ServiceState.FINISHED_WITH_ERROR)
                        return Observable.error(new Exception());
                    else
                        return Observable.just(serviceState);
                })
                .filter(serviceState -> serviceState == ServiceState.FINISHED_WITH_SUCCESS)
                .take(1)
                .subscribe(serviceState -> showAttachment(),
                        throwable -> {
                            getViewState().showError(mContext.getString(R.string.error_preloading), ActionType.GET_ATTACHMENT);
                            throwable.printStackTrace();
                        });
    }

    private void handleAction(ActionResult actionResult) {
        DialogModel dialogModel = new ActionHandler(actionResult, mContext).handle();
        if (dialogModel.getType() == DialogModel.Type.Violation
                && dialogModel.getEvent() != PREMATURE_TERMINATION_VIOLATION) {
            SyncService.startService(mContext, SyncService.SyncType.SYNC_VIOLATIONS);
        }
        if (actionResult.isShowDialog()) {
            getViewState().showDialog(dialogModel);
        }
    }

    public void dialogPositiveButtonClicked(DialogModel dialogModel) {
        getViewState().closeDialog();
        if (dialogModel.getType() != DialogModel.Type.Info) {
            Disposable disposable1 = mStepsInteractor.positiveAction(dialogModel.getEvent())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> getViewState().showProgress())
                    .doOnSuccess(__ -> getViewState().hideProgress())
                    .doOnError(__ -> getViewState().hideProgress())
                    .subscribe(actionResult -> {
                        switch (dialogModel.getEvent()) {
                            case PREMATURE_TERMINATION_VIOLATION:
                            case CONFIRMATION_END_DAY: {
                                checkQuestionnaireAvailable();
                                break;
                            }
                            case BREAK_IN_LAST_HOUR_WARNING:
                            case BREAK_IN_FIRST_HOUR_WARNING:
                            case MORE_ON_BREAK_PER_HOUR_WARNING:
                                getViewState().setStateSwitchBreak(false);
                                break;
                        }
                    });
            compositeDisposable.add(disposable1);
        }
    }

    @SuppressLint("CheckResult")
    public void checkQuestionnaireAvailable() {
        mStepsInteractor.checkQuestionnaireAvailable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doAfterTerminate(getViewState()::hideProgress)
                .subscribe(isAvailable -> {
                    if (isAvailable) {
                        getViewState().showSuccessFinishDialog();
                    } else {
                        completeEndDay();
                    }
                }, throwable -> getViewState().showError("Не удалось получить данные об опросе", ActionType.CHECK_QUESTIONNAIRE_AVAILABLE));
    }

    private void completeEndDay() {
        //unsubscribeFromTopic
        mPreferences.notificationSubscribe().getOptional()
                .ifPresent(topic -> {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
                    mPreferences.notificationSubscribe().set(null);
                });

        SyncService.startService(mContext, SyncService.SyncType.SYNC_END_DAY);
        mStateBehaviorSubject.onNext(ServiceState.RUNNING);
        Optional.ofNullable(mSubscribeSyncServiceState)
                .ifPresent(Disposable::dispose);
        mSubscribeSyncServiceState = mStateBehaviorSubject
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgressDialog(R.string.title_end_day, ActionType.END_DAY))
                .doOnDispose(() -> getViewState().hideProgressDialog())
                .flatMap(serviceState -> {
                    if (serviceState == ServiceState.FINISHED_WITH_ERROR)
                        return Observable.error(new Exception());
                    else
                        return Observable.just(serviceState);
                })
                .filter(serviceState -> serviceState == ServiceState.FINISHED_WITH_SUCCESS)
                .take(1)
                .subscribe(serviceState -> getViewState().recreateActivity(),
                        throwable -> {
                            getViewState().showError(mContext.getString(R.string.error_end_day), ActionType.END_DAY);
                            throwable.printStackTrace();
                        });
    }

    public void repeatEndDay() {
        getViewState().closeError();
        completeEndDay();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Optional.ofNullable(mSubscribeSyncServiceState)
                .ifPresent(Disposable::dispose);
    }

    public void dialogNegativeButtonClicked(DialogModel dialogModel) {
        getViewState().closeDialog();
        Disposable disposable1 = mStepsInteractor.negativeAction(dialogModel.getEvent())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doOnSuccess(__ -> getViewState().hideProgress())
                .doOnError(__ -> getViewState().hideProgress())
                .subscribe(actionResult -> {
                    if (actionResult.isContainsEvent()) {
                        handleAction(actionResult);
                        ActionResult.Event event = dialogModel.getEvent();
                        if (event == BREAK_IN_FIRST_HOUR_WARNING ||
                                event == BREAK_IN_LAST_HOUR_WARNING ||
                                event == MORE_ON_BREAK_PER_HOUR_WARNING) {
                            SyncService.startService(mContext, SyncService.SyncType.SYNC_WORK);
                        }
                    }
                });
        compositeDisposable.add(disposable1);
    }

    public void repeatGetStep() {
        getViewState().closeError();
        loadStep();
    }

    public void repeatDownloadAttachment() {
        getViewState().closeError();
        startDownloadAttachment();
    }

    public void cancelDownloadAttachment() {
        Optional.ofNullable(mSubscribePreloadingServiceState)
                .ifPresent(Disposable::dispose);
        getViewState().closeError();
        getViewState().hideProgressDialog();
    }

    public enum ActionType {
        GETTING_STEP,
        END_DAY,
        GET_ATTACHMENT,
        CHECK_QUESTIONNAIRE_AVAILABLE
    }
}
