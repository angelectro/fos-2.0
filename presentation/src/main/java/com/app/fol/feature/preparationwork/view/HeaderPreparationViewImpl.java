package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.domain.model.PromoterModel;
import com.app.domain.model.ScheduleModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.fol.FosApplication;
import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;

public class HeaderPreparationViewImpl extends RelativeLayoutBinded{
    @BindView(R.id.text_name)
    protected TextView mTextName;
    @BindView(R.id.text_address)
    protected TextView mTextAddress;
    @BindView(R.id.text_schedule)
    protected TextView mTextSchedule;

    public HeaderPreparationViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if(isInEditMode())
            return;
        ApplicationPreferences applicationPreferences = FosApplication.applicationComponent.applicationPreferences();
        PromoterModel promoterModel = applicationPreferences.promoterModel().get();
        ScheduleModel scheduleModel = applicationPreferences.scheduleModel().get();
        mTextName.setText(promoterModel.toString());
        mTextSchedule.setText(getContext().getString(R.string.text_pattern_schedule,scheduleModel.getBeginAt(),scheduleModel.getEndAt()));
        mTextAddress.setText(getContext().getString(R.string.text_pattern_address, scheduleModel.getAddress()));
    }
}
