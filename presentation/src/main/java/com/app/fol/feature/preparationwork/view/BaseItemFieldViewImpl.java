package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;

import com.app.domain.model.FieldModel;
import com.app.fol.base.adapter.ListItemView;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

public class BaseItemFieldViewImpl extends RelativeLayoutBinded implements ListItemView<FieldModel> {
    public BaseItemFieldViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        setActivated(entity.isEmptyField());
    }
}
