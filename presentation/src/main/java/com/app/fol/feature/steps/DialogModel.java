package com.app.fol.feature.steps;

import com.app.domain.model.steps.ActionResult;

public class DialogModel {
    private Type mType;
    private String text;
    private ActionResult.Event mEvent;

    private DialogModel(Type type, String text) {
        mType = type;
        this.text = text;
    }

    public static DialogModel create(Type type, String text) {
        return new DialogModel(type, text);
    }

    public ActionResult.Event getEvent() {
        return mEvent;
    }

    public void setEvent(ActionResult.Event event) {
        mEvent = event;
    }

    public Type getType() {
        return mType;
    }

    public String getText() {
        return text;
    }

    public enum Type {
        Violation,
        Warning,
        Confirmation,
        Info
    }
}
