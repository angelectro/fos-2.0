package com.app.fol.feature.drawer;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.domain.model.ProjectModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.ScheduleModel;
import com.app.fol.R;
import com.app.fol.base.butterknife.LinearLayoutBinded;
import com.annimon.stream.Optional;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;



public class DrawerHeaderViewImpl extends LinearLayoutBinded implements DrawerHeaderView {

    @BindView(R.id.image_avatar)
    CircleImageView mImageAvatar;
    @BindView(R.id.text_description)
    TextView mTextDescription;
    @BindView(R.id.text_name)
    TextView mTextName;
    @BindView(R.id.text_schedule)
    TextView mTextRetainChain;
    @BindView(R.id.text_address)
    TextView mTextAddress;

    public DrawerHeaderViewImpl(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(ProjectModel projectModel, PromoterModel promoterModel, ScheduleModel retainChainsModel) {
        Optional.ofNullable(projectModel.getBrandLogo())
                .ifPresent(s -> Glide.with(getContext())
                        .load(s)
                        .placeholder(R.drawable.header_portrait)
                        .error(R.drawable.header_portrait)
                        .dontAnimate()
                        .into(mImageAvatar)
                );

        mTextDescription.setText(projectModel.getDescription());
        mTextName.setText(promoterModel.toString());
        mTextRetainChain.setText(retainChainsModel.getRetainChain());
        mTextAddress.setText(getContext().getString(R.string.text_pattern_address, retainChainsModel.getAddress()));
    }
}
