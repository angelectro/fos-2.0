package com.app.fol.feature.steps;


import android.content.Context;

import com.app.domain.model.steps.ActionResult;
import com.app.fol.R;

import static com.app.fol.feature.steps.DialogModel.Type.Confirmation;
import static com.app.fol.feature.steps.DialogModel.Type.Violation;
import static com.app.fol.feature.steps.DialogModel.Type.Warning;

public class ActionHandler {

    private ActionResult mActionResult;
    private Context mContext;

    public ActionHandler(ActionResult actionResult, Context context) {
        mActionResult = actionResult;
        mContext = context;
    }

    public DialogModel handle() {
        DialogModel dialogModel = null;
        switch (mActionResult.getEvent()) {
            case PREMATURE_TERMINATION_VIOLATION:
                dialogModel = DialogModel.create(Violation, mContext.getString(R.string.violation_text_premature_termination));
                break;
            case PREMATURE_TERMINATION_WARNING:
                dialogModel = DialogModel.create(Warning, mContext.getString(R.string.warning_text_premature_termination));
                break;
            case CONFIRMATION_END_DAY:
                dialogModel = DialogModel.create(Confirmation, mContext.getString(R.string.text_alert_end_working));
                break;
            case BREAK_IN_FIRST_HOUR_WARNING:
                dialogModel = DialogModel.create(Warning, mContext.getString(R.string.warning_text_break_in_first_hour));
                break;
            case BREAK_IN_FIRST_HOUR_VIOLATION:
                dialogModel = DialogModel.create(Violation, mContext.getString(R.string.violation_text_break_in_first_hour));
                break;
            case BREAK_IN_LAST_HOUR_WARNING:
                dialogModel = DialogModel.create(Warning, mContext.getString(R.string.warning_text_break_in_last_hour));
                break;
            case BREAK_IN_LAST_HOUR_VIOLATION:
                dialogModel = DialogModel.create(Violation, mContext.getString(R.string.violation_text_break_in_last_hour));
                break;
            case MORE_ON_BREAK_PER_HOUR_WARNING:
                dialogModel = DialogModel.create(Warning, mContext.getString(R.string.warning_text_more_one_break_per_hour));
                break;
            case MORE_ON_BREAK_PER_HOUR_VIOLATION:
                dialogModel = DialogModel.create(Violation, mContext.getString(R.string.violation_text_more_one_break_per_hour));
                break;
            case DELAY_ON_BREAK_VIOLATION:
                dialogModel = DialogModel.create(Violation, mContext.getString(R.string.violation_text_delay_on_break, mActionResult.getInfo()[0]));
                break;
            case FALSIFICATION_OF_STATISTICS_VIOLATION:
                dialogModel = DialogModel.create(Violation, mContext.getString(R.string.violation_falsification_of_statistics));
                break;
            case FALSIFICATION_OF_TIME_VIOLATION:
                dialogModel = DialogModel.create(Violation, mContext.getString(R.string.violation_falsification_of_time));
                break;
        }
        dialogModel.setEvent(mActionResult.getEvent());
        return dialogModel;
    }
}
