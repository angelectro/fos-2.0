package com.app.fol.feature.preparationwork.adapter;

import com.app.domain.model.FieldModel;

public class FieldAction {
    private FieldModel mPreparationFieldModel;
    private FieldActionType mFieldActionType;

    public FieldAction(FieldModel preparationFieldModel, FieldActionType fieldActionType) {

        mPreparationFieldModel = preparationFieldModel;
        mFieldActionType = fieldActionType;
    }

    public FieldActionType getFieldActionType() {
        return mFieldActionType;
    }

    public FieldModel getFieldModel() {

        return mPreparationFieldModel;
    }
}
