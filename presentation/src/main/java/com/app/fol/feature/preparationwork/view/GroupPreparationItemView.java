package com.app.fol.feature.preparationwork.view;

import com.app.domain.model.FieldModel;
import com.app.fol.base.adapter.ListItemView;

public interface GroupPreparationItemView extends ListItemView<FieldModel> {
}
