package com.app.fol.feature.preparationwork.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.domain.model.FieldModel;
import com.app.domain.model.TypeField;
import com.app.fol.R;
import com.app.fol.base.adapter.AbsTypedViewHolder;
import com.app.fol.base.adapter.HeaderFooterAdapter;
import com.app.fol.base.adapter.ListItemView;
import com.app.fol.feature.preparationwork.view.AudioTypeView;
import com.app.fol.feature.preparationwork.view.PhotoPreparationItemView;
import com.app.fol.feature.preparationwork.view.QrTypeItemView;
import com.app.fol.feature.preparationwork.view.SignTypeView;
import com.app.fol.feature.preparationwork.view.VideoPreparationItemView;

import rx.subjects.PublishSubject;

public class FieldsAdapter extends HeaderFooterAdapter<FieldModel> {
    private PublishSubject<FieldAction> mFieldActionPublishSubject = PublishSubject.create();

    public FieldsAdapter(View headerView, View footerView) {
        super(0, headerView, footerView);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER || viewType == FOOTER)
            return super.onCreateViewHolder(parent, viewType);
        return new AbsTypedViewHolder<FieldModel>(
                LayoutInflater.from(parent.getContext()).inflate(getSuitableLayout(viewType), parent, false)
        );
    }

    public PublishSubject<FieldAction> getFieldActionPublishSubject() {
        return mFieldActionPublishSubject;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        if (holder instanceof AbsTypedViewHolder) {
            ListItemView listItemView = ((AbsTypedViewHolder) holder).listItemView;
            switch (getItemType(getItemViewType(position))) {
                case HEADER:
                    break;
                case PHOTO:
                    ((PhotoPreparationItemView) listItemView).actionField(mFieldActionPublishSubject::onNext);
                    break;
                case VIDEO:
                    ((VideoPreparationItemView) listItemView).actionField(mFieldActionPublishSubject::onNext);
                    break;
                case AUDIO:
                    ((AudioTypeView) listItemView).actionField(mFieldActionPublishSubject::onNext);
                    break;
                case TEXTAREA:
                    break;
                case SELECT:
                    break;
                case INPUT:
                    break;
                case NUMBER:
                    break;
                case GROUP_CHECKBOX:
                    break;
                case GROUP:
                    break;
                case FOOTER:
                    break;
                case QR:
                    ((QrTypeItemView) listItemView).actionField(mFieldActionPublishSubject::onNext);
                    break;
                case SIGN:
                    ((SignTypeView) listItemView).actionField(mFieldActionPublishSubject::onNext);
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        int itemViewType = super.getItemViewType(position);
        if (itemViewType == ITEM)
            return entities.get(position - 1).getTypeField().ordinal();
        return itemViewType;
    }

    private TypeField getItemType(int position) {
        return TypeField.values()[position];
    }

    private int getSuitableLayout(int position) {
        switch (getItemType(position)) {
            case PHOTO:
                return R.layout.item_photo_preparotion;
            case VIDEO:
                return R.layout.item_video_preparation;
            case AUDIO:
                return R.layout.item_audio_type;
            case TEXTAREA:
                return R.layout.item_textarea_type;
            case SELECT:
                return R.layout.item_select_type;
            case INPUT:
                return R.layout.item_input_type;
            case NUMBER:
                return R.layout.item_number_type;
            case GROUP_CHECKBOX:
                return R.layout.item_group_checkbox_type;
            case CHECKBOX:
                return R.layout.item_checkbox_type;
            case QR:
                return R.layout.item_type_qr;
            case SIGN:
                return R.layout.item_type_sign;
            case GROUP:
            default:
                return R.layout.item_group_preparation;
        }
    }

    public void updateItem(FieldModel fieldModel) {
        int indexOf = getEntities().indexOf(fieldModel);
        notifyItemChanged(indexOf + 1);
    }
}
