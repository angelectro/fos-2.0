package com.app.fol.feature.go_work.view;

import rx.functions.Action0;

public interface GoWorkView {
    void actionGoWork(Action0 action0);
    void actionGoBack(Action0 action0);
}
