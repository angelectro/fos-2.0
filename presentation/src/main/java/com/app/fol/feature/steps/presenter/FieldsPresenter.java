package com.app.fol.feature.steps.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.app.domain.model.FieldModel;
import com.app.domain.utils.EventBus;
import com.app.domain.utils.FileUtils;
import com.app.fol.feature.audioplayer.AudioPlayerActivity;
import com.app.fol.feature.preparationwork.adapter.FieldAction;
import com.app.fol.feature.scanner.ScannerActivity;
import com.app.fol.feature.sign.ResultCallback;
import com.app.fol.feature.steps.view.FieldsBaseViewNew;
import com.app.fol.feature.videoplayer.VideoPlayerActivity;
import com.app.fol.utils.IntentUtils;
import com.app.fol.utils.PermissionUtils;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;


public class FieldsPresenter<T extends FieldsBaseViewNew> extends MvpPresenter<T> implements ResultCallback {
    private static final int PERMISSION_PHOTO_REQUEST_CODE = 2;
    private static final int PERMISSION_VIDEO_REQUEST_CODE = 4;
    private static final int PERMISSION_WRITE_REQUEST_CODE = 5;
    private static final int PERMISSION_SCANNER_REQUEST_CODE = 6;
    private static final int REQUEST_PICTURE_CAPTURE = 1;
    private static final int REQUEST_VIDEO_CAPTURE = 2;
    private static final int REQUEST_AUDIO_SOUND = 3;
    private static final int REQUEST_QR_CODE = 4;

    @Inject
    Context mContext;

    @Inject
    @Named("projectId")
    int projectId;

    private IntentUtils.DispatchResult mDispatchResult;
    private FieldAction mFieldAction;
    private List<FieldModel> mFieldModels;
    private boolean isDestroyed = false;

    @Override
    public void attachView(T view) {
        super.attachView(view);
        if (!isDestroyed) {
            EventBus.getInstance().register(ResultCallback.class, this);
        }
    }


    public void actionField(FieldAction fieldAction) {
        mFieldAction = fieldAction;
        switch (fieldAction.getFieldActionType()) {
            case TAKE_PHOTO:
                if (PermissionUtils.checkSelfPermission(mContext, Manifest.permission.CAMERA)) {
                    getViewState().requestPermission(PERMISSION_PHOTO_REQUEST_CODE, Manifest.permission.CAMERA);
                    return;
                }
                mDispatchResult = IntentUtils.dispatchTakePictureIntent(mContext, projectId);
                getViewState().startIntent(mDispatchResult.getIntent(), REQUEST_PICTURE_CAPTURE);
                break;
            case TAKE_VIDEO:
                if (PermissionUtils.checkSelfPermission(mContext, Manifest.permission.CAMERA)) {
                    getViewState().requestPermission(PERMISSION_VIDEO_REQUEST_CODE, Manifest.permission.CAMERA);
                    return;
                }
                mDispatchResult = IntentUtils.dispatchTakeVideoIntent(mContext, projectId);
                getViewState().startIntent(mDispatchResult.getIntent(), REQUEST_VIDEO_CAPTURE);
                break;
            case TAKE_AUDIO:
                if (PermissionUtils.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO)) {
                    getViewState().requestPermission(PERMISSION_WRITE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO);
                    return;
                }
                mDispatchResult = IntentUtils.dispatchRecordAudioIntent(mContext, projectId);
                getViewState().startIntent(mDispatchResult.getIntent(), REQUEST_AUDIO_SOUND);
                break;
            case SCAN_QR:
                if (PermissionUtils.checkSelfPermission(mContext, Manifest.permission.CAMERA)) {
                    getViewState().requestPermission(PERMISSION_SCANNER_REQUEST_CODE, Manifest.permission.CAMERA);
                    return;
                }
                getViewState().startIntent(new Intent(mContext, ScannerActivity.class), REQUEST_QR_CODE);
                break;
            case ADD_SIGN:
                getViewState().showSignDialog(
                        fieldAction.getFieldModel(), resultPath -> {
                            mFieldAction.getFieldModel().setResult(resultPath);
                            updateField(mFieldAction.getFieldModel());
                        }
                );
                break;
            case REMOVE:
                String path = fieldAction.getFieldModel().getResult();
                if (FileUtils.removeFile(path)) {
                    fieldAction.getFieldModel().setResult(null);
                    updateField(fieldAction.getFieldModel());
                }
                break;
            case PLAY_VIDEO:
                getViewState().startIntent(VideoPlayerActivity.getIntent(mContext, fieldAction.getFieldModel().getResult()));
                break;
            case PLAY_AUDIO:
                getViewState().startIntent(AudioPlayerActivity.getStartIntent(mContext, fieldAction.getFieldModel().getResult()));
                break;
        }
    }

    public void updateField(FieldModel fieldModel) {
        getViewState().updateField(fieldModel);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (Stream.of(REQUEST_PICTURE_CAPTURE, REQUEST_AUDIO_SOUND, REQUEST_VIDEO_CAPTURE).anyMatch(value -> value == requestCode)) {
                Optional.ofNullable(mFieldAction)
                        .map(FieldAction::getFieldModel)
                        .ifPresent(fieldModel -> {
                            Optional.ofNullable(fieldModel.getResult())
                                    .ifPresent(FileUtils::removeFile);
                            fieldModel.setResult(mDispatchResult.getFile().getAbsolutePath());
                            updateField(fieldModel);
                        });
            } else if (requestCode == REQUEST_QR_CODE) {
                Optional.ofNullable(mFieldAction)
                        .map(FieldAction::getFieldModel)
                        .ifPresent(fieldModel -> {
                            Optional.ofNullable(fieldModel.getResult())
                                    .ifPresent(FileUtils::removeFile);
                            fieldModel.setResult(data.getStringExtra(ScannerActivity.EXTRA_CONTENT));
                            updateField(fieldModel);
                        });
            }
        }
    }

    public void onPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (PermissionUtils.checkPermissionGranted(grantResults)) {
            if (requestCode == PERMISSION_PHOTO_REQUEST_CODE) {
                mDispatchResult = IntentUtils.dispatchTakePictureIntent(mContext, projectId);
                getViewState().startIntent(mDispatchResult.getIntent(), REQUEST_PICTURE_CAPTURE);
            } else if (requestCode == PERMISSION_VIDEO_REQUEST_CODE) {
                mDispatchResult = IntentUtils.dispatchTakeVideoIntent(mContext, projectId);
                getViewState().startIntent(mDispatchResult.getIntent(), REQUEST_VIDEO_CAPTURE);
            } else if (requestCode == PERMISSION_WRITE_REQUEST_CODE) {
                mDispatchResult = IntentUtils.dispatchRecordAudioIntent(mContext, projectId);
                getViewState().startIntent(mDispatchResult.getIntent(), REQUEST_AUDIO_SOUND);
            } else if (requestCode == PERMISSION_SCANNER_REQUEST_CODE) {
                getViewState().startIntent(new Intent(mContext, ScannerActivity.class), REQUEST_QR_CODE);
            }
        }

    }

    @Override
    public void result(String path) {
        mFieldAction.getFieldModel().setResult(path);
        updateField(mFieldAction.getFieldModel());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
    }

    @Override
    public void detachView(T view) {
        super.detachView(view);
        if (!isDestroyed) {
            EventBus.getInstance().unregister(ResultCallback.class);
        }
    }

    public boolean checkRequiredFields(List<FieldModel> fieldModels) {
        if (fieldModels == null || fieldModels.isEmpty())
            return false;
        return Stream.of(fieldModels)
                .filter(FieldModel::isRequired)
                .filter(fieldModel -> TextUtils.isEmpty(fieldModel.getResult()))
                .map(fieldModel -> {
                    fieldModel.setEmptyField(true);
                    return fieldModel;
                })
                .map(FieldModel::isEmptyField)
                .reduce((value1, value2) -> value1 || value2)
                .orElse(false);
    }
}
