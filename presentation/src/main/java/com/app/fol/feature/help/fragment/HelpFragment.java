package com.app.fol.feature.help.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.fol.R;
import com.app.fol.base.fragment.GenericToolbarPresenterFragment;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.help.view.HelpView;

import javax.inject.Inject;

public class HelpFragment extends GenericToolbarPresenterFragment<HelpView> {


    @Inject
    ApplicationPreferences mPreferences;

    public static Fragment create() {
        return new HelpFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_help;
    }

    @Override
    protected void onView(HelpView view) {
        view.populate(mPreferences.authCredentialsModel().get().getProjectModel().getHelp());
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.title_help);
    }
}
