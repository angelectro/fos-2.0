package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;
import com.app.fol.feature.preparationwork.adapter.FieldAction;
import com.app.fol.feature.preparationwork.adapter.FieldsAdapter;
import com.app.fol.utils.view.RecyclerItemSpacingDecoration;
import com.annimon.stream.Stream;

import java.util.List;

import butterknife.BindView;
import rx.functions.Action0;
import rx.functions.Action1;

public class PreparationViewImpl extends RelativeLayoutBinded implements PreparationStepView {

    private static final String MANAGER_STATE = "MANAGER_STATE";
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.button_back)
    Button mButtonBack;
    @BindView(R.id.button_reload)
    Button mButtonReload;
    FooterPreparationView mFooterPreparationView;
    private FieldsAdapter mFieldsAdapter;
    private List<FieldModel> mPreparationFieldModels;

    public PreparationViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            mButtonBack.setVisibility(GONE);
            mRecyclerView.addItemDecoration(new RecyclerItemSpacingDecoration(getContext(), R.dimen.margin_10, R.dimen.margin_10, R.dimen.margin_10));
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(MANAGER_STATE, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        mRecyclerView.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(MANAGER_STATE));
    }

    @Override
    public void populate(List<FieldModel> preparationFieldModels, int order, boolean isLast) {
        mRecyclerView.removeAllViews();
        mFooterPreparationView = (FooterPreparationView) LayoutInflater.from(getContext()).inflate(R.layout.footer_preparation, mRecyclerView, false);
        mFieldsAdapter = new FieldsAdapter(order == 0 ? LayoutInflater.from(getContext()).inflate(R.layout.header_preparation, mRecyclerView, false) : new View(getContext()), (View) mFooterPreparationView);
        mRecyclerView.setAdapter(mFieldsAdapter);
        mPreparationFieldModels = preparationFieldModels;
        mFieldsAdapter.setEntities(preparationFieldModels);
        mFooterPreparationView.populate(getContext().getString(!isLast ? R.string.text_button_next : R.string.text_start_day));
        if (order != 0)
            mButtonReload.setVisibility(GONE);
    }


    @Override
    public void actionField(Action1<FieldAction> fieldActionAction1) {
        mFieldsAdapter.getFieldActionPublishSubject().subscribe(fieldActionAction1);
    }

    @Override
    public void actionNext(Action0 action0) {
        mFooterPreparationView.clickAction(o -> action0.call());
    }

    @Override
    public void actionUpdate(Action0 action0) {
        mButtonReload.setOnClickListener(v -> action0.call());
    }

    @Override
    public void updateField(FieldModel preparationFieldModel) {
        mFieldsAdapter.notifyItemChanged(Stream.range(0, mPreparationFieldModels.size())
                .filter(position -> TextUtils.equals(preparationFieldModel.getUuid(), mPreparationFieldModels.get(position).getUuid()))
                .map(position -> position + 1)
                .findFirst().orElse(0));
    }

    @Override
    public void displayMarkErrorFields() {
        mFieldsAdapter.notifyDataSetChanged();
        Snackbar.make(this, R.string.warning_required_field, BaseTransientBottomBar.LENGTH_LONG).show();
    }
}
