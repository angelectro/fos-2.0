package com.app.fol.feature.auth.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;

import com.app.domain.model.CityModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.SupervisorModel;
import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;
import com.app.fol.utils.RxUtils;
import com.annimon.stream.Optional;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import io.reactivex.Observable;
import rx.functions.Action0;
import rx.functions.Action3;


public class StaffSelectionViewImpl extends RelativeLayoutBinded implements StaffSelectionView {

    public static final String CITY_ARGS = "CITY_ARGS";
    public static final String PROMOTER_ARGS = "PROMOTER_ARGS";
    public static final String SUPERVISOR_ARGS = "SUPERVISOR_ARGS";
    @BindView(R.id.imageBackground)
    protected ImageView mImageBackground;
    @BindView(R.id.button_reload)
    protected Button mButtonReload;
    @BindView(R.id.button_city)
    protected Button mButtonCity;
    @BindView(R.id.button_promoter)
    protected Button mButtonPromoter;
    @BindView(R.id.button_supervisor)
    protected Button mButtonSupervisor;
    @BindView(R.id.button_next)
    protected Button mButtonNext;
    @BindView(R.id.button_back)
    protected Button mButtonBack;
    private CityModel mCityModel;
    private SupervisorModel mSupervisorModel;
    private PromoterModel mPromoterModel;

    public StaffSelectionViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode())
            return;
        mButtonNext.setEnabled(false);
        mButtonPromoter.setEnabled(false);
        mButtonSupervisor.setEnabled(false);

        Observable.combineLatest(
                RxUtils.textChanged(mButtonCity).map(s -> !TextUtils.isEmpty(s)),
                RxUtils.textChanged(mButtonPromoter).map(s -> !TextUtils.isEmpty(s)),
                RxUtils.textChanged(mButtonSupervisor).map(s -> !TextUtils.isEmpty(s)),
                (aBoolean, aBoolean2, aBoolean3) -> aBoolean && aBoolean2 && aBoolean3)
                .subscribe(mButtonNext::setEnabled);

        RxUtils.textChanged(mButtonCity)
                .map(s -> !TextUtils.isEmpty(s))
                .subscribe(enabled -> {
                    mButtonPromoter.setEnabled(enabled);
                    mButtonSupervisor.setEnabled(enabled);
                });
    }


    @Override
    public void onBackPressedAction(Action0 action0) {
        mButtonBack.setOnClickListener(v -> action0.call());
    }

    @Override
    public void selectCityAction(Action0 action0) {
        mButtonCity.setOnClickListener(view -> action0.call());
    }

    @Override
    public void selectPromoterAction(Action0 action0) {
        mButtonPromoter.setOnClickListener(view -> action0.call());
    }

    @Override
    public void selectSupervisorAction(Action0 action0) {
        mButtonSupervisor.setOnClickListener(view -> action0.call());
    }

    @Override
    public void showCity(CityModel cityModel) {
        if (mCityModel != null && mCityModel.getId() != cityModel.getId()) {
            showPromoter(null);
            showSupervisor(null);
        }
        mCityModel = cityModel;
        mButtonCity.setText(cityModel.toString());
    }

    @Override
    public void showSupervisor(SupervisorModel supervisorModel) {
        mSupervisorModel = supervisorModel;
        mButtonSupervisor.setText(Optional.ofNullable(supervisorModel)
                .map(SupervisorModel::toString).orElse(""));
    }

    @Override
    public void showPromoter(PromoterModel promoterModel) {
        mPromoterModel = promoterModel;
        mButtonPromoter.setText(Optional.ofNullable(promoterModel)
                .map(PromoterModel::toString).orElse(""));
    }

    @Override
    public void populate(CityModel cityModel, PromoterModel promoterModel, SupervisorModel supervisorModel) {
        Optional.ofNullable(cityModel).ifPresent(this::showCity);
        Optional.ofNullable(promoterModel).ifPresent(this::showPromoter);
        Optional.ofNullable(supervisorModel).ifPresent(this::showSupervisor);
    }

    @Override
    public void nextAction(Action3<CityModel, PromoterModel, SupervisorModel> action3) {
        mButtonNext.setOnClickListener(view -> action3.call(mCityModel, mPromoterModel, mSupervisorModel));
    }

    @Override
    public void showBackground(String path) {
        Glide.with(getContext())
                .load(path)
                .asBitmap()
                .placeholder(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.gainsboro)))
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(mImageBackground);
    }

    @Override
    public void updateDataAction(Action0 action0) {
        mButtonReload.setOnClickListener(view -> action0.call());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(PROMOTER_ARGS, mPromoterModel);
        outState.putParcelable(SUPERVISOR_ARGS, mSupervisorModel);
        outState.putParcelable(CITY_ARGS, mCityModel);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        Optional.ofNullable((CityModel) savedInstanceState.getParcelable(CITY_ARGS))
                .ifPresent(this::showCity);
        Optional.ofNullable((PromoterModel) savedInstanceState.getParcelable(PROMOTER_ARGS))
                .ifPresent(this::showPromoter);
        Optional.ofNullable((SupervisorModel) savedInstanceState.getParcelable(SUPERVISOR_ARGS))
                .ifPresent(this::showSupervisor);
    }
}
