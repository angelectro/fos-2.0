package com.app.fol.feature.auth.view;


import com.app.domain.model.ProjectModel;
import com.app.fol.base.view.BaseView;

import rx.functions.Action0;
import rx.functions.Action2;

public interface SelectProjectView extends BaseView {
    void signInAction(Action2<ProjectModel, String> action2);
    void selectProjectAction(Action0 action0);
    void showSelectedProject(ProjectModel projectModel);
    void populate(ProjectModel projectModel);

    void updateDataAction(Action0 action0);
}
