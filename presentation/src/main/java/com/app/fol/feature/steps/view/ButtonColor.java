package com.app.fol.feature.steps.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.app.fol.R;
import com.app.fol.utils.ScaleUtils;

public class ButtonColor extends android.support.v7.widget.AppCompatButton {
    private int mColor;

    public ButtonColor(Context context, int color) {
        super(context);
        mColor = color;
        setEnabled(true);
        init();
    }

    public ButtonColor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ButtonColor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        int margin = getContext().getResources().getDimensionPixelSize(R.dimen.margin_5);
        setSingleLine(false);
        setPadding(margin, 0, margin, 0);
    }

    @Override
    public void setTextColor(@ColorInt int colorText) {
        ColorStateList colorStateList = new ColorStateList(new int[][]{
                new int[]{-android.R.attr.state_enabled},
                new int[]{android.R.attr.state_enabled}
        },
                new int[]{
                        ContextCompat.getColor(getContext(), R.color.gainsboro),
                        colorText,
                });
        super.setTextColor(colorStateList);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            setBackground(new ColorDrawable(mColor));
        } else {
            GradientDrawable gradientDrawableDisabled = new GradientDrawable();
            gradientDrawableDisabled.setStroke(ScaleUtils.dpToPx(2), mColor);
            setBackground(gradientDrawableDisabled);
        }
    }
}
