package com.app.fol.feature.imageviewer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.app.fol.R;
import com.app.fol.utils.view.TouchImageView;
import com.annimon.stream.Optional;
import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageViewerActivity extends AppCompatActivity {

    private static final String URL_IMAGE = "URL_IMAGE";
    private static final String PATH_IMAGE = "PATH_IMAGE";
    private static final int MAX_WIDTH = 1024;
    @BindView(R.id.image_view)
    protected TouchImageView mImageView;
    @BindView(R.id.button_back)
    protected Button mButtonBack;
    @BindView(R.id.progressBar)
    protected ProgressBar mProgressBar;
    @BindView(R.id.buttonRepeat)
    protected Button mButtonRepeat;
    @BindView(R.id.viewError)
    protected LinearLayout mViewError;

    public static void createViewerFromFile(Context context, String pathFile) {
        Intent intent = new Intent(context, ImageViewerActivity.class);
        intent.putExtra(PATH_IMAGE, pathFile);
        context.startActivity(intent);
    }

    public static void createViewerFromUrl(Context context, String url) {
        Intent intent = new Intent(context, ImageViewerActivity.class);
        intent.putExtra(URL_IMAGE, url);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewver);
        ButterKnife.bind(this);
        showImage();
    }

    private void showImage() {
        mViewError.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        DrawableTypeRequest uriDrawableTypeRequest = null;
        uriDrawableTypeRequest = Optional.ofNullable(getIntent().getStringExtra(PATH_IMAGE))
                .map(File::new)
                .map(file -> Glide.with(this)
                        .load(file)).orElse(null);
        uriDrawableTypeRequest = Optional.ofNullable(getIntent().getStringExtra(URL_IMAGE))
                .map(url -> Glide.with(this)
                        .load(url)).orElse(uriDrawableTypeRequest);
        uriDrawableTypeRequest
                .override(MAX_WIDTH, MAX_WIDTH)
                .listener(new RequestListener<Comparable<? extends Comparable<?>>, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Comparable<? extends Comparable<?>> model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressBar.setVisibility(View.GONE);
                        mViewError.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Comparable<? extends Comparable<?>> model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mImageView);
    }

    @OnClick({R.id.button_back, R.id.buttonRepeat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_back:
                onBackPressed();
                break;
            case R.id.buttonRepeat:
                showImage();
                break;
        }
    }
}
