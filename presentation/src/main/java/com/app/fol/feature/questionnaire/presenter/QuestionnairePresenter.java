package com.app.fol.feature.questionnaire.presenter;

import android.content.Context;

import com.app.domain.model.ProgressState;
import com.app.domain.model.QuestionnaireModel;
import com.app.domain.usecase.GetFinishStepUseCase;
import com.app.domain.usecase.ProgressStateUpdateUseCase;
import com.app.domain.usecase.SaveLocalResultFinishStepUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.questionnaire.view.QuestionnaireView;
import com.app.fol.feature.steps.presenter.FieldsPresenter;
import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class QuestionnairePresenter extends FieldsPresenter<QuestionnaireView> {
    @Inject
    GetFinishStepUseCase getFinishStepUseCase;
    @Inject
    SaveLocalResultFinishStepUseCase saveLocalResultFinishStepUseCase;
    @Inject
    ProgressStateUpdateUseCase mProgressStateUpdateUseCase;
    @Inject
    Context context;
    private int order;
    private QuestionnaireModel questionnaireModel;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public QuestionnairePresenter(int order, HomeComponent homeComponent) {
        this.order = order;
        homeComponent.inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().setVisibilityReloadButton(order == 0);
        updateData();
    }

    public void updateData() {
        getFinishStepUseCase.setOrder(order)
                .buildUseCaseObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgressBar())
                .doOnTerminate(getViewState()::hideProgressBar)
                .subscribe(FosDefaultSubscriber.onPositiveNegative(null, (questionnaireModel) -> {
                    boolean last = questionnaireModel.isLast();
                    getViewState().setNextButtonTitle(context.getString(!last ? R.string.text_button_next : R.string.text_finish_questionnaire));
                    this.questionnaireModel = questionnaireModel;
                    getViewState().showFields(questionnaireModel.getListFieldModels());
                }, errorModel -> {
                    getViewState().showError(errorModel.getMessage());
                    errorModel.getThrowable().printStackTrace();
                }));
    }

    public void clickedNextButton() {
        if (checkRequiredFields(questionnaireModel.getListFieldModels())) {
            getViewState().displayMarkErrorFields();
        } else {
            nextStepOrFinishQuestionnaire();
        }
    }


    private void nextStepOrFinishQuestionnaire() {
        Disposable subscribe = saveLocalResultFinishStepUseCase.setQuestionnaireModel(questionnaireModel)
                .buildUseCaseObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(__ -> {
                    if (questionnaireModel.isLast()) {
                        finishQuestionnaire();
                    } else {
                        getViewState().segueNextStep(order + 1);
                    }
                });
        compositeDisposable.add(subscribe);
    }

    private void finishQuestionnaire() {
        Disposable subscribe = mProgressStateUpdateUseCase.update(ProgressState.COMPLETE_FINISH_QUESTIONNAIRE)
                .subscribe(() -> getViewState().recreateActivity());
        compositeDisposable.add(subscribe);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
