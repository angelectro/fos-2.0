package com.app.fol.feature.multimedia.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.FieldStatus;
import com.app.fol.R;
import com.app.fol.base.adapter.ListItemView;
import com.annimon.stream.Stream;
import com.bumptech.glide.Glide;

import butterknife.BindView;

public class ImageViewImpl extends SquareFrameLayout implements ListItemView<FieldResultModel> {
    @BindView(R.id.image_view)
    protected ImageView mImageView;
    @BindView(R.id.text_status)
    protected TextView mTextStatus;

    public ImageViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void populate(FieldResultModel entity) {
        Glide.with(getContext())
                .load(entity.getResult())
                .into(mImageView);
        mTextStatus.setText(getContext().getString(Stream.of(FieldStatus.SENT, FieldStatus.UPLOADED).anyMatch(value -> value == entity.getStatus()) ? R.string.sent : R.string.not_send));
    }
}
