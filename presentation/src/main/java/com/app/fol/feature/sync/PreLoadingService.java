package com.app.fol.feature.sync;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.app.domain.model.ServiceState;
import com.app.domain.model.steps.AttachmentModel;
import com.app.domain.model.steps.StepModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.PreparationRepository;
import com.app.domain.repository.QuestionnaireRepository;
import com.app.domain.repository.StepsRepository;
import com.app.domain.utils.FileUtils;
import com.app.fol.FosApplication;
import com.annimon.stream.Optional;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

import static com.app.fol.di.application.ApplicationModule.PRELOADING_SERVICE_STATE;

public class PreLoadingService extends Service {

    public static final String TAG = PreLoadingService.class.getSimpleName();
    @Inject
    ApplicationPreferences preferences;
    @Inject
    StepsRepository stepsRepository;
    @Inject
    PreparationRepository mPreparationRepository;
    @Inject
    QuestionnaireRepository mQuestionnaireRepository;
    @Inject
    @Named(PRELOADING_SERVICE_STATE)
    BehaviorSubject<ServiceState> mStateBehaviorSubject;
    private Disposable mSubscriber;
    private int projectId;

    public static void startService(Context context) {
        context.startService(new Intent(context, PreLoadingService.class));
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FosApplication.applicationComponent.inject(this);
        projectId = preferences.authCredentialsModel().get().getProjectModel().getId();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Optional.ofNullable(mSubscriber)
                .map(disposable -> !disposable.isDisposed())
                .orElse(false)) {
            return super.onStartCommand(intent, flags, startId);
        }
        mStateBehaviorSubject.onNext(ServiceState.RUNNING);
        Observable preparationObservable = mPreparationRepository.listPreparation(projectId);
        Observable questionnaireObservable = mQuestionnaireRepository.getAll(projectId);
        Observable<File> fileObservable = stepsRepository.listSteps(projectId)
                .flatMap(Observable::fromIterable)
                .filter(stepModel -> stepModel.getAttachment() != null)
                .map(StepModel::getAttachment)
                .map(AttachmentModel::getPath)
                .doOnNext(s -> Log.d(TAG, "file downloading " + s))
                .flatMap(this::downloadFile)
                .doOnNext(s -> Log.d(TAG, "file downloaded " + s.getPath()));
        List<Observable<?>> ts = Arrays.asList(
                preparationObservable,
                fileObservable,
                questionnaireObservable);
        mSubscriber = Observable.concat(ts)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    Log.d(TAG, "done");
                }, throwable -> {
                    mStateBehaviorSubject.onNext(ServiceState.FINISHED_WITH_ERROR);
                    stopSelf();
                    Log.e(TAG, "error " + throwable.getMessage());
                }, () -> {
                    mStateBehaviorSubject.onNext(ServiceState.FINISHED_WITH_SUCCESS);
                    stopSelf();
                    Log.d(TAG, "complete");
                });

        return super.onStartCommand(intent, flags, startId);
    }

    private Observable<File> downloadFile(String path) {
        return Observable.fromCallable(() -> {
            File file = null;
            try {
                URL url = new URL(path);
                file = FileUtils.getFileForProject(this, String.valueOf(projectId), FileUtils.getFileNameFromUrl(url));
                if (file.exists() && file.length() != 0)
                    return file;
                file.createNewFile();
                ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                FileOutputStream fos = new FileOutputStream(file);
                WritableByteChannel outChannel = Channels.newChannel(fos);
                ByteBuffer buffer = ByteBuffer.allocate(8192);
                int read;

                while ((read = rbc.read(buffer)) > 0) {
                    buffer.rewind();
                    buffer.limit(read);
                    while (read > 0) {
                        read -= outChannel.write(buffer);
                    }
                    buffer.clear();
                }
                return file;
            } catch (Exception ex) {
                Optional.ofNullable(file)
                        .ifPresent(File::delete);
                throw ex;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Optional.ofNullable(mSubscriber)
                .ifPresent(Disposable::dispose);
    }
}
