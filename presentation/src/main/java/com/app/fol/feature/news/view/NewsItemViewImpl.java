package com.app.fol.feature.news.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.domain.model.news.NewsModel;
import com.app.fol.R;
import com.app.fol.base.adapter.ListItemView;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;


public class NewsItemViewImpl extends RelativeLayoutBinded implements ListItemView<NewsModel> {
    @BindView(R.id.text_date)
    protected TextView mTextDate;
    @BindView(R.id.text_content)
    protected TextView mTextContent;

    public NewsItemViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void populate(NewsModel entity) {
        mTextDate.setText(entity.getDateAsText());
        mTextContent.setText(entity.getText());
    }
}
