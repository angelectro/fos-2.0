package com.app.fol.feature.selectionplace.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.app.domain.model.ScheduleModel;
import com.app.fol.R;
import com.app.fol.base.adapter.GenericAdapter;
import com.app.fol.base.butterknife.RelativeLayoutBinded;
import com.app.fol.base.view.NavigationViewImpl;

import java.util.List;

import butterknife.BindView;
import rx.functions.Action0;
import rx.functions.Action1;


public class PlaceSelectionViewImpl extends RelativeLayoutBinded implements PlaceSelectionView {
    private static final String MANAGER_STATE = "manager_state";
    @BindView(R.id.recycler_view)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.navigationView)
    protected NavigationViewImpl mNavigationView;
    private GenericAdapter<ScheduleModel> mAdapter;


    public PlaceSelectionViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode())
            return;
        int columns = getResources().getInteger(R.integer.grid_columns);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), columns));
        mAdapter = new GenericAdapter<>(R.layout.item_work_place);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void populate(List<ScheduleModel> retainChainsModels) {
        mAdapter.replace(retainChainsModels);
    }

    @Override
    public void backAction(Action0 action0) {
        mNavigationView.actionButtonBack(action0);
    }

    @Override
    public void itemClickAction(Action1<ScheduleModel> action1) {
        mAdapter.getItemClickObservable().subscribe(action1);
    }

    @Override
    public void updateDataAction(Action0 action0) {
        mNavigationView.actionButtonReload(action0);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(MANAGER_STATE, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        mRecyclerView.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(MANAGER_STATE));
    }
}
