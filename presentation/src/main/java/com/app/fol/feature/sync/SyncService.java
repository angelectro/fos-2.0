package com.app.fol.feature.sync;


import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.app.data.entity.mapper.FieldResultModelToFieldContactRequestEntityMapper;
import com.app.data.entity.mapper.FieldResultModelToFinishStepRequestEntityMapper;
import com.app.data.entity.mapper.FieldResultModelToRequestEntityMapper;
import com.app.data.entity.sync.ChoiceRequestEntity;
import com.app.data.entity.sync.ContactRequestEntity;
import com.app.data.entity.sync.ViolationRequestEntity;
import com.app.data.exceptions.NoUploadFileException;
import com.app.data.service.FolService;
import com.app.domain.model.FieldResultModel;
import com.app.domain.model.FieldStatus;
import com.app.domain.model.ServiceState;
import com.app.domain.model.sync.ChoiceResultEntity;
import com.app.domain.model.sync.ContactEntity;
import com.app.domain.model.sync.FieldResultStepEntity;
import com.app.domain.model.sync.ViolationEntity;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.BreakRepository;
import com.app.domain.repository.ContactRepository;
import com.app.domain.repository.FileRepository;
import com.app.domain.repository.FinishStepFieldsRepository;
import com.app.domain.repository.InitStepFieldsRepository;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.repository.WorkRepository;
import com.app.domain.usecase.EndWorkdayUseCase;
import com.app.fol.FosApplication;
import com.app.fol.R;
import com.app.fol.base.progress.AlertUtils;
import com.app.fol.di.application.ApplicationModule;
import com.app.fol.feature.home.activity.HomeActivity;
import com.annimon.stream.Optional;
import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

public class SyncService extends Service {
    public static final String TAG_INIT_STEPS = "sync init-steps";
    public static final String TAG_FINISH_STEPS = "sync finish-steps";
    private static final int SERVICE_NOTIFICATION_ID = 1;
    private static final String TAG_CONTACT = "sync contact";
    private static final String TAG_VIOLATION = "sync violation";
    private static final String SYNC_TYPE = "sync_type";
    @Inject
    InitStepFieldsRepository mInitStepFieldsRepository;
    @Inject
    FinishStepFieldsRepository mFinishStepFieldsRepository;
    @Inject
    FileRepository mFileRepository;
    @Inject
    ViolationRepository mViolationRepository;
    @Inject
    FolService mFosService;
    @Inject
    ApplicationPreferences mApplicationPreferences;
    @Inject
    FieldResultModelToRequestEntityMapper mFieldResultModelToRequestEntityMapper;
    @Inject
    FieldResultModelToFinishStepRequestEntityMapper mFieldResultModelToFinishStepRequestEntityMapper;
    @Inject
    ContactRepository mContactRepository;
    @Inject
    BreakRepository mBreakRepository;
    @Inject
    WorkRepository mWorkRepository;
    @Inject
    FieldResultModelToFieldContactRequestEntityMapper mFieldResultModelToFieldContactRequestEntityMapper;
    @Inject
    EndWorkdayUseCase mEndWorkdayUseCase;
    @Inject
    @Named(ApplicationModule.SYNC_SERVICE_STATE)
    BehaviorSubject<ServiceState> mStateBehaviorSubject;
    int projectId;
    int scheduleId;
    private Queue<SyncType> mActionsQueue = new LinkedList<>();
    private SyncType mCurrentSyncType;
    private Disposable mDisposable;

    public static void startService(Context context, SyncType syncType) {
        Intent intent = new Intent(null, null, context, SyncService.class);
        intent.putExtra(SYNC_TYPE, syncType.toString());
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FosApplication.applicationComponent.inject(this);
        sendNotification(getString(R.string.text_sync_title), getString(R.string.sending_data));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SyncType syncType = SyncType.valueOf(intent.getStringExtra(SYNC_TYPE));
        if (!mActionsQueue.contains(syncType) &&
                Optional.ofNullable(mCurrentSyncType)
                        .map(type -> type != SyncType.SYNC_END_DAY)
                        .orElse(true)) {
            mActionsQueue.add(syncType);
        }
        mStateBehaviorSubject.onNext(ServiceState.RUNNING);
        if (!Optional.ofNullable(mDisposable).map(Disposable::isDisposed).orElse(true)) {
            return super.onStartCommand(intent, flags, startId);
        }
        startNextAction();
        return super.onStartCommand(intent, flags, startId);
    }

    private void startNextAction() {
        projectId = mApplicationPreferences.authCredentialsModel().get().getProjectModel().getId();
        scheduleId = mApplicationPreferences.scheduleModel().get().getId();
        Completable completable = null;
        mCurrentSyncType = mActionsQueue.poll();
        switch (mCurrentSyncType) {
            case SYNC_INIT_STEPS:
                completable = syncInitStepObservable();
                break;
            case SYNC_CONTACTS:
                completable = syncContactsObservable();
                break;
            case SYNC_VIOLATIONS:
                completable = syncViolationsObservable();
                break;
            case SYNC_WORK:
                completable = syncWorkStatus();
                break;
            case SYNC_ALL:
                completable = Completable.concat(Arrays.asList(syncWorkStatus(), syncInitStepObservable(), syncContactsObservable(), syncViolationsObservable()));
                break;
            case SYNC_END_DAY:
                completable = Completable.concat(Arrays.asList(syncWorkStatus(), syncInitStepObservable(), syncContactsObservable(), syncViolationsObservable(),syncFinishStepObservable()));
                completable = completable
                        .toSingleDefault(true)
                        .flatMap(aBoolean -> mEndWorkdayUseCase.buildUseCaseObservable()
                                .toList())
                        .toCompletable();
                break;
        }
        if (completable != null)
            mDisposable = completable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                                if (mCurrentSyncType != SyncType.SYNC_VIOLATIONS &&
                                        mCurrentSyncType != SyncType.SYNC_WORK) {
                                    AlertUtils.showToast(getApplicationContext(), R.string.text_send_data_success);
                                }
                                if (mActionsQueue.isEmpty()) {
                                    mStateBehaviorSubject.onNext(ServiceState.FINISHED_WITH_SUCCESS);
                                    stopSelf();
                                    stopForeground(true);
                                } else {
                                    startNextAction();
                                }
                            },
                            throwable -> {
                                Crashlytics.logException(throwable);
                                mStateBehaviorSubject.onNext(ServiceState.FINISHED_WITH_ERROR);
                                if (mCurrentSyncType != SyncType.SYNC_VIOLATIONS) {
                                    handleError(throwable);
                                }
                                stopSelf();
                                stopForeground(true);
                                throwable.printStackTrace();
                            });
    }

    private Completable syncWorkStatus() {
        Completable completableBreak = Observable.just(getString(R.string.sending_data))
                .doOnNext(s -> sendNotification(getString(R.string.text_sync_title), s))
                .flatMapCompletable(s -> mBreakRepository.sendAll());
        Completable completableWork = Observable.just(getString(R.string.sending_data))
                .doOnNext(s -> sendNotification(getString(R.string.text_sync_title), s))
                .flatMap(s -> mWorkRepository.sendAll())
                .toList()
                .toCompletable();
        return completableWork
                .andThen(completableBreak);
    }

    private Completable syncInitStepObservable() {
        return mInitStepFieldsRepository.getAll()
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .doOnNext(fieldResultModels -> sendNotification(getString(R.string.text_sync_title), "Отправка данных работника"))
                .flatMap(Observable::fromIterable)
                .filter(fieldResultModel -> !TextUtils.isEmpty(fieldResultModel.getResult()))
                .filter(fieldResultModel -> fieldResultModel.getStatus() != FieldStatus.SENT)
                .flatMap(fieldResultModel -> {
                    if (fieldResultModel.isFile() &&
                            fieldResultModel.getStatus() != FieldStatus.UPLOADED) {
                        return uploadInitStepsFileObservable(fieldResultModel);
                    }
                    return Observable.just(fieldResultModel);
                })
                .doOnNext(fieldResultModel -> Log.d(TAG_INIT_STEPS, "field sending - " + fieldResultModel.getResult()))
                .map(mFieldResultModelToRequestEntityMapper)
                .toList()
                .filter(fieldResultRequestEntities -> !fieldResultRequestEntities.isEmpty())
                .toObservable()
                .flatMap(entities -> mFosService.sendInitStepsData(projectId, scheduleId, entities))
                .flatMap(o -> mInitStepFieldsRepository.getAll())
                .flatMap(Observable::fromIterable)
                .doOnNext(resultModel -> mInitStepFieldsRepository.updateStatus(resultModel.getUuid(), FieldStatus.SENT))
                .toList()
                .toCompletable();
    }

    private Completable syncFinishStepObservable() {
        return mFinishStepFieldsRepository.getAll()
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .doOnNext(fieldResultModels -> sendNotification(getString(R.string.text_sync_title), "Отправка данных работника"))
                .flatMap(Observable::fromIterable)
                .filter(fieldResultModel -> !TextUtils.isEmpty(fieldResultModel.getResult()))
                .filter(fieldResultModel -> fieldResultModel.getStatus() != FieldStatus.SENT)
                .flatMap(fieldResultModel -> {
                    if (fieldResultModel.isFile() &&
                            fieldResultModel.getStatus() != FieldStatus.UPLOADED) {
                        return uploadFinishStepsFileObservable(fieldResultModel);
                    }
                    return Observable.just(fieldResultModel);
                })
                .doOnNext(fieldResultModel -> Log.d(TAG_FINISH_STEPS, "field sending - " + fieldResultModel.getResult()))
                .map(mFieldResultModelToFinishStepRequestEntityMapper)
                .toList()
                .filter(fieldResultRequestEntities -> !fieldResultRequestEntities.isEmpty())
                .toObservable()
                .flatMap(entities -> mFosService.sendFinishStepsData(projectId, scheduleId, entities))
                .flatMap(o -> mFinishStepFieldsRepository.getAll())
                .flatMap(Observable::fromIterable)
                .doOnNext(resultModel -> mFinishStepFieldsRepository.updateStatus(resultModel.getUuid(), FieldStatus.SENT))
                .toList()
                .toCompletable();
    }


    private Completable syncContactsObservable() {
        final int[] size = {0, 0};
        return mContactRepository.getAll()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.newThread())
                .flatMap(Observable::fromIterable)
                .filter(ContactEntity::isNotSent)
                .observeOn(Schedulers.io())
                .toList()
                .toObservable()
                .doOnNext(contactModels -> size[0] = contactModels.size())
                .flatMap(Observable::fromIterable)
                .doOnNext(contactModel -> size[1]++)
                .doOnNext(fieldResultModels -> {
                    sendNotification(getString(R.string.text_sync_title), String.format("Отправка контактов %d из %d", size[1], size[0]));
                    Log.d(TAG_CONTACT, String.format("Отправка контактов %d из %d", size[1], size[0]));
                })
                .flatMap(contactModel -> Observable.fromIterable(contactModel.getStepResultEntities())
                        .flatMap(this::observableStepRequest)
                        .toList()
                        .toObservable()
                        .map((stepRequestEntities) -> new ContactRequestEntity(stepRequestEntities, contactModel.getCreateTime()))
                        .flatMap(contactRequestEntity -> mFosService.sendContactData(projectId, scheduleId, contactRequestEntity))
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnNext(o -> Log.d(TAG_CONTACT, "contact sent: " + contactModel.getId()))
                        .flatMap(resultModel -> mContactRepository.updateContactStatus(contactModel, true)))
                .toList()
                .toCompletable();
    }

    private Observable<FieldResultModel> uploadInitStepsFileObservable(FieldResultModel fieldResultModel) {
        return Observable.fromCallable(() -> {
            File file = new File(fieldResultModel.getResult());
            if (file.exists()) return file;
            throw new FileNotFoundException(file.getAbsolutePath());
        })
                .doOnNext(file -> mInitStepFieldsRepository.updateStatus(fieldResultModel.getUuid(), FieldStatus.UPLOADING))
                .doOnNext(s -> Log.d(TAG_INIT_STEPS, "sending - " + fieldResultModel.getResult()))
                .flatMap(mFileRepository::uploadFileInitSteps)
                .onErrorReturn(throwable -> {
                    throw new NoUploadFileException();
                })
                .doOnNext(s -> Log.d(TAG_INIT_STEPS, "sent - " + fieldResultModel.getResult()))
                .doOnNext(s -> mInitStepFieldsRepository.updateStatus(fieldResultModel.getUuid(), FieldStatus.UPLOADED))
                .doOnNext(fieldResultModel::setResultPath)
                .map(s -> fieldResultModel)
                .doOnNext(fieldResultModel1 -> fieldResultModel.setStatus(FieldStatus.UPLOADED))
                .flatMap(mInitStepFieldsRepository::update);
    }

    private Observable<FieldResultModel> uploadFinishStepsFileObservable(FieldResultModel fieldResultModel) {
        return Observable.fromCallable(() -> {
            File file = new File(fieldResultModel.getResult());
            if (file.exists()) return file;
            throw new FileNotFoundException(file.getAbsolutePath());
        })
                .doOnNext(file -> mFinishStepFieldsRepository.updateStatus(fieldResultModel.getUuid(), FieldStatus.UPLOADING))
                .doOnNext(s -> Log.d(TAG_FINISH_STEPS, "sending - " + fieldResultModel.getResult()))
                .flatMap(mFileRepository::uploadFileFinishSteps)
                .onErrorReturn(throwable -> {
                    throw new NoUploadFileException();
                })
                .doOnNext(s -> Log.d(TAG_FINISH_STEPS, "sent - " + fieldResultModel.getResult()))
                .doOnNext(s -> mFinishStepFieldsRepository.updateStatus(fieldResultModel.getUuid(), FieldStatus.UPLOADED))
                .doOnNext(fieldResultModel::setResultPath)
                .map(s -> fieldResultModel)
                .doOnNext(fieldResultModel1 -> fieldResultModel.setStatus(FieldStatus.UPLOADED))
                .flatMap(mFinishStepFieldsRepository::update);
    }

    private Observable<FieldResultStepEntity> uploadContactFileObservable(FieldResultStepEntity fieldResultModel) {
        return Observable.fromCallable(() -> {
            File file = new File(fieldResultModel.getResult());
            if (file.exists()) return file;
            throw new FileNotFoundException(file.getAbsolutePath());
        })
                .doOnNext(s -> Log.d(TAG_CONTACT, "sending - " + fieldResultModel.getResult()))
                .flatMap(mFileRepository::uploadFileSteps)
                .onErrorReturn(throwable -> {
                    throw new NoUploadFileException();
                })
                .doOnNext(s -> Log.d(TAG_CONTACT, "sent - " + fieldResultModel.getResult()))
                .doOnNext(fieldResultModel::setResultPath)
                .doOnNext(s -> fieldResultModel.setStatus(FieldStatus.UPLOADED))
                .flatMap(s -> mContactRepository.updateField(fieldResultModel));
    }

    private Observable<ChoiceRequestEntity> observableStepRequest(ChoiceResultEntity stepResultEntity) {
        return Observable.fromIterable(stepResultEntity.getFieldResultStepEntities())
                .filter(fieldResultModel -> !TextUtils.isEmpty(fieldResultModel.getResult()))
                .filter(fieldResultModel -> fieldResultModel.getStatus() != FieldStatus.SENT)
                .flatMap(fieldResultModel -> {
                    if (fieldResultModel.isFile() &&
                            fieldResultModel.getStatus() != FieldStatus.UPLOADED) {
                        return uploadContactFileObservable(fieldResultModel);
                    }
                    return Observable.just(fieldResultModel);
                })
                .doOnNext(fieldResultModel -> Log.d(TAG_CONTACT, "field sending - " + fieldResultModel.getResult()))
                .map(mFieldResultModelToFieldContactRequestEntityMapper)
                .toList()
                .toObservable()
                .map(fieldResultContactRequestEntities -> new ChoiceRequestEntity(stepResultEntity.getChoiceId(),
                        fieldResultContactRequestEntities));
    }

    private Completable syncViolationsObservable() {
        return mViolationRepository.getAll()
                .doOnNext(violationEntities -> sendNotification(getString(R.string.text_sync_title), getString(R.string.sending_violations)))
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(Observable::fromIterable)
                .filter(ViolationEntity::isNotSent)
                .doOnNext(s -> Log.d(TAG_VIOLATION, "sending - " + s.getId()))
                .flatMap(violationEntity -> mFosService.createViolation(projectId,
                        new ViolationRequestEntity(violationEntity.getIdViolation(),
                                scheduleId,
                                violationEntity.getDate()))
                        .map(o -> violationEntity))
                .doOnNext(s -> Log.d(TAG_VIOLATION, "sent - " + s.getId()))
                .doOnNext(violationEntity -> violationEntity.setSent(true))
                .flatMap(violationEntity -> mViolationRepository.update(violationEntity).toObservable())
                .toList()
                .toCompletable();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void sendNotification(String Title, String Text) {
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentIntent(contentIntent)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(Title)
                .setContentText(Text)
                .setWhen(System.currentTimeMillis());
        startForeground(SERVICE_NOTIFICATION_ID, builder.build());
    }

    private void handleError(Throwable throwable) {
        int message = R.string.message_exception_not_send_data;
        if (throwable instanceof CompositeException) {
            CompositeException compositeException = (CompositeException) throwable;
            Throwable throwable1 = compositeException.getExceptions().get(compositeException.getExceptions().size() - 1);
            if (throwable1 instanceof NoUploadFileException)
                message = R.string.message_exception_no_upload_file;
        }
        AlertUtils.showToast(getApplicationContext(), message);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Optional.ofNullable(mDisposable)
                .filter(value -> !value.isDisposed())
                .ifPresent(Disposable::dispose);
        stopForeground(true);
    }

    public enum SyncType {
        SYNC_INIT_STEPS,
        SYNC_CONTACTS,
        SYNC_ALL,
        SYNC_VIOLATIONS,
        SYNC_WORK,
        SYNC_END_DAY
    }
}
