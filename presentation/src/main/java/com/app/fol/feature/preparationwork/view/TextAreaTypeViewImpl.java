package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.annimon.stream.Optional;
import com.jakewharton.rxbinding.widget.RxTextView;

import butterknife.BindView;

public class TextAreaTypeViewImpl extends BaseItemFieldViewImpl implements TextAreaTypeView {
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.text_input)
    EditText mTextInput;

    public TextAreaTypeViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void populate(FieldModel entity) {
        mTextTitle.setText(entity.getName());
        mTextInput.setActivated(entity.isEmptyField());
        Optional.ofNullable(entity.getResult())
                .executeIfPresent(mTextInput::setText);
        RxTextView.textChanges(mTextInput)
                .map(CharSequence::toString)
                .doOnNext(entity::setResult)
                .filter(s -> !s.isEmpty())
                .doOnNext(s -> entity.setEmptyField(false))
                .doOnNext(s -> mTextInput.setActivated(false))
                .subscribe();
    }
}
