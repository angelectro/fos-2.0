package com.app.fol.feature.preparationwork.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;

import com.app.domain.model.FieldModel;
import com.app.domain.utils.EventBus;
import com.app.domain.utils.FileUtils;
import com.app.fol.R;
import com.app.fol.base.fragment.GenericToolbarStateFragment;
import com.app.fol.base.progress.ProgressView;
import com.app.fol.base.progress.ProgressViewProvider;
import com.app.fol.feature.audioplayer.AudioPlayerActivity;
import com.app.fol.feature.preparationwork.adapter.FieldAction;
import com.app.fol.feature.preparationwork.view.FieldsBaseView;
import com.app.fol.feature.scanner.ScannerActivity;
import com.app.fol.feature.sign.ResultCallback;
import com.app.fol.feature.sign.SignDialog;
import com.app.fol.feature.videoplayer.VideoPlayerActivity;
import com.app.fol.utils.IntentUtils;
import com.app.fol.utils.PermissionUtils;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.ButterKnife;
import io.reactivex.Observable;

public abstract class FieldsBaseFragment<T extends FieldsBaseView> extends GenericToolbarStateFragment<T> implements ProgressViewProvider, ResultCallback {
    private static final int PERMISSION_PHOTO_REQUEST_CODE = 2;
    private static final int PERMISSION_VIDEO_REQUEST_CODE = 4;
    private static final int PERMISSION_WRITE_REQUEST_CODE = 5;
    private static final int PERMISSION_SCANNER_REQUEST_CODE = 6;
    private static final int REQUEST_PICTURE_CAPTURE = 1;
    private static final int REQUEST_VIDEO_CAPTURE = 2;
    private static final int REQUEST_AUDIO_SOUND = 3;
    private static final int REQUEST_QR_CODE = 4;
    @Inject
    protected Activity mActivity;
    @Inject
    @Named("projectId")
    int projectId;
    private FieldAction mFieldAction;
    private String targetPath;

    protected boolean checkRequiredFields(List<FieldModel> fieldModels) {
        return Stream.of(fieldModels)
                .filter(FieldModel::isRequired)
                .filter(fieldModel -> TextUtils.isEmpty(fieldModel.getResult()))
                .map(fieldModel -> {
                    fieldModel.setEmptyField(true);
                    return fieldModel;
                })
                .map(FieldModel::isEmptyField)
                .reduce((value1, value2) -> value1 || value2)
                .orElse(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getInstance().register(ResultCallback.class, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getInstance().unregister(ResultCallback.class);
    }

    @Override
    public void result(String path) {
        mFieldAction.getFieldModel().setResult(path);
        viewImpl.updateField(mFieldAction.getFieldModel());
    }

    protected void actionField(FieldAction fieldAction) {
        mFieldAction = fieldAction;
        switch (fieldAction.getFieldActionType()) {
            case TAKE_PHOTO:
                if (PermissionUtils.checkSelfPermission(mActivity, Manifest.permission.CAMERA)) {
                    PermissionUtils.requestPermissionFragment(this, PERMISSION_PHOTO_REQUEST_CODE, Manifest.permission.CAMERA);
                    return;
                }
                targetPath = IntentUtils.dispatchTakePictureIntent(this, REQUEST_PICTURE_CAPTURE, projectId);
                break;
            case TAKE_VIDEO:
                if (PermissionUtils.checkSelfPermission(mActivity, Manifest.permission.CAMERA)) {
                    PermissionUtils.requestPermissionFragment(this, PERMISSION_VIDEO_REQUEST_CODE, Manifest.permission.CAMERA);
                    return;
                }
                targetPath = IntentUtils.dispatchTakeVideoIntent(this, REQUEST_VIDEO_CAPTURE, projectId);
                break;
            case TAKE_AUDIO:
                if (PermissionUtils.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO)) {
                    PermissionUtils.requestPermissionFragment(this, PERMISSION_WRITE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO);
                    return;
                }
                targetPath = IntentUtils.dispatchRecordAudioIntent(this, REQUEST_AUDIO_SOUND, projectId);
                break;
            case SCAN_QR:
                if (PermissionUtils.checkSelfPermission(mActivity, Manifest.permission.CAMERA)) {
                    PermissionUtils.requestPermissionFragment(this, PERMISSION_SCANNER_REQUEST_CODE, Manifest.permission.CAMERA);
                    return;
                }
                IntentUtils.dispatchScannerIntent(this, REQUEST_QR_CODE);
                break;
            case ADD_SIGN:
                SignDialog signDialog = new SignDialog();
                signDialog.show(getFragmentManager(), fieldAction.getFieldModel());
                break;
            case REMOVE:
                String path = fieldAction.getFieldModel().getResult();
                if (FileUtils.removeFile(path)) {
                    fieldAction.getFieldModel().setResult(null);
                    viewImpl.updateField(fieldAction.getFieldModel());
                }
                break;
            case PLAY_VIDEO:
                VideoPlayerActivity.create(mActivity, fieldAction.getFieldModel().getResult());
                break;
            case PLAY_AUDIO:
                AudioPlayerActivity.startPlay(mActivity, fieldAction.getFieldModel().getResult());
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (Stream.of(REQUEST_PICTURE_CAPTURE, REQUEST_AUDIO_SOUND, REQUEST_VIDEO_CAPTURE).anyMatch(value -> value == requestCode)) {
                Optional.ofNullable(mFieldAction)
                        .map(FieldAction::getFieldModel)
                        .ifPresent(fieldModel -> {
                            Optional.ofNullable(fieldModel.getResult())
                                    .ifPresent(FileUtils::removeFile);
                            fieldModel.setResult(targetPath);
                            viewImpl.updateField(fieldModel);
                        });
            } else if (requestCode == REQUEST_QR_CODE) {
                Optional.ofNullable(mFieldAction)
                        .map(FieldAction::getFieldModel)
                        .ifPresent(fieldModel -> {
                            Optional.ofNullable(fieldModel.getResult())
                                    .ifPresent(FileUtils::removeFile);
                            fieldModel.setResult(data.getStringExtra(ScannerActivity.EXTRA_CONTENT));
                            viewImpl.updateField(fieldModel);
                        });
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionUtils.checkPermissionGranted(grantResults)) {
            if (requestCode == PERMISSION_PHOTO_REQUEST_CODE) {
                targetPath = IntentUtils.dispatchTakePictureIntent(this, REQUEST_PICTURE_CAPTURE, projectId);
            } else if (requestCode == PERMISSION_VIDEO_REQUEST_CODE) {
                targetPath = IntentUtils.dispatchTakeVideoIntent(this, REQUEST_VIDEO_CAPTURE, projectId);
            } else if (requestCode == PERMISSION_WRITE_REQUEST_CODE) {
                targetPath = IntentUtils.dispatchRecordAudioIntent(this, REQUEST_AUDIO_SOUND, projectId);
            } else if (requestCode == PERMISSION_SCANNER_REQUEST_CODE) {
                IntentUtils.dispatchScannerIntent(this, REQUEST_QR_CODE);
            }
        }

    }

    @Override
    public ProgressView getProgress() {
        return ButterKnife.findById((View) viewImpl, R.id.progressContainer);
    }

    @Override
    public Observable<FragmentEvent> getEventObservable() {
        return lifecycle();
    }
}
