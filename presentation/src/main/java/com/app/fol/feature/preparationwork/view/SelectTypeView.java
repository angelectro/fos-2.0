package com.app.fol.feature.preparationwork.view;

import com.app.domain.model.FieldModel;
import com.app.fol.base.adapter.ListItemView;

public interface SelectTypeView extends ListItemView<FieldModel> {
}
