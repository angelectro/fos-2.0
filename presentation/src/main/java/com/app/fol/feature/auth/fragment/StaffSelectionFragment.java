package com.app.fol.feature.auth.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.app.domain.model.CityModel;
import com.app.domain.model.ProgressState;
import com.app.domain.model.ProjectModel;
import com.app.domain.model.StaffModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.usecase.GetCombineStaffUseCase;
import com.app.domain.usecase.GetPromotersUseCase;
import com.app.domain.usecase.GetSupervisorsUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.base.NavigationFragmentListener;
import com.app.fol.base.fragment.GenericBaseStateFragment;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.auth.view.StaffSelectionView;
import com.app.fol.feature.selectionplace.fragment.PlaceSelectionFragment;
import com.app.fol.utils.DialogUtils;
import com.annimon.stream.Optional;

import java.util.List;

import javax.inject.Inject;


public class StaffSelectionFragment extends GenericBaseStateFragment<StaffSelectionView> {
    @Inject
    Activity mActivity;
    @Inject
    GetCombineStaffUseCase mGetCombineStaffUseCase;
    @Inject
    GetPromotersUseCase mGetPromotersUseCase;
    @Inject
    GetSupervisorsUseCase mGetSupervisorsUseCase;
    @Inject
    ApplicationPreferences mApplicationPreferences;
    StaffModel mStaffModel;
    CityModel mCurrentCityModel;
    private boolean firstLaunch = true;


    public static Fragment create() {
        return new StaffSelectionFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_staff_selection;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            mCurrentCityModel = mApplicationPreferences.cityModel().get();
            viewImpl.populate(mCurrentCityModel,
                    mApplicationPreferences.promoterModel().get(),
                    mApplicationPreferences.supervisorModel().get());
        }
    }

    @Override
    protected void onView(StaffSelectionView view) {
        if (firstLaunch) {
            updateData();
            firstLaunch = false;
        }
        ProjectModel projectModel = mApplicationPreferences.authCredentialsModel().get().getProjectModel();
        view.showBackground(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                projectModel.getBackgroundPortrait() : projectModel.getBackgroundLand());
        view.selectCityAction(() -> Optional.ofNullable(mStaffModel)
                .map(StaffModel::getCityModels)
                .ifPresent(list ->
                        DialogUtils.showItemsSelector(mActivity, list, cityModel -> {
                            view.showCity(cityModel);
                            mCurrentCityModel = cityModel;
                        })));

        view.selectPromoterAction(() -> mGetPromotersUseCase.setCityModel(mCurrentCityModel)
                .execute(FosDefaultSubscriber.<List>onPositive(this, o -> {
                    DialogUtils.showItemsSelector(mActivity, o, view::showPromoter);
                })));
        view.selectSupervisorAction(() -> mGetSupervisorsUseCase.setCityModel(mCurrentCityModel)
                .execute(FosDefaultSubscriber.<List>onPositive(this, o -> {
                    DialogUtils.showItemsSelector(mActivity, o, view::showSupervisor);
                })));
        view.onBackPressedAction(() -> getActivity().onBackPressed());
        view.updateDataAction(this::updateData);
        view.nextAction((cityModel, promoterModel, supervisorModel) -> {
            mApplicationPreferences.cityModel().set(cityModel);
            mApplicationPreferences.promoterModel().set(promoterModel);
            mApplicationPreferences.supervisorModel().set(supervisorModel);
            mApplicationPreferences.progressState().set(ProgressState.SELECTION_RETAIN_CHAIN);
            ((NavigationFragmentListener) getActivity()).replaceFragment(PlaceSelectionFragment.create(), true);
        });
    }

    private void updateData() {
        addUseCase(mGetCombineStaffUseCase);
        mGetCombineStaffUseCase.execute(FosDefaultSubscriber.<StaffModel>onPositive(this, o ->
                mStaffModel = o));

    }
}
