package com.app.fol.feature.sign;

import android.Manifest;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.domain.model.FieldModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.preferences.internal.ApplicationPreferencesImpl;
import com.app.domain.utils.EventBus;
import com.app.fol.R;
import com.app.fol.feature.preparationwork.view.FreeDrawViewCustom;
import com.app.fol.utils.FileUtils;
import com.app.fol.utils.PermissionUtils;
import com.annimon.stream.Optional;
import com.rm.freedrawview.FreeDrawSerializableState;
import com.rm.freedrawview.FreeDrawView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SignDialog extends DialogFragment {
    private static final String BUNDLE_SIGN = "BUNDLE_SIGN";
    private static final String BUNDLE_FIELD = "BUNDLE_FIELD";
    private static final int PERMISSION_WRITE_REQUEST_CODE = 234;
    @BindView(R.id.buttonClose)
    ImageView mButtonClose;
    @BindView(R.id.textTitle)
    TextView mTextTitle;
    @BindView(R.id.draw_view)
    FreeDrawViewCustom mDrawView;
    @BindView(R.id.buttonApply)
    Button mButtonApply;
    @BindView(R.id.buttonClear)
    Button mButtonClear;
    Unbinder unbinder;
    private SignFieldModel mFieldModel;
    private FreeDrawSerializableState mFreeDrawSerializableState;
    private ApplicationPreferences mPreferences = new ApplicationPreferencesImpl();

    public SignDialog() {
        setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.dialog_sign, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        mTextTitle.setText(mFieldModel.getTitle());
        Optional.ofNullable(mFreeDrawSerializableState)
                .ifPresent(mDrawView::restoreStateFromSerializable);
        return inflate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Optional.ofNullable(savedInstanceState)
                .ifPresent(bundle -> {
                    if (bundle.containsKey(BUNDLE_SIGN)) {
                        mFreeDrawSerializableState = (FreeDrawSerializableState) bundle.getSerializable(BUNDLE_SIGN);
                        mFieldModel = (SignFieldModel) bundle.getSerializable(BUNDLE_FIELD);
                    }
                });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_WRITE_REQUEST_CODE) {
            if (PermissionUtils.checkPermissionGranted(grantResults)) {
                saveSign();
            } else {
                Toast.makeText(getActivity(), "Не удалось сохранить подпись", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.size_520);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(BUNDLE_SIGN, mDrawView.getCurrentViewStateAsSerializable());
        outState.putSerializable(BUNDLE_FIELD, mFieldModel);
    }

    public void show(FragmentManager manager, FieldModel fieldModel) {
        mFieldModel = new SignFieldModel(fieldModel.getName(), fieldModel.getUuid(), fieldModel.getResult());
        super.show(manager, "Sign");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonClose, R.id.buttonApply, R.id.buttonClear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonClose:
                dismiss();
                break;
            case R.id.buttonApply:
                if (PermissionUtils.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    PermissionUtils.requestPermissionFragment(this, PERMISSION_WRITE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    return;
                } else {
                    saveSign();
                }
                break;
            case R.id.buttonClear:
                mDrawView.clearDrawAndHistory();
                break;
        }
    }

    private void saveSign() {
        mDrawView.getDrawScreenshot(new FreeDrawView.DrawCreatorListener() {
            @Override
            public void onDrawCreated(Bitmap draw) {
                int id = mPreferences.authCredentialsModel().get().getProjectModel().getId();
                try {
                    if (!TextUtils.isEmpty(mFieldModel.getPath())) {
                        FileUtils.removeFile(mFieldModel.getPath());
                    }
                    File file = FileUtils.createFile(getActivity(), FileUtils.FORMAT_JPEG, id);
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                    draw.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    EventBus.getInstance().getOptional(ResultCallback.class)
                            .ifPresent(resultCallback -> {
                                resultCallback.result(file.getAbsolutePath());
                                dismiss();
                            });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDrawCreationError() {

            }
        });
    }
}
