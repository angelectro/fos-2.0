package com.app.fol.feature.multimedia.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.app.domain.model.FieldResultModel;
import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;
import com.app.fol.feature.multimedia.adapter.MultimediaAdapter;

import java.util.List;

import butterknife.BindView;
import rx.functions.Action1;

public class MultimediaFragmentViewImpl extends RelativeLayoutBinded implements MultimediaFragmentView {
    private static final String MANAGER_STATE = "manager";
    @BindView(R.id.recycler_view)
    protected RecyclerView mRecyclerView;
    MultimediaAdapter mMultimediaAdapter;

    public MultimediaFragmentViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode())
            return;
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), getContext().getResources().getInteger(R.integer.grid_columns_multimedia)));
        mMultimediaAdapter = new MultimediaAdapter();
        mRecyclerView.setAdapter(mMultimediaAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(MANAGER_STATE, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        mRecyclerView.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(MANAGER_STATE));
    }

    @Override
    public void populate(List<FieldResultModel> fieldResultModels) {
        mMultimediaAdapter.replace(fieldResultModels);
    }

    @Override
    public void clickAction(Action1<FieldResultModel> fieldResultModelAction) {
        mMultimediaAdapter.getItemClickObservable().subscribe(fieldResultModelAction);
    }
}
