package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;
import rx.functions.Action1;

public class FooterPreparationViewImpl extends RelativeLayoutBinded implements FooterPreparationView {
    @BindView(R.id.button_next)
    Button mButtonNext;

    public FooterPreparationViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void clickAction(Action1 action1) {
        mButtonNext.setOnClickListener(action1::call);
    }

    @Override
    public void populate(String text) {
        mButtonNext.setText(text);
    }
}
