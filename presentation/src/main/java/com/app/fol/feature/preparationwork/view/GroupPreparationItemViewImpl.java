package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.jakewharton.rxbinding.widget.RxRadioGroup;

import butterknife.BindView;


public class GroupPreparationItemViewImpl extends BaseItemFieldViewImpl implements GroupPreparationItemView {
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.radio_group)
    RadioGroup mRadioGroup;

    public GroupPreparationItemViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mTextTitle.setText(entity.getName());
        mRadioGroup.removeAllViews();
        mRadioGroup.clearCheck();
        Stream.range(0, entity.getListPreparationSubFields().size())
                .map(index -> {
                    RadioButton radioButton = (RadioButton) LayoutInflater.from(getContext()).inflate(R.layout.item_radio_subfield, null);
                    radioButton.setText(entity.getListPreparationSubFields().get(index).getData());
                    radioButton.setId(index);
                    return radioButton;
                }).forEach(mRadioGroup::addView);
        Optional.ofNullable(entity.getResult())
                .map(o -> Stream.range(0, entity.getListPreparationSubFields().size())
                        .filter(value -> TextUtils.equals(entity.getListPreparationSubFields().get(value).getData(), o))
                        .single())
                .executeIfPresent(o -> RxRadioGroup.checked(mRadioGroup).call(o));
        RxRadioGroup.checkedChanges(mRadioGroup)
                .subscribe(integer -> {
                    if (integer >= 0) {
                        entity.setResult(entity.getListPreparationSubFields().get(integer).getData());
                        entity.setEmptyField(false);
                        setActivated(false);
                    }
                });
    }
}
