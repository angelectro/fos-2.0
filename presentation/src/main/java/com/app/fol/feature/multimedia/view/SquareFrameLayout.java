package com.app.fol.feature.multimedia.view;

import android.content.Context;
import android.util.AttributeSet;

import com.app.fol.base.butterknife.RelativeLayoutBinded;

public class SquareFrameLayout extends RelativeLayoutBinded {

    public SquareFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
