package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.app.fol.feature.preparationwork.adapter.FieldAction;
import com.app.fol.feature.preparationwork.adapter.FieldActionType;
import com.annimon.stream.Optional;

import butterknife.BindView;
import rx.functions.Action1;

class QrTypeItemViewImpl extends BaseItemFieldViewImpl implements QrTypeItemView {
    public QrTypeItemViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.text_result)
    TextView mTextResult;
    @BindView(R.id.button_scan_qr)
    Button mButtonScanQr;
    @BindView(R.id.button_delete)
    Button mButtonDelete;
    @BindView(R.id.text_description)
    TextView mTextDescription;
    private FieldModel mEntity;

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mEntity = entity;
        mTextTitle.setText(entity.getName());
        mTextDescription.setText(entity.getDescription());
        Optional.ofNullable(entity.getResult())
                .executeIfPresent(text -> {
                    mTextResult.setVisibility(VISIBLE);
                    entity.setEmptyField(false);
                    setActivated(false);
                    mTextResult.setText(text);
                    mButtonDelete.setVisibility(VISIBLE);
                })
                .executeIfAbsent(() -> {
                    mTextResult.setVisibility(GONE);
                    mButtonDelete.setVisibility(GONE);
                });
    }

    @Override
    public void actionField(Action1<FieldAction> fieldActionType) {
        mButtonScanQr.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.SCAN_QR)));
        mButtonDelete.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.REMOVE)));
    }
}
