package com.app.fol.feature.auth.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.data.exceptions.UnauthorizedException;
import com.app.domain.model.ProjectModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.usecase.AuthorizationUseCase;
import com.app.domain.usecase.GetProjectsUseCase;
import com.app.domain.usecase.RemoveArchivedProjectsUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.base.NavigationFragmentListener;
import com.app.fol.base.fragment.GenericBaseStateFragment;
import com.app.fol.base.progress.ProgressViewProvider;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.auth.view.SelectProjectView;
import com.app.fol.utils.DialogUtils;
import com.app.fol.utils.RxUtils;
import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;


public class SelectProjectFragment extends GenericBaseStateFragment<SelectProjectView> implements ProgressViewProvider {
    @Inject
    protected GetProjectsUseCase mGetProjectsUseCase;
    @Inject
    protected RemoveArchivedProjectsUseCase mRemoveArchivedProjectsUseCase;

    @Inject
    protected AuthorizationUseCase mAuthorizationUseCase;

    @Inject
    protected Activity mActivity;

    @Inject
    protected ApplicationPreferences mApplicationPreferences;

    List<ProjectModel> mProjectModels;
    private boolean isShowingSelector;
    private boolean firstLaunch = true;
    private MaterialDialog mMaterialDialog;

    public static SelectProjectFragment create() {
        return new SelectProjectFragment();
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_select_project;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mGetProjectsUseCase == null)
            ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
    }


    @Override
    protected void onView(SelectProjectView view) {
        if (firstLaunch) {
            updateData();
            firstLaunch = false;
        }
        view.signInAction((projectModel, password) -> mAuthorizationUseCase.setParams(projectModel, password).execute(FosDefaultSubscriber.onPositiveNegative(this,
                success -> ((NavigationFragmentListener) getActivity()).replaceFragment(StaffSelectionFragment.create(), true),
                errorModel -> {
                    if (errorModel.getThrowable() instanceof UnauthorizedException)
                        getProgress().showError("Введенный пароль неверный"); //// FIXME: 04.08.2017
                    else
                        getProgress().showError(errorModel.getMessage());
                })));
        view.updateDataAction(this::updateData);
        view.selectProjectAction(() -> {
            isShowingSelector = true;
            showSelector();
        });
        if (isShowingSelector) {
            showSelector();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RxUtils.ifOrElse(savedInstanceState == null)
                .positive(() -> Optional.ofNullable(mApplicationPreferences.authCredentialsModel().get())
                        .ifPresent(authCredentialsModel -> viewImpl.populate(authCredentialsModel.getProjectModel())));
    }

    private void showSelector() {
        Optional.ofNullable(mProjectModels)
                .ifPresent(projectModels -> {
                    mMaterialDialog = DialogUtils.showItemsSelector(getActivity(), projectModels,
                            projectModel -> {
                                isShowingSelector = false;
                                viewImpl.showSelectedProject(projectModel);
                            });
                });
    }

    private void updateData() {
        addUseCase(mGetProjectsUseCase);
        mGetProjectsUseCase.execute(FosDefaultSubscriber.<List<ProjectModel>>onPositive(this, projectModels -> {
            mProjectModels = Stream.of(projectModels)
                    .filter((projectModel) -> !projectModel.isArchived())
                    .collect(Collectors.toList());

            List<Integer> list = Stream.of(projectModels)
                    .filter(ProjectModel::isArchived)
                    .map(ProjectModel::getId)
                    .collect(Collectors.toList());
            if (!list.isEmpty()) {
                mRemoveArchivedProjectsUseCase.setProjectIds(list)
                        .execute(FosDefaultSubscriber.onPositive(o -> Log.d("fos", "removed archived projects")));
            }
        }));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Optional.ofNullable(mMaterialDialog)
                .ifPresent(MaterialDialog::dismiss);
    }
}
