package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.domain.model.SubFieldModel;
import com.app.fol.R;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;


public class SelectTypeViewImpl extends BaseItemFieldViewImpl implements SelectTypeView {
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.spinner)
    MaterialSpinner mSpinner;

    public SelectTypeViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mTextTitle.setText(entity.getName());
        mSpinner.setItems(entity.getListPreparationSubFields());
        Optional.ofNullable(entity.getResult())
                .map(data -> Stream.range(0, entity.getListPreparationSubFields().size())
                        .filter(index -> TextUtils.equals(entity.getListPreparationSubFields().get(index).getData(), data)).single())
                .executeIfPresent(mSpinner::setSelectedIndex)
                .executeIfAbsent(() -> {
                    if (!entity.getListPreparationSubFields().isEmpty())
                        entity.setResult(entity.getListPreparationSubFields().get(0).toString());
                });
        mSpinner.setOnItemSelectedListener((view, position, id, item) -> {
            entity.setEmptyField(false);
            setActivated(false);
            entity.setResult(((SubFieldModel) item).getData());
        });
    }
}
