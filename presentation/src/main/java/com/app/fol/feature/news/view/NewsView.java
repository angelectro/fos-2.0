package com.app.fol.feature.news.view;

import com.app.domain.model.news.NewsModel;
import com.arellomobile.mvp.MvpView;

import java.util.List;

public interface NewsView extends MvpView {
    void showProgress();
    void hideProgress();
    void showError(String message);
    void showNews(List<NewsModel> newsModels);
}
