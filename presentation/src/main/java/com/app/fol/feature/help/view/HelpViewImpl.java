package com.app.fol.feature.help.view;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;

public class HelpViewImpl extends RelativeLayoutBinded implements HelpView {
    @BindView(R.id.text_content)
    TextView mTextContent;

    public HelpViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(String text) {
        mTextContent.setText(Html.fromHtml(text));
    }
}
