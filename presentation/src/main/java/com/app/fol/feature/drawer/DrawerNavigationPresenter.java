package com.app.fol.feature.drawer;

import android.app.Activity;
import android.app.Fragment;

import com.mikepenz.materialdrawer.Drawer;

import rx.functions.Action1;


public interface DrawerNavigationPresenter {
    Drawer initialize(Activity activity, Action1<Fragment> onFragmentSwitch);
    Drawer getDrawer();
    boolean isDrawerOpened();
    void closeDrawer();
}
