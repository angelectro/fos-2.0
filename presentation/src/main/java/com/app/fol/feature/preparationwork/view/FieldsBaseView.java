package com.app.fol.feature.preparationwork.view;

import com.app.domain.model.FieldModel;
import com.app.fol.base.view.BaseView;
import com.app.fol.feature.preparationwork.adapter.FieldAction;

import rx.functions.Action1;

public interface FieldsBaseView extends BaseView {
    void actionField(Action1<FieldAction> fieldActionAction1);
    void updateField(FieldModel preparationFieldModel);
    void displayMarkErrorFields();
}
