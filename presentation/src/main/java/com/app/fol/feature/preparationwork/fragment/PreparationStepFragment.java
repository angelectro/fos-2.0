package com.app.fol.feature.preparationwork.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.app.domain.model.PreparationWorkStepModel;
import com.app.domain.model.ProgressState;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.usecase.GetPreparationStepUseCase;
import com.app.domain.usecase.ProgressStateUpdateUseCase;
import com.app.domain.usecase.SaveLocalResultInitStepUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.home.activity.HomeActivity;
import com.app.fol.feature.preparationwork.view.PreparationStepView;
import com.app.fol.feature.sync.SyncService;
import com.app.fol.utils.RxUtils;

import javax.inject.Inject;

public class PreparationStepFragment extends FieldsBaseFragment<PreparationStepView> {

    private static final String ORDER_ARGS = "order";

    @Inject
    GetPreparationStepUseCase mGetStepUseCase;
    @Inject
    SaveLocalResultInitStepUseCase mSaveResultUseCase;
    @Inject
    ApplicationPreferences mPreferences;
    @Inject
    ProgressStateUpdateUseCase mProgressStateUpdateUseCase;
    boolean isFirst = true;
    private PreparationWorkStepModel mPreparationWorkStepModel;
    private int currentOrder;

    public static Fragment create(int order) {
        PreparationStepFragment preparationStepFragment = new PreparationStepFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ORDER_ARGS, order);
        preparationStepFragment.setArguments(bundle);
        return preparationStepFragment;
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_preparation_work;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
        currentOrder = getArguments().getInt(ORDER_ARGS, 0);
    }

    @Override
    protected void onView(PreparationStepView view) {
        view.actionUpdate(this::updateData);
        if (isFirst) {
            updateData();
            isFirst = false;
        } else if (mPreparationWorkStepModel != null) {
            view.populate(mPreparationWorkStepModel.getListFieldModels(), currentOrder, mPreparationWorkStepModel.isLast());
            initActions();
        }

    }

    private void initActions() {
        viewImpl.actionField(this::actionField);
        viewImpl.actionNext(() -> RxUtils.ifOrElse(checkRequiredFields(mPreparationWorkStepModel.getListFieldModels()))
                .positive(viewImpl::displayMarkErrorFields)
                .negative(() -> mSaveResultUseCase.setPreparationWorkStepModel(mPreparationWorkStepModel)
                        .execute(FosDefaultSubscriber.onPositive(this, o -> {
                            if (mPreparationWorkStepModel.isLast()) {
                                mProgressStateUpdateUseCase.update(ProgressState.COMPLETE_PREPARATION)
                                        .subscribe(() -> {
                                            SyncService.startService(mActivity, SyncService.SyncType.SYNC_WORK);
                                            SyncService.startService(mActivity, SyncService.SyncType.SYNC_INIT_STEPS);
                                            HomeActivity.create(mActivity);
                                        });

                            } else
                                replaceFragment(PreparationStepFragment.create(currentOrder + 1), true);
                        })))
        );
    }

    private void updateData() {
        addUseCase(mGetStepUseCase);
        mGetStepUseCase.setOrder(currentOrder)
                .execute(FosDefaultSubscriber.<PreparationWorkStepModel>onPositive(this, preparationWorkStepModel -> {
                            mPreparationWorkStepModel = preparationWorkStepModel;
                            if (viewImpl != null) {
                                viewImpl.populate(mPreparationWorkStepModel.getListFieldModels(), currentOrder, mPreparationWorkStepModel.isLast());
                                initActions();
                            }
                        }
                ));
    }


    @Override
    protected String toolbarTitle() {
        return getString(R.string.title_preparation_for_work);
    }


}
