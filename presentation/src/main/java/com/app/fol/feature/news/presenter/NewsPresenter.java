package com.app.fol.feature.news.presenter;

import com.app.domain.usecase.GetNewsUseCase;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.news.view.NewsView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

@InjectViewState
public class NewsPresenter extends MvpPresenter<NewsView> {

    @Inject
    protected GetNewsUseCase mGetNewsUseCase;

    public NewsPresenter(HomeComponent homeComponent) {
        homeComponent.inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        mGetNewsUseCase.getNews()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .doOnTerminate(getViewState()::hideProgress)
                .subscribe(FosDefaultSubscriber.onPositiveNegative(null,
                        getViewState()::showNews,
                        errorModel -> getViewState().showError(errorModel.getMessage())));
    }
}
