package com.app.fol.feature.home.activity;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.app.domain.model.ProgressState;
import com.app.fol.FosApplication;
import com.app.fol.R;
import com.app.fol.base.view.BaseToolbarNavigationActivity;
import com.app.fol.di.activity.ActivityModule;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.DaggerHomeComponent;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.auth.activity.AuthorizationActivity;
import com.app.fol.feature.go_work.fragment.GoWorkFragment;
import com.app.fol.feature.news.fragment.NewsFragment;
import com.app.fol.feature.preparationwork.fragment.PreparationStepFragment;
import com.app.fol.feature.questionnaire.fragment.QuestionnaireFragment;
import com.app.fol.feature.steps.fragment.StepFragmentNew;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

public class HomeActivity extends BaseToolbarNavigationActivity implements ComponentsHolder {


    private HomeComponent mHomeComponent;

    public static void create(Context context) {
        context.startActivity(new Intent(context, HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        ProgressState currentState = FosApplication.applicationComponent.applicationPreferences().progressState().get();
        boolean isAuthorized = Stream.of(ProgressState.FINISHED_WORK_DAY,
                ProgressState.COMPLETE_FINISH_QUESTIONNAIRE,
                ProgressState.COMPLETE_AUTH,
                ProgressState.COMPLETE_GO_WORK,
                ProgressState.COMPLETE_PREPARATION,
                ProgressState.CHECK_QUESTIONNAIRE_AVAILABLE)
                .anyMatch(state -> currentState == state);
        if (!isAuthorized) {
            AuthorizationActivity.create(this);
            finish();
        }
        super.onCreate(savedInstanceState);
        Fragment fragmentByTag = getFragmentManager().findFragmentByTag(TAG_TASK_FRAGMENT);
        if (fragmentByTag == null && isAuthorized) {
            Fragment fragment;
            switch (currentState) {
                case COMPLETE_PREPARATION:
                    fragment = GoWorkFragment.create();
                    break;
                case COMPLETE_FINISH_QUESTIONNAIRE:
                case CHECK_QUESTIONNAIRE_AVAILABLE:
                case COMPLETE_GO_WORK:
                    fragment = StepFragmentNew.create(-1);
                    break;
                case FINISHED_WORK_DAY:
                    fragment = QuestionnaireFragment.create(0);
                    break;
                case COMPLETE_AUTH:
                default:
                    fragment = PreparationStepFragment.create(0);
            }
            replaceFragment(fragment, false);
        }
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        boolean needOpenNews = Optional.ofNullable(intent)
                .map(Intent::getAction)
                .filter(value -> TextUtils.equals(value, "news"))
                .isPresent();
        needOpenNews = needOpenNews ||
                Optional.ofNullable(intent)
                        .map(Intent::getExtras)
                        .map(bundle -> bundle.getString("from"))
                        .filter(value -> {
                            String topic = FosApplication.applicationComponent.applicationPreferences().notificationSubscribe().get();
                            return TextUtils.equals(value, "/topics/" + topic);
                        }).isPresent();
        if (needOpenNews) {
            replaceFragment(NewsFragment.create(), true);
        }
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_base;
    }

    private void initializeComponents() {
        mHomeComponent = DaggerHomeComponent
                .builder().activityModule(new ActivityModule(this))
                .applicationComponent(FosApplication.applicationComponent)
                .build();
    }

    @Override
    public <T> T getComponent(Class<T> aClass) {
        if (TextUtils.equals(aClass.getSimpleName(), HomeComponent.class.getSimpleName())) {
            if (mHomeComponent == null)
                initializeComponents();
            return aClass.cast(mHomeComponent);
        }
        throw new IllegalArgumentException("not match component");
    }
}
