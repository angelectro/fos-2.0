package com.app.fol.feature.auth.view;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

import com.app.domain.model.ProjectModel;
import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;
import com.app.fol.utils.RxUtils;
import com.annimon.stream.Optional;
import com.jakewharton.rxbinding.view.RxView;

import butterknife.BindView;
import io.reactivex.Observable;
import rx.functions.Action0;
import rx.functions.Action2;


public class SelectProjectViewImpl extends RelativeLayoutBinded implements SelectProjectView {
    public static final String PROJECT_ARGS = "project";
    public static final String PASSWORD_ARGS = "password";
    @BindView(R.id.button_reload)
    protected Button mButtonReload;
    @BindView(R.id.button_project)
    protected Button mButtonProject;
    @BindView(R.id.text_password)
    protected EditText mTextPassword;
    @BindView(R.id.button_sign_in)
    protected Button mButtonSignIn;
    private ProjectModel mProjectModel;

    public SelectProjectViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode())
            return;
        mButtonSignIn.setEnabled(false);
        Observable.combineLatest(
                RxUtils.textChanged(mButtonProject).map(s -> !TextUtils.isEmpty(s)),
                RxUtils.textChanged(mTextPassword).map(s -> !TextUtils.isEmpty(s)),
                (aBoolean, aBoolean2) -> aBoolean && aBoolean2)
                .subscribe(mButtonSignIn::setEnabled);
    }

    @Override
    public void signInAction(Action2<ProjectModel, String> action2) {
        mButtonSignIn.setOnClickListener(view -> action2.call(mProjectModel, mTextPassword.getText().toString()));
    }

    @Override
    public void selectProjectAction(Action0 action0) {
        RxView.clicks(mButtonProject).subscribe(view -> action0.call());
    }

    @Override
    public void showSelectedProject(ProjectModel projectModel) {
        mProjectModel = projectModel;
        mButtonProject.setText(projectModel.toString());
    }

    @Override
    public void populate(ProjectModel projectModel) {
        Optional.ofNullable(projectModel).ifPresent(this::showSelectedProject);
    }

    @Override
    public void updateDataAction(Action0 action0) {
        mButtonReload.setOnClickListener(view -> action0.call());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(PROJECT_ARGS, mProjectModel);
        outState.putString(PASSWORD_ARGS, mTextPassword.getText().toString());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        Optional.ofNullable((ProjectModel) savedInstanceState.getParcelable(PROJECT_ARGS)).
                ifPresent(this::showSelectedProject);
        Optional.ofNullable(savedInstanceState.getString(PASSWORD_ARGS))
                .ifPresent(mTextPassword::setText);

    }
}
