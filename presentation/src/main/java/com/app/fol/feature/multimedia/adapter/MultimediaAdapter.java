package com.app.fol.feature.multimedia.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.TypeField;
import com.app.fol.R;
import com.app.fol.base.adapter.AbsTypedViewHolder;
import com.app.fol.base.adapter.GenericAdapter;

import java.util.List;

public class MultimediaAdapter extends GenericAdapter<FieldResultModel> {
    public MultimediaAdapter(List<FieldResultModel> entities) {
        super(entities, 0);
    }

    public MultimediaAdapter() {
        super(0);
    }

    @Override
    public int getItemViewType(int position) {
        return getEntities().get(position).getType().ordinal();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        if (viewType == TypeField.PHOTO.ordinal()) {
            layout = R.layout.item_image_multimedia;
        } else {
            layout = R.layout.item_video_multimedia;
        }
        return new AbsTypedViewHolder<FieldResultModel>(LayoutInflater.from(parent.getContext()).inflate(layout, parent, false));
    }
}
