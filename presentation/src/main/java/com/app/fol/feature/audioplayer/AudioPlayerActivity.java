package com.app.fol.feature.audioplayer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.fol.R;
import com.app.fol.feature.audiorecorder.Util;
import com.cleveroad.audiovisualization.DbmHandler;
import com.cleveroad.audiovisualization.GLAudioVisualizationView;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AudioPlayerActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    private static final String FULL_PATH_AUDIO = "audio";
    @BindView(R.id.status)
    TextView mStatusView;
    @BindView(R.id.timer)
    TextView mTimerView;
    @BindView(R.id.button_play)
    ImageButton mButtonPlay;
    @BindView(R.id.content)
    RelativeLayout mContent;
    int color;
    private PlayStatus mPlayStatus = PlayStatus.STOP;
    private GLAudioVisualizationView mVisualizationView;
    private MediaPlayer player;
    private long time;
    private Disposable mSubscribeTimer;

    public static void startPlay(Context activity, String fullPathAudio) {
        Intent intent = new Intent(activity, AudioPlayerActivity.class);
        intent.putExtra(FULL_PATH_AUDIO, fullPathAudio);
        activity.startActivity(intent);
    }
    public static Intent getStartIntent(Context activity, String fullPathAudio) {
        Intent intent = new Intent(activity, AudioPlayerActivity.class);
        intent.putExtra(FULL_PATH_AUDIO, fullPathAudio);
        return intent;
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_player);
        ButterKnife.bind(this);
        color = ContextCompat.getColor(this, R.color.accent);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setBackgroundDrawable(
                    new ColorDrawable(Util.getDarkerColor(color)));
        }

        mVisualizationView = new GLAudioVisualizationView.Builder(this)
                .setLayersCount(1)
                .setWavesCount(6)
                .setWavesHeight(R.dimen.aar_wave_height)
                .setWavesFooterHeight(R.dimen.aar_footer_height)
                .setBubblesPerLayer(20)
                .setBubblesSize(R.dimen.aar_bubble_size)
                .setBubblesRandomizeSize(true)
                .setBackgroundColor(Util.getDarkerColor(color))
                .setLayerColors(new int[]{color})
                .build();
        mContent.addView(mVisualizationView, 0);
        startPlaying();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mVisualizationView.onResume();
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        try {
            mVisualizationView.release();
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    private void startPlaying() {
        if (mPlayStatus == PlayStatus.STOP)
            try {
                player = new MediaPlayer();
                player.setDataSource(getIntent().getStringExtra(FULL_PATH_AUDIO));
                player.prepare();
                player.start();
                mPlayStatus = PlayStatus.PLAYING;

                mVisualizationView.linkTo(DbmHandler.Factory.newVisualizerHandler(this, player));
                mVisualizationView.post(() -> player.setOnCompletionListener(AudioPlayerActivity.this));

                mTimerView.setText("00:00:00");
                mStatusView.setText(R.string.aar_playing);
                mStatusView.setVisibility(View.VISIBLE);
                mButtonPlay.setImageResource(R.drawable.aar_ic_pause);
                startTimer();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startTimer() {
        mSubscribeTimer = Observable.interval(1, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(aLong -> time++)
                .map(aLong -> time)
                .filter(aLong -> aLong % 1000 == 0)
                .map(aLong -> aLong / 1000)
                .subscribe(second -> mTimerView.setText(Util.formatSeconds(second.intValue())));
    }

    private void stopTimer() {
        if (mSubscribeTimer != null)
            mSubscribeTimer.dispose();
    }

    private void stopPlaying() {
        mStatusView.setText("");
        mStatusView.setVisibility(View.INVISIBLE);
        mButtonPlay.setImageResource(R.drawable.aar_ic_play);

        mVisualizationView.release();

        if (player != null) {
            try {
                player.stop();
                player.reset();
            } catch (Exception e) {
            }
        }
        stopTimer();
        time = -1;
    }

    private void pausePlaying() {
        if (player != null) {
            player.pause();
            mStatusView.setText(R.string.aar_paused);
            mPlayStatus = PlayStatus.PAUSE;
            mButtonPlay.setImageResource(R.drawable.aar_ic_play);
            stopTimer();
        }

    }


    private void replay() {
        if (player != null) {
            player.start();
            mPlayStatus = PlayStatus.PLAYING;
            startTimer();
            mButtonPlay.setImageResource(R.drawable.aar_ic_pause);
            mStatusView.setText(R.string.aar_playing);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopPlaying();
        mPlayStatus = PlayStatus.STOP;
    }

    @OnClick(R.id.button_play)
    public void onViewClicked() {
        switch (mPlayStatus) {
            case PLAYING:
                pausePlaying();
                break;
            case PAUSE:
                replay();
                break;
            case STOP:
                startPlaying();
                break;
        }
    }


    enum PlayStatus {
        PLAYING,
        PAUSE,
        STOP
    }
}
