package com.app.fol.feature.preparationwork.view;

import rx.functions.Action1;

public interface FooterPreparationView{
    void clickAction(Action1 action1);
    void populate(String text);
}
