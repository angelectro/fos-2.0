package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.app.fol.feature.preparationwork.adapter.FieldAction;
import com.app.fol.feature.preparationwork.adapter.FieldActionType;
import com.annimon.stream.Optional;

import butterknife.BindView;
import rx.functions.Action1;


public class AudioTypeViewImpl extends BaseItemFieldViewImpl implements AudioTypeView {
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.button_play_audio)
    Button mButtonPlayAudio;
    @BindView(R.id.button_take_audio)
    Button mButtonTakeAudio;
    @BindView(R.id.button_delete)
    Button mButtonDelete;
    @BindView(R.id.text_description)
    TextView mTextDescription;
    private FieldModel mEntity;

    public AudioTypeViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mEntity = entity;
        mTextTitle.setText(entity.getName());
        if (!TextUtils.isEmpty(entity.getDescription())) {
            mTextDescription.setVisibility(VISIBLE);
            mTextDescription.setText(entity.getDescription());
        } else
            mTextDescription.setVisibility(GONE);
        Optional.ofNullable(entity.getResult())
                .executeIfPresent(o -> {
                    entity.setEmptyField(false);
                    setActivated(false);
                    mButtonDelete.setVisibility(VISIBLE);
                    mButtonPlayAudio.setVisibility(VISIBLE);
                })
                .executeIfAbsent(() -> {
                    mButtonDelete.setVisibility(GONE);
                    mButtonPlayAudio.setVisibility(GONE);
                });
    }

    @Override
    public void actionField(Action1<FieldAction> fieldActionType) {
        mButtonPlayAudio.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.PLAY_AUDIO)));
        mButtonDelete.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.REMOVE)));
        mButtonTakeAudio.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.TAKE_AUDIO)));
    }
}
