package com.app.fol.feature.questionnaire.view;

import com.app.fol.base.progress.ProgressView;
import com.app.fol.feature.steps.view.FieldsBaseViewNew;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface QuestionnaireView extends FieldsBaseViewNew, ProgressView {
    void setVisibilityReloadButton(boolean isVisible);

    void setNextButtonTitle(String title);

    void recreateActivity();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void segueNextStep(int nextOrder);
}
