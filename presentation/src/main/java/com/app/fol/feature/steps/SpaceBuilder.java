package com.app.fol.feature.steps;

import android.content.Context;
import android.widget.GridLayout;
import android.widget.Space;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

public class SpaceBuilder {
    private int rowCount;
    private int columnCount;
    private int itemSize;
    private Context context;
    private Orientation orientation;

    public SpaceBuilder(Context context) {
        this.context = context;
    }

    public SpaceBuilder setOrientation(Orientation orientation) {
        this.orientation = orientation;
        return this;
    }

    public SpaceBuilder rowCount(int rowCount) {
        this.rowCount = rowCount;
        return this;
    }

    public SpaceBuilder columnCount(int columnCount) {
        this.columnCount = columnCount;
        return this;
    }

    public SpaceBuilder itemSize(int itemSize) {
        this.itemSize = itemSize;
        return this;
    }

    public List<Space> build() {
        return Stream.range(0, orientation == Orientation.Horizontal ? columnCount : rowCount)
                .map(index -> {
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
                    if (orientation == Orientation.Horizontal) {
                        layoutParams.columnSpec = GridLayout.spec(index);
                        layoutParams.rowSpec = GridLayout.spec(rowCount - 1);
                        layoutParams.width = itemSize;
                    } else {
                        layoutParams.rowSpec = GridLayout.spec(index);
                        layoutParams.columnSpec = GridLayout.spec(columnCount - 1);
                        layoutParams.height = itemSize;
                    }
                    Space space = new Space(context);
                    space.setLayoutParams(layoutParams);
                    return space;
                }).collect(Collectors.toList());
    }

    public enum Orientation {
        Horizontal,
        Vertical
    }
}
