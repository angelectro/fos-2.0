package com.app.fol.feature.steps;


import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.widget.Button;
import android.widget.GridLayout;

import com.app.domain.model.steps.ChoiceModel;
import com.app.fol.R;
import com.app.fol.feature.steps.view.ButtonColor;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

public class ButtonsBuilder {

    Context context;
    private int itemMargin;
    private List<ChoiceModel> choiceModels;
    private boolean buttonsEnabled;

    public ButtonsBuilder(Context context) {
        this.context = context;
    }

    public ButtonsBuilder setButtonsEnabled(boolean buttonsEnabled) {
        this.buttonsEnabled = buttonsEnabled;
        return this;
    }

    public ButtonsBuilder setItemMargin(int itemMargin) {
        this.itemMargin = itemMargin;
        return this;
    }

    public ButtonsBuilder setChoiceModels(List<ChoiceModel> choiceModels) {
        this.choiceModels = choiceModels;
        return this;
    }

    public List<Button> build() {
        return Stream.of(choiceModels)
                .map(choice -> {
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
                    layoutParams.columnSpec = GridLayout.spec(choice.getX(), choice.getCols());
                    layoutParams.rowSpec = GridLayout.spec(choice.getY(), choice.getRows());
                    layoutParams.setGravity(Gravity.FILL);
                    layoutParams.setMargins(itemMargin, itemMargin, itemMargin, itemMargin);
                    ButtonColor button;
                    try {
                        button = new ButtonColor(context, Color.parseColor(choice.getColor()));
                    } catch (Exception e) {
                        button = new ButtonColor(context, Color.WHITE);
                        e.printStackTrace();
                    }
                    button.setText(choice.getTitle());
                    button.setTextColor(ContextCompat.getColor(context, choice.isBlack() ? R.color.black : R.color.white));
                    button.setId(choice.getId());
                    button.setLayoutParams(layoutParams);
                    int padding = context.getResources().getDimensionPixelSize(R.dimen.margin_5);
                    button.setPadding(padding, padding, padding, padding);
                    button.setMaxWidth(1);
                    button.setEnabled(buttonsEnabled);
                    return button;
                })
                .collect(Collectors.toList());
    }
}
