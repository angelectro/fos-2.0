package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.app.fol.feature.preparationwork.adapter.FieldAction;
import com.app.fol.feature.preparationwork.adapter.FieldActionType;
import com.annimon.stream.Optional;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import rx.functions.Action1;

public class PhotoPreparationItemViewImpl extends BaseItemFieldViewImpl implements PhotoPreparationItemView {
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.image)
    ImageView mImage;
    @BindView(R.id.button_take_photo)
    Button mButtonTakePhoto;
    @BindView(R.id.button_delete)
    Button mButtonDelete;
    @BindView(R.id.text_description)
    TextView mTextDescription;
    private FieldModel mEntity;

    public PhotoPreparationItemViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mEntity = entity;
        mTextTitle.setText(entity.getName());
        mTextDescription.setText(entity.getDescription());
        Optional.ofNullable(entity.getResult())
                .executeIfPresent(fullPath -> {
                    mImage.setVisibility(VISIBLE);
                    entity.setEmptyField(false);
                    setActivated(false);
                    Glide.with(getContext())
                            .load(fullPath)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(mImage);
                    mButtonDelete.setVisibility(VISIBLE);
                })
                .executeIfAbsent(() -> {
                    mImage.setVisibility(GONE);
                    mButtonDelete.setVisibility(GONE);
                });
    }

    @Override
    public void actionField(Action1<FieldAction> fieldActionType) {
        mButtonTakePhoto.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.TAKE_PHOTO)));
        mButtonDelete.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.REMOVE)));
    }
}
