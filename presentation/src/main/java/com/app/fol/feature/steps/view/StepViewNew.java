package com.app.fol.feature.steps.view;

import android.support.annotation.StringRes;

import com.app.domain.model.steps.ChoiceModel;
import com.app.domain.model.steps.TypeAttachment;
import com.app.fol.feature.steps.DialogModel;
import com.app.fol.feature.steps.presenter.StepPresenter;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.io.File;
import java.util.List;

public interface StepViewNew extends FieldsBaseViewNew {
    void setSizeGridLayout(int rowCount, int columnCount);

    void buildButton(List<ChoiceModel> choiceModels, boolean enabledButtons);

    void buildSpaces(int rowCount, int columnCount);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void segueNextStep(int stepId);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setEnabledRegistrationButton(boolean isVisible);

    void setVisibleRegistrationButton(boolean isVisible);

    void setEnabledChoiceButtons(boolean isVisible);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showNews();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showMediaFiles();

    void setVisibleAttachmentButton(boolean b);

    void showViolationDialog(DialogModel dialogModel);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showDialog(DialogModel dialogModel);

    void closeDialog();

    void showWarningDialog(DialogModel dialogModel);

    void showConfirmationDialog(DialogModel dialogModel);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showInfoDialog(DialogModel dialogModel);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setStateSwitchBreak(Boolean isChecked);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setStateSwitchOnline(Boolean isChecked);

    void showProgress();

    void hideProgress();

    void updateToolbarTitle(String name);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showAttachment(TypeAttachment type, File mFileForProject, String url);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void recreateActivity();

    void hideProgressDialog();

    void showError(String message, StepPresenter.ActionType gettingStep);

    void closeError();

    void showProgressDialog(@StringRes int text, StepPresenter.ActionType actionType);

    void showSuccessFinishDialog();
}
