package com.app.fol.feature.go_work.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.usecase.StartWorkUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.base.fragment.GenericBasePresenterFragment;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.go_work.view.GoWorkView;
import com.app.fol.feature.home.activity.HomeActivity;
import com.app.fol.feature.sync.SyncService;

import javax.inject.Inject;

public class GoWorkFragment extends GenericBasePresenterFragment<GoWorkView> {


    @Inject
    Activity mActivity;
    @Inject
    ApplicationPreferences mApplicationPreferences;
    @Inject
    StartWorkUseCase mStartWorkUseCase;
    boolean isDelayAtStart = false;

    public static Fragment create() {
        return new GoWorkFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_go_work;
    }

    @Override
    protected void onView(GoWorkView view) {
        view.actionGoBack(() -> getActivity().onBackPressed());
        view.actionGoWork(() -> mStartWorkUseCase.execute(FosDefaultSubscriber.<Boolean>onPositive(
                delay -> {
                    isDelayAtStart = delay;
                    SyncService.startService(mActivity, SyncService.SyncType.SYNC_WORK);
                    if (delay) {
                        SyncService.startService(mActivity, SyncService.SyncType.SYNC_VIOLATIONS);
                        showDialog();
                    } else {
                        HomeActivity.create(mActivity);
                    }

                }
        )));
        if (isDelayAtStart)
            showDialog();
    }

    private void showDialog() {
        new MaterialDialog.Builder(mActivity)
                .title(R.string.violation)
                .cancelable(false)
                .content(R.string.violation_day_start)
                .positiveText(R.string.ok)
                .onPositive((dialog, which) -> {
                    HomeActivity.create(mActivity);
                }).show();
    }
}
