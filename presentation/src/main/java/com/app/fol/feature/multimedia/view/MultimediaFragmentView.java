package com.app.fol.feature.multimedia.view;

import com.app.domain.model.FieldResultModel;
import com.app.fol.base.view.BaseView;

import java.util.List;

import rx.functions.Action1;

public interface MultimediaFragmentView extends BaseView {
    void populate(List<FieldResultModel> fieldResultModels);
    void clickAction(Action1<FieldResultModel> fieldResultModelAction);
}
