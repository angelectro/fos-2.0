package com.app.fol.feature.steps.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.app.fol.base.fragment.ToolbarBaseFragment;
import com.app.fol.feature.preparationwork.adapter.FieldsAdapter;
import com.app.fol.feature.sign.SignDialog;
import com.app.fol.feature.steps.presenter.FieldsPresenter;
import com.app.fol.feature.steps.view.FieldsBaseViewNew;
import com.app.fol.utils.PermissionUtils;
import com.app.fol.utils.view.RecyclerItemSpacingDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

public abstract class BaseStepFragment extends ToolbarBaseFragment implements FieldsBaseViewNew {

    @BindView(R.id.recycler_view)
    protected RecyclerView mRecyclerView;

    private FieldsAdapter mFieldsAdapter;

    protected abstract FieldsPresenter getPresenter();


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getPresenter().onPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void showFields(List<FieldModel> fieldModels) {
        mFieldsAdapter.setEntities(fieldModels);
        mFieldsAdapter.getFieldActionPublishSubject().subscribe(getPresenter()::actionField);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        mFieldsAdapter = getFieldsAdapter();
        mRecyclerView.setAdapter(mFieldsAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.addItemDecoration(new RecyclerItemSpacingDecoration(getActivity(), R.dimen.margin_10, R.dimen.margin_10, R.dimen.margin_10));
        return view;
    }

    @Override
    public void updateField(FieldModel fieldModel) {
        mFieldsAdapter.updateItem(fieldModel);
    }

    @Override
    public void displayMarkErrorFields() {
        mFieldsAdapter.notifyDataSetChanged();
        Snackbar.make(mRecyclerView, R.string.warning_required_field, BaseTransientBottomBar.LENGTH_LONG).show();
    }

    @Override
    public void requestPermission(int requestCode, String... permissions) {
        PermissionUtils.requestPermissionFragment(this, requestCode, permissions);
    }

    @Override
    public void startIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void startIntent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showSignDialog(FieldModel fieldModel, Action1<String> stringAction1) {
        SignDialog signDialog = new SignDialog();
        signDialog.show(getFragmentManager(), fieldModel);
    }


    public FieldsAdapter getFieldsAdapter() {
        return new FieldsAdapter(new View(getActivity()), new View(getActivity()));
    }
}
