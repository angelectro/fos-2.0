package com.app.fol.feature.selectionplace.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.domain.model.ProgressState;
import com.app.domain.model.ScheduleModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.usecase.GetScheduleUseCase;
import com.app.domain.usecase.ProgressStateUpdateUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.base.fragment.GenericToolbarStateFragment;
import com.app.fol.base.progress.ProgressView;
import com.app.fol.base.progress.ProgressViewProvider;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.home.activity.HomeActivity;
import com.app.fol.feature.selectionplace.view.PlaceSelectionView;
import com.app.fol.feature.sync.PreLoadingService;
import com.app.fol.feature.sync.SyncService;
import com.annimon.stream.Optional;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.reactivex.Observable;


public class PlaceSelectionFragment extends GenericToolbarStateFragment<PlaceSelectionView> implements ProgressViewProvider {

    @Inject
    GetScheduleUseCase mGetRetainChainsUseCase;
    @Inject
    ApplicationPreferences mApplicationPreferences;
    @Inject
    ProgressStateUpdateUseCase mProgressStateUpdateUseCase;
    @Inject
    Activity mActivity;
    List<ScheduleModel> mRetainChainsModels;
    private boolean firstLaunch = true;

    public static PlaceSelectionFragment create() {
        return new PlaceSelectionFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_place_selection;
    }

    @Override
    protected void onView(PlaceSelectionView view) {
        if (firstLaunch) {
            updateData();
            firstLaunch = false;
        }
        Optional.ofNullable(mRetainChainsModels).ifPresent(retainChainsModels -> view.populate(mRetainChainsModels));
        view.backAction(() -> getActivity().onBackPressed());
        view.updateDataAction(this::updateData);
        view.itemClickAction(retainChainsModel -> new MaterialDialog.Builder(mActivity)
                .title(retainChainsModel.getRetainChain())
                .content(R.string.text_question_reatin_chain)
                .positiveText(R.string.select)
                .negativeText(R.string.cancel)
                .onPositive((dialog, which) -> {
                    mApplicationPreferences.scheduleModel().set(retainChainsModel);
                    mProgressStateUpdateUseCase.update(ProgressState.COMPLETE_AUTH)
                            .doOnComplete(() -> SyncService.startService(mActivity, SyncService.SyncType.SYNC_WORK))
                            .subscribe(() -> {
                                mActivity.startService(new Intent(mActivity, PreLoadingService.class));
                                HomeActivity.create(mActivity);
                            });

                })
                .onNegative((dialog, which) -> {
                }).show());
    }

    private void updateData() {
        mGetRetainChainsUseCase.execute(FosDefaultSubscriber.<List<ScheduleModel>>onPositive(this, retainChainsModels -> {
                    mRetainChainsModels = retainChainsModels;
                    Optional.ofNullable(viewImpl).ifPresent(placeSelectionView -> placeSelectionView.populate(mRetainChainsModels));
                }
        ));
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.text_place_selection);
    }

    @Override
    public ProgressView getProgress() {
        return ButterKnife.findById((View) viewImpl, R.id.progressContainer);
    }

    @Override
    public Observable<FragmentEvent> getEventObservable() {
        return lifecycle();
    }
}
