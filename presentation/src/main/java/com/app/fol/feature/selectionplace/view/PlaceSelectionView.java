package com.app.fol.feature.selectionplace.view;

import com.app.domain.model.ScheduleModel;
import com.app.fol.base.view.BaseView;

import java.util.List;

import rx.functions.Action0;
import rx.functions.Action1;


public interface PlaceSelectionView extends BaseView{
    void populate(List<ScheduleModel> retainChainsModels);
    void backAction(Action0 action0);
    void itemClickAction(Action1<ScheduleModel> action1);

    void updateDataAction(Action0 action0);
}
