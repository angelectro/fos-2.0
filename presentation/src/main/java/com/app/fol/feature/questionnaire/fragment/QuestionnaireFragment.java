package com.app.fol.feature.questionnaire.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.app.fol.R;
import com.app.fol.base.progress.ProgressView;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.home.activity.HomeActivity;
import com.app.fol.feature.preparationwork.adapter.FieldsAdapter;
import com.app.fol.feature.preparationwork.view.FooterPreparationView;
import com.app.fol.feature.questionnaire.presenter.QuestionnairePresenter;
import com.app.fol.feature.questionnaire.view.QuestionnaireView;
import com.app.fol.feature.steps.fragment.BaseStepFragment;
import com.app.fol.feature.steps.presenter.FieldsPresenter;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.arellomobile.mvp.presenter.ProvidePresenterTag;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;

public class QuestionnaireFragment extends BaseStepFragment implements QuestionnaireView {

    private static final String ARGS_ORDER = "ARGS_ORDER_Questionnaire";
    @InjectPresenter
    QuestionnairePresenter presenter;

    @BindView(R.id.button_back)
    Button mButtonBack;
    @BindView(R.id.button_reload)
    Button mButtonReload;
    @BindView(R.id.progressContainer)
    ProgressView progressView;
    private FooterPreparationView mFooterPreparationView;


    public static Fragment create(int order) {
        QuestionnaireFragment questionnaireFragment = new QuestionnaireFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARGS_ORDER, order);
        questionnaireFragment.setArguments(bundle);
        return questionnaireFragment;
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    QuestionnairePresenter providePresenter() {
        int step = getArguments().getInt(ARGS_ORDER, -1);
        HomeComponent homeComponent = ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class);
        return new QuestionnairePresenter(step, homeComponent);
    }

    @ProvidePresenterTag(presenterClass = QuestionnairePresenter.class, type = PresenterType.LOCAL)
    String provideRepositoryPresenterTag() {
        return String.valueOf(getArguments().getInt(ARGS_ORDER, -1));
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.title_fineshed_questionnaire);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_questionnaire;
    }

    @Override
    protected FieldsPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setVisibilityReloadButton(boolean isVisible) {
        ButterKnife.bind(this, getView());
        mButtonBack.setVisibility(GONE);
        mButtonReload.setVisibility(isVisible ? View.VISIBLE : GONE);
    }

    @Override
    public void setNextButtonTitle(String title) {
        mFooterPreparationView.populate(title);
    }

    @Override
    public void recreateActivity() {
        HomeActivity.create(getActivity());
    }

    @Override
    public void segueNextStep(int nextOrder) {
        replaceFragment(QuestionnaireFragment.create(nextOrder), true);
    }

    @Override
    public FieldsAdapter getFieldsAdapter() {
        mFooterPreparationView = (FooterPreparationView) LayoutInflater.from(getActivity()).inflate(R.layout.footer_preparation, mRecyclerView, false);
        mFooterPreparationView.clickAction(v -> {
            presenter.clickedNextButton();
        });
        return new FieldsAdapter(new View(getActivity()), (View) mFooterPreparationView);
    }

    @Override
    public void showProgressBar() {
        progressView.showProgressBar();
    }

    @Override
    public void hideProgressBar() {
        progressView.hideProgressBar();
    }

    @Override
    public void showError(String error) {
        progressView.showError(error);
    }

    @OnClick(R.id.button_reload)
    public void clickedReloadButton() {
        presenter.updateData();
    }
}
