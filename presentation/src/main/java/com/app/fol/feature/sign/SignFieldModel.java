package com.app.fol.feature.sign;

import java.io.Serializable;

public class SignFieldModel implements Serializable{
    private String title;
    private String uuid;
    private String path;

    SignFieldModel(String title, String uuid, String path) {
        this.title = title;
        this.uuid = uuid;
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public String getUuid() {
        return uuid;
    }

    public String getPath() {
        return path;
    }
}
