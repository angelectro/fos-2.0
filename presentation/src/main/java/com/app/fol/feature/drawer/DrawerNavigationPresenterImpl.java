package com.app.fol.feature.drawer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.annotation.StringRes;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.domain.model.AuthCredentialsModel;
import com.app.domain.model.ProgressState;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.preferences.internal.ObjectPref;
import com.app.domain.usecase.EndWorkdayUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.help.fragment.HelpFragment;
import com.app.fol.feature.home.activity.HomeActivity;
import com.app.fol.feature.multimedia.fragment.MultimediaFragment;
import com.app.fol.feature.news.fragment.NewsFragment;
import com.app.fol.feature.steps.fragment.StepFragmentNew;
import com.app.fol.feature.sync.SyncService;
import com.app.fol.feature.targetaudience.fragment.TargetAudienceFragment;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.util.KeyboardUtil;

import java.util.List;

import javax.inject.Inject;

import rx.functions.Action1;


public class DrawerNavigationPresenterImpl implements DrawerNavigationPresenter {
    @Inject
    ApplicationPreferences mApplicationPreferences;
    @Inject
    EndWorkdayUseCase mEndWorkdayUseCase;
    private Drawer mDrawer;
    private Activity mActivity;

    public Fragment fragmentByDrawerItem(Item drawerItem) {
        switch (drawerItem) {
            case NEWS:
                return NewsFragment.create();
            case MEDIA_FILES:
                return MultimediaFragment.create();
            case STRUCTURE_CONTACT:
                return TargetAudienceFragment.create(mApplicationPreferences.authCredentialsModel().get().getProjectModel().getContactStructure(), R.string.title_structure_contact);
            case TARGET_AUDIENCE:
                return TargetAudienceFragment.create(mApplicationPreferences.authCredentialsModel().get().getProjectModel().getTargetAudience(), R.string.title_target_audience);
            case SEND_ALL_DATA:
                SyncService.startService(mActivity, SyncService.SyncType.SYNC_ALL);
                return null;
            case HELP:
                return HelpFragment.create();
            case LOGOUT:
                new MaterialDialog.Builder(mActivity)
                        .content(R.string.text_dialog_alert_logout)
                        .positiveText(R.string.yes)
                        .negativeText(R.string.cancel)
                        .onPositive((dialog, which) -> {
                            mEndWorkdayUseCase.execute(FosDefaultSubscriber.onPositive(o -> {
                                HomeActivity.create(mActivity);
                            }));
                        })
                        .onNegative((dialog, which) -> {
                        }).show();
            default:
                return null;
        }
    }

    @Override
    public Drawer initialize(Activity activity, Action1<Fragment> onFragmentSwitch) {
        mActivity = activity;
        ((ComponentsHolder) activity).getComponent(HomeComponent.class).inject(this);
        mDrawer = DrawerFunctions.buildUsingDefaultStyle(activity)
                .withHeader(R.layout.drawer_header)
                .withDrawerItems(getItems(activity))
                .withSelectedItem(-1)
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        KeyboardUtil.hideKeyboard(mActivity);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    Fragment fragment = fragmentByDrawerItem(Item.fromTitle((Integer) drawerItem.getTag()));
                    onFragmentSwitch.call(fragment);
                    closeDrawer();
                    return true;
                })
                .build();
        DrawerHeaderView headerView = (DrawerHeaderView) mDrawer.getHeader();
        ProgressState progressState = mApplicationPreferences.progressState().get();
        if (Stream.of(ProgressState.COMPLETE_AUTH,
                ProgressState.COMPLETE_PREPARATION,
                ProgressState.COMPLETE_GO_WORK,
                ProgressState.COMPLETE_FINISH_QUESTIONNAIRE,
                ProgressState.CHECK_QUESTIONNAIRE_AVAILABLE,
                ProgressState.FINISHED_WORK_DAY)
                .anyMatch(value -> value == progressState)) {
            ObjectPref<AuthCredentialsModel> authCredentialsModelObjectPref = mApplicationPreferences.authCredentialsModel();
            authCredentialsModelObjectPref.getOptional()
                    .ifPresent(authCredentialsModel -> {
                        headerView.populate(authCredentialsModel.getProjectModel(),
                                mApplicationPreferences.promoterModel().get(),
                                mApplicationPreferences.scheduleModel().get());
                    });

        }
        if (progressState == ProgressState.COMPLETE_AUTH ||
                progressState == ProgressState.COMPLETE_PREPARATION) {
            mDrawer.addStickyFooterItem(DrawerFunctions.composeDrawerItemWithIcon(Item.LOGOUT, activity, R.drawable.ic_logout));
        }
        if (progressState == ProgressState.COMPLETE_GO_WORK) {
            mDrawer.getHeader().setClickable(true);
            mDrawer.getHeader().setOnClickListener(v -> {
                onFragmentSwitch.call(StepFragmentNew.create(-1));
            });
        }
        return mDrawer;
    }

    private List<IDrawerItem> getItems(Context context) {
        return Stream.of(Item.values())
                .filter(value -> value != Item.LOGOUT)
                .map(item -> DrawerFunctions.composeDrawerItem(item, context))
                .collect(Collectors.toList());
    }

    @Override
    public Drawer getDrawer() {
        return mDrawer;
    }

    @Override
    public boolean isDrawerOpened() {
        return mDrawer.isDrawerOpen();
    }

    @Override
    public void closeDrawer() {
        mDrawer.closeDrawer();
    }

    public enum Item {
        NEWS(R.string.drawer_title_news),
        MEDIA_FILES(R.string.drawer_media_files),
        STRUCTURE_CONTACT(R.string.drawer_structure_contact),
        TARGET_AUDIENCE(R.string.drawer_target_audience),
        SEND_ALL_DATA(R.string.drawer_send_all_data),
        HELP(R.string.drawer_help),
        LOGOUT(R.string.drawer_logout);

        @StringRes
        int title;

        Item(int title) {
            this.title = title;
        }

        public static Item fromTitle(int tag) {
            return Stream.of(values())
                    .filter(type -> type.title == tag)
                    .findFirst().get();
        }

        public
        @StringRes
        int getTitle() {
            return title;
        }
    }
}
