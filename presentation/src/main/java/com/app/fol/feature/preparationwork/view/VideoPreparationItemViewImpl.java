package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.app.fol.feature.preparationwork.adapter.FieldAction;
import com.app.fol.feature.preparationwork.adapter.FieldActionType;
import com.annimon.stream.Optional;

import butterknife.BindView;
import rx.functions.Action1;

public class VideoPreparationItemViewImpl extends BaseItemFieldViewImpl implements VideoPreparationItemView {
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.image)
    ImageView mImage;
    @BindView(R.id.button_take_audio)
    Button mButtonTakeVideo;
    @BindView(R.id.button_delete)
    Button mButtonDelete;
    @BindView(R.id.text_description)
    TextView mTextDescription;
    @BindView(R.id.image_container)
    RelativeLayout mImageContainer;
    private FieldModel mEntity;

    public VideoPreparationItemViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mEntity = entity;
        mTextTitle.setText(entity.getName());
        mTextDescription.setText(entity.getDescription());
        Optional.ofNullable(entity.getResult())
                .executeIfPresent(fullPath -> {
                    mImageContainer.setVisibility(VISIBLE);
                    entity.setEmptyField(false);
                    setActivated(false);
                    mImage.setImageBitmap(ThumbnailUtils.createVideoThumbnail(fullPath, MediaStore.Video.Thumbnails.MINI_KIND));
                    mButtonDelete.setVisibility(VISIBLE);
                })
                .executeIfAbsent(() -> {
                    mButtonDelete.setVisibility(GONE);
                    mImageContainer.setVisibility(GONE);
                });
    }


    @Override
    public void actionField(Action1<FieldAction> fieldActionType) {
        mButtonTakeVideo.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.TAKE_VIDEO)));
        mButtonDelete.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.REMOVE)));
        mImageContainer.setClickable(true);
        mImageContainer.setOnClickListener(v -> fieldActionType.call(new FieldAction(mEntity, FieldActionType.PLAY_VIDEO)));
    }
}
