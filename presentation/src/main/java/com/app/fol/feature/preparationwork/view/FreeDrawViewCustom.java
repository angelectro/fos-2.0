package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.app.fol.R;
import com.rm.freedrawview.FreeDrawView;

public class FreeDrawViewCustom extends FreeDrawView {
    public FreeDrawViewCustom(Context context) {
        super(context);
    }

    public FreeDrawViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FreeDrawViewCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getContext().getResources().getInteger(R.integer.orientation_is_horizontal) == 1) {
            WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            DisplayMetrics metrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(metrics);
            int height = getContext().getResources().getDimensionPixelSize(R.dimen.size_300);
            int width = metrics.heightPixels - getContext().getResources().getDimensionPixelSize(R.dimen.margin_38);
            this.setMeasuredDimension(width, height);
        }
    }
}
