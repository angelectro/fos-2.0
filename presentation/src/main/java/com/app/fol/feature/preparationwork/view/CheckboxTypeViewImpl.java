package com.app.fol.feature.preparationwork.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.TextView;

import com.app.domain.model.FieldModel;
import com.app.fol.R;
import com.annimon.stream.Optional;
import com.jakewharton.rxbinding.widget.RxCompoundButton;

import butterknife.BindView;


public class CheckboxTypeViewImpl extends BaseItemFieldViewImpl implements GroupCheckboxTypeView {
    @BindView(R.id.checkbox)
    protected CheckBox mCheckBox;
    @BindView(R.id.text_description)
    protected TextView mTextDescription;

    public CheckboxTypeViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldModel entity) {
        super.populate(entity);
        mCheckBox.setText(entity.getName());
        mTextDescription.setText(entity.getDescription());
        mCheckBox.setChecked(Optional.ofNullable(entity.getResult())
                .map(Boolean::parseBoolean)
                .orElse(false));
        RxCompoundButton.checkedChanges(mCheckBox)
                .subscribe(aBoolean -> entity.setResult(aBoolean.toString()));
    }

}
