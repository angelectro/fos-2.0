package com.app.fol.feature.auth.view;


import com.app.domain.model.CityModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.SupervisorModel;
import com.app.fol.base.view.BaseView;

import rx.functions.Action0;
import rx.functions.Action3;

public interface StaffSelectionView extends BaseView {
    void onBackPressedAction(Action0 action0);
    void selectCityAction(Action0 action0);
    void selectPromoterAction(Action0 action0);
    void selectSupervisorAction(Action0 action0);
    void showCity(CityModel cityModel);
    void showSupervisor(SupervisorModel supervisorModel);
    void showPromoter(PromoterModel promoterModel);
    void populate(CityModel cityModel,PromoterModel promoterModel, SupervisorModel supervisorModel);
    void nextAction(Action3<CityModel,PromoterModel,SupervisorModel> action3);
    void showBackground(String path);

    void updateDataAction(Action0 action0);
}
