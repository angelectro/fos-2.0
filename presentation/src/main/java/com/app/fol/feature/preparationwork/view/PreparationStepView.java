package com.app.fol.feature.preparationwork.view;

import com.app.domain.model.FieldModel;

import java.util.List;

import rx.functions.Action0;

public interface PreparationStepView extends FieldsBaseView {

    void populate(List<FieldModel> preparationFieldModels, int order, boolean isLast);
    void actionNext(Action0 action0);
    void actionUpdate(Action0 action0);
}
