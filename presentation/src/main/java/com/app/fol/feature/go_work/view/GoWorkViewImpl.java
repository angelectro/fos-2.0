package com.app.fol.feature.go_work.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;
import rx.functions.Action0;

public class GoWorkViewImpl extends RelativeLayoutBinded implements GoWorkView {
    @BindView(R.id.button_back)
    protected Button mButtonBack;
    @BindView(R.id.button_go)
    protected Button mButtonGo;

    public GoWorkViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void actionGoWork(Action0 action0) {
        mButtonGo.setOnClickListener(v -> action0.call());
    }

    @Override
    public void actionGoBack(Action0 action0) {
        mButtonBack.setOnClickListener(v -> action0.call());
    }
}
