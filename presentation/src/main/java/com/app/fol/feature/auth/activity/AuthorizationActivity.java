package com.app.fol.feature.auth.activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.app.fol.FosApplication;
import com.app.fol.R;
import com.app.fol.base.fragment.NavigationDrawerPresenter;
import com.app.fol.base.view.BaseActivity;
import com.app.fol.di.activity.ActivityModule;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.DaggerHomeComponent;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.auth.fragment.SelectProjectFragment;


public class AuthorizationActivity extends BaseActivity implements ComponentsHolder, NavigationDrawerPresenter {

    HomeComponent mAuthComponent;

    public static void create(Context context) {
        context.startActivity(new Intent(context, AuthorizationActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fragment fragmentByTag = getFragmentManager().findFragmentByTag(TAG_TASK_FRAGMENT);
        if (fragmentByTag == null) {
            Fragment fragment = SelectProjectFragment.create();
            /*switch (FosApplication.applicationComponent.applicationPreferences().progressState().get()) {
                case SELECTION_PROJECT:
                    fragment = SelectProjectFragment.createViewerFromFile();
                    break;
                case SELECTION_STAFF:
                    fragment = StaffSelectionFragment.createViewerFromFile();
                    break;
                case SELECTION_RETAIN_CHAIN:
                    fragment = PlaceSelectionFragment.createViewerFromFile();
                    break;
            }*/
            replaceFragment(fragment, false);
        }
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_base;
    }

    private void initializeComponents() {
        mAuthComponent = DaggerHomeComponent
                .builder().activityModule(new ActivityModule(this))
                .applicationComponent(FosApplication.applicationComponent)
                .build();
    }

    @Override
    public <T> T getComponent(Class<T> aClass) {
        if (TextUtils.equals(aClass.getSimpleName(), HomeComponent.class.getSimpleName())) {
            if (mAuthComponent == null)
                initializeComponents();
            return aClass.cast(mAuthComponent);
        }
        throw new IllegalArgumentException("no match component");
    }

    @Override
    public void initializeBehaviour(Toolbar toolbar) {

    }
}
