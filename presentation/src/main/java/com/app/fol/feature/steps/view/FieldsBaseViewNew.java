package com.app.fol.feature.steps.view;


import android.content.Intent;

import com.app.domain.model.FieldModel;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import rx.functions.Action1;

public interface FieldsBaseViewNew extends MvpView {
    void updateField(FieldModel position);

    void displayMarkErrorFields();

    void showFields(List<FieldModel> fieldModels);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void requestPermission(int requestCode, String... permissions);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void startIntent(Intent intent, int requestCode);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void startIntent(Intent intent);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showSignDialog(FieldModel fieldModel, Action1<String> stringAction1);
}
