package com.app.fol.feature.targetaudience.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.fol.R;
import com.app.fol.base.fragment.GenericToolbarPresenterFragment;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.targetaudience.view.TargetAudienceView;

import javax.inject.Inject;

public class TargetAudienceFragment extends GenericToolbarPresenterFragment<TargetAudienceView> {


    private static String ARGS_CONTENT = "ARGS_CONTENT";
    private static String ARGS_TITLE = "ARGS_TITLE";
    @Inject
    ApplicationPreferences mPreferences;

    public static Fragment create(String content, @StringRes int title) {
        Bundle bundle = new Bundle();
        bundle.putString(ARGS_CONTENT, content);
        bundle.putInt(ARGS_TITLE, title);
        TargetAudienceFragment targetAudienceFragment = new TargetAudienceFragment();
        targetAudienceFragment.setArguments(bundle);
        return targetAudienceFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_target_audience;
    }

    @Override
    protected void onView(TargetAudienceView view) {
        view.populate(getArguments().getString(ARGS_CONTENT));
    }

    @Override
    protected String toolbarTitle() {
        return getString(getArguments().getInt(ARGS_TITLE));
    }
}
