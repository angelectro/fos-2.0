package com.app.fol.feature.multimedia.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.app.domain.model.FieldResultModel;
import com.app.domain.usecase.GetMultimediaFileUseCase;
import com.app.fol.R;
import com.app.fol.base.FosDefaultSubscriber;
import com.app.fol.base.fragment.GenericToolbarStateFragment;
import com.app.fol.di.activity.ComponentsHolder;
import com.app.fol.di.fos.HomeComponent;
import com.app.fol.feature.imageviewer.ImageViewerActivity;
import com.app.fol.feature.multimedia.view.MultimediaFragmentView;
import com.app.fol.feature.videoplayer.VideoPlayerActivity;
import com.annimon.stream.Optional;

import java.util.List;

import javax.inject.Inject;

public class MultimediaFragment extends GenericToolbarStateFragment<MultimediaFragmentView> {


    @Inject
    GetMultimediaFileUseCase mGetMultimediaFileUseCase;
    @Inject
    Activity mActivity;
    boolean isFirst = true;
    List<FieldResultModel> mFieldResultModels;

    public static MultimediaFragment create() {
        return new MultimediaFragment();
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_multimedia;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ComponentsHolder) getActivity()).getComponent(HomeComponent.class).inject(this);
    }

    @Override
    protected void onView(MultimediaFragmentView view) {
        if (isFirst && mFieldResultModels == null)
            update();
        else
            view.populate(mFieldResultModels);
        isFirst = false;

        view.clickAction(fieldResultModel -> {
            switch (fieldResultModel.getType()) {
                case PHOTO:
                    ImageViewerActivity.createViewerFromFile(mActivity, fieldResultModel.getResult());
                    break;
                case VIDEO:
                    VideoPlayerActivity.create(mActivity, fieldResultModel.getResult());
                    break;
                case AUDIO:
                    break;
            }
        });
    }

    private void update() {
        mGetMultimediaFileUseCase.execute(FosDefaultSubscriber.<List<FieldResultModel>>onPositive(null, fieldResultModels -> {
            mFieldResultModels = fieldResultModels;
            Optional.ofNullable(viewImpl).ifPresent(view -> view.populate(mFieldResultModels));
        }));
    }

    @Override
    protected String toolbarTitle() {
        return getString(R.string.text_photo_and_video_reports);
    }
}
