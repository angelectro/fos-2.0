package com.app.fol.feature.drawer;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.view.View;

import com.app.fol.R;
import com.app.fol.utils.ScaleUtils;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class DrawerFunctions {


    @NonNull
    public static PrimaryDrawerItem composeDrawerItem(DrawerNavigationPresenterImpl.Item item, Context context) {
        return new PrimaryDrawerItem()
                .withIdentifier(item.title)
                .withName(context.getString(item.title).toUpperCase())
                .withTag(item.title)
                .withSelectable(false)
                .withTypeface(TypefaceUtils.load(context.getAssets(), "font/Roboto-Medium.ttf"))
                .withTextColorRes(R.color.nobel)
                .withSelectedTextColorRes(R.color.nobel);
    }

    @NonNull
    public static PrimaryDrawerItem composeDrawerItemWithIcon(DrawerNavigationPresenterImpl.Item item, Context context, @DrawableRes int icon) {
        return composeDrawerItem(item, context)
                .withIcon(icon);
    }

    public static DrawerBuilder buildUsingDefaultStyle(Activity activity) {
        return new DrawerBuilder()
                .withActivity(activity)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withShowDrawerOnFirstLaunch(true)
                .withOnDrawerNavigationListener((View clickedView) -> {
                    activity.onBackPressed();
                    return true;
                })
                .withDrawerWidthPx((int) (ScaleUtils.screenMetrics(activity).first * 0.84f))
                .withHeaderDivider(false)
                .withFooterDivider(false)
                .withStickyFooterShadow(false)
                .withStickyHeaderShadow(false);
    }
}
