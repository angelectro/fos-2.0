package com.app.fol.feature.drawer;


import com.app.domain.model.ProjectModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.ScheduleModel;

public interface DrawerHeaderView {

    void populate(ProjectModel projectModel, PromoterModel promoterModel, ScheduleModel retainChainsModel);
}
