package com.app.fol.feature.targetaudience.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;

public class TargetAudienceFragmentViewImpl extends RelativeLayoutBinded implements TargetAudienceView {
    @BindView(R.id.text_content)
    TextView mTextContent;

    public TargetAudienceFragmentViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(String text) {
        mTextContent.setText(text);
    }
}
