package com.app.fol.feature.selectionplace.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.domain.model.ScheduleModel;
import com.app.fol.R;
import com.app.fol.base.adapter.ListItemView;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;


public class ItemRetainChainsImpl extends RelativeLayoutBinded implements ListItemView<ScheduleModel> {
    @BindView(R.id.retail_chains)
    protected TextView mRetailChains;
    @BindView(R.id.text_address)
    protected TextView mTextAddress;
    @BindView(R.id.work_schedule)
    protected TextView mWorkSchedule;

    public ItemRetainChainsImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(ScheduleModel entity) {
        mRetailChains.setText(entity.getRetainChain());
        mTextAddress.setText(getContext().getString(R.string.text_pattern_address, entity.getAddress()));
        mWorkSchedule.setText(getContext().getString(R.string.text_pattern_schedule, entity.getBeginAt(), entity.getEndAt()));
    }
}
