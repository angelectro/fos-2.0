package com.app.fol.feature.preparationwork.view;

import com.app.domain.model.FieldModel;
import com.app.fol.base.adapter.ListItemView;
import com.app.fol.feature.preparationwork.adapter.FieldAction;

import rx.functions.Action1;

public interface QrTypeItemView extends ListItemView<FieldModel> {
    void actionField(Action1<FieldAction> fieldActionType);
}
