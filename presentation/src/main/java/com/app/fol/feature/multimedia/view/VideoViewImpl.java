package com.app.fol.feature.multimedia.view;

import android.content.Context;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.FieldStatus;
import com.app.fol.R;
import com.app.fol.base.adapter.ListItemView;
import com.annimon.stream.Stream;

import butterknife.BindView;

public class VideoViewImpl extends SquareFrameLayout implements ListItemView<FieldResultModel> {
    @BindView(R.id.image_view)
    protected ImageView mImageView;
    @BindView(R.id.text_status)
    protected TextView mTextStatus;

    public VideoViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void populate(FieldResultModel entity) {
        mImageView.setImageBitmap(ThumbnailUtils.createVideoThumbnail(entity.getResult(), MediaStore.Video.Thumbnails.MINI_KIND));
        mTextStatus.setText(getContext().getString(Stream.of(FieldStatus.SENT, FieldStatus.UPLOADED).anyMatch(value -> value == entity.getStatus()) ? R.string.sent : R.string.not_send));
    }
}
