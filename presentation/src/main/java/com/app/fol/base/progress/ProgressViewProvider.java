package com.app.fol.base.progress;

import com.trello.rxlifecycle2.android.FragmentEvent;

import io.reactivex.Observable;


public interface ProgressViewProvider {
    ProgressView getProgress();
    Observable<FragmentEvent> getEventObservable();
}
