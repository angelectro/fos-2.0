package com.app.fol.base.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.app.fol.R;
import com.app.fol.base.butterknife.RelativeLayoutBinded;

import butterknife.BindView;
import rx.functions.Action0;


public class NavigationViewImpl extends RelativeLayoutBinded implements NavigationView {
    @BindView(R.id.button_back)
    Button mButtonBack;
    @BindView(R.id.button_reload)
    Button mButtonReload;

    public NavigationViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setVisibleUpdateButton(boolean isVisible) {
        mButtonReload.setVisibility(isVisible ? VISIBLE : GONE);
    }

    @Override
    public void actionButtonBack(Action0 action0) {
        mButtonBack.setOnClickListener(v -> action0.call());
    }

    @Override
    public void actionButtonReload(Action0 action0) {
        mButtonReload.setOnClickListener(v -> action0.call());
    }
}
