package com.app.fol.base.adapter;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

public class GenericAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected final List<T> entities;
    protected final
    @LayoutRes
    int layoutResId;
    protected PublishSubject<T> itemClickObservable = PublishSubject.create();

    public GenericAdapter(List<T> entities, int layoutResId) {
        this.entities = entities;
        this.layoutResId = layoutResId;
    }

    public GenericAdapter(int layoutResId) {
        this(new ArrayList<T>(), layoutResId);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AbsTypedViewHolder<T>(LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(v -> itemClickObservable.onNext(entities.get(position)));

        if (holder instanceof AbsTypedViewHolder) {
            ((AbsTypedViewHolder) holder).populate(entities.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public List<T> getEntities() {
        return entities;
    }


    public Observable<T> getItemClickObservable() {
        return itemClickObservable;
    }

    public void attach(List<T> xs) {
        entities.addAll(xs);
        notifyDataSetChanged();
    }

    public void attach(List<T> xs, int position) {
        entities.addAll(position, xs);
        notifyDataSetChanged();
    }

    public void replace(List<T> xs) {
        entities.clear();
        entities.addAll(xs);
        notifyDataSetChanged();
    }
}
