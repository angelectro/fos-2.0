package com.app.fol.base.butterknife;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;

public class RelativeLayoutBinded extends RelativeLayout {
    public RelativeLayoutBinded(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (!isInEditMode())
            ButterKnife.bind(this);
    }
}
