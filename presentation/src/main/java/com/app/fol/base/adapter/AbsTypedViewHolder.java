package com.app.fol.base.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class AbsTypedViewHolder<T> extends RecyclerView.ViewHolder {

    public final ListItemView<T> listItemView;

    public AbsTypedViewHolder(View itemView) {
        super(itemView);
        this.listItemView = (ListItemView<T>) itemView;
    }

    public void populate(T entity) {
        listItemView.populate(entity);
    }
}
