package com.app.fol.base.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/**
 * Created by DemaWork on 24.04.2017.
 */

public class IconTabLayout extends TabLayout implements ViewPager.OnPageChangeListener {

    private int[] icons;
    private int[] iconsActive;
    private int mCurrent;

    public IconTabLayout(Context context) {
        super(context);
    }

    public IconTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IconTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setupWithViewPager(@Nullable ViewPager viewPager) {
        assert viewPager != null;
        viewPager.addOnPageChangeListener(this);
        super.setupWithViewPager(viewPager);
        initTabs();
    }

    private void initTabs() {
        if (icons != null && iconsActive != null) {
            getTabAt(mCurrent).setIcon(iconsActive[mCurrent]);
            for (int i = 1; i < icons.length; i++) {
                getTabAt(i).setIcon(icons[i]);
            }
        }
    }

    public void setIcons(int[] icons, int[] iconsActive, int current) {
        this.icons = icons;
        this.iconsActive = iconsActive;
        mCurrent = current;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < icons.length; i++) {
            if (position == i) {
                getTabAt(i).setIcon(iconsActive[i]);
            } else {
                getTabAt(i).setIcon(icons[i]);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
