package com.app.fol.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.app.domain.usecase.internal.CompositeUseCase;
import com.app.domain.usecase.internal.UseCase;
import com.app.domain.utils.EventBus;
import com.app.fol.base.progress.ProgressView;
import com.app.fol.base.progress.ProgressViewProvider;
import com.annimon.stream.Optional;
import com.trello.rxlifecycle2.components.RxFragment;

public abstract class BaseFragment extends RxFragment {

    private CompositeUseCase compositeUseCase;
    private Optional<Class> connectedCallbacksClass = Optional.empty();

    protected <T extends UseCase> T addUseCase(UseCase useCase) {
        if (compositeUseCase == null) {
            compositeUseCase = new CompositeUseCase();
        }
        compositeUseCase.add(useCase);
        return (T) useCase;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    protected void addUseCases(UseCase... useCases) {
        for (UseCase useCase : useCases) {
            addUseCase(useCase);
        }
    }

    protected void connectCallbacks(Class cls) {
        this.connectedCallbacksClass = Optional.of(cls);
    }

    @Override
    public void onResume() {
        super.onResume();
        connectedCallbacksClass.ifPresent(clz -> EventBus.getInstance().register(clz, this));
    }

    @Override
    public void onPause() {
        super.onPause();
        connectedCallbacksClass.ifPresent(clz -> EventBus.getInstance().unregister(clz));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Optional.ofNullable(compositeUseCase)
                .ifPresent(CompositeUseCase::clear);
    }

    protected @Nullable
    ProgressView progressView() {
        return Optional.ofNullable(getActivity())
                .filter(value -> value instanceof ProgressViewProvider)
                .map(value -> ((ProgressViewProvider) value))
                .map(ProgressViewProvider::getProgress).orElse(null);
    }
}
