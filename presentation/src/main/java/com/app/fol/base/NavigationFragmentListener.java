package com.app.fol.base;

import android.app.Fragment;

/**
 * Created by DemaWork on 27.04.2017.
 */

public interface NavigationFragmentListener {
    public void replaceFragment(Fragment fragment, boolean addToBackstack);
    public void replaceFragment(Fragment fragment, boolean addToBackstack, int container);
}
