package com.app.fol.base.view;

import rx.functions.Action0;

/**
 * Created by Загит Талипов on 25.07.2017.
 */

public interface NavigationView {
    void setVisibleUpdateButton(boolean isVisible);
    void actionButtonBack(Action0 action0);
    void actionButtonReload(Action0 action0);
}
