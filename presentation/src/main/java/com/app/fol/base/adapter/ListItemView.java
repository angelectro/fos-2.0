package com.app.fol.base.adapter;

public interface ListItemView<T> {
    void populate(T entity);
}
