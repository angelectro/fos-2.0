package com.app.fol.base.view;

import android.support.v7.widget.Toolbar;

public interface ToolbarProvider {
    Toolbar provideToolbar();
}
