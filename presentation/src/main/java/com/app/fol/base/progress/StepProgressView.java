package com.app.fol.base.progress;

import android.app.Activity;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.fol.R;
import com.annimon.stream.Optional;

import java.lang.ref.WeakReference;

import rx.functions.Action0;

public class StepProgressView {
    private final WeakReference<MaterialDialog> dialog;
    private final WeakReference<Activity> activity;
    private WeakReference<MaterialDialog> errorDialog;

    public StepProgressView(Activity activity, Action0 actionCancel) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(activity)
                .cancelable(false)
                .title(R.string.text_sync_title)
                .content(R.string.wait)
                .widgetColorRes(R.color.accent)
                .progress(true, 0);
        Optional.ofNullable(actionCancel)
                .ifPresent(action0 -> {
                    builder.positiveText(R.string.cancel);
                    builder.onPositive((dialog1, which) -> actionCancel.call());
                });
        MaterialDialog d = builder.build();
        this.dialog = new WeakReference<>(d);
        this.activity = new WeakReference<>(activity);
    }

    public StepProgressView(Activity activity) {
        this(activity, null);
    }

    public void showProgressBar(@StringRes int text) {
        Optional.ofNullable(dialog.get())
                .executeIfPresent(materialDialog -> materialDialog.setTitle(text))
                .ifPresent(MaterialDialog::show);
        Optional.ofNullable(errorDialog)
                .map(WeakReference::get)
                .ifPresent(MaterialDialog::dismiss);
    }


    public void hideProgressBar() {
        Optional.ofNullable(dialog.get()).ifPresent(MaterialDialog::hide);
    }


    public void showError(String message, Action0 action0) {
        showError(message, action0, null);
    }

    public void showError(String message, Action0 actionPositive, Action0 actionNegative) {
        hideProgressBar();
        Optional.ofNullable(activity.get())
                .ifPresent(activity -> {
                    MaterialDialog.Builder builder = new MaterialDialog.Builder(activity)
                            .content(message)
                            .cancelable(false)
                            .positiveText(R.string.repeat)
                            .onPositive((dialog1, which) -> actionPositive.call());

                    Optional.ofNullable(actionNegative)
                            .ifPresent(action0 -> {
                                builder.negativeText(R.string.cancel)
                                        .onNegative((dialog1, which) -> actionNegative.call());
                            });

                    errorDialog = new WeakReference<>(builder.show());
                });
    }

    public void dismiss() {
        Optional.ofNullable(dialog.get()).ifPresent(MaterialDialog::dismiss);
        closeError();
    }

    public void closeError() {
        Optional.ofNullable(errorDialog)
                .map(WeakReference::get)
                .ifPresent(MaterialDialog::dismiss);
    }
}