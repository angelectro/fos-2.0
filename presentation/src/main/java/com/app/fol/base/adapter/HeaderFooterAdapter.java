package com.app.fol.base.adapter;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

public class HeaderFooterAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int HEADER = 100;
    public static final int FOOTER = 200;
    public static final int ITEM = 3;

    private final int layoutResId;
    private final View headerView;
    private final View footerView;
    protected List<T> entities;
    private Action1<T> clickListener;


    public HeaderFooterAdapter(@LayoutRes int layoutResId, View headerView, View footerView) {
        this.layoutResId = layoutResId;
        this.headerView = headerView;
        this.footerView = footerView;
        entities = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? HEADER
                : position == getItemCount() - 1 ? FOOTER : ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER: return new ViewHolder(headerView);
            case FOOTER: return new ViewHolder(footerView);
            default: return new AbsTypedViewHolder<T>(LayoutInflater.from(
                    parent.getContext()).inflate(layoutResId, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AbsTypedViewHolder) {
            ((AbsTypedViewHolder) holder).populate(getItem(position));

            if (clickListener != null) {
                holder.itemView.setOnClickListener(v -> clickListener.call(getItem(position)));
            }
        }
    }


    protected T getItem(int position) {
        return entities.get(position - 1);
    }

    @Override
    public int getItemCount() {
        return entities.size() + 2;
    }

    public void setEntities(List<T> entities) {
        this.entities = entities;
        notifyDataSetChanged();
    }

    public List<T> getEntities() {
        return entities;
    }

    public void setOnItemClickListener(Action1<T> clickListener) {
        this.clickListener = clickListener;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
