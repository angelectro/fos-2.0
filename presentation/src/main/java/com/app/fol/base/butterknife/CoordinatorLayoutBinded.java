package com.app.fol.base.butterknife;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;

import butterknife.ButterKnife;

public class CoordinatorLayoutBinded extends CoordinatorLayout {

    public CoordinatorLayoutBinded(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) ButterKnife.bind(this);
    }
}
