package com.app.fol.base.butterknife;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import butterknife.ButterKnife;

public class ScrollViewBinded extends ScrollView {
    public ScrollViewBinded(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (!isInEditMode())
            ButterKnife.bind(this);
    }
}
