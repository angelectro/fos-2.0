package com.app.fol.base.progress;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.app.fol.R;
import com.pnikosis.materialishprogress.ProgressWheel;


import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgressViewImpl extends FrameLayout implements ProgressView {

    @BindView(R.id.progressBar)
    protected ProgressWheel progressBar;
    @BindView(R.id.progressContainer)
    protected FrameLayout contentContainer;

    public ProgressViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (!isInEditMode()) {
            ButterKnife.bind(this);
        }
    }

    @Override
    public void showProgressBar() {
        contentContainer.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        contentContainer.setVisibility(GONE);
    }

    @Override
    public void showError(String error) {
        AlertUtils.showSimpleDialog(getContext(), error);
    }
}
