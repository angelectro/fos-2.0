package com.app.fol.base.butterknife;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;

import butterknife.ButterKnife;


public class NestedScrollViewBinded extends NestedScrollView {
    public NestedScrollViewBinded(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (!isInEditMode())
            ButterKnife.bind(this);
    }
}
