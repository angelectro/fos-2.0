package com.app.fol.base.fragment;


import android.support.v7.widget.Toolbar;

public interface NavigationDrawerPresenter {
    void initializeBehaviour(Toolbar toolbar);
}
