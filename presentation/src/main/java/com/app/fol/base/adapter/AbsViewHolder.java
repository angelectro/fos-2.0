package com.app.fol.base.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.View;

public class AbsViewHolder<V extends View> extends RecyclerView.ViewHolder {
    public final V itemView;

    public AbsViewHolder(View itemView) {
        super(itemView);
        this.itemView = ((V) itemView);
    }
}
