package com.app.fol.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.data.errorhandlers.FosErrorModelHandler;
import com.app.fol.base.progress.ProgressViewProvider;
import com.annimon.stream.function.Consumer;
import java.lang.ref.WeakReference;

import com.app.domain.model.ErrorModel;

public class FosDefaultSubscriber<T> extends PresentationDefaultSubscriber<T> {
    public FosDefaultSubscriber(ProgressViewProvider progressView) {
        super(progressView, new FosErrorModelHandler());
    }

    public FosDefaultSubscriber(@NonNull WeakReference<ProgressViewProvider> progressBarView) {
        super(progressBarView, new FosErrorModelHandler());
    }

    public FosDefaultSubscriber() {
        super(new FosErrorModelHandler());
    }

    public static <R> FosDefaultSubscriber<R> onPositive(Consumer<R> positiveConsumer) {
        return new FosDefaultSubscriber<R>() {
            @Override
            public void onNext(R r) {
                super.onNext(r);
                positiveConsumer.accept(r);
            }
        };
    }

    public static <R> FosDefaultSubscriber<R> onPositive(@Nullable ProgressViewProvider progressViewProvider, Consumer<R> positiveConsumer) {
        return new FosDefaultSubscriber<R>(progressViewProvider) {
            @Override
            public void onNext(R r) {
                    super.onNext(r);
                positiveConsumer.accept(r);
            }
        };
    }

    public static <R> FosDefaultSubscriber<R> onPositiveNegative(@Nullable ProgressViewProvider progressViewProvider, Consumer<R> positiveConsumer, Consumer<ErrorModel> negativeConsumer) {
        return new FosDefaultSubscriber<R>(progressViewProvider) {
            @Override
            public void onNext(R r) {
                super.onNext(r);
                positiveConsumer.accept(r);
            }

            @Override
            protected void onError(ErrorModel errorTitle) {
                negativeConsumer.accept(errorTitle);
            }
        };
    }
}