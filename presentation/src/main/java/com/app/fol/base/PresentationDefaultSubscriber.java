package com.app.fol.base;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import com.app.domain.model.ErrorModel;
import com.app.domain.usecase.internal.AbstractPresentationSubscriber;
import com.app.domain.usecase.internal.ErrorModelHandler;
import com.app.fol.BuildConfig;
import com.app.fol.FosApplication;
import com.app.fol.R;
import com.app.fol.base.progress.ProgressViewProvider;
import com.annimon.stream.Optional;
import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.Logger;
import com.trello.rxlifecycle2.android.FragmentEvent;

import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.UnknownHostException;

import io.reactivex.disposables.Disposable;

public class PresentationDefaultSubscriber<T> extends AbstractPresentationSubscriber<T> {

    private WeakReference<ProgressViewProvider> progressBarView;
    private ErrorModelHandler mErrorModelHandler;
    private Disposable mDisposableFragmentLifecycle;

    public PresentationDefaultSubscriber(ProgressViewProvider progressViewProvider, ErrorModelHandler errorModelHandler) {
        this.progressBarView = new WeakReference<>(progressViewProvider);
        mErrorModelHandler = errorModelHandler;
        initSubscribeLifecycle();
    }

    public PresentationDefaultSubscriber(@NonNull WeakReference<ProgressViewProvider> progressBarView, ErrorModelHandler errorModelHandler) {
        this.progressBarView = progressBarView;
        mErrorModelHandler = errorModelHandler;
        initSubscribeLifecycle();
    }

    public PresentationDefaultSubscriber(ErrorModelHandler errorModelHandler) {
        mErrorModelHandler = errorModelHandler;
        progressBarView = new WeakReference<>(null);
    }

    private void initSubscribeLifecycle() {
        Optional.ofNullable(progressBarView.get())
                .ifPresent(progressViewProvider -> {
                    mDisposableFragmentLifecycle = progressViewProvider.getEventObservable().subscribe(fragmentEvent -> {
                        if (fragmentEvent == FragmentEvent.RESUME && !isDisposed()) {
                            onStart();
                        }
                    });
                });
    }

    public WeakReference<ProgressViewProvider> getProgressBarView() {
        return progressBarView;
    }

    @CallSuper
    @Override
    public void onComplete() {
        super.onComplete();
        hideProgress();
        Optional.ofNullable(mDisposableFragmentLifecycle)
                .ifPresent(Disposable::dispose);
    }

    @Override
    public void onNext(T t) {
        super.onNext(t);
        Optional.ofNullable(mDisposableFragmentLifecycle)
                .ifPresent(Disposable::dispose);
        hideProgress();
    }


    @Override
    protected void handleError(Throwable throwable) {
        ErrorModel errorEntity = mErrorModelHandler.fetchError(throwable).orElseGet(() -> {
            String error = throwable instanceof UnknownHostException || throwable instanceof ConnectException
                    ? FosApplication.applicationContext.getString(R.string.error_not_connected)
                    : FosApplication.applicationContext.getString(R.string.general_error);
            return new ErrorModel(error, throwable);
        });

        onError(errorEntity);
        Optional.ofNullable(mDisposableFragmentLifecycle)
                .ifPresent(Disposable::dispose);
        hideProgress();
        throwable.printStackTrace();
        Logger.t("PresentationDefaultSubscriber", 6).e(throwable, throwable.getStackTrace()[0].toString());
    }

    protected final void hideProgress() {
        Optional.ofNullable(progressBarView.get())
                .ifPresent(progressViewProvider -> progressViewProvider.getProgress().hideProgressBar());
    }

    @Override
    protected void onError(ErrorModel errorTitle) {
        hideProgress();
        Optional.ofNullable(progressBarView.get())
                .ifPresent(view -> view.getProgress().showError(errorTitle.getMessage()));
    }

    @Override
    protected final void log(Throwable throwable) {
        if (BuildConfig.CRASHLYTICS_ENABLED) {
            Crashlytics.getInstance().core.logException(throwable);
        }
    }

    @CallSuper
    @Override
    public void onStart() {
        Optional.ofNullable(progressBarView.get())
                .ifPresent(progressViewProvider -> progressViewProvider.getProgress().showProgressBar());
    }

}
