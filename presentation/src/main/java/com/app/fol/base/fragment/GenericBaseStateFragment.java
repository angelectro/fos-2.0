package com.app.fol.base.fragment;

import android.os.Bundle;
import android.view.View;

import com.app.fol.R;
import com.app.fol.base.progress.ProgressView;
import com.app.fol.base.progress.ProgressViewProvider;
import com.app.fol.base.view.BaseView;
import com.annimon.stream.Optional;
import com.trello.rxlifecycle2.android.FragmentEvent;

import butterknife.ButterKnife;
import io.reactivex.Observable;


public abstract class GenericBaseStateFragment<T extends BaseView> extends GenericBasePresenterFragment<T> implements ProgressViewProvider {

    private ProgressView mProgressView;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        viewImpl.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        Optional.ofNullable(savedInstanceState)
                .ifPresent(viewImpl::onViewStateRestored);
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public ProgressView getProgress() {
        return mProgressView = Optional.ofNullable(mProgressView).orElse(ButterKnife.findById((View) viewImpl, R.id.progressContainer));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mProgressView = null;
    }

    @Override
    public Observable<FragmentEvent> getEventObservable() {
        return lifecycle();
    }
}
