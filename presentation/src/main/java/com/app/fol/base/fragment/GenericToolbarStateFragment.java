package com.app.fol.base.fragment;

import android.os.Bundle;

import com.app.fol.base.view.BaseView;
import com.annimon.stream.Optional;



public abstract class GenericToolbarStateFragment<T extends BaseView> extends GenericToolbarPresenterFragment<T> {

    @Override
    public void onSaveInstanceState(Bundle outState) {
        viewImpl.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        Optional.ofNullable(savedInstanceState)
                .ifPresent(viewImpl::onViewStateRestored);
        super.onViewStateRestored(savedInstanceState);
    }
}
