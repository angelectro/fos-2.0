package com.app.fol.base.view;


import android.os.Bundle;

public interface BaseView {
    void onSaveInstanceState(Bundle outState);
    void onViewStateRestored(Bundle savedInstanceState);
}
