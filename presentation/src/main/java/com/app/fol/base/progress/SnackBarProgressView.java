package com.app.fol.base.progress;


import android.view.View;

import java.lang.ref.WeakReference;

public class SnackBarProgressView implements ProgressView {

    private final WeakReference<View> viewReference;

    public SnackBarProgressView(View view) {
        this.viewReference = new WeakReference<>(view);
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void showError(String error) {
        if (viewReference.get() == null) {
            return;
        }

        AlertUtils.showSnackBar(viewReference.get(), error);
    }
}
