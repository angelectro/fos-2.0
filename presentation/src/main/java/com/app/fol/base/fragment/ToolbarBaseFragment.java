package com.app.fol.base.fragment;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.fol.R;
import com.mikepenz.materialdrawer.util.KeyboardUtil;

import butterknife.ButterKnife;

public abstract class ToolbarBaseFragment extends MvpBaseFragment {

    private TextView mToolbarTitle;

    protected void replaceFragment(android.app.Fragment instance, boolean addToBackstack) {
        KeyboardUtil.hideKeyboard(getActivity());
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        if (addToBackstack) {
            transaction.setCustomAnimations(
                    R.animator.slide_in_left, // 2 appearance
                    R.animator.slide_out_right, // 1 disappearance
                    R.animator.slide_in_right, // 1 appearance
                    R.animator.slide_out_left); // 2 disappearance
        }
        transaction.replace(R.id.fragment_place, instance);
        if (addToBackstack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() instanceof NavigationDrawerPresenter) {
            Toolbar toolbar = getToolbar(view);
            ((NavigationDrawerPresenter) getActivity()).initializeBehaviour(toolbar);
            mToolbarTitle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
            if (mToolbarTitle != null)
                mToolbarTitle
                        .setText(toolbarTitle());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(provideLayout(), container, false);
    }

    protected abstract String toolbarTitle();

    protected void updateTitle(String name) {
        mToolbarTitle.setText(name);
    }

    protected abstract
    @LayoutRes
    int provideLayout();


    private Toolbar getToolbar(View view) {
        return ButterKnife.findById(view, R.id.toolbar);
    }

}
