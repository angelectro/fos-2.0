package com.app.fol.base.butterknife;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import butterknife.ButterKnife;

public class FrameLayoutBinded extends FrameLayout {
    public FrameLayoutBinded(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) ButterKnife.bind(this);
    }
}
