package com.app.fol.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.app.fol.R;

import butterknife.ButterKnife;

public abstract class GenericToolbarPresenterFragment<T> extends GenericBasePresenterFragment<T> {

    private TextView mToolbarTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() instanceof NavigationDrawerPresenter) {
            Toolbar toolbar = getToolbar(view);
            ((NavigationDrawerPresenter) getActivity()).initializeBehaviour(toolbar);
            mToolbarTitle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
            if (mToolbarTitle != null)
                mToolbarTitle
                        .setText(toolbarTitle());
        }
    }

    protected abstract String toolbarTitle();

    public void updateTitle(String title) {
        mToolbarTitle.setText(title);
    }

    private Toolbar getToolbar(View view) {
        return ButterKnife.findById(view, R.id.toolbar);
    }
}