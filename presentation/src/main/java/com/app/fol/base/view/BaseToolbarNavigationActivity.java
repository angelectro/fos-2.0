package com.app.fol.base.view;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.app.fol.base.fragment.NavigationDrawerPresenter;
import com.app.fol.feature.drawer.DrawerNavigationPresenter;
import com.app.fol.feature.drawer.DrawerNavigationPresenterImpl;
import com.app.fol.feature.home.activity.HomeActivity;
import com.app.fol.feature.steps.fragment.StepFragmentNew;
import com.mikepenz.materialdrawer.Drawer;

public abstract class BaseToolbarNavigationActivity extends BaseActivity implements NavigationDrawerPresenter {

    private DrawerNavigationPresenter drawerNavigation;
    private int backStackEntryCount = 0;

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawerNavigation = new DrawerNavigationPresenterImpl();
        drawerNavigation.initialize(this, fragment -> {
            if (fragment instanceof StepFragmentNew) {
                if (!(getFragmentManager().findFragmentByTag(TAG_TASK_FRAGMENT) instanceof StepFragmentNew)) {
                    HomeActivity.create(this);
                } else {
                    drawerNavigation.closeDrawer();
                }
            } else {
                replaceFragment(fragment, true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerNavigation.isDrawerOpened())
            drawerNavigation.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    public void initializeBehaviour(Toolbar toolbar) {
        new Handler().postDelayed(() -> {
            Drawer drawer = drawerNavigation.getDrawer();

            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            drawer.setToolbar(this, toolbar, true);


            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            drawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
            drawer.getActionBarDrawerToggle().syncState();

            toolbar.setNavigationOnClickListener(v -> {
                if (backStackEntryCount > 0)
                    getFragmentManager().popBackStack();
                else
                    drawer.openDrawer();
            });
        }, 100);
    }

}
