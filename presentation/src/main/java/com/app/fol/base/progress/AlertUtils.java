package com.app.fol.base.progress;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.fol.R;

import rx.functions.Action0;

import static android.content.Context.NOTIFICATION_SERVICE;

public class AlertUtils {
    public static void showSnackBar(View v, @StringRes int text) {
        showSnackBar(v, text, Snackbar.LENGTH_SHORT);
    }

    public static void showSnackBar(View v, @StringRes int text, int length) {
        if (v == null) return;

        Snackbar snackBar = Snackbar.make(v, text, length);
        snackBar.show();
    }

    public static void showSnackBar(View v, String text) {
        showSnackBar(v, text, Snackbar.LENGTH_SHORT);
    }

    public static void showSnackBar(View v, String text, int length) {
        if (v == null) return;

        Snackbar snackBar = Snackbar.make(v, text, length);
        snackBar.show();
    }

    public static void showSnackBarIndefinite(View v, @StringRes int text) {
        if (v == null) return;

        Snackbar snackBar = Snackbar.make(v, text, Snackbar.LENGTH_SHORT);
        snackBar.show();
    }

    public static MaterialDialog showSimpleDialog(Context activity, @StringRes int text) {
        return showSimpleDialog(activity, activity.getString(text), null);
    }

    public static MaterialDialog  showSimpleDialog(Context activity, String text) {
       return showSimpleDialog(activity, text, null);
    }

    public static void showSimpleDialog(Context activity, @StringRes int text, @Nullable Action0 onSubmit) {
        showSimpleDialog(activity, activity.getString(text), onSubmit);
    }

    public static MaterialDialog showSimpleDialog(Context activity, String text, @Nullable Action0 onSubmit) {
          return new MaterialDialog.Builder(activity)
                    .content(text)
                    .positiveText(R.string.ok)
                    .onPositive((dialog, which) -> {
                        if (onSubmit != null) onSubmit.call();
                    })
                    .show();
    }

    public static void showSimpleDialog(Context activity, String title, String text, @Nullable Action0 onSubmit) {
        if (activity instanceof Activity)
            new MaterialDialog.Builder(activity)
                    .title(title)
                    .content(text)
                    .positiveText(R.string.ok)
                    .onPositive((dialog, which) -> {
                        if (onSubmit != null) onSubmit.call();
                    })
                    .show();
    }

    public static void showSimpleDialog(Context activity, @StringRes int title, String text, @Nullable Action0 onSubmit) {
        showSimpleDialog(activity, activity.getString(title), text, onSubmit);
    }


    public static void showNotification(Context context, String text, PendingIntent pendingIntent) {
        Notification n = new Notification.Builder(context)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(text)
//                .setSmallIcon(R.drawable.ic_notification)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setStyle(new Notification.BigTextStyle().bigText(text))
                .build();


        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, n);
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void showToast(Context context, @StringRes int text) {
        showToast(context, context.getString(text));
    }

}
