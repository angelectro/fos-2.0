package com.app.fol.base.view;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.app.fol.FosApplication;
import com.app.fol.R;
import com.app.fol.base.NavigationFragmentListener;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity implements NavigationFragmentListener {

    protected static final String TAG_TASK_FRAGMENT = "task_fragment";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void replaceFragment(Fragment instance, boolean addToBackstack) {
        replaceFragment(instance, addToBackstack, R.id.fragment_place);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayout());
    }


    protected abstract int setLayout();

    @Override
    public void replaceFragment(Fragment instance, boolean addToBackstack, int container) {
        if (instance == null) {
            return;
        }
        FosApplication.applicationComponent.applicationPreferences();
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        if (addToBackstack) {
            transaction.setCustomAnimations(
                    R.animator.slide_in_left, // 2 appearance
                    R.animator.slide_out_right, // 1 disappearance
                    R.animator.slide_in_right, // 1 appearance
                    R.animator.slide_out_left); // 2 disappearance
        }
        transaction.replace(container, instance, TAG_TASK_FRAGMENT);
        if (addToBackstack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }
}
