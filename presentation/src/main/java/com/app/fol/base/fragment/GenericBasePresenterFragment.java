package com.app.fol.base.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.fol.R;
import com.mikepenz.materialdrawer.util.KeyboardUtil;


public abstract class GenericBasePresenterFragment<T> extends BaseFragment {

    protected T viewImpl;

    @CallSuper
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.viewImpl = ((T) view);
        super.onViewCreated(view, savedInstanceState);
        onView(this.viewImpl);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(provideLayout(), container, false);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    protected abstract
    @LayoutRes
    int provideLayout();

    protected abstract void onView(T view);

    protected void replaceFragment(Fragment instance, boolean addToBackstack) {
        KeyboardUtil.hideKeyboard(getActivity());
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        if (addToBackstack) {
            transaction.setCustomAnimations(
                    R.animator.slide_in_left, // 2 appearance
                    R.animator.slide_out_right, // 1 disappearance
                    R.animator.slide_in_right, // 1 appearance
                    R.animator.slide_out_left); // 2 disappearance
        }
        transaction.replace(R.id.fragment_place, instance);
        if (addToBackstack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

}
