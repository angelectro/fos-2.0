package com.app.fol.base.view;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

import butterknife.ButterKnife;

public class CardViewBinded extends CardView {
    public CardViewBinded(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (!isInEditMode()) {
            ButterKnife.bind(this);
        }
    }
}
