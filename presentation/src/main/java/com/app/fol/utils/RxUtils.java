package com.app.fol.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import io.reactivex.Observable;
import rx.functions.Action0;


public class RxUtils {

    public static IfOrElse ifOrElse(boolean b) {
        return new IfOrElse(b);
    }

    public static Observable<String> textChanged(TextView textView) {
        return Observable.create(e -> {
            TextWatcher watcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    e.onNext(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
            e.setCancellable(() -> textView.removeTextChangedListener(watcher));
            textView.addTextChangedListener(watcher);
        });
    }

    public static class IfOrElse {
        private boolean b;

        private IfOrElse(boolean b) {
            this.b = b;
        }

        public IfOrElse positive(Action0 action0) {
            if (b) action0.call();
            return this;
        }

        public IfOrElse negative(Action0 action0) {
            if (!b) action0.call();
            return this;
        }
    }
}
