package com.app.fol.utils;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtils {

    public static final String FORMAT_JPEG = "jpg";
    public static final String FORMAT_MP4 = "mp4";
    public static final String FORMAT_WAV = "wav";

    public static File createFile(@NonNull Context context, @NonNull String format, int projectId) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = format.toUpperCase() + "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(String.valueOf(projectId));
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                "." + format,         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public static boolean removeFile(@NonNull String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
            return true;
        }
        return true;
    }

}
