package com.app.fol.utils.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.fol.R;


public class RecyclerItemSpacingDecoration extends RecyclerView.ItemDecoration {
    private final int itemSpacing;
    private final int headerSpacing;
    private final int footerSpacing;

    private RecyclerItemSpacingDecoration(int itemSpacing, int headerSpacing, int footerSpacing) {
        this.itemSpacing = itemSpacing;
        this.headerSpacing = headerSpacing;
        this.footerSpacing = footerSpacing;
    }

    public RecyclerItemSpacingDecoration(Context context) {
        this(context.getResources().getDimensionPixelSize(R.dimen.margin_8), 0, 0);
    }

    public RecyclerItemSpacingDecoration(Context context, @DimenRes int itemSpacing) {
        this(context.getResources().getDimensionPixelSize(itemSpacing), 0, 0);
    }

    public RecyclerItemSpacingDecoration(Context context, @DimenRes int itemSpacing,
                                         @DimenRes int headerSpacing, @DimenRes int footerSpacing) {
        this(context.getResources().getDimensionPixelSize(itemSpacing),
                context.getResources().getDimensionPixelSize(headerSpacing),
                context.getResources().getDimensionPixelSize(footerSpacing));
    }

    public RecyclerItemSpacingDecoration(Context context,
                                         @DimenRes int headerSpacing, @DimenRes int footerSpacing) {
        this(0,
                context.getResources().getDimensionPixelSize(headerSpacing),
                context.getResources().getDimensionPixelSize(footerSpacing));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (!(parent.getLayoutManager() instanceof LinearLayoutManager)) {
            throw new IllegalStateException("LinearLayoutManager only!");
        }

        if (((LinearLayoutManager) parent.getLayoutManager()).getOrientation() == LinearLayoutManager.VERTICAL) {
            if (parent.getChildAdapterPosition(view) != 0)
                outRect.top = itemSpacing;

            if (parent.getChildAdapterPosition(view) == 0)
                outRect.top = headerSpacing;

            if (state.getItemCount() > 0 &&
                    parent.getChildAdapterPosition(view) == state.getItemCount() - 1) {
                outRect.bottom = footerSpacing;
            }
        } else {
            if (parent.getChildAdapterPosition(view) != 0)
                outRect.left = itemSpacing;

            if (parent.getChildAdapterPosition(view) == 0)
                outRect.left = headerSpacing;

            if (state.getItemCount() > 0 &&
                    parent.getChildAdapterPosition(view) == state.getItemCount() - 1) {
                outRect.right = footerSpacing;
            }
        }
    }
}
