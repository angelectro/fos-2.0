package com.app.fol.utils;

import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;

public class PermissionUtils {

    public static boolean checkSelfPermission(@NonNull Context context, String... permissions) {

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                return true;
        }
        return false;
    }

    public static void requestPermissionFragment(@NonNull Fragment fragment, int requestCode, String... permissions) {
        FragmentCompat.requestPermissions(
                fragment,
                permissions,
                requestCode);
    }

    public static boolean checkPermissionGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED)
                return false;
        }
        return true;
    }
}
