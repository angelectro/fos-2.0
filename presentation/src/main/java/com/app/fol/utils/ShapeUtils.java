package com.app.fol.utils;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;

public class ShapeUtils {

    public static StateListDrawable getStateBackground(String color) {
        StateListDrawable stateListDrawable = new StateListDrawable();

        GradientDrawable gradientDrawableDisabled = new GradientDrawable();
        gradientDrawableDisabled.setStroke(ScaleUtils.dpToPx(2), Color.parseColor(color));
        stateListDrawable.addState(new int[]{-android.R.attr.enabled}, gradientDrawableDisabled);
        stateListDrawable.addState(new int[]{android.R.attr.enabled}, new ColorDrawable(Color.parseColor(color)));
        return stateListDrawable;
    }
}
