package com.app.fol.utils;


import android.content.Context;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.fol.R;

import java.util.List;

import rx.functions.Action0;
import rx.functions.Action1;

public class DialogUtils {

    public static <T> MaterialDialog showItemsSelector(Context context, List<T> list, Action1<T> action1) {
        if (list != null)
            return new MaterialDialog.Builder(context)
                    .items(list)
                    .itemsCallback((dialog, itemView, position, text) -> {
                        action1.call(list.get(position));
                    }).show();
        return null;
    }

    public static MaterialDialog showError(Context context, @StringRes int title, @StringRes int text) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .show();
    }

    public static MaterialDialog showError(Context context, String title, String text) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(text)
                .show();
    }


    public static MaterialDialog.Builder buildWarning(Context context,
                                                      @StringRes int text,
                                                      Action0 onPositive,
                                                      Action0 onNegative
    ) {
        return buildWarning(context, context.getString(text), onPositive, onNegative);
    }

    public static MaterialDialog.Builder buildWarning(Context context,
                                                      String text,
                                                      Action0 onPositive,
                                                      Action0 onNegative
    ) {
        return new MaterialDialog.Builder(context)
                .title(R.string.warning)
                .content(text)
                .cancelable(false)
                .positiveText(R.string.stay)
                .negativeText(R.string.leave)
                .onPositive((dialog, which) -> onPositive.call())
                .onNegative((dialog, which) -> onNegative.call());
    }

    public static MaterialDialog.Builder buildViolation(Context context,
                                                        @StringRes int text,
                                                        Action0 onSubmit
    ) {
        return buildViolation(context, context.getString(text), onSubmit);
    }

    public static MaterialDialog.Builder buildViolation(Context context,
                                                        String text,
                                                        Action0 onSubmit
    ) {
        return new MaterialDialog.Builder(context)
                .title(R.string.violation)
                .cancelable(false)
                .content(text)
                .positiveText(R.string.ok)
                .onPositive((dialog, which) -> onSubmit.call());
    }
}
