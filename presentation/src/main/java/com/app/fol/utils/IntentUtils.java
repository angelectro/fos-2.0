package com.app.fol.utils;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;

import com.app.domain.model.steps.TypeAttachment;
import com.app.fol.R;
import com.app.fol.feature.audiorecorder.AndroidAudioRecorder;
import com.app.fol.feature.audiorecorder.model.AudioChannel;
import com.app.fol.feature.audiorecorder.model.AudioSampleRate;
import com.app.fol.feature.audiorecorder.model.AudioSource;
import com.app.fol.feature.imageviewer.ImageViewerActivity;
import com.app.fol.feature.pdfviewer.PdfViewerActivity;
import com.app.fol.feature.scanner.ScannerActivity;
import com.app.fol.feature.videoplayer.VideoPlayerActivity;

import java.io.File;
import java.io.IOException;

import static com.app.fol.utils.FileUtils.FORMAT_JPEG;
import static com.app.fol.utils.FileUtils.FORMAT_MP4;
import static com.app.fol.utils.FileUtils.FORMAT_WAV;
import static com.app.fol.utils.FileUtils.createFile;

public class IntentUtils {

    public static final String AUTHORITY = "com.app.fol.android.fileprovider";

    public static String dispatchTakePictureIntent(Fragment fragment, int requestCode, int projectId) {
        DispatchResult dispatchResult = createAndStartIntent(fragment.getActivity(), MediaStore.ACTION_IMAGE_CAPTURE, FORMAT_JPEG, projectId);
        fragment.startActivityForResult(dispatchResult.getIntent(), requestCode);
        return dispatchResult
                .getFile().getAbsolutePath();
    }

    public static String dispatchTakeVideoIntent(Fragment fragment, int requestCode, int projectId) {
        DispatchResult dispatchResult = createAndStartIntent(fragment.getActivity(), MediaStore.ACTION_VIDEO_CAPTURE, FORMAT_MP4, projectId);
        fragment.startActivityForResult(dispatchResult.getIntent(), requestCode);
        return dispatchResult.getFile().getAbsolutePath();
    }

    public static String dispatchRecordAudioIntent(Fragment fragment, int requestCode, int projectId) {
        try {
            File file = createFile(fragment.getActivity(), FORMAT_WAV, projectId);
            AndroidAudioRecorder.with(fragment)
                    .setFilePath(file.getAbsolutePath())
                    .setColor(ContextCompat.getColor(fragment.getActivity(), R.color.accent))
                    .setRequestCode(requestCode)
                    .setSource(AudioSource.MIC)
                    .setChannel(AudioChannel.STEREO)
                    .setSampleRate(AudioSampleRate.HZ_48000)
                    .setKeepDisplayOn(true)
                    .recordFromFragment();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static DispatchResult dispatchTakePictureIntent(Context context, int projectId) {
        return createAndStartIntent(context, MediaStore.ACTION_IMAGE_CAPTURE, FORMAT_JPEG, projectId);
    }

    public static DispatchResult dispatchTakeVideoIntent(Context context, int projectId) {
        return createAndStartIntent(context, MediaStore.ACTION_VIDEO_CAPTURE, FORMAT_MP4, projectId);
    }

    public static DispatchResult dispatchRecordAudioIntent(Context context, int projectId) {
        try {
            File file = createFile(context, FORMAT_WAV, projectId);
            Intent intent = AndroidAudioRecorder.with(context)
                    .setFilePath(file.getAbsolutePath())
                    .setColor(ContextCompat.getColor(context, R.color.accent))
                    .setSource(AudioSource.MIC)
                    .setChannel(AudioChannel.STEREO)
                    .setSampleRate(AudioSampleRate.HZ_48000)
                    .setKeepDisplayOn(true)
                    .getIntent();
            return new DispatchResult(file, intent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void dispatchScannerIntent(Fragment fragment, int requestCode) {
        fragment.startActivityForResult(new Intent(fragment.getActivity(), ScannerActivity.class), requestCode);
    }

    private static DispatchResult createAndStartIntent(Context context, String action, String fileFormat, int projectId) {
        Intent intent = new Intent(action);
        File file = null;
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            try {
                file = createFile(context, fileFormat, projectId);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (file != null) {
                Uri targetURI;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    targetURI = FileProvider.getUriForFile(context,
                            AUTHORITY,
                            file);
                } else {
                    targetURI = Uri.fromFile(file);
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, targetURI);
            }
        }
        return new DispatchResult(file, intent);
    }

    public static void showAttachment(Context context, TypeAttachment typeField, File file, String url) {
        switch (typeField) {
            case VIDEO:
                if (file.exists())
                    VideoPlayerActivity.create(context, file.getAbsolutePath());
                else
                    VideoPlayerActivity.create(context, Uri.parse(url));
                break;
            case PHOTO:
                if (file.exists())
                    ImageViewerActivity.createViewerFromFile(context, file.getAbsolutePath());
                else
                    ImageViewerActivity.createViewerFromUrl(context, url);
                break;
            case DOC:
                if (file.exists())
                    PdfViewerActivity.createViewerFromFile(context, file.getAbsolutePath());
                else
                    PdfViewerActivity.createViewerFromUrl(context, url);
                break;
        }
    }

    public static class DispatchResult {
        private File file;
        private Intent intent;

        public DispatchResult(File file, Intent intent) {
            this.file = file;
            this.intent = intent;
        }

        public File getFile() {
            return file;
        }

        public Intent getIntent() {
            return intent;
        }
    }
}
