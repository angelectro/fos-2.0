package com.app.data.utils;


import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import io.realm.RealmList;
import io.realm.RealmObject;


public class RealmInteger extends RealmObject {
    public static TypeAdapter GSON_TYPE_ADAPTER = new Adapter();
    private int val;

    public RealmInteger() {
    }

    public RealmInteger(int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public static class Adapter extends TypeAdapter<RealmList<RealmInteger>> {

        @Override
        public void write(JsonWriter out, RealmList<RealmInteger> value) throws IOException {
            // Empty
        }

        @Override
        public RealmList<RealmInteger> read(JsonReader in) throws IOException {
            RealmList<RealmInteger> list = new RealmList<>();
            in.beginArray();
            while (in.hasNext()) {
                list.add(new RealmInteger(in.nextInt()));
            }
            in.endArray();
            return list;
        }
    }
}
