package com.app.data.entity.mapper;


import com.app.data.entity.steps.StepEntity;
import com.app.domain.model.steps.StepModel;
import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class StepEntityToModelMapper implements Function<StepEntity, StepModel> {
    @Inject
    ChoiceEntityToModelMapper mChoiceEntityToModelMapper;
    @Inject
    FieldEntityToModelMapper mFieldEntityToModelMapper;
    @Inject
    AttachmentToModelMapper mAttachmentToModelMapper;

    @Inject
    public StepEntityToModelMapper() {
    }

    @Override
    public StepModel apply(@NonNull StepEntity stepEntity) throws Exception {
        return new StepModel(stepEntity.getId(),
                stepEntity.getDescription(),
                stepEntity.isResultive(),
                Optional.ofNullable(stepEntity.getFields())
                        .map(fieldEntities -> Stream.of(fieldEntities)
                                .map(mFieldEntityToModelMapper)
                                .collect(Collectors.toList())).orElse(null),
                mAttachmentToModelMapper.apply(stepEntity.getAttachment()),
                stepEntity.getRows(),
                stepEntity.getCols(),
                stepEntity.getProjectId(),
                Stream.of(stepEntity.getChoices())
                        .map(mChoiceEntityToModelMapper)
                        .collect(Collectors.toList()),
                stepEntity.isBigBtn(),
                stepEntity.isFirst());
    }
}
