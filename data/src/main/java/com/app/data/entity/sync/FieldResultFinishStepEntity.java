package com.app.data.entity.sync;


import com.app.domain.model.FieldStatus;
import com.app.domain.model.TypeField;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FieldResultFinishStepEntity extends RealmObject {

    @PrimaryKey
    private String uuid;
    private String result;
    private String resultPath;
    private int finishStepId;
    private String type;
    private String status;

    public FieldResultFinishStepEntity() {
    }

    public FieldResultFinishStepEntity(String uuid, String result, int initStepId, TypeField type, String resultPath, FieldStatus status) {
        this.uuid = uuid;
        this.result = result;
        this.finishStepId = initStepId;
        this.type = type.toString();
        this.resultPath = resultPath;
        this.status = status.toString();
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public TypeField getType() {
        return TypeField.valueOf(type);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getFinishStepId() {
        return finishStepId;
    }

    public void setFinishStepId(int finishStepId) {
        this.finishStepId = finishStepId;
    }

    public FieldStatus getStatus() {
        return FieldStatus.valueOf(status);
    }

    public void setStatus(FieldStatus status) {
        this.status = status.toString();
    }

}
