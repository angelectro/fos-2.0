package com.app.data.entity.mapper;

import com.app.data.entity.staff.SupervisorEntity;
import com.app.domain.model.SupervisorModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class SupervisorEntityToModelMapper implements Function<SupervisorEntity, SupervisorModel> {

    @Inject
    public SupervisorEntityToModelMapper() {
    }

    @Override
    public SupervisorModel apply(@NonNull SupervisorEntity supervisorEntity) throws Exception {
        return new SupervisorModel(supervisorEntity.getId(),
                supervisorEntity.getFullName());
    }
}
