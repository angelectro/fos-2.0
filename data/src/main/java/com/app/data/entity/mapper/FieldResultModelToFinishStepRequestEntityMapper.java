package com.app.data.entity.mapper;

import com.app.data.entity.sync.FieldResultFinishRequestEntity;
import com.app.data.entity.sync.FieldResultInitStepRequestEntity;
import com.app.domain.model.FieldResultModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class FieldResultModelToFinishStepRequestEntityMapper implements Function<FieldResultModel, FieldResultFinishRequestEntity> {

    @Inject
    public FieldResultModelToFinishStepRequestEntityMapper() {
    }

    @Override
    public FieldResultFinishRequestEntity apply(@NonNull FieldResultModel fieldResultModel) throws Exception {
        return new FieldResultFinishRequestEntity(fieldResultModel.getStepId(),
                fieldResultModel.getUuid(),
                fieldResultModel.isFile() ?
                        fieldResultModel.getResultPath() :
                        fieldResultModel.getResult());
    }
}
