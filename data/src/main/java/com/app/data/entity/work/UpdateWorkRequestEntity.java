package com.app.data.entity.work;

import com.google.gson.annotations.SerializedName;


public class UpdateWorkRequestEntity {
    @SerializedName("end_at")
    private String endAt;
    @SerializedName("status")
    private int status;

    public UpdateWorkRequestEntity(int status) {
        this.status = status;
    }

    public UpdateWorkRequestEntity(String endAt, int status) {
        this.endAt = endAt;
        this.status = status;

    }

    public int getStatus() {
        return status;
    }

    public String getEndAt() {
        return endAt;
    }
}
