package com.app.data.entity.mapper;

import com.app.data.entity.questionnaire.QuestionnaireEntity;
import com.app.domain.model.QuestionnaireModel;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class QuestionnaireEntityToModelMapper implements Function<QuestionnaireEntity, QuestionnaireModel> {
    private FieldEntityToModelMapper mFieldEntityToModelMapper;

    @Inject
    public QuestionnaireEntityToModelMapper(FieldEntityToModelMapper fieldEntityToModelMapper) {
        mFieldEntityToModelMapper = fieldEntityToModelMapper;
    }

    @Override
    public QuestionnaireModel apply(@NonNull QuestionnaireEntity preparationEntity) throws Exception {
        return new QuestionnaireModel(preparationEntity.getId(),
                preparationEntity.getName(),
                preparationEntity.getProjectId(),
                preparationEntity.getOrder(),
                Stream.of(preparationEntity.getFields())
                        .map(mFieldEntityToModelMapper)
                        .collect(Collectors.toList())
        );
    }
}
