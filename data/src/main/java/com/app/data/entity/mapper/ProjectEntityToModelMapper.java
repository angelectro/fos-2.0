package com.app.data.entity.mapper;

import com.app.data.entity.project.ProjectEntity;
import com.app.data.entity.project.ViolationsEntity;
import com.app.domain.model.ProjectModel;
import com.app.domain.model.ValueModel;
import com.app.domain.model.ViolationModel;
import com.annimon.stream.Stream;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class ProjectEntityToModelMapper implements Function<ProjectEntity, ProjectModel> {

    @Inject
    public ProjectEntityToModelMapper() {
    }

    @Override
    public ProjectModel apply(@NonNull ProjectEntity projectEntity) throws Exception {
        return new ProjectModel(projectEntity.getId(),
                projectEntity.getName(),
                projectEntity.getAppPortraitCover(),
                projectEntity.getAppLandscapeCover(),
                projectEntity.getBrandColor(),
                projectEntity.getDescription(),
                projectEntity.getBrandLogo(),
                projectEntity.getArchived(),
                mapViolation(projectEntity.getViolations()),
                projectEntity.getTargetAudience(),
                projectEntity.getHelp(),
                projectEntity.getContactStructure(),
                projectEntity.getContactRegistrationTimeout());
    }

    private Map<ViolationModel.Type, ViolationModel> mapViolation(List<ViolationsEntity> violationsEntities) {
        Map<ViolationModel.Type, ViolationModel> map = new HashMap<>();
        Stream.of(violationsEntities)
                .map(violationsEntity -> {
                    ViolationModel violationModel = new ViolationModel(violationsEntity.getActive(), violationsEntity.getId());
                    switch (ViolationModel.Type.getById(violationsEntity.getId())) {
                        case DELAY_ON_BREAK:
                        case FALSIFICATION_OF_STATISTICS:
                        case FALSIFICATION_OF_TIME:
                            violationModel.setValue(new ValueModel(
                                    violationsEntity.getValue().getLimit(),
                                    violationsEntity.getValue().getMinutes(),
                                    violationsEntity.getValue().getMaterials()
                            ));
                            break;
                    }
                    return violationModel;
                })
                .forEach(violationModel -> map.put(ViolationModel.Type.getById(violationModel.getId()), violationModel));
        return map;
    }
}
