package com.app.data.entity.sync;

import com.google.gson.annotations.SerializedName;

public class UploadFileResponseEntity {
    @SerializedName("path")
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
