package com.app.data.entity.staff;

import com.app.data.utils.RealmInteger;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PromoterEntity extends RealmObject {
    @SerializedName("id")
    @PrimaryKey
    private Integer id;
    @SerializedName("fio")
    private String fullName;
    @SerializedName("project_id")
    private Integer projectId;
    @SerializedName("cities")
    private RealmList<RealmInteger> cities;

    public RealmList<RealmInteger> getCities() {
        return cities;
    }

    public void setCities(RealmList<RealmInteger> cities) {
        this.cities = cities;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
}
