package com.app.data.entity.mapper;

import com.app.data.entity.sync.FieldResultInitStepRequestEntity;
import com.app.domain.model.FieldResultModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class FieldResultModelToRequestEntityMapper implements Function<FieldResultModel, FieldResultInitStepRequestEntity> {

    @Inject
    public FieldResultModelToRequestEntityMapper() {
    }

    @Override
    public FieldResultInitStepRequestEntity apply(@NonNull FieldResultModel fieldResultModel) throws Exception {
        return new FieldResultInitStepRequestEntity(fieldResultModel.getStepId(),
                fieldResultModel.getUuid(),
                fieldResultModel.isFile() ?
                        fieldResultModel.getResultPath() :
                        fieldResultModel.getResult());
    }
}
