package com.app.data.entity.mapper;

import com.app.data.entity.work.WorkStatusResponseEntity;
import com.app.domain.model.work.WorkModel;

import javax.inject.Inject;

import io.reactivex.functions.Function;


public class WorkRequestEntityToModelMapper implements Function<WorkStatusResponseEntity, WorkModel> {

    @Inject
    public WorkRequestEntityToModelMapper() {
    }

    @Override
    public WorkModel apply(WorkStatusResponseEntity entity) throws Exception {
        return new WorkModel(entity.getId(),
                entity.getBeginAt(),
                entity.getEndAt(),
                entity.getStatus(),
                entity.getWorkScheduleId(),
                entity.getOnline());
    }
}
