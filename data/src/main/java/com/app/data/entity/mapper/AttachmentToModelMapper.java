package com.app.data.entity.mapper;

import android.text.TextUtils;

import com.app.data.entity.steps.AttachmentEntity;
import com.app.domain.model.steps.AttachmentModel;
import com.app.domain.model.steps.TypeAttachment;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class AttachmentToModelMapper implements Function<AttachmentEntity, AttachmentModel> {
    @Inject
    public AttachmentToModelMapper() {
    }

    @Override
    public AttachmentModel apply(@NonNull AttachmentEntity attachmentEntity) throws Exception {
        return attachmentEntity != null ? new AttachmentModel(attachmentEntity.getPath(),
                getType(attachmentEntity.getType())) : null;
    }

    private TypeAttachment getType(String type) {
        for (TypeAttachment typeAttachment : TypeAttachment.values()) {
            if (TextUtils.equals(typeAttachment.getType(), type))
                return typeAttachment;
        }
        return TypeAttachment.PHOTO;
    }
}
