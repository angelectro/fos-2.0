package com.app.data.entity.mapper;

import com.app.data.entity.staff.CityEntity;
import com.app.domain.model.CityModel;

import javax.inject.Inject;

import io.reactivex.functions.Function;


public class CityEntityToModelMapper implements Function<CityEntity, CityModel> {

    @Inject
    public CityEntityToModelMapper() {}

    @Override

    public CityModel apply(CityEntity cityEntity) {
        return new CityModel(cityEntity.getId(),
                cityEntity.getName(),
                cityEntity.getProjectId());
    }
}
