package com.app.data.entity.mapper;

import com.app.data.entity.sync.FieldResultFinishStepEntity;
import com.app.domain.model.FieldResultModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class FieldResultFinishStepEntityToModelMapper implements Function<FieldResultFinishStepEntity, FieldResultModel> {

    @Inject
    public FieldResultFinishStepEntityToModelMapper() {
    }

    @Override
    public FieldResultModel apply(@NonNull FieldResultFinishStepEntity fieldResultInitStepEntity) throws Exception {
        return new FieldResultModel(
                fieldResultInitStepEntity.getUuid(),
                fieldResultInitStepEntity.getResult(),
                fieldResultInitStepEntity.getFinishStepId(),
                fieldResultInitStepEntity.getType(),
                fieldResultInitStepEntity.getStatus().toString(),
                fieldResultInitStepEntity.getResultPath());
    }
}
