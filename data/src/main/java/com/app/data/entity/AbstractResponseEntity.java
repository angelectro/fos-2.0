package com.app.data.entity;


import com.google.gson.annotations.SerializedName;

public class AbstractResponseEntity {
    private @SerializedName("status") boolean status;

    public boolean isSuccessful() {
        return status;
    }

    public boolean isError() {
        return !isSuccessful();
    }
}
