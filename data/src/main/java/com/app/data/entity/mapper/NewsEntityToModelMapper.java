package com.app.data.entity.mapper;

import com.app.data.entity.news.NewsEntity;
import com.app.domain.model.news.NewsModel;

import javax.inject.Inject;

import io.reactivex.functions.Function;

public class NewsEntityToModelMapper implements Function<NewsEntity, NewsModel> {

    @Inject
    public NewsEntityToModelMapper() {
    }

    @Override
    public NewsModel apply(NewsEntity newsEntity) throws Exception {
        return new NewsModel(newsEntity.getId(),
                newsEntity.getText(),
                newsEntity.getDate(),
                newsEntity.getProjectId());
    }
}
