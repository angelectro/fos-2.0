package com.app.data.entity.mapper;

import com.app.data.entity.initsteps.FieldEntity;
import com.app.data.entity.initsteps.PreparationEntity;
import com.app.domain.model.PreparationWorkStepModel;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class PreparationEntityToModelMapper implements Function<PreparationEntity,PreparationWorkStepModel> {
    private FieldEntityToModelMapper mFieldEntityToModelMapper;

    @Inject
    public PreparationEntityToModelMapper(FieldEntityToModelMapper fieldEntityToModelMapper) {
        mFieldEntityToModelMapper = fieldEntityToModelMapper;
    }

    @Override
    public PreparationWorkStepModel apply(@NonNull PreparationEntity preparationEntity) throws Exception {
        return new PreparationWorkStepModel(preparationEntity.getId(),
                preparationEntity.getName(),
                preparationEntity.getProjectId(),
                preparationEntity.getOrder(),
                Stream.of(preparationEntity.getFields())
                        .map(mFieldEntityToModelMapper)
                        .collect(Collectors.toList())
                );
    }
}
