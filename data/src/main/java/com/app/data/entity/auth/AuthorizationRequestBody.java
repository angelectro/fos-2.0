package com.app.data.entity.auth;

import com.google.gson.annotations.SerializedName;

public class AuthorizationRequestBody {
    @SerializedName("password")
    private String password;

    public AuthorizationRequestBody(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
