package com.app.data.entity.sync;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ContactRequestEntity {
    @SerializedName("choices")
    private List<ChoiceRequestEntity> mStepRequestEntities;
    @SerializedName("time")
    private String time;

    public ContactRequestEntity(List<ChoiceRequestEntity> stepRequestEntities, Date time) {
        mStepRequestEntities = stepRequestEntities;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        this.time = simpleDateFormat.format(time);
    }

    public String getTime() {
        return time;
    }

    public List<ChoiceRequestEntity> getStepRequestEntities() {
        return mStepRequestEntities;
    }

}
