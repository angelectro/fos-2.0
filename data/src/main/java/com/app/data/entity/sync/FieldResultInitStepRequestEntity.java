package com.app.data.entity.sync;

import com.google.gson.annotations.SerializedName;

public class FieldResultInitStepRequestEntity {

    @SerializedName("init_step_id")
    private int initStepId;
    @SerializedName("uuid")
    private String uuid;
    @SerializedName("value")
    private String value;

    public FieldResultInitStepRequestEntity(int initStepId, String uuid, String value) {
        this.initStepId = initStepId;
        this.uuid = uuid;
        this.value = value;
    }
}
