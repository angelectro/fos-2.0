package com.app.data.entity.auth;

import com.google.gson.annotations.SerializedName;

public class AuthorizationResponseEntity {

    @SerializedName("password")
    private String password;

    public AuthorizationResponseEntity(String password) {
        this.password = password;
    }

    public String getToken() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
