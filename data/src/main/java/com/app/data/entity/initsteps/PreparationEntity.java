package com.app.data.entity.initsteps;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PreparationEntity extends RealmObject{
    @SerializedName("id")
    @PrimaryKey
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("project_id")
    private Integer projectId;
    @SerializedName("order")
    private Integer order;
    @SerializedName("fields")
    private RealmList<FieldEntity> fields;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<FieldEntity> getFields() {
        return fields;
    }

    public void setFields(RealmList<FieldEntity> fields) {
        this.fields = fields;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
