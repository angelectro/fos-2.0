package com.app.data.entity.initsteps;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FieldEntity extends RealmObject{
    @SerializedName("uuid")
    @PrimaryKey
    private String uuid;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("required")
    private boolean required;
    @SerializedName("type")
    private String type;
    @SerializedName("order")
    private Integer order;
    @SerializedName("sub")
    private RealmList<SubFieldEntity> sub = null;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<SubFieldEntity> getSub() {
        return sub;
    }

    public void setSub(RealmList<SubFieldEntity> sub) {
        this.sub = sub;
    }
}
