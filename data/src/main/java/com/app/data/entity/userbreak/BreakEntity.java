package com.app.data.entity.userbreak;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BreakEntity extends RealmObject {
    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private String beginAt;
    private String endAt;
    private boolean isSent;

    public BreakEntity() {
    }

    public BreakEntity(String beginAt, String endAt) {
        this.beginAt = beginAt;

        this.endAt = endAt;
        isSent = false;
    }

    public String getId() {
        return id;
    }

    public String getBeginAt() {
        return beginAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }
}
