package com.app.data.entity.steps;

import com.app.data.entity.initsteps.FieldEntity;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StepEntity extends RealmObject {

    @SerializedName("id")
    @PrimaryKey
    private Integer id;
    @SerializedName("description")
    private String description;
    @SerializedName("resultive")
    private boolean resultive;
    @SerializedName("fields")
    private RealmList<FieldEntity> fields;
    @SerializedName("attachment")
    private AttachmentEntity attachment;
    @SerializedName("rows")
    private Integer rows;
    @SerializedName("cols")
    private Integer cols;
    @SerializedName("project_id")
    private Integer projectId;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("big_btn")
    private boolean bigBtn;
    @SerializedName("is_first")
    private boolean isFirst;
    @SerializedName("choices")
    private RealmList<ChoiceEntity> choices;

    public AttachmentEntity getAttachment() {
        return attachment;
    }

    public void setAttachment(AttachmentEntity attachment) {
        this.attachment = attachment;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isResultive() {
        return resultive;
    }

    public void setResultive(boolean resultive) {
        this.resultive = resultive;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getCols() {
        return cols;
    }

    public void setCols(Integer cols) {
        this.cols = cols;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isBigBtn() {
        return bigBtn;
    }

    public void setBigBtn(boolean bigBtn) {
        this.bigBtn = bigBtn;
    }

    public RealmList<FieldEntity> getFields() {
        return fields;
    }

    public void setFields(RealmList<FieldEntity> fields) {
        this.fields = fields;
    }

    public RealmList<ChoiceEntity> getChoices() {
        return choices;
    }

    public void setChoices(RealmList<ChoiceEntity> choices) {
        this.choices = choices;
    }
}
