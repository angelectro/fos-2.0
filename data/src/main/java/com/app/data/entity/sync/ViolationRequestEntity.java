package com.app.data.entity.sync;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ViolationRequestEntity {
    @SerializedName("violation_id")
    private Integer violationId;
    @SerializedName("work_schedule_id")
    private Integer workScheduleId;
    @SerializedName("time")

    private String time;

    public ViolationRequestEntity(Integer violationId, Integer workScheduleId, Date time) {
        this.violationId = violationId;
        this.workScheduleId = workScheduleId;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        this.time = simpleDateFormat.format(time);
    }

    public Integer getViolationId() {
        return violationId;
    }

    public void setViolationId(Integer violationId) {
        this.violationId = violationId;
    }

    public Integer getWorkScheduleId() {
        return workScheduleId;
    }

    public void setWorkScheduleId(Integer workScheduleId) {
        this.workScheduleId = workScheduleId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
