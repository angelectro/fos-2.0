package com.app.data.entity.mapper;

import com.app.data.entity.sync.FieldResultContactRequestEntity;
import com.app.domain.model.FieldResultModel;
import com.app.domain.model.sync.FieldResultStepEntity;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class FieldResultModelToFieldContactRequestEntityMapper implements Function<FieldResultStepEntity, FieldResultContactRequestEntity> {

    @Inject
    public FieldResultModelToFieldContactRequestEntityMapper() {
    }

    @Override
    public FieldResultContactRequestEntity apply(@NonNull FieldResultStepEntity fieldResultModel) throws Exception {
        return new FieldResultContactRequestEntity(fieldResultModel.getStepId(),
                fieldResultModel.getUuid(),
                fieldResultModel.isFile() ?
                        fieldResultModel.getResultPath() :
                        fieldResultModel.getResult());
    }
}
