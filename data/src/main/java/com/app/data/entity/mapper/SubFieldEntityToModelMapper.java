package com.app.data.entity.mapper;

import com.app.data.entity.initsteps.SubFieldEntity;
import com.app.domain.model.SubFieldModel;
import com.app.domain.model.TypeSubField;
import com.annimon.stream.function.Function;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;


public class SubFieldEntityToModelMapper implements Function<SubFieldEntity, SubFieldModel> {

    @Inject
    public SubFieldEntityToModelMapper() {
    }

    @Override
    public SubFieldModel apply(@NonNull SubFieldEntity subFieldEntity){
        return new SubFieldModel(subFieldEntity.getName(),
                subFieldEntity.getOrder());
    }
}
