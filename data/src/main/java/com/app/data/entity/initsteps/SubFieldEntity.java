package com.app.data.entity.initsteps;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class SubFieldEntity extends RealmObject{
    @SerializedName("name")
    private String name;
    @SerializedName("order")
    private Integer order;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}