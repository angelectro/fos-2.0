package com.app.data.entity.mapper;

import com.app.domain.model.sync.FieldResultStepEntity;
import com.app.domain.model.FieldResultModel;
import com.annimon.stream.function.Function;

import javax.inject.Inject;

public class FieldResultModelToStepMapper implements Function<FieldResultModel, FieldResultStepEntity> {

    @Inject
    public FieldResultModelToStepMapper() {
    }

    @Override

    public FieldResultStepEntity apply(FieldResultModel fieldResultModel) {
        return new FieldResultStepEntity(
                fieldResultModel.getUuid(),
                fieldResultModel.getResult(),
                fieldResultModel.getStepId(),
                fieldResultModel.getType(),
                fieldResultModel.getResultPath(),
                fieldResultModel.getStatus());
    }
}
