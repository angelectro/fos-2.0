package com.app.data.entity.schedule;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class OutletEntity extends RealmObject {
    @SerializedName("id")
    @PrimaryKey
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("city_id")
    private Integer cityId;
    @SerializedName("address")
    private String address;
    @SerializedName("retail_chain_id")
    private Integer retailChainId;
    @SerializedName("project_id")
    private Integer projectId;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("city")
    private String city;
    @SerializedName("retail_chain")
    private String retailChain;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getRetailChainId() {
        return retailChainId;
    }

    public void setRetailChainId(Integer retailChainId) {
        this.retailChainId = retailChainId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRetailChain() {
        return retailChain;
    }

    public void setRetailChain(String retailChain) {
        this.retailChain = retailChain;
    }
}
