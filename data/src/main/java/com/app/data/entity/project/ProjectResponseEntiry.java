package com.app.data.entity.project;

import com.app.data.entity.AbstractResponseEntity;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ProjectResponseEntiry extends AbstractResponseEntity {

    @SerializedName("data")
    private List<ProjectEntity> projects;

    public List<ProjectEntity> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectEntity> projects) {
        this.projects = projects;
    }
}
