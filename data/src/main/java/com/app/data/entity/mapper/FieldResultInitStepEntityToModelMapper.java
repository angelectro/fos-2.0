package com.app.data.entity.mapper;

import com.app.data.entity.sync.FieldResultInitStepEntity;
import com.app.domain.model.FieldResultModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class FieldResultInitStepEntityToModelMapper implements Function<FieldResultInitStepEntity, FieldResultModel> {

    @Inject
    public FieldResultInitStepEntityToModelMapper() {
    }

    @Override
    public FieldResultModel apply(@NonNull FieldResultInitStepEntity fieldResultInitStepEntity) throws Exception {
        return new FieldResultModel(
                fieldResultInitStepEntity.getUuid(),
                fieldResultInitStepEntity.getResult(),
                fieldResultInitStepEntity.getInitStepId(),
                fieldResultInitStepEntity.getType(),
                fieldResultInitStepEntity.getStatus().toString(),
                fieldResultInitStepEntity.getResultPath());
    }
}
