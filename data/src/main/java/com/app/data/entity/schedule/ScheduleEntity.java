package com.app.data.entity.schedule;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ScheduleEntity extends RealmObject {
    @SerializedName("id")
    @PrimaryKey
    private Integer id;
    @SerializedName("date")
    private String date;
    @SerializedName("begin_at")
    private String beginAt;
    @SerializedName("end_at")
    private String endAt;
    @SerializedName("outlet_id")
    private Integer outletId;
    @SerializedName("promouter_id")
    private Integer promouterId;
    @SerializedName("supervisor_id")
    private Integer supervisorId;
    @SerializedName("project_id")
    private Integer projectId;
    @SerializedName("outlet")
    private OutletEntity outlet;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBeginAt() {
        return beginAt;
    }

    public void setBeginAt(String beginAt) {
        this.beginAt = beginAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }

    public Integer getOutletId() {
        return outletId;
    }

    public void setOutletId(Integer outletId) {
        this.outletId = outletId;
    }

    public Integer getPromouterId() {
        return promouterId;
    }

    public void setPromouterId(Integer promouterId) {
        this.promouterId = promouterId;
    }

    public Integer getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(Integer supervisorId) {
        this.supervisorId = supervisorId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public OutletEntity getOutlet() {
        return outlet;
    }

    public void setOutlet(OutletEntity outlet) {
        this.outlet = outlet;
    }
}
