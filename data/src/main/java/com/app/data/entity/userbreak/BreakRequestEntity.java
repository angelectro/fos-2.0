package com.app.data.entity.userbreak;

import com.google.gson.annotations.SerializedName;

public class BreakRequestEntity {
    @SerializedName("begin_at")
    private String beginAt;
    @SerializedName("end_at")
    private String endAt;

    public BreakRequestEntity(String beginAt, String endAt) {
        this.beginAt = beginAt;
        this.endAt = endAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public String getBeginAt() {
        return beginAt;
    }
}
