package com.app.data.entity.mapper;

import com.app.data.entity.staff.PromoterEntity;
import com.app.domain.model.PromoterModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class PromoterEntityToModelMapper implements Function<PromoterEntity, PromoterModel> {

    @Inject
    public PromoterEntityToModelMapper() {
    }

    @Override
    public PromoterModel apply(@NonNull PromoterEntity promoterEntity) throws Exception {
        return new PromoterModel(promoterEntity.getId(),
                promoterEntity.getFullName());
    }
}
