package com.app.data.entity.news;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class NewsEntity extends RealmObject{

    @SerializedName("id")
    private Integer id;
    @SerializedName("text")
    private String text;
    @SerializedName("date")
    private String date;
    @SerializedName("project_id")
    private Integer projectId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
}
