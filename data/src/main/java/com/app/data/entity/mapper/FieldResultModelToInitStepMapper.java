package com.app.data.entity.mapper;

import com.app.data.entity.sync.FieldResultInitStepEntity;
import com.app.domain.model.FieldResultModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class FieldResultModelToInitStepMapper implements Function<FieldResultModel, FieldResultInitStepEntity> {

    @Inject
    public FieldResultModelToInitStepMapper() {
    }


    @Override
    public FieldResultInitStepEntity apply(@NonNull FieldResultModel fieldResultModel){
        return new FieldResultInitStepEntity(
                fieldResultModel.getUuid(),
                fieldResultModel.getResult(),
                fieldResultModel.getStepId(),
                fieldResultModel.getType(),
                fieldResultModel.getResultPath(),
                fieldResultModel.getStatus());
    }
}
