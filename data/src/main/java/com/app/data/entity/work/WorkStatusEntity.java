package com.app.data.entity.work;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class WorkStatusEntity extends RealmObject {

    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private Date time;
    private int status;
    private boolean isSent;

    public WorkStatusEntity() {
    }

    public WorkStatusEntity(Date time, int status) {
        this.time = time;
        this.status = status;
        isSent = false;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }
}
