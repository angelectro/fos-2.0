package com.app.data.entity.work;

import com.google.gson.annotations.SerializedName;

public class WorkStatusResponseEntity {
    @SerializedName("id")
    private Integer id;
    @SerializedName("begin_at")
    private String beginAt;
    @SerializedName("end_at")
    private String endAt;
    @SerializedName("status")
    private String status;
    @SerializedName("work_schedule_id")
    private String workScheduleId;
    @SerializedName("online")
    private Boolean online;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBeginAt() {
        return beginAt;
    }

    public void setBeginAt(String beginAt) {
        this.beginAt = beginAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWorkScheduleId() {
        return workScheduleId;
    }

    public void setWorkScheduleId(String workScheduleId) {
        this.workScheduleId = workScheduleId;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }
}
