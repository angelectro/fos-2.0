package com.app.data.entity.project;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProjectEntity {
    @SerializedName("violations")
    private List<ViolationsEntity> violations;
    @SerializedName("id")
    private Integer id;
    @SerializedName("contact_registration_timeout")
    private Integer contactRegistrationTimeout;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("creator_id")
    private Integer creatorId;
    @SerializedName("client_id")
    private Integer clientId;
    @SerializedName("brand_id")
    private Integer brandId;
    @SerializedName("brand_color")
    private String brandColor;
    @SerializedName("brand_logo")
    private String brandLogo;
    @SerializedName("app_portrait_cover")
    private String appPortraitCover;
    @SerializedName("app_landscape_cover")
    private String appLandscapeCover;
    @SerializedName("archived")
    private Boolean archived;
    @SerializedName("target_audience")
    private String targetAudience;
    @SerializedName("contact_structure")
    private String contactStructure;
    @SerializedName("help")
    private String help;

    public Integer getContactRegistrationTimeout() {
        return contactRegistrationTimeout;
    }

    public void setContactRegistrationTimeout(Integer contactRegistrationTimeout) {
        this.contactRegistrationTimeout = contactRegistrationTimeout;
    }

    public String getContactStructure() {
        return contactStructure;
    }

    public void setContactStructure(String contactStructure) {
        this.contactStructure = contactStructure;
    }

    public String getTargetAudience() {
        return targetAudience;
    }

    public void setTargetAudience(String targetAudience) {
        this.targetAudience = targetAudience;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public List<ViolationsEntity> getViolations() {
        return violations;
    }

    public void setViolations(List<ViolationsEntity> violations) {
        this.violations = violations;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandColor() {
        return brandColor;
    }

    public void setBrandColor(String brandColor) {
        this.brandColor = brandColor;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public String getAppPortraitCover() {
        return appPortraitCover;
    }

    public void setAppPortraitCover(String appPortraitCover) {
        this.appPortraitCover = appPortraitCover;
    }

    public String getAppLandscapeCover() {
        return appLandscapeCover;
    }

    public void setAppLandscapeCover(String appLandscapeCover) {
        this.appLandscapeCover = appLandscapeCover;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }
}
