package com.app.data.entity.sync;


import com.app.domain.model.FieldStatus;
import com.app.domain.model.TypeField;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FieldResultInitStepEntity extends RealmObject {

    @PrimaryKey
    private String uuid;
    private String result;
    private String resultPath;
    private int initStepId;
    private String type;
    private String status;

    public FieldResultInitStepEntity() {
    }

    public FieldResultInitStepEntity(String uuid, String result, int initStepId, TypeField type, String resultPath, FieldStatus status) {
        this.uuid = uuid;
        this.result = result;
        this.initStepId = initStepId;
        this.type = type.toString();
        this.resultPath = resultPath;
        this.status = status.toString();
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public TypeField getType() {
        return TypeField.valueOf(type);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getInitStepId() {
        return initStepId;
    }

    public void setInitStepId(int initStepId) {
        this.initStepId = initStepId;
    }

    public FieldStatus getStatus() {
        return FieldStatus.valueOf(status);
    }

    public void setStatus(FieldStatus status) {
        this.status = status.toString();
    }

}
