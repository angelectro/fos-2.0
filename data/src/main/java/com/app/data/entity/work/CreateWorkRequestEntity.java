package com.app.data.entity.work;

import com.google.gson.annotations.SerializedName;

public class CreateWorkRequestEntity {
    @SerializedName("begin_at")
    private String beginAt;

    public CreateWorkRequestEntity(String beginAt) {
        this.beginAt = beginAt;
    }

    public String getBeginAt() {

        return beginAt;
    }

    public void setBeginAt(String beginAt) {
        this.beginAt = beginAt;
    }
}
