package com.app.data.entity.mapper;

import com.app.data.entity.schedule.ScheduleEntity;
import com.app.domain.model.ScheduleModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class ScheduleEntityToModelMapper implements Function<ScheduleEntity, ScheduleModel> {

    @Inject
    public ScheduleEntityToModelMapper() {
    }

    @Override
    public ScheduleModel apply(@NonNull ScheduleEntity scheduleEntity) throws Exception {
        return new ScheduleModel(
                scheduleEntity.getId(),
                scheduleEntity.getDate(),
                scheduleEntity.getBeginAt(),
                scheduleEntity.getEndAt(),
                scheduleEntity.getOutlet().getRetailChain(),
                scheduleEntity.getOutlet().getRetailChainId(),
                scheduleEntity.getOutlet().getCityId(),
                scheduleEntity.getOutlet().getAddress(),
                scheduleEntity.getOutletId(),
                scheduleEntity.getPromouterId(),
                scheduleEntity.getSupervisorId(),
                scheduleEntity.getProjectId()
        );
    }
}
