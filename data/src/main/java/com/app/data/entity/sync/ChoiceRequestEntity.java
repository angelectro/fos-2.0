package com.app.data.entity.sync;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChoiceRequestEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("fields")
    private List<FieldResultContactRequestEntity> fieldResultRequestEntities;

    public ChoiceRequestEntity(int id, List<FieldResultContactRequestEntity> fieldResultRequestEntities) {

        this.id = id;
        this.fieldResultRequestEntities = fieldResultRequestEntities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<FieldResultContactRequestEntity> getFieldResultRequestEntities() {
        return fieldResultRequestEntities;
    }

    public void setFieldResultRequestEntities(List<FieldResultContactRequestEntity> fieldResultRequestEntities) {
        this.fieldResultRequestEntities = fieldResultRequestEntities;
    }
}
