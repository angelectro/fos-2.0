package com.app.data.entity.mapper;

import com.app.data.entity.initsteps.FieldEntity;
import com.app.domain.model.FieldModel;
import com.app.domain.model.TypeField;
import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;


public class FieldEntityToModelMapper implements Function<FieldEntity, FieldModel> {
    private SubFieldEntityToModelMapper mSubfieldEntityToModelMapper;

    @Inject
    public FieldEntityToModelMapper(SubFieldEntityToModelMapper subfieldEntityToModelMapper) {
        mSubfieldEntityToModelMapper = subfieldEntityToModelMapper;
    }

    @Override
    public FieldModel apply(@NonNull FieldEntity fieldEntity) {
        return new FieldModel(fieldEntity.getUuid(),
                fieldEntity.getName(),
                fieldEntity.getDescription(),
                getType(fieldEntity.getType()),
                fieldEntity.getOrder(),
                Optional.ofNullable(fieldEntity.getSub())
                        .map(subFieldEntities -> Stream.of(subFieldEntities)
                                .map(mSubfieldEntityToModelMapper)
                                .collect(Collectors.toList())).orElse(null),
                fieldEntity.getRequired());
    }

    private TypeField getType(String type) {
        switch (type) {
            case "photo":
                return TypeField.PHOTO;
            case "textarea":
                return TypeField.TEXTAREA;
            case "text":
                return TypeField.INPUT;
            case "number":
                return TypeField.NUMBER;
            case "group-radio":
                return TypeField.GROUP;
            case "audio":
                return TypeField.AUDIO;
            case "group-checkbox":
                return TypeField.GROUP_CHECKBOX;
            case "checkbox":
                return TypeField.CHECKBOX;
            case "select":
                return TypeField.SELECT;
            case "qr":
                return TypeField.QR;
            case "sign":
                return TypeField.SIGN;
            case "video":
            default:
                return TypeField.VIDEO;
        }
    }
}
