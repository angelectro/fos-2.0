package com.app.data.entity.project;

import com.google.gson.annotations.SerializedName;

public class ViolationsEntity {
    @SerializedName("active")
    private Boolean active;
    @SerializedName("id")
    private Integer id;
    @SerializedName("value")
    private Value value;

    public ViolationsEntity() {
    }

    public ViolationsEntity(Integer id, Boolean active) {
        this.active = active;
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public class Value {

        @SerializedName("limit")
        private Integer limit;
        @SerializedName("minutes")
        private Integer minutes;
        @SerializedName("materials")
        private Integer materials;

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }

        public Integer getMinutes() {
            return minutes;
        }

        public void setMinutes(Integer minutes) {
            this.minutes = minutes;
        }

        public Integer getMaterials() {
            return materials;
        }

        public void setMaterials(Integer materials) {
            this.materials = materials;
        }

    }
}


