package com.app.data.entity.mapper;

import com.app.data.entity.sync.FieldResultFinishStepEntity;
import com.app.domain.model.FieldResultModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


public class FieldResultModelToFinishStepMapper implements Function<FieldResultModel, FieldResultFinishStepEntity> {

    @Inject
    public FieldResultModelToFinishStepMapper() {
    }


    @Override
    public FieldResultFinishStepEntity apply(@NonNull FieldResultModel fieldResultModel) {
        return new FieldResultFinishStepEntity(
                fieldResultModel.getUuid(),
                fieldResultModel.getResult(),
                fieldResultModel.getStepId(),
                fieldResultModel.getType(),
                fieldResultModel.getResultPath(),
                fieldResultModel.getStatus());
    }
}
