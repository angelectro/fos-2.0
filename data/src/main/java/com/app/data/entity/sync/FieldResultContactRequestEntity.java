package com.app.data.entity.sync;

import com.google.gson.annotations.SerializedName;

public class FieldResultContactRequestEntity {
    @SerializedName("step_id")
    private int stepId;
    @SerializedName("uuid")
    private String uuid;
    @SerializedName("value")
    private String value;

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public FieldResultContactRequestEntity(int stepId, String uuid, String value) {
        this.stepId = stepId;
        this.uuid = uuid;
        this.value = value;

    }
}
