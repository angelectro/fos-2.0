package com.app.data.entity.sync;

import com.google.gson.annotations.SerializedName;

public class FieldResultFinishRequestEntity {

    @SerializedName("finish_step_id")
    private int finishStepId;
    @SerializedName("uuid")
    private String uuid;
    @SerializedName("value")
    private String value;

    public FieldResultFinishRequestEntity(int initStepId, String uuid, String value) {
        this.finishStepId = initStepId;
        this.uuid = uuid;
        this.value = value;
    }
}
