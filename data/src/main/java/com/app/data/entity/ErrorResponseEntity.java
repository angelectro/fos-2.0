package com.app.data.entity;


import com.google.gson.annotations.SerializedName;

public class ErrorResponseEntity {

    @SerializedName("error")
    private String error;


    public String getError() {
        return error;
    }

}
