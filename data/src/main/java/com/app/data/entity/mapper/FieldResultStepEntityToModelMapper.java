package com.app.data.entity.mapper;

import com.app.domain.model.sync.FieldResultStepEntity;
import com.app.domain.model.FieldResultModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

public class FieldResultStepEntityToModelMapper  implements Function<FieldResultStepEntity, FieldResultModel> {

    @Inject
    public FieldResultStepEntityToModelMapper() {
    }

    @Override
    public FieldResultModel apply(@NonNull FieldResultStepEntity fieldResultStepEntity){
        return new FieldResultModel(
                fieldResultStepEntity.getUuid(),
                fieldResultStepEntity.getResult(),
                fieldResultStepEntity.getStepId(),
                fieldResultStepEntity.getType(),
                fieldResultStepEntity.getStatus().toString(),
                fieldResultStepEntity.getResultPath());
    }
}
