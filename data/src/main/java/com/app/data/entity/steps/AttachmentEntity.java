package com.app.data.entity.steps;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class AttachmentEntity extends RealmObject {

    @SerializedName("path")
    private String path;
    @SerializedName("type")
    private String type;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
