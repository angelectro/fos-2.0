package com.app.data.entity.mapper;

import com.app.data.entity.steps.ChoiceEntity;
import com.app.domain.model.steps.ChoiceModel;
import com.annimon.stream.function.Function;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;


public class ChoiceEntityToModelMapper implements Function<ChoiceEntity,ChoiceModel> {
    @Inject
    public ChoiceEntityToModelMapper() {}

    @Override
    public ChoiceModel apply(@NonNull ChoiceEntity choiceEntity){
        return new ChoiceModel(choiceEntity.getId(),
                choiceEntity.getStepId(),
                choiceEntity.getX(),
                choiceEntity.getY(),
                choiceEntity.getCols(),
                choiceEntity.getRows(),
                choiceEntity.getColor(),
                choiceEntity.getTitle(),
                choiceEntity.getNextStepId(),
                choiceEntity.isBlack(),
                choiceEntity.getType());
    }
}
