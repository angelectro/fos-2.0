package com.app.data.repository;

import com.app.data.entity.mapper.FieldResultFinishStepEntityToModelMapper;
import com.app.data.entity.mapper.FieldResultInitStepEntityToModelMapper;
import com.app.data.entity.mapper.FieldResultModelToFinishStepMapper;
import com.app.data.entity.mapper.FieldResultModelToInitStepMapper;
import com.app.data.entity.sync.FieldResultFinishStepEntity;
import com.app.data.entity.sync.FieldResultInitStepEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.domain.model.FieldResultModel;
import com.app.domain.model.FieldStatus;
import com.app.domain.repository.FinishStepFieldsRepository;
import com.app.domain.repository.InitStepFieldsRepository;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

@Singleton
public class FinishFieldsRepositoryImpl implements FinishStepFieldsRepository {
    private RealmProvider mRealmProvider;
    private FieldResultFinishStepEntityToModelMapper mMapper;
    private FieldResultModelToFinishStepMapper mFieldResultModelToInitStepMapper;

    @Inject
    public FinishFieldsRepositoryImpl(RealmProvider realmProvider,
                                      FieldResultFinishStepEntityToModelMapper mapper,
                                      FieldResultModelToFinishStepMapper fieldResultModelToInitStepMapper) {
        mRealmProvider = realmProvider;
        mMapper = mapper;
        mFieldResultModelToInitStepMapper = fieldResultModelToInitStepMapper;
    }

    @Override
    public Observable<Boolean> saveFieldsToLocal(List<FieldResultModel> fieldModels) {
        return Observable.fromCallable(() -> {
            List<FieldResultFinishStepEntity> collect = Stream.of(fieldModels)
                    .map(mFieldResultModelToInitStepMapper::apply)
                    .collect(Collectors.toList());
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().copyToRealmOrUpdate(collect);
            mRealmProvider.get().commitTransaction();
            return true;
        });
    }

    @Override
    public Observable<List<FieldResultModel>> getResultModels(int finishStepId) {
        return Observable.fromCallable(() ->
                mRealmProvider.get().where(FieldResultFinishStepEntity.class)
                        .equalTo("finishStepId", finishStepId)
                        .findAll())
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList()
                .toObservable();
    }

    @Override
    public Observable<List<FieldResultModel>> getAll() {
        return Observable.fromCallable(() ->
                mRealmProvider.get().copyFromRealm(
                        mRealmProvider.get().where(FieldResultFinishStepEntity.class).findAll()))
                .subscribeOn(AndroidSchedulers.mainThread())
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList()
                .toObservable();
    }

    @Override
    public Observable<FieldResultModel> update(FieldResultModel fieldResultModel) {
        return Observable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().copyToRealmOrUpdate(mFieldResultModelToInitStepMapper.apply(fieldResultModel));
            mRealmProvider.get().commitTransaction();
            return fieldResultModel;
        });
    }

    @Override
    public Boolean updateStatus(String uuid, FieldStatus status) {
        mRealmProvider.get().beginTransaction();
        FieldResultFinishStepEntity stepEntity = mRealmProvider.get().where(FieldResultFinishStepEntity.class)
                .equalTo("uuid", uuid).findFirst();
        stepEntity.setStatus(status);
        mRealmProvider.get().commitTransaction();
        return true;
    }

    @Override
    public Observable<Boolean> removeAll() {
        return Observable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().where(FieldResultFinishStepEntity.class).findAll().deleteAllFromRealm();
            mRealmProvider.get().commitTransaction();
            return true;
        });
    }
}
