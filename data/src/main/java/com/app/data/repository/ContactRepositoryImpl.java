package com.app.data.repository;

import android.util.Log;

import com.app.data.repository.realm.RealmProvider;
import com.app.domain.model.FieldStatus;
import com.app.domain.model.steps.ChoiceTempModel;
import com.app.domain.model.sync.ChoiceResultEntity;
import com.app.domain.model.sync.ContactEntity;
import com.app.domain.model.sync.FieldResultStepEntity;
import com.app.domain.repository.ContactRepository;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.realm.RealmList;

@Singleton
public class ContactRepositoryImpl implements ContactRepository {

    @Inject
    RealmProvider mRealmProvider;
    private String TAG = "contact";

    @Inject
    public ContactRepositoryImpl() {
    }

    @Override
    public Observable<Boolean> create(Set<ChoiceTempModel> fieldModelList) {
        return Observable.fromCallable(() -> {
            RealmList<ChoiceResultEntity> collect = Stream.of(fieldModelList)
                    .map(stepModel -> new ChoiceResultEntity(stepModel.getId(), Stream.of(stepModel.getFields())
                            .map(fieldModel -> new FieldResultStepEntity(
                                    fieldModel.getUuid(),
                                    fieldModel.getResult(),
                                    stepModel.getId(),
                                    fieldModel.getTypeField(),
                                    null,
                                    FieldStatus.NOTHING
                            )).collect(Collectors.toCollection(RealmList::new)), stepModel.isResultive()))
                    .collect(Collectors.toCollection(RealmList::new));
            mRealmProvider.get().beginTransaction();
            ContactEntity contactEntity = new ContactEntity(collect);
            mRealmProvider.get().copyToRealmOrUpdate(contactEntity);
            mRealmProvider.get().commitTransaction();
            Log.d(TAG, "created contact: " + contactEntity.getId());
            return true;
        });
    }

    @Override
    public Observable<List<ContactEntity>> getAll() {
        return Observable.fromCallable(() -> mRealmProvider.get().copyFromRealm(
                mRealmProvider.get().where(ContactEntity.class).findAll()));
    }

    @Override
    public Observable<ContactEntity> updateContactStatus(ContactEntity contactEntity, boolean isSent) {
        return Observable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().where(ContactEntity.class)
                    .equalTo("id", contactEntity.getId())
                    .findFirst().setSent(isSent);
            mRealmProvider.get().commitTransaction();
            return contactEntity;
        });
    }

    @Override
    public Observable<FieldResultStepEntity> updateField(FieldResultStepEntity fieldResultModel) {
        return Observable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().copyToRealmOrUpdate(fieldResultModel);
            mRealmProvider.get().commitTransaction();
            return fieldResultModel;
        });
    }


    @Override
    public Observable<Boolean> removeAll() {
        return Observable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().where(ContactEntity.class).findAll().deleteAllFromRealm();
            mRealmProvider.get().commitTransaction();
            return true;
        });
    }
}
