package com.app.data.repository;

import com.app.data.entity.auth.AuthorizationRequestBody;
import com.app.data.entity.auth.AuthorizationResponseEntity;
import com.app.data.service.FolService;
import com.app.domain.model.AuthCredentialsModel;
import com.app.domain.model.ProjectModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.AuthorizationRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AuthorizationRepositoryImpl implements AuthorizationRepository {

    private FolService mAuthService;
    private ApplicationPreferences mPreferences;

    @Inject
    public AuthorizationRepositoryImpl(FolService authService, ApplicationPreferences preferences) {
        mAuthService = authService;
        mPreferences = preferences;
    }

    @Override
    public Observable<AuthCredentialsModel> auth(ProjectModel project, String password) {
        return mAuthService.auth(project.getId(), new AuthorizationRequestBody(password))
                .map(AuthorizationResponseEntity::getToken)
                .map(token -> new AuthCredentialsModel(project, token))
                .doOnNext(authCredentialsModel -> mPreferences.authCredentialsModel().set(authCredentialsModel));
    }
}
