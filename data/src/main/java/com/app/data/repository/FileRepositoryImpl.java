package com.app.data.repository;

import com.app.data.entity.sync.UploadFileResponseEntity;
import com.app.data.service.FolService;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.FileRepository;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@Singleton
public class FileRepositoryImpl implements FileRepository {

    private FolService mAuthService;
    private ApplicationPreferences mPreferences;

    @Inject
    public FileRepositoryImpl(FolService authService, ApplicationPreferences preferences) {
        mAuthService = authService;
        mPreferences = preferences;
    }

    @Override
    public Observable<String> uploadFileInitSteps(File file) {
        return mAuthService.uploadFileInitSteps(mPreferences.authCredentialsModel().get().getProjectModel().getId(),
                mPreferences.cityModel().get().getId(),
                buildBody(file))
                .map(UploadFileResponseEntity::getPath);
    }

    @Override
    public Observable<String> uploadFileFinishSteps(File file) {
        return mAuthService.uploadFileFinishSteps(mPreferences.authCredentialsModel().get().getProjectModel().getId(),
                mPreferences.cityModel().get().getId(),
                buildBody(file))
                .map(UploadFileResponseEntity::getPath);
    }

    @Override
    public Observable<String> uploadFileSteps(File file) {
        return mAuthService.uploadFileSteps(mPreferences.authCredentialsModel().get().getProjectModel().getId(),
                mPreferences.cityModel().get().getId(),
                buildBody(file))
                .map(UploadFileResponseEntity::getPath);
    }

    private MultipartBody.Part buildBody(File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData("file", file.getName(), requestFile);
    }
}
