package com.app.data.repository;

import com.app.data.entity.userbreak.BreakEntity;
import com.app.data.entity.userbreak.BreakRequestEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.BreakModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.BreakRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class BreakRepositoryImpl implements BreakRepository {

    @Inject
    protected FolService mFosService;
    @Inject
    protected RealmProvider mRealmProvider;
    @Inject
    protected ApplicationPreferences mApplicationPreferences;

    @Inject
    public BreakRepositoryImpl() {
    }

    @Override
    public Completable createBreak(BreakModel breakModel) {
        return Completable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            BreakEntity breakEntity = new BreakEntity(breakModel.getBeginAt(), breakModel.getEndAt());
            mRealmProvider.get().copyToRealmOrUpdate(breakEntity);
            mRealmProvider.get().commitTransaction();
            return true;
        });
    }

    private void update(BreakEntity breakEntity) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().copyToRealmOrUpdate(breakEntity);
        mRealmProvider.get().commitTransaction();
    }


    @Override
    public Completable sendAll() {
        List<BreakEntity> breakEntities = mRealmProvider.get().copyFromRealm(
                mRealmProvider.get().where(BreakEntity.class).equalTo("isSent", false).findAll());
        int projectId = mApplicationPreferences.authCredentialsModel().get().getProjectModel().getId();
        int scheduleId = mApplicationPreferences.scheduleModel().get().getId();
        if (breakEntities.isEmpty()) {
            return Completable.complete();
        } else {
            return Observable.fromIterable(breakEntities)
                    .observeOn(Schedulers.io())
                    .flatMapCompletable(breakEntity -> {
                        BreakRequestEntity requestEntity = new BreakRequestEntity(breakEntity.getBeginAt(), breakEntity.getEndAt());
                        return mFosService.sendBreak(projectId, scheduleId, requestEntity)
                                .doOnComplete(() -> breakEntity.setSent(true))
                                .doOnComplete(() -> update(breakEntity));
                    });
        }
    }

    @Override
    public Observable<Boolean> removeAll() {
        return Observable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().where(BreakEntity.class).findAll().deleteAllFromRealm();
            mRealmProvider.get().commitTransaction();
            return true;
        });
    }
}
