package com.app.data.repository;

import com.app.data.entity.mapper.ProjectEntityToModelMapper;
import com.app.data.service.FolService;
import com.app.domain.model.ProjectModel;
import com.app.domain.model.ViolationModel;
import com.app.domain.repository.ProjectRepository;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ProjectRepositoryImpl implements ProjectRepository {


    private FolService mAuthService;
    private ProjectEntityToModelMapper mMapper;

    @Inject
    public ProjectRepositoryImpl(FolService authService, ProjectEntityToModelMapper mapper) {
        mAuthService = authService;
        mMapper = mapper;
    }

    @Override
    public Observable<List<ProjectModel>> projects() {
        return mAuthService.getProjects()
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .map(projectEntity -> {
                            Map<ViolationModel.Type, ViolationModel> typeViolationModelMap = projectEntity.getTypeViolationModelMap();
                            if (typeViolationModelMap.isEmpty()) {
                                typeViolationModelMap.put(ViolationModel.Type.DELAY_AT_START, new ViolationModel(false, 1));
                                typeViolationModelMap.put(ViolationModel.Type.PREMATURE_TERMINATION, new ViolationModel(false, 1));
                                typeViolationModelMap.put(ViolationModel.Type.BREAK_IN_THE_FIRST_HOUR, new ViolationModel(false, 1));
                                typeViolationModelMap.put(ViolationModel.Type.BREAK_IN_THE_LAST_HOUR, new ViolationModel(false, 1));
                                typeViolationModelMap.put(ViolationModel.Type.MORE_ON_BREAK_PER_HOUR, new ViolationModel(false, 1));
                                typeViolationModelMap.put(ViolationModel.Type.DELAY_ON_BREAK, new ViolationModel(false, 1));
                                typeViolationModelMap.put(ViolationModel.Type.FALSIFICATION_OF_STATISTICS, new ViolationModel(false, 1));
                                typeViolationModelMap.put(ViolationModel.Type.FALSIFICATION_OF_TIME, new ViolationModel(false, 1));
                            }
                            return projectEntity;
                        }
                )
                .toList()
                .toObservable();
    }
}
