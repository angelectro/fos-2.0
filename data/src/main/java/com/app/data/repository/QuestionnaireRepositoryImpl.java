package com.app.data.repository;

import android.text.TextUtils;

import com.app.data.entity.mapper.QuestionnaireEntityToModelMapper;
import com.app.data.entity.questionnaire.QuestionnaireEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.FieldModel;
import com.app.domain.model.QuestionnaireModel;
import com.app.domain.repository.QuestionnaireRepository;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class QuestionnaireRepositoryImpl extends AbstractRepository<List<QuestionnaireEntity>> implements QuestionnaireRepository {


    private FolService mFosService;
    private RealmProvider mRealmProvider;
    private QuestionnaireEntityToModelMapper mMapper;

    @Inject
    public QuestionnaireRepositoryImpl(FolService fosService,
                                       RealmProvider realmProvider,
                                       QuestionnaireEntityToModelMapper mapper) {
        mFosService = fosService;
        mRealmProvider = realmProvider;
        mMapper = mapper;
    }

    @Override
    void saveToCache(List<QuestionnaireEntity> questionnaireEntities, int idProject) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(QuestionnaireEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(questionnaireEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(QuestionnaireEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    @Override
    Observable<List<QuestionnaireEntity>> getFromCache(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get().copyFromRealm(mRealmProvider.get()
                .where(QuestionnaireEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId).findAll()));
    }

    @Override
    Observable<List<QuestionnaireEntity>> getFromNetwork(int projectId) {
        return mFosService.finishSteps(projectId);
    }

    @Override
    public Observable<List<QuestionnaireModel>> getAll(int projectId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList()
                .toObservable();
    }


    public Observable<Boolean> remove(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .where(QuestionnaireEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll())
                .doOnNext(scheduleEntities -> mRealmProvider.get().beginTransaction())
                .doOnNext(realmResults -> realmResults.deleteAllFromRealm())
                .doOnNext(scheduleEntities -> mRealmProvider.get().commitTransaction())
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .map(QuestionnaireModel::getListFieldModels)
                .flatMap(Observable::fromIterable)
                .filter(fieldModel -> fieldModel.getTypeField().isFile())
                .filter(fieldModel -> !TextUtils.isEmpty(fieldModel.getResult()))
                .map(FieldModel::getResult)
                .map(File::new)
                .filter(File::exists)
                .map(File::delete);
    }

    @Override
    public Observable<QuestionnaireModel> getByOrder(int order, int projectId) {
        return (order == 0 ? getData(projectId) : getFromCache(projectId))
                .flatMap(Observable::fromIterable)
                .filter(value -> value.getOrder() == order)
                .map(mMapper)
                .map(questionnaireModel -> {
                    questionnaireModel.setLast(mRealmProvider.get()
                            .where(QuestionnaireEntity.class)
                            .equalTo(PROJECT_ID_FIELD, projectId)
                            .equalTo("order", order + 1).count() == 0);
                    return questionnaireModel;
                })
                .firstElement()
                .toObservable();
    }

}
