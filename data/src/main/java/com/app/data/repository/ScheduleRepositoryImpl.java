package com.app.data.repository;

import com.app.data.entity.mapper.ScheduleEntityToModelMapper;
import com.app.data.entity.schedule.ScheduleEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.ScheduleModel;
import com.app.domain.repository.ScheduleRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ScheduleRepositoryImpl extends AbstractRepository<List<ScheduleEntity>> implements ScheduleRepository {

    private final FolService mFosService;
    private final ScheduleEntityToModelMapper mMapper;
    private RealmProvider mRealmProvider;

    @Inject
    public ScheduleRepositoryImpl(FolService fosService,
                                  ScheduleEntityToModelMapper mapper,
                                  RealmProvider realmProvider) {
        mFosService = fosService;
        mMapper = mapper;
        mRealmProvider = realmProvider;
    }

    @Override
    void saveToCache(List<ScheduleEntity> scheduleEntities, int idProject) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(ScheduleEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(scheduleEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(ScheduleEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    @Override
    Observable<List<ScheduleEntity>> getFromCache(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get().copyFromRealm(mRealmProvider.get()
                .where(ScheduleEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll()));
    }

    @Override
    Observable<List<ScheduleEntity>> getFromNetwork(int projectId) {
        return mFosService.schedules(projectId);
    }

    @Override
    public Observable<List<ScheduleModel>> getSchedule(int projectId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList().toObservable();
    }

    public Observable<Boolean> remove(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .where(ScheduleEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll())
                .doOnNext(scheduleEntities -> mRealmProvider.get().beginTransaction())
                .map(realmResults -> realmResults.deleteAllFromRealm())
                .doOnNext(scheduleEntities -> mRealmProvider.get().commitTransaction());
    }

    @Override
    public Observable<List<ScheduleModel>> getSchedule(int projectId, int cityId, int promoterId, int supervisorId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .filter(scheduleEntity -> scheduleEntity.getPromouterId() == promoterId)
                .filter(scheduleEntity -> scheduleEntity.getSupervisorId() == supervisorId)
                .filter(scheduleEntity -> scheduleEntity.getOutlet().getCityId() == cityId)
                .map(mMapper)
                .toList().toObservable();
    }

}
