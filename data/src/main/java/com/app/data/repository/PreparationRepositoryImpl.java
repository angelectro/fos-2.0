package com.app.data.repository;

import android.text.TextUtils;

import com.app.data.entity.initsteps.PreparationEntity;
import com.app.data.entity.mapper.PreparationEntityToModelMapper;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.FieldModel;
import com.app.domain.model.PreparationWorkStepModel;
import com.app.domain.repository.PreparationRepository;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class PreparationRepositoryImpl extends AbstractRepository<List<PreparationEntity>> implements PreparationRepository {


    private FolService mFosService;
    private RealmProvider mRealmProvider;
    private PreparationEntityToModelMapper mMapper;

    @Inject
    public PreparationRepositoryImpl(FolService fosService,
                                     RealmProvider realmProvider,
                                     PreparationEntityToModelMapper mapper) {
        mFosService = fosService;
        mRealmProvider = realmProvider;
        mMapper = mapper;
    }

    @Override
    void saveToCache(List<PreparationEntity> preparationEntities, int idProject) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(PreparationEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(preparationEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(PreparationEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    @Override
    Observable<List<PreparationEntity>> getFromCache(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get().copyFromRealm(mRealmProvider.get()
                .where(PreparationEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId).findAll()));
    }

    @Override
    Observable<List<PreparationEntity>> getFromNetwork(int projectId) {
        return mFosService.initSteps(projectId);
    }

    @Override
    public Observable<List<PreparationWorkStepModel>> listPreparation(int projectId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList()
                .toObservable();
    }


    public Observable<Boolean> remove(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .where(PreparationEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll())
                .doOnNext(scheduleEntities -> mRealmProvider.get().beginTransaction())
                .doOnNext(realmResults -> realmResults.deleteAllFromRealm())
                .doOnNext(scheduleEntities -> mRealmProvider.get().commitTransaction())
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .map(PreparationWorkStepModel::getListFieldModels)
                .flatMap(Observable::fromIterable)
                .filter(fieldModel -> fieldModel.getTypeField().isFile())
                .filter(fieldModel -> !TextUtils.isEmpty(fieldModel.getResult()))
                .map(FieldModel::getResult)
                .map(File::new)
                .filter(File::exists)
                .map(File::delete);
    }

    @Override
    public Observable<PreparationWorkStepModel> getByOrder(int order, int projectId) {
        return (order == 0 ? getData(projectId) : getFromCache(projectId))
                .flatMap(Observable::fromIterable)
                .filter(value -> value.getOrder() == order)
                .map(mMapper)
                .map(preparationWorkStepModel -> {
                    preparationWorkStepModel.setLast(mRealmProvider.get()
                            .where(PreparationEntity.class)
                            .equalTo(PROJECT_ID_FIELD, projectId)
                            .equalTo("order", order + 1).count() == 0);
                    return preparationWorkStepModel;
                })
                .firstElement()
                .toObservable();
    }

}
