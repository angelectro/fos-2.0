package com.app.data.repository;

import com.app.data.entity.mapper.SupervisorEntityToModelMapper;
import com.app.data.entity.staff.SupervisorEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.data.utils.RealmInteger;
import com.app.domain.model.SupervisorModel;
import com.app.domain.repository.SupervisorRepository;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class SupervisorRepositoryImpl extends AbstractRepository<List<SupervisorEntity>> implements SupervisorRepository {


    private FolService mFosService;
    private SupervisorEntityToModelMapper mMapper;
    private RealmProvider mRealmProvider;

    @Inject
    public SupervisorRepositoryImpl(FolService fosService,
                                    SupervisorEntityToModelMapper mapper,
                                    RealmProvider realmProvider) {
        mFosService = fosService;
        mMapper = mapper;
        mRealmProvider = realmProvider;
    }

    @Override
    void saveToCache(List<SupervisorEntity> supervisorEntities, int idProject) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(SupervisorEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(supervisorEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(SupervisorEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    public Observable<Boolean> remove(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .where(SupervisorEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll())
                .doOnNext(scheduleEntities -> mRealmProvider.get().beginTransaction())
                .map(realmResults -> realmResults.deleteAllFromRealm())
                .doOnNext(scheduleEntities -> mRealmProvider.get().commitTransaction());
    }

    @Override
    Observable<List<SupervisorEntity>> getFromCache(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .copyFromRealm(mRealmProvider.get()
                        .where(SupervisorEntity.class)
                        .equalTo(PROJECT_ID_FIELD, projectId)
                        .findAll()));
    }

    @Override
    Observable<List<SupervisorEntity>> getFromNetwork(int projectId) {
        return mFosService.supervisors(projectId);
    }


    @Override
    public Observable<List<SupervisorModel>> supervisors(int projectId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList().toObservable();
    }

    @Override
    public Observable<List<SupervisorModel>> supervisorsByCity(int cityId, int projectId) {
        return getFromCache(projectId)
                .flatMap(Observable::fromIterable)
                .filter(supervisorEntity -> Stream.of(supervisorEntity.getCities())
                        .map(RealmInteger::getVal)
                        .anyMatch(value -> value == cityId))
                .map(mMapper)
                .toList().toObservable();
    }
}
