package com.app.data.repository;

import android.util.Log;

import com.app.data.entity.mapper.WorkRequestEntityToModelMapper;
import com.app.data.entity.work.CreateWorkRequestEntity;
import com.app.data.entity.work.UpdateWorkRequestEntity;
import com.app.data.entity.work.WorkStatusEntity;
import com.app.data.entity.work.WorkStatusResponseEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.work.WorkStatus;
import com.app.domain.model.work.WorkStatusModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.WorkRepository;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class WorkRepositoryImpl implements WorkRepository {
    public static final String TAG = "SYNC" + WorkRepository.class.getSimpleName();
    @Inject
    protected FolService mFosService;
    @Inject
    protected RealmProvider mRealmProvider;
    @Inject
    protected ApplicationPreferences mApplicationPreferences;
    @Inject
    protected WorkRequestEntityToModelMapper mMapper;

    @Inject
    public WorkRepositoryImpl() {

    }

    private void update(WorkStatusEntity workStatusEntity) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().copyToRealmOrUpdate(workStatusEntity);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    public void addStatus(WorkStatusModel workStatusModel) {
        Log.d(TAG, "addStatus: " + workStatusModel.getWorkStatus());
        mRealmProvider.get().beginTransaction();
        WorkStatusEntity workStatusEntity = new WorkStatusEntity(workStatusModel.getTime(), workStatusModel.getWorkStatus().getValue());
        mRealmProvider.get().copyToRealmOrUpdate(workStatusEntity);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    public Observable<Boolean> sendAll() {
        Log.d(TAG, "sendAll: ");
        List<WorkStatusEntity> workStatusEntities = mRealmProvider.get().copyFromRealm(
                mRealmProvider.get().where(WorkStatusEntity.class).equalTo("isSent", false).findAllSorted("time"));

        if (workStatusEntities.isEmpty()) {
            Log.d(TAG, "sendAll: empty");
            return Observable.just(true);
        }

        int projectId = mApplicationPreferences.authCredentialsModel().get().getProjectModel().getId();
        int scheduleId = mApplicationPreferences.scheduleModel().get().getId();
        return Observable.fromIterable(workStatusEntities)
                .filter(workStatusEntity -> !workStatusEntity.isSent())
                .observeOn(Schedulers.io())
                .flatMap(workStatusEntity -> {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                    Observable<WorkStatusResponseEntity> observable = mApplicationPreferences.workModel().getOptional()
                            .map(workModel -> {
                                UpdateWorkRequestEntity entity;
                                if (workStatusEntity.getStatus() == WorkStatus.FINISHED_WORK.getValue())
                                    entity = new UpdateWorkRequestEntity(simpleDateFormat.format(workStatusEntity.getTime()), workStatusEntity.getStatus());
                                else
                                    entity = new UpdateWorkRequestEntity(workStatusEntity.getStatus());
                                return mFosService.updateWork(projectId,
                                        scheduleId,
                                        workModel.getId(),
                                        entity);
                            })
                            .orElse(mFosService.createWork(projectId,
                                    scheduleId,
                                    new CreateWorkRequestEntity(simpleDateFormat.format(workStatusEntity.getTime())))
                                    .doOnNext(entity -> mApplicationPreferences.workModel().set(mMapper.apply(entity))));
                    return observable
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnNext(workStatusResponseEntity -> workStatusEntity.setSent(true))
                            .doOnNext(workStatusResponseEntity -> update(workStatusEntity));
                })
                .toList()
                .toObservable()
                .map(__ -> true);
    }

    @Override
    public void removeAll() {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(WorkStatusEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().commitTransaction();
        mApplicationPreferences.workModel().set(null);
    }
}
