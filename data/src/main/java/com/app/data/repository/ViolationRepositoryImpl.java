package com.app.data.repository;

import android.util.Log;

import com.app.data.entity.sync.ViolationRequestEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.ViolationModel;
import com.app.domain.model.sync.ViolationEntity;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.ViolationRepository;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class ViolationRepositoryImpl implements ViolationRepository {
    public static final String TAG = "SYNC" + ViolationRepository.class.getSimpleName();

    @Inject
    RealmProvider realmProvider;
    @Inject
    ApplicationPreferences mApplicationPreferences;
    @Inject
    FolService mFolService;

    @Inject
    public ViolationRepositoryImpl() {
    }

    @Override
    public Single<ViolationEntity> createViolation(int id, Date date) {
        return Single.fromCallable(() -> {
            realmProvider.get().beginTransaction();
            ViolationEntity violationEntity = new ViolationEntity(id, date);
            realmProvider.get().copyToRealmOrUpdate(violationEntity);
            realmProvider.get().commitTransaction();
            return violationEntity;
        });
    }

    @Override
    public Observable<List<ViolationEntity>> getAll() {
        return Observable.fromCallable(() -> realmProvider.get().copyFromRealm(realmProvider.get().where(ViolationEntity.class).equalTo("isSent", false).findAll()));
    }

    @Override
    public Single<ViolationEntity> update(ViolationEntity violationEntity) {
        return Single.fromCallable(() -> {
            realmProvider.get().beginTransaction();
            realmProvider.get().copyToRealmOrUpdate(violationEntity);
            realmProvider.get().commitTransaction();
            return violationEntity;
        });
    }

    @Override
    public Optional<ViolationEntity> getLastViolation(ViolationModel.Type type) {
        return Stream.of(realmProvider.get().copyFromRealm(realmProvider.get().where(ViolationEntity.class).findAll()))
                .filter(violationEntity -> violationEntity.getIdViolation() == type.getId())
                .sorted((o1, o2) -> o2.getDate().compareTo(o1.getDate()))
                .map(violationEntity -> violationEntity)
                .findFirst();
    }

    @Override
    public void removeAll() {
        realmProvider.get().beginTransaction();
        realmProvider.get().where(ViolationEntity.class).findAll().deleteAllFromRealm();
        realmProvider.get().commitTransaction();
    }

    @Override
    public Observable<Boolean> sendAll() {
        Log.d(TAG, "sendAll: ");
        List<ViolationEntity> violationEntities = realmProvider.get().copyFromRealm(realmProvider.get().where(ViolationEntity.class).equalTo("isSent", false).findAll());

        if (violationEntities.isEmpty()) {
            return Observable.just(true);
        }

        int projectId = mApplicationPreferences.authCredentialsModel().get().getProjectModel().getId();
        int scheduleId = mApplicationPreferences.scheduleModel().get().getId();
        return getAll()
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(Observable::fromIterable)
                .filter(ViolationEntity::isNotSent)
                .flatMap(violationEntity -> mFolService.createViolation(projectId,
                        new ViolationRequestEntity(violationEntity.getIdViolation(),
                                scheduleId,
                                violationEntity.getDate()))
                        .map(o -> violationEntity))
                .doOnNext(violationEntity -> violationEntity.setSent(true))
                .flatMap(violationEntity -> update(violationEntity).toObservable())
                .toList()
                .toObservable()
                .map(__ -> true);
    }
}
