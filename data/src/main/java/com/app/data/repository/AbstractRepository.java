package com.app.data.repository;

import io.reactivex.Observable;

public abstract class AbstractRepository<T> {

    public static final String PROJECT_ID_FIELD = "projectId";

    abstract void saveToCache(T t, int idProject);

    abstract boolean isCached(int projectId);

    abstract Observable<T> getFromCache(int projectId);

    abstract Observable<T> getFromNetwork(int projectId);


    public Observable<T> getData(int projectId) {
        return getFromNetwork(projectId)
                .onErrorResumeNext(throwable -> {
                    if (isCached(projectId))
                        return getFromCache(projectId);
                    throw new Exception("not cashed");
                })
                .doOnNext((t) -> saveToCache(t, projectId));
    }
}
