package com.app.data.repository;

import com.app.data.entity.mapper.CityEntityToModelMapper;
import com.app.data.entity.staff.CityEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.CityModel;
import com.app.domain.repository.CityRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class CityRepositoryImpl extends AbstractRepository<List<CityEntity>> implements CityRepository {
    private FolService mFosService;
    private CityEntityToModelMapper mMapper;
    private RealmProvider mRealmProvider;

    @Inject
    public CityRepositoryImpl(FolService fosService, CityEntityToModelMapper mapper, RealmProvider realmProvider) {
        mFosService = fosService;
        mMapper = mapper;
        mRealmProvider = realmProvider;
    }

    @Override
    public Observable<List<CityModel>> getCities(int projectId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList()
                .toObservable();
    }

    @Override
    public void saveToCache(List<CityEntity> cityEntities, int projectId) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(CityEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(cityEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    public Observable<Boolean> remove(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .where(CityEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll())
                .doOnNext(scheduleEntities -> mRealmProvider.get().beginTransaction())
                .map(realmResults -> realmResults.deleteAllFromRealm())
                .doOnNext(scheduleEntities -> mRealmProvider.get().commitTransaction());
    }

    @Override
    public boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(CityEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    @Override
    public Observable<List<CityEntity>> getFromCache(int projectId) {
        List<CityEntity> cityEntities = mRealmProvider.get()
                .copyFromRealm(mRealmProvider.get()
                        .where(CityEntity.class)
                        .equalTo(PROJECT_ID_FIELD, projectId)
                        .findAll());
        return Observable.fromCallable(() -> cityEntities);
    }

    @Override
    Observable<List<CityEntity>> getFromNetwork(int projectId) {
        return mFosService.cities(projectId);
    }

}
