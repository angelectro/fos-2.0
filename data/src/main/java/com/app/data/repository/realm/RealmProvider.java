package com.app.data.repository.realm;

import io.realm.Realm;

public interface RealmProvider {
    Realm get();
}