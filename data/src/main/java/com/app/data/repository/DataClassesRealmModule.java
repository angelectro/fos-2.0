package com.app.data.repository;

import io.realm.annotations.RealmModule;

@RealmModule(library = true, allClasses = true)
public class DataClassesRealmModule {

}
