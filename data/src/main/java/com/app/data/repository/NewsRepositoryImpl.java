package com.app.data.repository;

import android.content.Context;

import com.app.data.entity.mapper.NewsEntityToModelMapper;
import com.app.data.entity.news.NewsEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.news.NewsModel;
import com.app.domain.repository.NewsRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class NewsRepositoryImpl extends AbstractRepository<List<NewsEntity>> implements NewsRepository {

    private RealmProvider mRealmProvider;
    private Context mContext;
    private FolService mFosService;
    private NewsEntityToModelMapper mMapper;

    @Inject
    public NewsRepositoryImpl(RealmProvider realmProvider,
                              Context context,
                              FolService fosService,
                              NewsEntityToModelMapper mapper) {
        mRealmProvider = realmProvider;
        mContext = context;
        mFosService = fosService;
        mMapper = mapper;
    }

    @Override
    void saveToCache(List<NewsEntity> newsEntities, int idProject) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(NewsEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(newsEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(NewsEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    @Override
    Observable<List<NewsEntity>> getFromCache(int projectId) {
        return Observable.fromCallable(() ->
                mRealmProvider.get().copyFromRealm(mRealmProvider.get()
                        .where(NewsEntity.class)
                        .equalTo(PROJECT_ID_FIELD, projectId)
                        .findAll()));
    }

    @Override
    Observable<List<NewsEntity>> getFromNetwork(int projectId) {
        return mFosService.getNews(projectId);
    }

    @Override
    public Observable<List<NewsModel>> getNews(int projectId) {
        Observable<List<NewsModel>> fromCache = getFromCache(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList()
                .toObservable();
        Observable<List<NewsModel>> fromNetwork = getFromNetwork(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList()
                .toObservable();
        return Observable.merge(fromCache, fromNetwork);
    }

    @Override
    public Observable<Boolean> removeAll() {
        return Observable.fromCallable(() -> {
            mRealmProvider.get().beginTransaction();
            mRealmProvider.get().where(NewsEntity.class).findAll().deleteAllFromRealm();
            mRealmProvider.get().commitTransaction();
            return true;
        });
    }
}
