package com.app.data.repository;

import android.content.Context;

import com.app.data.entity.mapper.StepEntityToModelMapper;
import com.app.data.entity.steps.StepEntity;
import com.app.data.entity.work.WorkStatusEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.domain.model.steps.AttachmentModel;
import com.app.domain.model.steps.StepModel;
import com.app.domain.repository.StepsRepository;
import com.app.domain.utils.FileUtils;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class StepsRepositoryImpl extends AbstractRepository<List<StepEntity>> implements StepsRepository {

    private RealmProvider mRealmProvider;
    private Context mContext;
    private FolService mFosService;
    private StepEntityToModelMapper mMapper;

    @Inject
    public StepsRepositoryImpl(RealmProvider realmProvider,
                               Context context,
                               FolService fosService,
                               StepEntityToModelMapper mapper) {
        mRealmProvider = realmProvider;
        mContext = context;
        mFosService = fosService;
        mMapper = mapper;
    }

    @Override
    void saveToCache(List<StepEntity> stepEntities, int idProject) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(StepEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(stepEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(StepEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    @Override
    Observable<List<StepEntity>> getFromCache(int projectId) {
        return Observable.fromCallable(() ->
                mRealmProvider.get().copyFromRealm(mRealmProvider.get()
                        .where(StepEntity.class)
                        .equalTo(PROJECT_ID_FIELD, projectId)
                        .findAll()));
    }

    @Override
    Observable<List<StepEntity>> getFromNetwork(int projectId) {
        return mFosService.steps(projectId);
    }

    @Override
    public Observable<List<StepModel>> listSteps(int projectId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList().toObservable();
    }

    @Override
    public Single<StepModel> getStepById(int stepId, int projectId) {
        return Observable.fromCallable(() ->
                mRealmProvider.get().copyFromRealm(mRealmProvider.get()
                        .where(StepEntity.class)
                        .equalTo(PROJECT_ID_FIELD, projectId)
                        .equalTo("id", stepId)
                        .findFirst()))
                .subscribeOn(Schedulers.io())
                .map(mMapper)
                .firstOrError();
    }

    public Observable<Boolean> remove(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .where(StepEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll())
                .doOnNext(scheduleEntities -> mRealmProvider.get().beginTransaction())
                .doOnNext(realmResults -> realmResults.deleteAllFromRealm())
                .doOnNext(scheduleEntities -> mRealmProvider.get().commitTransaction())
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .filter(stepModel -> stepModel.getAttachment() != null)
                .map(StepModel::getAttachment)
                .map(AttachmentModel::getPath)
                .map(path -> FileUtils.getFileForProject(mContext, String.valueOf(projectId), FileUtils.getFileNameFromUrl(path)))
                .filter(File::exists)
                .map(File::delete);
    }

    @Override
    public Observable<StepModel> getFirstStep(int projectId) {
        Observable<List<StepEntity>> listObservable = isCached(projectId) ? getFromCache(projectId) : getData(projectId);
        return listObservable
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .filter((stepModel) -> stepModel.isFirst() || stepModel.isVisibleRegistrationButton())
                .firstElement()
                .toObservable();
    }

    @Override
    public Observable<Boolean> removeAll() {
       return Observable.fromCallable(() -> {
           mRealmProvider.get().beginTransaction();
           mRealmProvider.get().where(StepEntity.class).findAll().deleteAllFromRealm();
           mRealmProvider.get().commitTransaction();
            return true;
        });
    }

}
