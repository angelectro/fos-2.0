package com.app.data.repository;

import com.app.data.entity.mapper.PromoterEntityToModelMapper;
import com.app.data.entity.staff.PromoterEntity;
import com.app.data.repository.realm.RealmProvider;
import com.app.data.service.FolService;
import com.app.data.utils.RealmInteger;
import com.app.domain.model.PromoterModel;
import com.app.domain.repository.PromoterRepository;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class PromoterRepositoryImpl extends AbstractRepository<List<PromoterEntity>> implements PromoterRepository {


    private FolService mFosService;
    private PromoterEntityToModelMapper mMapper;
    private RealmProvider mRealmProvider;

    @Inject
    public PromoterRepositoryImpl(FolService fosService,
                                  PromoterEntityToModelMapper mapper,
                                  RealmProvider realmProvider) {
        mFosService = fosService;
        mMapper = mapper;
        mRealmProvider = realmProvider;
    }

    @Override
    void saveToCache(List<PromoterEntity> promoterEntities, int idProject) {
        mRealmProvider.get().beginTransaction();
        mRealmProvider.get().where(PromoterEntity.class).findAll().deleteAllFromRealm();
        mRealmProvider.get().copyToRealmOrUpdate(promoterEntities);
        mRealmProvider.get().commitTransaction();
    }

    @Override
    boolean isCached(int projectId) {
        return mRealmProvider.get()
                .where(PromoterEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .count() != 0;
    }

    @Override
    Observable<List<PromoterEntity>> getFromCache(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .copyFromRealm(mRealmProvider.get()
                        .where(PromoterEntity.class)
                        .equalTo(PROJECT_ID_FIELD, projectId)
                        .findAll()));
    }

    @Override
    Observable<List<PromoterEntity>> getFromNetwork(int projectId) {
        return mFosService.promoters(projectId);
    }

    public Observable<Boolean> remove(int projectId) {
        return Observable.fromCallable(() -> mRealmProvider.get()
                .where(PromoterEntity.class)
                .equalTo(PROJECT_ID_FIELD, projectId)
                .findAll())
                .doOnNext(scheduleEntities -> mRealmProvider.get().beginTransaction())
                .map(realmResults -> realmResults.deleteAllFromRealm())
                .doOnNext(scheduleEntities -> mRealmProvider.get().commitTransaction());
    }

    @Override
    public Observable<List<PromoterModel>> promoters(int projectId) {
        return getData(projectId)
                .flatMap(Observable::fromIterable)
                .map(mMapper)
                .toList().toObservable();
    }

    @Override
    public Observable<List<PromoterModel>> promotersByCity(int cityId, int projectId) {
        return getFromCache(projectId)
                .flatMap(Observable::fromIterable)
                .filter(promoterEntity -> Stream.of(promoterEntity.getCities())
                        .map(RealmInteger::getVal)
                        .anyMatch(value -> value == cityId))
                .map(mMapper)
                .toList().toObservable();
    }

}
