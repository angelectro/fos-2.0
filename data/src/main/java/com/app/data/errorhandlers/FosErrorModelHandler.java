package com.app.data.errorhandlers;

import com.app.data.entity.ErrorResponseEntity;
import com.app.data.exceptions.UnauthentizedUserException;
import com.app.data.exceptions.UnauthorizedException;
import com.app.domain.model.ErrorModel;
import com.app.domain.usecase.internal.ErrorModelHandler;
import com.annimon.stream.Optional;

import retrofit2.HttpException;
import retrofit2.Response;

public class FosErrorModelHandler extends ErrorModelHandler {

    public final Optional<ErrorModel> fetchError(Throwable throwable) {
        return Optional.of(throwable)
                .filter(ex -> ex instanceof HttpException)
                .map(ex -> ((HttpException) ex))
                .map(ex -> ((HttpException) ex).response())
                .flatMap(response -> {
                    try {
                        return Optional.of(response.errorBody().string())
                                .map(it -> gson.fromJson(it, ErrorResponseEntity.class))
                                .map(errorResponseEntity -> new ErrorModel(errorResponseEntity.getError(), Optional.ofNullable(getSuitableException(response)).orElse(throwable)));
                    } catch (Exception ex) {
                        return Optional.empty();
                    }
                });
    }

    private Throwable getSuitableException(Response response) {
        switch (response.code()) {
            case 401:
                return new UnauthorizedException();
            case 403:
                return new UnauthentizedUserException();
            default:
                return null;
        }
    }

}