package com.app.data.service;


import com.app.data.entity.auth.AuthorizationRequestBody;
import com.app.data.entity.auth.AuthorizationResponseEntity;
import com.app.data.entity.initsteps.PreparationEntity;
import com.app.data.entity.news.NewsEntity;
import com.app.data.entity.project.ProjectEntity;
import com.app.data.entity.questionnaire.QuestionnaireEntity;
import com.app.data.entity.schedule.ScheduleEntity;
import com.app.data.entity.staff.CityEntity;
import com.app.data.entity.staff.PromoterEntity;
import com.app.data.entity.staff.SupervisorEntity;
import com.app.data.entity.steps.StepEntity;
import com.app.data.entity.sync.ContactRequestEntity;
import com.app.data.entity.sync.FieldResultFinishRequestEntity;
import com.app.data.entity.sync.FieldResultInitStepRequestEntity;
import com.app.data.entity.sync.UploadFileResponseEntity;
import com.app.data.entity.sync.ViolationRequestEntity;
import com.app.data.entity.userbreak.BreakRequestEntity;
import com.app.data.entity.work.CreateWorkRequestEntity;
import com.app.data.entity.work.UpdateWorkRequestEntity;
import com.app.data.entity.work.WorkStatusResponseEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FolService {

    @GET("mobile/projects")
    Observable<List<ProjectEntity>> getProjects();

    @POST("mobile/projects/{id}/auth")
    Observable<AuthorizationResponseEntity> auth(@Path("id") int projectId, @Body AuthorizationRequestBody authorizationRequestBody);

    @GET("mobile/projects/{id}/cities")
    Observable<List<CityEntity>> cities(@Path("id") int projectId);

    @GET("mobile/projects/{id}/promouters")
    Observable<List<PromoterEntity>> promoters(@Path("id") int projectId);

    @GET("mobile/projects/{id}/supervisors")
    Observable<List<SupervisorEntity>> supervisors(@Path("id") int projectId);

    @GET("mobile/projects/{id}/work-schedules")
    Observable<List<ScheduleEntity>> schedules(@Path("id") int projectId);

    @GET("mobile/projects/{id}/init-steps")
    Observable<List<PreparationEntity>> initSteps(@Path("id") int projectId);

    @GET("mobile/projects/{id}/finish-steps")
    Observable<List<QuestionnaireEntity>> finishSteps(@Path("id") int projectId);

    @GET("mobile/projects/{id}/steps    ")
    Observable<List<StepEntity>> steps(@Path("id") int projectId);

    @Multipart
    @POST("mobile/projects/{id}/steps/upload")
    Observable<UploadFileResponseEntity> uploadFileSteps(@Path("id") int projectId,
                                                         @Query("city_id") int cityId,
                                                         @Part MultipartBody.Part file);

    @Multipart
    @POST("mobile/projects/{id}/init-steps/upload")
    Observable<UploadFileResponseEntity> uploadFileInitSteps(@Path("id") int projectId,
                                                             @Query("city_id") int cityId,
                                                             @Part MultipartBody.Part file);

    @Multipart
    @POST("mobile/projects/{id}/finish-steps/upload")
    Observable<UploadFileResponseEntity> uploadFileFinishSteps(@Path("id") int projectId,
                                                             @Query("city_id") int cityId,
                                                             @Part MultipartBody.Part file);

    @POST("mobile/projects/{id}/work-schedules/{schedule_id}/init-data")
    Observable<Object> sendInitStepsData(@Path("id") int projectId,
                                         @Path("schedule_id") int scheduleId,
                                         @Body List<FieldResultInitStepRequestEntity> entities);

    @POST("mobile/projects/{id}/work-schedules/{schedule_id}/finish-data")
    Observable<Object> sendFinishStepsData(@Path("id") int projectId,
                                         @Path("schedule_id") int scheduleId,
                                         @Body List<FieldResultFinishRequestEntity> entities);

    @POST("mobile/projects/{id}/work-schedules/{schedule_id}/create-contact")
    Observable<Object> sendContactData(@Path("id") int projectId,
                                       @Path("schedule_id") int scheduleId,
                                       @Body ContactRequestEntity entity);

    @POST("mobile/projects/{id}/violations/")
    Observable<Object> createViolation(@Path("id") int projectId,
                                       @Body ViolationRequestEntity entity);

    @POST("mobile/projects/{id}/work-schedules/{schedule_id}/breaks/")
    Completable sendBreak(
            @Path("id") int projectId,
            @Path("schedule_id") int scheduleId,
            @Body BreakRequestEntity entity);

    @POST("mobile/projects/{id}/work-schedules/{schedule_id}/works/")
    Observable<WorkStatusResponseEntity> createWork(
            @Path("id") int projectId,
            @Path("schedule_id") int scheduleId,
            @Body CreateWorkRequestEntity entity);

    @PUT("mobile/projects/{id}/work-schedules/{schedule_id}/works/{work_id}")
    Observable<WorkStatusResponseEntity> updateWork(
            @Path("id") int projectId,
            @Path("schedule_id") int scheduleId,
            @Path("work_id") int workId,
            @Body UpdateWorkRequestEntity entity);

    @GET("/mobile/projects/{id}/news/")
    Observable<List<NewsEntity>> getNews(@Path("id") int projectId);
}
