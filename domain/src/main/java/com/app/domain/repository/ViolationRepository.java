package com.app.domain.repository;

import com.app.domain.model.ViolationModel;
import com.app.domain.model.sync.ViolationEntity;
import com.annimon.stream.Optional;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface ViolationRepository {

    Single<ViolationEntity> createViolation(int id, Date date);
    Observable<List<ViolationEntity>> getAll();
    Single<ViolationEntity> update(ViolationEntity violationEntity);
    Optional<ViolationEntity> getLastViolation(ViolationModel.Type type);
    void removeAll();
    Observable<Boolean> sendAll();
}
