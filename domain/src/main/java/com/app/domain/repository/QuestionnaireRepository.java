package com.app.domain.repository;

import com.app.domain.model.PreparationWorkStepModel;
import com.app.domain.model.QuestionnaireModel;

import java.util.List;

import io.reactivex.Observable;

public interface QuestionnaireRepository extends BaseRepository{

    Observable<List<QuestionnaireModel>> getAll(int projectId);
    Observable<QuestionnaireModel> getByOrder(int order, int projectId);

}
