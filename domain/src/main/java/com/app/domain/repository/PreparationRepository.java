package com.app.domain.repository;

import com.app.domain.model.PreparationWorkStepModel;
import com.annimon.stream.Optional;

import java.util.List;

import io.reactivex.Observable;

public interface PreparationRepository extends BaseRepository{

    Observable<List<PreparationWorkStepModel>> listPreparation(int projectId);
    Observable<PreparationWorkStepModel> getByOrder(int order, int projectId);

}
