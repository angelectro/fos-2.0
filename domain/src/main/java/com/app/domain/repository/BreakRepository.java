package com.app.domain.repository;

import com.app.domain.model.BreakModel;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface BreakRepository {
    Completable createBreak(BreakModel breakModel);
    Completable sendAll();

    Observable<Boolean> removeAll();
}
