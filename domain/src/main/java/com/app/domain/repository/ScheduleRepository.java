package com.app.domain.repository;

import com.app.domain.model.ScheduleModel;

import java.util.List;

import io.reactivex.Observable;

public interface ScheduleRepository extends BaseRepository{
    Observable<List<ScheduleModel>> getSchedule(int projectId);
    Observable<List<ScheduleModel>> getSchedule(int projectId, int cityId, int promoterId, int supervisorId);
}
