package com.app.domain.repository;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.FieldStatus;

import java.util.List;

import io.reactivex.Observable;

public interface FinishStepFieldsRepository {
    Observable<Boolean> saveFieldsToLocal(List<FieldResultModel> fieldModels);

    Observable<List<FieldResultModel>> getResultModels(int initStepId);

    Observable<List<FieldResultModel>> getAll();

    Observable<FieldResultModel> update(FieldResultModel fieldResultModel);

    Boolean updateStatus(String uuid, FieldStatus status);

    Observable<Boolean> removeAll();
}
