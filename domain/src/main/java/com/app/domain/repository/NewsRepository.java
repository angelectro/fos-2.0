package com.app.domain.repository;

import com.app.domain.model.news.NewsModel;

import java.util.List;

import io.reactivex.Observable;

public interface NewsRepository {
    Observable<List<NewsModel>> getNews(int projectId);

    Observable<Boolean> removeAll();
}
