package com.app.domain.repository;

import com.app.domain.model.PromoterModel;

import java.util.List;

import io.reactivex.Observable;

public interface PromoterRepository extends BaseRepository {
    Observable<List<PromoterModel>> promoters(int projectId);

    Observable<List<PromoterModel>> promotersByCity(int id, int projectId);
}
