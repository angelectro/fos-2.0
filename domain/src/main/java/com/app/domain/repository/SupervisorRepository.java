package com.app.domain.repository;

import com.app.domain.model.CityModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.SupervisorModel;

import java.util.List;

import io.reactivex.Observable;

public interface SupervisorRepository extends BaseRepository{
    Observable<List<SupervisorModel>> supervisors(int projectId);

    Observable<List<SupervisorModel>> supervisorsByCity(int id, int projectId);
}
