package com.app.domain.repository;

import com.app.domain.model.steps.StepModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface StepsRepository extends BaseRepository{
    Observable<List<StepModel>> listSteps(int projectId);
    Single<StepModel> getStepById(int stepId, int projectId);
    Observable<StepModel> getFirstStep(int projectId);
    Observable<Boolean> removeAll();
}
