package com.app.domain.repository;

import com.app.domain.model.steps.ChoiceTempModel;
import com.app.domain.model.sync.ContactEntity;
import com.app.domain.model.sync.FieldResultStepEntity;

import java.util.List;
import java.util.Set;

import io.reactivex.Observable;

public interface ContactRepository {
    Observable<Boolean> create(Set<ChoiceTempModel> fieldModelList);

    Observable<List<ContactEntity>> getAll();

    Observable<ContactEntity> updateContactStatus(ContactEntity contactEntity, boolean isSent);

    Observable<FieldResultStepEntity> updateField(FieldResultStepEntity entity);

    Observable<Boolean> removeAll();
}
