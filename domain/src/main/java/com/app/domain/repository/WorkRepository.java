package com.app.domain.repository;

import com.app.domain.model.work.WorkStatusModel;

import io.reactivex.Observable;

public interface WorkRepository {

    void addStatus(WorkStatusModel workStatusModel);

    Observable<Boolean> sendAll();

    void removeAll();
}
