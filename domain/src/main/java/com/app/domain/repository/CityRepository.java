package com.app.domain.repository;

import com.app.domain.model.CityModel;

import java.util.List;

import io.reactivex.Observable;

public interface CityRepository extends BaseRepository{
    Observable<List<CityModel>> getCities(int projectId);
}
