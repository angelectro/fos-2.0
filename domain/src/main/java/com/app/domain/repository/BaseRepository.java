package com.app.domain.repository;

import io.reactivex.Observable;

public interface BaseRepository{
    Observable<Boolean> remove(int projectId);
}
