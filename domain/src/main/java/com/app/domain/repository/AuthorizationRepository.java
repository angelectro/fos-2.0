package com.app.domain.repository;

import com.app.domain.model.AuthCredentialsModel;
import com.app.domain.model.ProjectModel;

import io.reactivex.Observable;


public interface AuthorizationRepository {

    Observable<AuthCredentialsModel> auth(ProjectModel project, String password);
}
