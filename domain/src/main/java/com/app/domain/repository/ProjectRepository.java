package com.app.domain.repository;

import com.app.domain.model.ProjectModel;

import java.util.List;

import io.reactivex.Observable;

public interface ProjectRepository {
    Observable<List<ProjectModel>> projects();
}
