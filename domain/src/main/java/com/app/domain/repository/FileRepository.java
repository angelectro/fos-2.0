package com.app.domain.repository;

import java.io.File;

import io.reactivex.Observable;

public interface FileRepository {

    Observable<String> uploadFileInitSteps(File file);
    Observable<String> uploadFileFinishSteps(File file);
    Observable<String> uploadFileSteps(File file);
}
