package com.app.domain.model.steps;

import com.app.domain.model.FieldModel;

import java.util.List;

public class StepModel {
    private Integer id;
    private String description;
    private boolean resultive;
    private List<FieldModel> fields;
    private AttachmentModel attachment;
    private Integer rows;
    private Integer cols;
    private Integer projectId;
    private boolean visibleRegistrationButton;
    private boolean isFirst;
    private List<ChoiceModel> choices;

    public StepModel(Integer id, String description, boolean resultive, List<FieldModel> fields, AttachmentModel attachment, Integer rows, Integer cols, Integer projectId, List<ChoiceModel> choices, boolean visibleRegistrationButton, boolean isFirst) {
        this.id = id;
        this.description = description;
        this.resultive = resultive;
        this.fields = fields;
        this.attachment = attachment;
        this.rows = rows;
        this.cols = cols;
        this.projectId = projectId;
        this.choices = choices;
        this.visibleRegistrationButton = visibleRegistrationButton;
        this.isFirst = isFirst;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public boolean isVisibleRegistrationButton() {
        return visibleRegistrationButton;
    }

    public void setVisibleRegistrationButton(boolean visibleRegistrationButton) {
        this.visibleRegistrationButton = visibleRegistrationButton;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isResultive() {
        return resultive;
    }

    public void setResultive(boolean resultive) {
        this.resultive = resultive;
    }

    public List<FieldModel> getFields() {
        return fields;
    }

    public void setFields(List<FieldModel> fields) {
        this.fields = fields;
    }

    public AttachmentModel getAttachment() {
        return attachment;
    }

    public void setAttachment(AttachmentModel attachment) {
        this.attachment = attachment;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getCols() {
        return cols;
    }

    public void setCols(Integer cols) {
        this.cols = cols;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public List<ChoiceModel> getChoices() {
        return choices;
    }

    public void setChoices(List<ChoiceModel> choices) {
        this.choices = choices;
    }
}
