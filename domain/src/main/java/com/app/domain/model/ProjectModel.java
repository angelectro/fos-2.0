package com.app.domain.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

public class ProjectModel implements Parcelable {
    public static final Creator<ProjectModel> CREATOR = new Creator<ProjectModel>() {
        @Override
        public ProjectModel createFromParcel(Parcel in) {
            return new ProjectModel(in);
        }

        @Override
        public ProjectModel[] newArray(int size) {
            return new ProjectModel[size];
        }
    };
    private int id;
    private String name;
    private boolean archived;
    private String backgroundPortrait;
    private String backgroundLand;
    private String brandColor;
    private String description;
    private String brandLogo;
    private Map<ViolationModel.Type, ViolationModel> mTypeViolationModelMap;
    private String targetAudience;
    private String help;
    private String contactStructure;

    private Integer contactRegistrationTimeout;

    public ProjectModel(int id, String name, String backgroundPortrait, String backgroundLand, String brandColor, String description, String brandLogo, boolean archived, Map<ViolationModel.Type, ViolationModel> typeViolationModelMap, String targetAudience, String help, String contactStructure, Integer contactRegistrationTimeout) {
        this.id = id;
        this.name = name;
        this.backgroundPortrait = backgroundPortrait;
        this.backgroundLand = backgroundLand;
        this.brandColor = brandColor;
        this.description = description;
        this.brandLogo = brandLogo;
        this.archived = archived;
        mTypeViolationModelMap = typeViolationModelMap;
        this.targetAudience = targetAudience;
        this.help = help;
        this.contactStructure = contactStructure;
        this.contactRegistrationTimeout = contactRegistrationTimeout;
    }

    public Integer getContactRegistrationTimeout() {
        return contactRegistrationTimeout;
    }

    protected ProjectModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        backgroundPortrait = in.readString();
        backgroundLand = in.readString();
        brandColor = in.readString();
        description = in.readString();
        archived = in.readByte() == 1;
    }

    public String getContactStructure() {
        return contactStructure;
    }

    public String getTargetAudience() {
        return targetAudience;
    }

    public void setTargetAudience(String targetAudience) {
        this.targetAudience = targetAudience;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackgroundPortrait() {
        return backgroundPortrait;
    }

    public void setBackgroundPortrait(String backgroundPortrait) {
        this.backgroundPortrait = backgroundPortrait;
    }

    public String getBackgroundLand() {
        return backgroundLand;
    }

    public void setBackgroundLand(String backgroundLand) {
        this.backgroundLand = backgroundLand;
    }

    public String getBrandColor() {
        return brandColor;
    }

    public void setBrandColor(String brandColor) {
        this.brandColor = brandColor;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(backgroundPortrait);
        parcel.writeString(backgroundLand);
        parcel.writeString(brandColor);
        parcel.writeString(description);
        parcel.writeByte((byte) (archived ? 1 : 0));
    }

    public Map<ViolationModel.Type, ViolationModel> getTypeViolationModelMap() {
        return mTypeViolationModelMap;
    }
}
