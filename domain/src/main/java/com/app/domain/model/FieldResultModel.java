package com.app.domain.model;

public class FieldResultModel {
    private String uuid;
    private String result;
    private String resultPath;
    private int stepId;
    private TypeField type;
    private String status;

    public FieldResultModel(String uuid, String result, int stepId, TypeField type, String status, String resultPath) {
        this.uuid = uuid;
        this.result = result;
        this.stepId = stepId;
        this.type = type;
        this.status = status;
        this.resultPath = resultPath;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public TypeField getType() {
        return type;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isFile() {
        return type.isFile();
    }

    public FieldStatus getStatus() {
        return FieldStatus.valueOf(status);
    }

    public void setStatus(FieldStatus status) {
        this.status = status.toString();
    }

}
