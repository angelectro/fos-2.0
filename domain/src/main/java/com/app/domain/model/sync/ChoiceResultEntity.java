package com.app.domain.model.sync;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ChoiceResultEntity extends RealmObject {

    private int choiceId;
    private boolean resultive;
    private RealmList<FieldResultStepEntity> mFieldResultStepEntities;

    public ChoiceResultEntity() {
    }

    public ChoiceResultEntity(int choiceId, RealmList<FieldResultStepEntity> fieldResultStepEntities, boolean resultive) {

        this.choiceId = choiceId;
        mFieldResultStepEntities = fieldResultStepEntities;
        this.resultive = resultive;
    }

    public boolean isResultive() {
        return resultive;
    }

    public int getChoiceId() {

        return choiceId;
    }

    public void setChoiceId(int choiceId) {
        this.choiceId = choiceId;
    }

    public RealmList<FieldResultStepEntity> getFieldResultStepEntities() {
        return mFieldResultStepEntities;
    }

    public void setFieldResultStepEntities(RealmList<FieldResultStepEntity> fieldResultStepEntities) {
        mFieldResultStepEntities = fieldResultStepEntities;
    }
}
