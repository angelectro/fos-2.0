package com.app.domain.model;

import java.util.List;

public class StaffModel {
    private List<PromoterModel> mPromoterModels;
    private List<SupervisorModel> mSupervisorModels;
    private List<CityModel> mCityModels;

    public StaffModel(List<PromoterModel> promoterModels, List<SupervisorModel> supervisorModels, List<CityModel> cityModels) {

        mPromoterModels = promoterModels;
        mSupervisorModels = supervisorModels;
        mCityModels = cityModels;
    }

    public List<PromoterModel> getPromoterModels() {
        return mPromoterModels;
    }

    public List<SupervisorModel> getSupervisorModels() {
        return mSupervisorModels;
    }

    public List<CityModel> getCityModels() {
        return mCityModels;
    }
}
