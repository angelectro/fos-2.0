package com.app.domain.model.steps;

import com.app.domain.model.FieldModel;

import java.util.List;

public class ChoiceTempModel {
    private int id;
    private List<FieldModel> fields;

    public boolean isResultive() {
        return isResultive;
    }

    private boolean isResultive;

    public ChoiceTempModel(int id, List<FieldModel> fields, boolean isResultive) {
        this.id = id;
        this.fields = fields;
        this.isResultive = isResultive;
    }

    public int getId() {
        return id;
    }

    public List<FieldModel> getFields() {
        return fields;
    }
}
