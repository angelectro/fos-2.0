package com.app.domain.model;

import com.annimon.stream.Stream;

public enum TypeField {
    HEADER,
    PHOTO,
    VIDEO,
    AUDIO,
    TEXTAREA,
    SELECT,
    INPUT,
    GROUP_CHECKBOX,
    CHECKBOX,
    GROUP,
    NUMBER,
    FOOTER,
    QR,
    SIGN;


    public boolean isFile() {
        return Stream.of(AUDIO, VIDEO, PHOTO, SIGN).anyMatch(value -> value == this);
    }
}
