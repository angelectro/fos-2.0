package com.app.domain.model.steps;

public class AttachmentModel {

    private String path;
    private TypeAttachment type;

    public AttachmentModel(String path, TypeAttachment type) {
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public TypeAttachment getType() {

        return type;
    }

    public void setType(TypeAttachment type) {
        this.type = type;
    }
}
