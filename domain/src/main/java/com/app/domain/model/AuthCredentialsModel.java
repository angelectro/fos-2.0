package com.app.domain.model;

public class AuthCredentialsModel {

    private final ProjectModel projectModel;
    private final String password;

    public AuthCredentialsModel(ProjectModel projectModel, String password) {
        this.projectModel = projectModel;
        this.password = password;
    }



    public String getPassword() {
        return password;
    }

    public ProjectModel getProjectModel() {

        return projectModel;
    }

}

