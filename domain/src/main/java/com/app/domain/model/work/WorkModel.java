package com.app.domain.model.work;

public class WorkModel {

    private Integer id;
    private String beginAt;
    private String endAt;
    private String status;
    private String workScheduleId;
    private Boolean online;

    public WorkModel(Integer id, String beginAt, String endAt, String status, String workScheduleId, Boolean online) {

        this.id = id;
        this.beginAt = beginAt;
        this.endAt = endAt;
        this.status = status;
        this.workScheduleId = workScheduleId;
        this.online = online;
    }

    public Integer getId() {
        return id;
    }

    public String getBeginAt() {
        return beginAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public String getStatus() {
        return status;
    }

    public String getWorkScheduleId() {
        return workScheduleId;
    }

    public Boolean getOnline() {
        return online;
    }
}
