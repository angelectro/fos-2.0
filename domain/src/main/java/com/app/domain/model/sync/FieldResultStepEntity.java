package com.app.domain.model.sync;

import com.app.domain.model.FieldStatus;
import com.app.domain.model.TypeField;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FieldResultStepEntity extends RealmObject {
    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private String uuid;
    private String result;
    private String resultPath;
    private int stepId;
    private String type;
    private String status;

    public FieldResultStepEntity() {
    }

    public FieldResultStepEntity(String uuid, String result, int stepId, TypeField type, String resultPath, FieldStatus status) {
        this.uuid = uuid;
        this.result = result;
        this.resultPath = resultPath;
        this.stepId = stepId;
        this.type = type.toString();
        this.status = status.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public TypeField getType() {
        return TypeField.valueOf(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FieldStatus getStatus() {
        return FieldStatus.valueOf(status);
    }

    public void setStatus(FieldStatus status) {
        this.status = status.toString();
    }

    public boolean isFile() {
        return getType().isFile();
    }
}
