package com.app.domain.model.steps;

public class ActionResult {
    private boolean isContainsEvent;
    private Event event;
    private String[] info;
    private boolean isShowDialog;

    private ActionResult(boolean isCompleted, Event event, boolean isShowDialog) {
        this.isContainsEvent = isCompleted;
        this.event = event;
        this.isShowDialog = isShowDialog;

    }

    public ActionResult(boolean isContainsEvent, Event event, boolean isShowDialog, String[] info) {
        this.isContainsEvent = isContainsEvent;
        this.event = event;
        this.isShowDialog = isShowDialog;
        this.info = info;
    }

    private ActionResult(boolean isContainsEvent) {
        this.isContainsEvent = isContainsEvent;
    }

    public static ActionResult createResultWithoutEvent() {
        return new ActionResult(false);
    }

    public static ActionResult createResult(Event event) {
        return new ActionResult(true, event, true);
    }

    public static ActionResult createResult(Event event, boolean isShowDialog) {
        return new ActionResult(true, event, isShowDialog);
    }

    public static ActionResult createResult(Event event, String... info) {
        return new ActionResult(true, event, true, info);
    }

    public boolean isShowDialog() {
        return isShowDialog;
    }

    public String[] getInfo() {
        return info;
    }

    public boolean isContainsEvent() {
        return isContainsEvent;
    }

    public void setContainsEvent(boolean containsEvent) {
        isContainsEvent = containsEvent;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public enum Event {
        PREMATURE_TERMINATION_WARNING,
        PREMATURE_TERMINATION_VIOLATION,
        CONFIRMATION_END_DAY,
        BREAK_IN_FIRST_HOUR_WARNING,
        BREAK_IN_FIRST_HOUR_VIOLATION,
        BREAK_IN_LAST_HOUR_WARNING,
        BREAK_IN_LAST_HOUR_VIOLATION,
        MORE_ON_BREAK_PER_HOUR_WARNING,
        MORE_ON_BREAK_PER_HOUR_VIOLATION,
        DELAY_ON_BREAK_VIOLATION,
        FALSIFICATION_OF_STATISTICS_VIOLATION,
        FALSIFICATION_OF_TIME_VIOLATION;
    }
}
