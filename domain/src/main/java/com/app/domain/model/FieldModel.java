package com.app.domain.model;

import java.util.ArrayList;
import java.util.List;

public class FieldModel {
    private String uuid;
    private String name;
    private String description;
    private TypeField typeField;
    private boolean required;
    private boolean emptyField;
    private int order;
    private List<SubFieldModel> listPreparationSubFields;
    private String result;
    private Object data;

    public FieldModel(TypeField typeField) {
        this.typeField = typeField;
    }

    public FieldModel(String uuid, String name, String description, TypeField typeField, int order, List<SubFieldModel> listPreparationSubFields, boolean required) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.typeField = typeField;
        this.order = order;
        this.listPreparationSubFields = listPreparationSubFields;
        this.required = required;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FieldModel that = (FieldModel) o;

        return uuid != null ? uuid.equals(that.uuid) : that.uuid == null;
    }

    @Override
    public int hashCode() {
        return uuid != null ? uuid.hashCode() : 0;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isEmptyField() {
        return emptyField;
    }

    public void setEmptyField(boolean emptyField) {
        this.emptyField = emptyField;
    }

    public boolean isRequired() {
        return required;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {

        return name;
    }

    public String getDescription() {
        return description;
    }

    public TypeField getTypeField() {
        return typeField;
    }

    public int getOrder() {
        return order;
    }

    public List<SubFieldModel> getListPreparationSubFields() {
        if (listPreparationSubFields != null)
            return listPreparationSubFields;
        return listPreparationSubFields = new ArrayList<>();
    }

    public void setListPreparationSubFields(List<SubFieldModel> listPreparationSubFields) {
        this.listPreparationSubFields = listPreparationSubFields;
    }
}
