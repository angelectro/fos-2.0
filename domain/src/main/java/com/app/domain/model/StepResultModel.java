package com.app.domain.model;

import java.util.List;

public class StepResultModel {
    private int stepId;
    private List<FieldResultModel> mFieldResultModels;

    public StepResultModel(int stepId, List<FieldResultModel> fieldResultModels) {

        this.stepId = stepId;
        mFieldResultModels = fieldResultModels;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public List<FieldResultModel> getFieldResultModels() {
        return mFieldResultModels;
    }

    public void setFieldResultModels(List<FieldResultModel> fieldResultModels) {
        mFieldResultModels = fieldResultModels;
    }
}
