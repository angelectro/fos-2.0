package com.app.domain.model;

import io.realm.annotations.RealmModule;

@RealmModule(library = true, allClasses = true)
public class DomainClassesRealmModule {

}
