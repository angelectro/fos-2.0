package com.app.domain.model.steps;

public class ChoiceModel {
    private int id;
    private Integer stepId;
    private Integer x;
    private Integer y;
    private Integer cols;
    private Integer rows;
    private String color;
    private String title;
    private Integer nextStepId;
    private boolean isBlack;
    private int type;

    public ChoiceModel(Integer id, Integer stepId, Integer x, Integer y, Integer cols, Integer rows, String color, String title, Integer nextStepId, boolean isBlack, int type) {
        this.id = id;
        this.stepId = stepId;
        this.x = x;
        this.y = y;
        this.cols = cols;
        this.rows = rows;
        this.color = color;
        this.title = title;
        this.nextStepId = nextStepId;
        this.isBlack = isBlack;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public boolean isBlack() {
        return isBlack;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getCols() {
        return cols;
    }

    public void setCols(Integer cols) {
        this.cols = cols;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getNextStepId() {
        return nextStepId;
    }

    public void setNextStepId(Integer nextStepId) {
        this.nextStepId = nextStepId;
    }

    //Тип плитки (type) бывает: 0 - промежуточный 1 - результативный 2 - нерезультативный
    public boolean isResultive() {
        return type == 1 || type == 2 || nextStepId == null;
    }

}
