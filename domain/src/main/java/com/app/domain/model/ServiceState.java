package com.app.domain.model;

public enum ServiceState {
    RUNNING,
    FINISHED_WITH_ERROR,
    FINISHED_WITH_SUCCESS,
    NO_RUNNING
}
