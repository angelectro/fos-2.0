package com.app.domain.model.sync;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ContactEntity extends RealmObject {
    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private RealmList<ChoiceResultEntity> mStepResultEntities;
    private boolean isSent;
    private Date createTime;

    public ContactEntity() {
    }

    public ContactEntity(RealmList<ChoiceResultEntity> fieldResultStepEntities) {
        mStepResultEntities = fieldResultStepEntities;
        createTime = new Date();

        isSent = false;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        this.isSent = sent;
    }

    public boolean isNotSent() {
        return !isSent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RealmList<ChoiceResultEntity> getStepResultEntities() {
        return mStepResultEntities;
    }

    public void setStepResultEntities(RealmList<ChoiceResultEntity> stepResultEntities) {
        mStepResultEntities = stepResultEntities;
    }
}
