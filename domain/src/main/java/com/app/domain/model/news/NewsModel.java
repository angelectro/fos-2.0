package com.app.domain.model.news;


import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewsModel implements Comparable<NewsModel> {

    private Integer id;
    private String text;
    private String date;
    private Integer projectId;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public NewsModel(Integer id, String text, String date, Integer projectId) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.projectId = projectId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDateAsText() {
        return date;
    }

    public Date getDate() {
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException();
        }
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @Override
    public int compareTo(@NonNull NewsModel o) {
        try {
            return simpleDateFormat.parse(o.getDateAsText()).compareTo(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            return 0;
        }
    }
}
