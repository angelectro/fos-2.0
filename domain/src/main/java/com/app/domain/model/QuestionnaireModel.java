package com.app.domain.model;


import java.util.List;

public class QuestionnaireModel {
    private int id;
    private String name;
    private int projectId;
    private int order;
    private boolean isLast;
    private List<FieldModel> listFieldModels;

    public QuestionnaireModel(int id, String name, int projectId, int order, List<FieldModel> listFieldModels) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.order = order;
        this.listFieldModels = listFieldModels;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getProjectId() {
        return projectId;
    }

    public int getOrder() {
        return order;
    }

    public List<FieldModel> getListFieldModels() {
        return listFieldModels;
    }
}
