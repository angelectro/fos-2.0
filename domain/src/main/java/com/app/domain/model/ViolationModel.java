package com.app.domain.model;

import com.annimon.stream.Stream;

public class ViolationModel {
    private Boolean active;
    private Integer id;
    private ValueModel value;

    public ViolationModel(Boolean active, Integer id) {
        this.active = active;
        this.id = id;
    }

    public ViolationModel(Boolean active, Integer id, ValueModel value) {
        this.active = active;
        this.id = id;
        this.value = value;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ValueModel getValue() {
        return value;
    }

    public void setValue(ValueModel value) {
        this.value = value;
    }

    public enum Type {
        DELAY_AT_START(1),              //Опоздание на старт
        PREMATURE_TERMINATION(2),       //Преждевременное завершение рабочего дня
        BREAK_IN_THE_FIRST_HOUR(3),     //Перерыв в первый час работы
        BREAK_IN_THE_LAST_HOUR(4),      //Перерыв в последний час работы
        MORE_ON_BREAK_PER_HOUR(5),      //Более одного перерыва в час
        DELAY_ON_BREAK(6),              //Задержка на перерыве
        FALSIFICATION_OF_STATISTICS(7), //Фальсификация по статистике
        FALSIFICATION_OF_TIME(8),       //Фальсификация по времени
        INACTION(9);                    //Простой todo

        int id;

        public int getId() {
            return id;
        }

        Type(int id) {
            this.id = id;
        }

        public static Type getById(int id) {
            return Stream.of(values())
                    .filter(value1 -> value1.id == id)
                    .findFirst().orElse(null);
        }
    }
}

