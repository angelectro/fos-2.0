package com.app.domain.model.sync;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ViolationEntity extends RealmObject {
    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private int idViolation;
    private Date date;
    private boolean isSent;

    public ViolationEntity() {
    }

    public ViolationEntity(int idViolation, Date date) {
        this.idViolation = idViolation;
        this.date = date;
        isSent = false;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSent() {
        return isSent;
    }
    public boolean isNotSent() {
        return !isSent;
    }
    public void setSent(boolean sent) {
        isSent = sent;
    }

    public int getIdViolation() {
        return idViolation;
    }

    public void setIdViolation(int idViolation) {
        this.idViolation = idViolation;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
