package com.app.domain.model.work;

public enum WorkStatus {
    BEGIN_PREPARATION(1),
    COMPLETE_PREPARATION(2),
    WORKS(3),
    IN_BREAK(4),
    FINISHED_WORK(5);

    private int value;

    WorkStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
