package com.app.domain.model.work;


import java.util.Date;

public class WorkStatusModel {
    private Date time;
    private WorkStatus workStatus;

    private WorkStatusModel(Date time, WorkStatus workStatus) {
        this.time = time;
        this.workStatus = workStatus;
    }

    public static WorkStatusModel create(WorkStatus workStatus) {
        return new WorkStatusModel(new Date(), workStatus);

    }

    public Date getTime() {
        return time;
    }

    public WorkStatus getWorkStatus() {
        return workStatus;
    }
}
