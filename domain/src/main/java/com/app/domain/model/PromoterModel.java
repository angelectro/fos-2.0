package com.app.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;


public class PromoterModel implements Parcelable, Comparable {
    public static final Creator<PromoterModel> CREATOR = new Creator<PromoterModel>() {
        @Override
        public PromoterModel createFromParcel(Parcel in) {
            return new PromoterModel(in);
        }

        @Override
        public PromoterModel[] newArray(int size) {
            return new PromoterModel[size];
        }
    };
    int id;
    String fullName;

    public PromoterModel(int id, String fullName) {
        this.id = id;
        this.fullName = fullName;
    }

    protected PromoterModel(Parcel in) {
        id = in.readInt();
        fullName = in.readString();
    }

    @Override
    public String toString() {
        return fullName;
    }

    public int getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(fullName);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        PromoterModel promoterModel = (PromoterModel) o;
        return fullName.toLowerCase().compareTo(promoterModel.getFullName().toLowerCase());
    }
}
