package com.app.domain.model.steps;

public enum TypeAttachment {
    VIDEO("video"),
    PHOTO("photo"),
    DOC("doc");

    private String type;

    TypeAttachment(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
