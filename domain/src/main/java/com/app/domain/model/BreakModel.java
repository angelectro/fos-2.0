package com.app.domain.model;

public class BreakModel {
    private String beginAt;
    private String endAt;

    public BreakModel(String beginAt, String endAt) {

        this.beginAt = beginAt;
        this.endAt = endAt;
    }

    public String getBeginAt() {
        return beginAt;
    }

    public String getEndAt() {
        return endAt;
    }
}
