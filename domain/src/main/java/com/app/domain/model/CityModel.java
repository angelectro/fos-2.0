package com.app.domain.model;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class CityModel implements Parcelable ,Comparable{
    public static final Creator<CityModel> CREATOR = new Creator<CityModel>() {
        @Override
        public CityModel createFromParcel(Parcel in) {
            return new CityModel(in);
        }

        @Override
        public CityModel[] newArray(int size) {
            return new CityModel[size];
        }
    };
    private int id;
    private String name;
    private int projectId;

    public CityModel(int id, String name, int projectId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
    }

    protected CityModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        projectId = in.readInt();
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getProjectId() {
        return projectId;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeInt(projectId);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        CityModel cityModel = (CityModel) o;
        return name.toLowerCase().compareTo(cityModel.getName().toLowerCase());
    }
}
