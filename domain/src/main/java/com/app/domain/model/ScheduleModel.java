package com.app.domain.model;


public class ScheduleModel {
    private Integer id;
    private String date;
    private String beginAt;
    private String endAt;
    private String retainChain;
    private Integer retainChainId;
    private Integer cityId;
    private String address;
    private Integer outletId;
    private Integer promoterId;
    private Integer supervisorId;
    private Integer projectId;

    public ScheduleModel(Integer id, String date, String beginAt, String endAt, String retainChain, Integer retainChainId, Integer cityId, String address, Integer outletId, Integer promouterId, Integer supervisorId, Integer projectId) {
        this.id = id;
        this.date = date;
        this.beginAt = beginAt;
        this.endAt = endAt;
        this.retainChain = retainChain;
        this.retainChainId = retainChainId;
        this.cityId = cityId;
        this.address = address;
        this.outletId = outletId;
        this.promoterId = promouterId;
        this.supervisorId = supervisorId;
        this.projectId = projectId;
    }

    public Integer getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getBeginAt() {
        return beginAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public Integer getRetainChainId() {
        return retainChainId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public Integer getOutletId() {
        return outletId;
    }

    public Integer getPromoterId() {
        return promoterId;
    }

    public Integer getSupervisorId() {
        return supervisorId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public String getRetainChain() {

        return retainChain;
    }

    public String getAddress() {
        return address;
    }
}
