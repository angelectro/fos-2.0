package com.app.domain.model;

public class SubFieldModel {
    private int id;
    private String data;
    private int order;

    public SubFieldModel(int id, String data, int order) {
        this.id = id;
        this.data = data;
        this.order = order;
    }

    public SubFieldModel(String data, int order) {
        this.data = data;
        this.order = order;
    }

    @Override
    public String toString() {
        return data;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
