package com.app.domain.model;

public enum FieldStatus {
    UPLOADED,
    UPLOADING,
    NOTHING,
    SENT,
    SENDING
}
