package com.app.domain.model;

public  class ValueModel {
    private Integer limit;
    private Integer minutes;
    private Integer materials;

    public ValueModel(Integer limit, Integer minutes, Integer materials) {
        this.limit = limit;
        this.minutes = minutes;
        this.materials = materials;
    }

    public Integer getLimit() {
        return limit;
    }

    public Integer getMinutes() {
        return minutes;
    }


    public Integer getMaterials() {
        return materials;
    }

}
