package com.app.domain.preferences.internal;


import android.support.annotation.Nullable;

import com.app.domain.model.AuthCredentialsModel;
import com.app.domain.model.CityModel;
import com.app.domain.model.ProgressState;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.ScheduleModel;
import com.app.domain.model.SupervisorModel;
import com.app.domain.model.steps.ChoiceTempModel;
import com.app.domain.model.work.WorkModel;
import com.orhanobut.hawk.Hawk;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ApplicationPreferencesImpl implements ApplicationPreferences {
    private static final String AUTH_CREDENTIALS = "AUTH_CREDENTIALS";
    private static final String CITY_MODEL = "CITY_MODEL";
    private static final String PROGRESS_STATE = "PROGRESS_STATE";
    private static final String PROMOTER_MODEL = "PROMOTER_MODEL";
    private static final String WORK_MODEL = "WORK_MODEL";
    private static final String SUPERVISOR_MODEL = "SUPERVISOR_MODEL";
    private static final String RETAIN_CHAIN_MODEL = "RETAIN_CHAIN_MODEL";
    private static final String TEMP_CONTACT_DATA = "TEMP_CONTACT_DATA";
    private static final String ONLINE_STATE = "ONLINE_STATE";
    private static final String BREAK_TIME = "BREAK_TIME";
    private static final String BREAK_STATE = "BREAK_STATE";
    private static final String NOTIFICATION_SUBSCRIBE = "NOTIFICATION_SUBSCRIBE";
    private static final String LAST_CLICKED_DATE = "LAST_CLICKED_DATE";

    @Override
    public void clearPreferences() {
        Hawk.delete(AUTH_CREDENTIALS);
        Hawk.delete(CITY_MODEL);
        Hawk.delete(PROGRESS_STATE);
        Hawk.delete(PROMOTER_MODEL);
        Hawk.delete(WORK_MODEL);
        Hawk.delete(SUPERVISOR_MODEL);
        Hawk.delete(RETAIN_CHAIN_MODEL);
        Hawk.delete(TEMP_CONTACT_DATA);
        Hawk.delete(ONLINE_STATE);
        Hawk.delete(BREAK_TIME);
        Hawk.delete(BREAK_STATE);
        Hawk.delete(LAST_CLICKED_DATE);
    }

    @Override
    public ObjectPref<AuthCredentialsModel> authCredentialsModel() {
        return new ObjectPref<AuthCredentialsModel>() {
            @Override
            public AuthCredentialsModel get() {
                return Hawk.get(AUTH_CREDENTIALS);
            }

            @Override
            public void set(@Nullable AuthCredentialsModel value) {
                Hawk.put(AUTH_CREDENTIALS, value);
            }
        };
    }

    @Override
    public ObjectPref<CityModel> cityModel() {
        return new ObjectPref<CityModel>() {
            @Override
            public CityModel get() {
                return Hawk.get(CITY_MODEL);
            }

            @Override
            public void set(@Nullable CityModel value) {
                Hawk.put(CITY_MODEL, value);
            }
        };
    }

    @Override
    public ObjectPref<PromoterModel> promoterModel() {
        return new ObjectPref<PromoterModel>() {
            @Override
            public PromoterModel get() {
                return Hawk.get(PROMOTER_MODEL);
            }

            @Override
            public void set(@Nullable PromoterModel value) {
                Hawk.put(PROMOTER_MODEL, value);
            }
        };
    }

    @Override
    public ObjectPref<SupervisorModel> supervisorModel() {
        return new ObjectPref<SupervisorModel>() {
            @Override
            public SupervisorModel get() {
                return Hawk.get(SUPERVISOR_MODEL);
            }

            @Override
            public void set(@Nullable SupervisorModel value) {
                Hawk.put(SUPERVISOR_MODEL, value);
            }
        };
    }

    @Override
    public ObjectPref<ScheduleModel> scheduleModel() {
        return new ObjectPref<ScheduleModel>() {
            @Override
            public ScheduleModel get() {
                return Hawk.get(RETAIN_CHAIN_MODEL);
            }

            @Override
            public void set(@Nullable ScheduleModel value) {
                Hawk.put(RETAIN_CHAIN_MODEL, value);
            }
        };
    }

    @Override
    public ObjectPref<ProgressState> progressState() {
        return new ObjectPref<ProgressState>() {
            @Override
            public ProgressState get() {
                return Hawk.get(PROGRESS_STATE, ProgressState.SELECTION_PROJECT);
            }

            @Override
            public void set(@Nullable ProgressState value) {
                Hawk.put(PROGRESS_STATE, value);
            }
        };
    }

    @Override
    public ObjectPref<Boolean> onlineState() {
        return new ObjectPref<Boolean>() {
            @Override
            public Boolean get() {
                return Hawk.get(ONLINE_STATE, true);
            }

            @Override
            public void set(@Nullable Boolean value) {
                Hawk.put(ONLINE_STATE, value);
            }
        };
    }

    @Override
    public ObjectPref<Boolean> breakState() {
        return new ObjectPref<Boolean>() {
            @Override
            public Boolean get() {
                return Hawk.get(BREAK_STATE, false);
            }

            @Override
            public void set(@Nullable Boolean value) {
                Hawk.put(BREAK_STATE, value);
            }
        };
    }

    @Override
    public ObjectPref<Date> breakTime() {
        return new ObjectPref<Date>() {
            @Override
            public Date get() {
                return Hawk.get(BREAK_TIME);
            }

            @Override
            public void set(@Nullable Date value) {
                Hawk.put(BREAK_TIME, value);
            }
        };
    }

    @Override
    public ObjectPref<Set<ChoiceTempModel>> tempContactData() {
        return new ObjectPref<Set<ChoiceTempModel>>() {
            @Override
            public Set<ChoiceTempModel> get() {
                return Hawk.get(TEMP_CONTACT_DATA, new HashSet<>());
            }

            @Override
            public void set(@Nullable Set<ChoiceTempModel> value) {
                Hawk.put(TEMP_CONTACT_DATA, value);
            }
        };
    }

    @Override
    public ObjectPref<WorkModel> workModel() {
        return new ObjectPref<WorkModel>() {
            @Override
            public WorkModel get() {
                return Hawk.get(WORK_MODEL);
            }

            @Override
            public void set(@Nullable WorkModel value) {
                Hawk.put(WORK_MODEL, value);
            }
        };
    }

    @Override
    public ObjectPref<String> notificationSubscribe() {
        return new ObjectPref<String>() {
            @Override
            public String get() {
                return Hawk.get(NOTIFICATION_SUBSCRIBE);
            }

            @Override
            public void set(@Nullable String value) {
                Hawk.put(NOTIFICATION_SUBSCRIBE, value);
            }
        };
    }

    @Override
    public ObjectPref<Date> lastDateClickedRegistrationButton() {
        return new ObjectPref<Date>() {
            @Override
            public Date get() {
                return Hawk.get(LAST_CLICKED_DATE, null);
            }

            @Override
            public void set(@Nullable Date value) {
                Hawk.put(LAST_CLICKED_DATE, value);
            }
        };
    }


}
