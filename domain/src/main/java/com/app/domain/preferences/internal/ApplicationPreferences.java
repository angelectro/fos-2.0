package com.app.domain.preferences.internal;


import com.app.domain.model.AuthCredentialsModel;
import com.app.domain.model.ProgressState;
import com.app.domain.model.CityModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.ScheduleModel;
import com.app.domain.model.SupervisorModel;
import com.app.domain.model.steps.ChoiceTempModel;
import com.app.domain.model.work.WorkModel;

import java.util.Date;
import java.util.Set;

public interface ApplicationPreferences {
    ObjectPref<AuthCredentialsModel> authCredentialsModel();
    ObjectPref<CityModel> cityModel();
    ObjectPref<PromoterModel> promoterModel();
    ObjectPref<SupervisorModel> supervisorModel();
    ObjectPref<ScheduleModel> scheduleModel();
    ObjectPref<ProgressState> progressState();
    ObjectPref<Boolean> onlineState();
    ObjectPref<Boolean> breakState();
    ObjectPref<Date> breakTime();
    ObjectPref<Set<ChoiceTempModel>> tempContactData();
    ObjectPref<WorkModel> workModel();
    ObjectPref<String> notificationSubscribe();
    ObjectPref<Date> lastDateClickedRegistrationButton();
    void clearPreferences();
}
