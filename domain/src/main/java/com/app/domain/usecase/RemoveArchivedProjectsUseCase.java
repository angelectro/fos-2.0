package com.app.domain.usecase;

import android.content.Context;

import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.BaseRepository;
import com.app.domain.repository.CityRepository;
import com.app.domain.repository.PreparationRepository;
import com.app.domain.repository.PromoterRepository;
import com.app.domain.repository.ScheduleRepository;
import com.app.domain.repository.StepsRepository;
import com.app.domain.repository.SupervisorRepository;
import com.app.domain.usecase.internal.UseCase;
import com.app.domain.utils.FileUtils;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class RemoveArchivedProjectsUseCase extends UseCase<Boolean> {

    List<Integer> projectIds;
    List<BaseRepository> mBaseRepositories = new ArrayList<>();
    List<Observable<Boolean>> mObservables = new ArrayList<>();
    private Context mContext;
    private PreparationRepository mPreparationRepository;
    private StepsRepository mStepsRepository;

    @Inject
    public RemoveArchivedProjectsUseCase(Scheduler scheduler,
                                         Context context,
                                         ApplicationPreferences applicationPreferences,
                                         CityRepository cityRepository,
                                         PreparationRepository preparationRepository,
                                         PromoterRepository promoterRepository,
                                         SupervisorRepository supervisorRepository,
                                         ScheduleRepository scheduleRepository,
                                         StepsRepository stepsRepository) {
        super(scheduler);
        mContext = context;
        mPreparationRepository = preparationRepository;
        mStepsRepository = stepsRepository;
        mBaseRepositories.add(cityRepository);
        mBaseRepositories.add(preparationRepository);
        mBaseRepositories.add(promoterRepository);
        mBaseRepositories.add(scheduleRepository);
        mBaseRepositories.add(supervisorRepository);
        mBaseRepositories.add(stepsRepository);
        mBaseRepositories.add(projectId -> Observable.fromCallable(() -> FileUtils.removeDirectory(FileUtils.getProjectDirectoryPath(mContext, String.valueOf(projectId)))));
        // добавь в список mBaseRepositories репозиторий, данные из которой хочешь удалить и все будет тип-топ
    }

    public RemoveArchivedProjectsUseCase setProjectIds(List<Integer> projectIds) {
        this.projectIds = projectIds;
        for (Integer projectId : projectIds) {
            Stream.of(mBaseRepositories)
                    .map(baseRepository -> baseRepository.remove(projectId))
                    .forEach(mObservables::add);
        }
        return this;
    }

    @Override
    protected Observable<Boolean> buildUseCaseObservable() {
        return Observable.concat(mObservables)
                .all(Boolean::booleanValue)
                .toObservable();
    }
}
