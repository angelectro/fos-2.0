package com.app.domain.usecase;

import com.app.domain.model.ViolationModel;
import com.app.domain.model.sync.ChoiceResultEntity;
import com.app.domain.model.sync.ViolationEntity;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.ContactRepository;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.usecase.internal.UseCase;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class CheckFalsificationsUseCase extends UseCase<List<ViolationModel.Type>> {
    private ContactRepository mContactRepository;
    private ViolationRepository mViolationRepository;
    private ApplicationPreferences mPreferences;

    @Inject
    public CheckFalsificationsUseCase(Scheduler scheduler,
                                      ContactRepository contactRepository,
                                      ViolationRepository violationRepository,
                                      ApplicationPreferences preferences) {
        super(scheduler);
        mContactRepository = contactRepository;
        mViolationRepository = violationRepository;
        mPreferences = preferences;
    }

    @Override
    public Observable<List<ViolationModel.Type>> buildUseCaseObservable() {
        return Observable.concat(checkFalsificationOfStatistics(), checkFalsificationOfTime())
                .flatMap(type -> mViolationRepository.createViolation(type.getId(), new Date())
                        .toObservable().map(o -> type))
                .toList()
                .toObservable();
    }

    private Observable<ViolationModel.Type> checkFalsificationOfTime() {
        ViolationModel violationModel = mPreferences.authCredentialsModel().get().getProjectModel().getTypeViolationModelMap().get(ViolationModel.Type.FALSIFICATION_OF_TIME);
        if (!violationModel.getActive())
            return Observable.empty();
        Integer maxMaterials = violationModel.getValue().getMaterials();
        Integer minutes = violationModel.getValue().getMinutes();
        Optional<ViolationEntity> lastViolation = mViolationRepository.getLastViolation(ViolationModel.Type.FALSIFICATION_OF_TIME);
        return mContactRepository.getAll()
                .flatMap(Observable::fromIterable)
                .filter(contactEntity -> lastViolation.map(violationEntity ->
                        contactEntity.getCreateTime().after(violationEntity.getDate())).orElse(true))
                .toList()
                .toObservable()
                .filter(contactEntities -> contactEntities.size() > maxMaterials)
                .flatMap(Observable::fromIterable)
                .sorted((o1, o2) -> o1.getCreateTime().compareTo(o2.getCreateTime()))
                .takeLast(maxMaterials + 1)
                .toList()
                .filter(contactEntities -> !contactEntities.isEmpty())
                .filter(contactEntities -> contactEntities.get(contactEntities.size() - 1).getCreateTime().getTime() - contactEntities.get(0).getCreateTime().getTime() <= TimeUnit.MINUTES.toMillis(minutes))
                .map(contactEntities -> ViolationModel.Type.FALSIFICATION_OF_TIME)
                .toObservable();
    }

    private Observable<ViolationModel.Type> checkFalsificationOfStatistics() {
        ViolationModel violationModel = mPreferences.authCredentialsModel().get().getProjectModel().getTypeViolationModelMap().get(ViolationModel.Type.FALSIFICATION_OF_STATISTICS);
        if (!violationModel.getActive())
            return Observable.empty();
        Integer maxSameContact = violationModel.getValue().getLimit();
        Optional<ViolationEntity> lastViolation = mViolationRepository.getLastViolation(ViolationModel.Type.FALSIFICATION_OF_STATISTICS);

        return mContactRepository.getAll()
                .flatMap(Observable::fromIterable)
                .filter(contactEntity -> lastViolation.map(violationEntity ->
                        contactEntity.getCreateTime().after(violationEntity.getDate())).orElse(true))
                .toList()
                .toObservable()
                .filter(contactEntities -> contactEntities.size() > maxSameContact)
                .flatMap(Observable::fromIterable)
                .sorted((o1, o2) -> o1.getCreateTime().compareTo(o2.getCreateTime()))
                .takeLast(maxSameContact + 1)
                .flatMap(contactEntity -> Observable.fromIterable(contactEntity.getStepResultEntities()))
                .filter(ChoiceResultEntity::isResultive)
                .toList()
                .filter(stepResultEntities -> !stepResultEntities.isEmpty())
                .filter(stepResultEntities -> Stream.of(stepResultEntities)
                        .allMatch(value -> value.getChoiceId() == stepResultEntities.get(0).getChoiceId()))
                .map(aBoolean -> ViolationModel.Type.FALSIFICATION_OF_STATISTICS)
                .toObservable();
    }
}
