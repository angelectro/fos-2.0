package com.app.domain.usecase;

import com.app.domain.model.FieldModel;
import com.app.domain.model.FieldResultModel;
import com.app.domain.repository.FileRepository;
import com.app.domain.usecase.internal.UseCase;

import java.io.File;
import java.io.FileNotFoundException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class UploadFileUseCase extends UseCase<String> {

    private FieldResultModel mFieldModel;
    private Type type;
    private FileRepository mFileRepository;

    @Inject
    public UploadFileUseCase(Scheduler scheduler,
                             FileRepository fileRepository) {
        super(scheduler);
        mFileRepository = fileRepository;
    }

    public UploadFileUseCase setFieldModel(FieldResultModel fieldModel) {
        mFieldModel = fieldModel;
        return this;
    }

    public UploadFileUseCase setType(Type type) {
        this.type = type;
        return this;
    }

    @Override
    protected Observable<String> buildUseCaseObservable() {
        File file = new File(mFieldModel.getResult());
        if (file.exists())
            switch (type) {
                case STEPS:
                    return mFileRepository.uploadFileSteps(file);
                case INIT_STEPS:
                    return mFileRepository.uploadFileInitSteps(file);
            }
        return Observable.error(new FileNotFoundException());
    }

    public enum Type {
        STEPS,
        INIT_STEPS
    }
}
