package com.app.domain.usecase;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.FieldStatus;
import com.app.domain.model.PreparationWorkStepModel;
import com.app.domain.repository.InitStepFieldsRepository;
import com.app.domain.usecase.internal.UseCase;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class SaveLocalResultInitStepUseCase extends UseCase<Boolean> {

    private PreparationWorkStepModel mPreparationWorkStepModel;
    private InitStepFieldsRepository mInitStepFieldsRepository;

    @Inject
    public SaveLocalResultInitStepUseCase(Scheduler scheduler, InitStepFieldsRepository initStepFieldsRepository) {
        super(scheduler);
        mInitStepFieldsRepository = initStepFieldsRepository;
    }

    public SaveLocalResultInitStepUseCase setPreparationWorkStepModel(PreparationWorkStepModel preparationWorkStepModel) {
        mPreparationWorkStepModel = preparationWorkStepModel;
        return this;
    }

    @Override
    protected Observable<Boolean> buildUseCaseObservable() {
        List<FieldResultModel> fieldModels = Stream.of(mPreparationWorkStepModel.getListFieldModels())
                .map(fieldModel -> new FieldResultModel(
                        fieldModel.getUuid(),
                        fieldModel.getResult(),
                        mPreparationWorkStepModel.getId(),
                        fieldModel.getTypeField(),
                        FieldStatus.NOTHING.toString(),
                        null)).collect(Collectors.toList());
        return mInitStepFieldsRepository.saveFieldsToLocal(fieldModels);
    }
}
