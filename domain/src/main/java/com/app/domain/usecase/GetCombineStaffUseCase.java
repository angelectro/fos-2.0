package com.app.domain.usecase;

import com.app.domain.model.CityModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.model.StaffModel;
import com.app.domain.model.SupervisorModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.CityRepository;
import com.app.domain.repository.PromoterRepository;
import com.app.domain.repository.SupervisorRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;


public class GetCombineStaffUseCase extends UseCase<StaffModel> {


    private int mProjectId;
    private PromoterRepository mPromoterRepository;
    private SupervisorRepository mSupervisorRepository;
    private CityRepository mCityRepository;

    @Inject
    public GetCombineStaffUseCase(Scheduler scheduler,
                                  PromoterRepository promoterRepository,
                                  SupervisorRepository supervisorRepository,
                                  CityRepository cityRepository,
                                  ApplicationPreferences preferences) {
        super(scheduler);
        mPromoterRepository = promoterRepository;
        mSupervisorRepository = supervisorRepository;
        mCityRepository = cityRepository;
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
    }


    @Override
    protected Observable<StaffModel> buildUseCaseObservable() {
        Observable<List<PromoterModel>> promoters = mPromoterRepository.promoters(mProjectId)
                .flatMap(Observable::fromIterable)
                .sorted()
                .toList()
                .toObservable();
        Observable<List<SupervisorModel>> supervisors = mSupervisorRepository.supervisors(mProjectId)
                .flatMap(Observable::fromIterable)
                .sorted()
                .toList()
                .toObservable();
        Observable<List<CityModel>> cities = mCityRepository.getCities(mProjectId)
                .flatMap(Observable::fromIterable)
                .sorted()
                .toList()
                .toObservable();
        return Observable.zip(promoters,
                supervisors,
                cities,
                StaffModel::new);
    }
}
