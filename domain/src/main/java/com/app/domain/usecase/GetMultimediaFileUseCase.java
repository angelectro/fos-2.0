package com.app.domain.usecase;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.TypeField;
import com.app.domain.model.sync.ChoiceResultEntity;
import com.app.domain.model.sync.ContactEntity;
import com.app.domain.model.sync.FieldResultStepEntity;
import com.app.domain.repository.ContactRepository;
import com.app.domain.repository.InitStepFieldsRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class GetMultimediaFileUseCase extends UseCase<List<FieldResultModel>> {
    private ContactRepository mContactRepository;
    private InitStepFieldsRepository mInitStepFieldsRepository;

    @Inject
    public GetMultimediaFileUseCase(Scheduler scheduler,
                                    ContactRepository contactRepository,
                                    InitStepFieldsRepository initStepFieldsRepository) {
        super(scheduler);
        mContactRepository = contactRepository;
        mInitStepFieldsRepository = initStepFieldsRepository;
    }

    @Override
    protected Observable<List<FieldResultModel>> buildUseCaseObservable() {
        return Observable.merge(mContactRepository.getAll()
                        .flatMap(Observable::fromIterable)
                        .map(ContactEntity::getStepResultEntities)
                        .flatMap(Observable::fromIterable)
                        .map(ChoiceResultEntity::getFieldResultStepEntities)
                        .flatMap(Observable::fromIterable)
                        .filter(fieldResultModel -> fieldResultModel.getResult() != null)
                        .filter(FieldResultStepEntity::isFile)
                        .filter(fieldResultModel -> fieldResultModel.getType() == TypeField.PHOTO || fieldResultModel.getType() == TypeField.VIDEO)
                        .map(entity -> new FieldResultModel(entity.getUuid(),
                                entity.getResult(),
                                entity.getStepId(),
                                entity.getType(),
                                entity.getStatus().toString(),
                                entity.getResultPath()))
                , mInitStepFieldsRepository.getAll()
                        .flatMap(Observable::fromIterable)
                        .filter(fieldResultModel -> fieldResultModel.getResult() != null)
                        .filter(FieldResultModel::isFile))
                .subscribeOn(AndroidSchedulers.mainThread())
                .filter(fieldResultModel -> fieldResultModel.getType() == TypeField.PHOTO || fieldResultModel.getType() == TypeField.VIDEO)
                .toList()
                .toObservable();
    }
}
