package com.app.domain.usecase;

import com.app.domain.model.ScheduleModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.ScheduleRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;


public class GetScheduleUseCase extends UseCase<List<ScheduleModel>> {

    private int mCityId;
    private int mPromoterId;
    private int mSupervisorId;
    private int mProjectId;
    private ScheduleRepository mScheduleRepository;

    @Inject
    public GetScheduleUseCase(Scheduler scheduler,
                              ScheduleRepository scheduleRepository,
                              ApplicationPreferences preferences) {
        super(scheduler);
        mScheduleRepository = scheduleRepository;
        mCityId = preferences.cityModel().get().getId();
        mPromoterId = preferences.promoterModel().get().getId();
        mSupervisorId = preferences.supervisorModel().get().getId();
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
    }

    @Override
    protected Observable<List<ScheduleModel>> buildUseCaseObservable() {
        return mScheduleRepository.getSchedule(mProjectId, mCityId, mPromoterId, mSupervisorId);
    }
}
