package com.app.domain.usecase;

import com.app.domain.model.AuthCredentialsModel;
import com.app.domain.model.ProjectModel;
import com.app.domain.repository.AuthorizationRepository;
import com.app.domain.usecase.internal.UseCase;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;


public class AuthorizationUseCase extends UseCase<AuthCredentialsModel> {
    private ProjectModel mProjectModel;
    private String mPassword;
    private AuthorizationRepository mRepository;


    @Inject
    public AuthorizationUseCase(Scheduler scheduler,
                                AuthorizationRepository repository) {
        super(scheduler);
        mRepository = repository;

    }

    public AuthorizationUseCase setParams(ProjectModel projectModel, String password) {
        mProjectModel = projectModel;
        mPassword = password;
        return this;
    }

    @Override
    protected Observable<AuthCredentialsModel> buildUseCaseObservable() {
        return mRepository.auth(mProjectModel, mPassword);
    }
}
