package com.app.domain.usecase;

import com.app.domain.model.ViolationModel;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.Date;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class CreateViolationUseCase extends UseCase<Boolean> {

    private ViolationRepository mViolationRepository;
    private ViolationModel.Type mType;

    @Inject
    public CreateViolationUseCase(Scheduler scheduler, ViolationRepository violationRepository) {
        super(scheduler);
        mViolationRepository = violationRepository;
    }

    public CreateViolationUseCase setType(ViolationModel.Type type) {
        mType = type;
        return this;
    }

    @Override
    public Observable<Boolean> buildUseCaseObservable() {
        return mViolationRepository.createViolation(mType.getId(), new Date())
                .toObservable()
                .map(o -> true);
    }
}
