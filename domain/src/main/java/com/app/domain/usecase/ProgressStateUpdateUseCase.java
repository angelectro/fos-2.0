package com.app.domain.usecase;


import com.app.domain.model.ProgressState;
import com.app.domain.model.work.WorkStatus;
import com.app.domain.model.work.WorkStatusModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.repository.WorkRepository;

import javax.inject.Inject;

import io.reactivex.Completable;

public class ProgressStateUpdateUseCase {

    @Inject
    protected ApplicationPreferences mApplicationPreferences;
    @Inject
    protected WorkRepository mWorkRepository;
    @Inject
    ViolationRepository mViolationRepository;

    @Inject
    public ProgressStateUpdateUseCase() {
    }

    public Completable update(ProgressState progressState) {
        mApplicationPreferences.progressState().set(progressState);
        Completable completable;
        if (progressState == ProgressState.COMPLETE_AUTH) {
            completable = Completable.fromAction(() -> {
                mWorkRepository.removeAll();
                mViolationRepository.removeAll();
                mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.BEGIN_PREPARATION));
            });
        } else if (progressState == ProgressState.COMPLETE_PREPARATION) {
            completable = Completable.fromAction(() -> {
                mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.COMPLETE_PREPARATION));
            });

        } else {
            completable = Completable.complete();
        }
        return completable;
    }
}
