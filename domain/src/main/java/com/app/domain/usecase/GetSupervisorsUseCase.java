package com.app.domain.usecase;

import com.app.domain.model.CityModel;
import com.app.domain.model.SupervisorModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.SupervisorRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;


public class GetSupervisorsUseCase extends UseCase<List<SupervisorModel>> {
    private final int mProjectId;
    private CityModel mCityModel;
    private SupervisorRepository mSupervisorRepository;

    @Inject
    public GetSupervisorsUseCase(Scheduler scheduler,
                                 SupervisorRepository supervisorRepository,
                                 ApplicationPreferences preferences) {
        super(scheduler);
        mSupervisorRepository = supervisorRepository;
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
    }


    public GetSupervisorsUseCase setCityModel(CityModel cityModel) {
        mCityModel = cityModel;
        return this;
    }

    @Override
    protected Observable<List<SupervisorModel>> buildUseCaseObservable() {
        return mSupervisorRepository.supervisorsByCity(mCityModel.getId(),mProjectId);
    }
}
