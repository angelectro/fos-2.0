package com.app.domain.usecase;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.sync.ChoiceResultEntity;
import com.app.domain.model.sync.ContactEntity;
import com.app.domain.model.sync.FieldResultStepEntity;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.BreakRepository;
import com.app.domain.repository.ContactRepository;
import com.app.domain.repository.FinishStepFieldsRepository;
import com.app.domain.repository.InitStepFieldsRepository;
import com.app.domain.repository.NewsRepository;
import com.app.domain.repository.StepsRepository;
import com.app.domain.usecase.internal.UseCase;

import java.io.File;
import java.util.Arrays;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class EndWorkdayUseCase extends UseCase<Boolean> {


    private ApplicationPreferences mPreferences;
    private InitStepFieldsRepository mInitStepFieldsRepository;
    private FinishStepFieldsRepository mFinishStepFieldsRepository;
    private ContactRepository mContactRepository;
    private BreakRepository mBreakRepository;
    private NewsRepository mNewsRepository;
    private StepsRepository mStepsRepository;

    @Inject
    public EndWorkdayUseCase(Scheduler scheduler,
                             ApplicationPreferences preferences,
                             InitStepFieldsRepository initStepFieldsRepository,
                             ContactRepository contactRepository,
                             BreakRepository breakRepository,
                             NewsRepository newsRepository,
                             StepsRepository stepsRepository,
                             FinishStepFieldsRepository finishStepFieldsRepository) {
        super(scheduler);
        mPreferences = preferences;
        mInitStepFieldsRepository = initStepFieldsRepository;
        mContactRepository = contactRepository;
        mBreakRepository = breakRepository;
        mNewsRepository = newsRepository;
        mStepsRepository = stepsRepository;
        mFinishStepFieldsRepository = finishStepFieldsRepository;
    }

    @Override
    public Observable<Boolean> buildUseCaseObservable() {
        return Observable.concat(
                Arrays.asList(Observable.fromCallable(() -> {
                            mPreferences.clearPreferences();
                            return true;
                        }),
                        mStepsRepository.removeAll(),
                        mInitStepFieldsRepository.getAll()
                                .flatMap(Observable::fromIterable)
                                .filter(fieldResultModel -> fieldResultModel.getResult() != null)
                                .filter(FieldResultModel::isFile)
                                .map(fieldResultModel -> new File(fieldResultModel.getResult()))
                                .filter(File::exists)
                                .map(File::delete),
                        mInitStepFieldsRepository.removeAll(),
                        mFinishStepFieldsRepository.getAll()
                        .flatMap(Observable::fromIterable)
                        .filter(fieldResultModel -> fieldResultModel.getResult() != null)
                        .filter(FieldResultModel::isFile)
                        .map(fieldResultModel -> new File(fieldResultModel.getResult()))
                        .filter(File::exists)
                        .map(File::delete),
                        mFinishStepFieldsRepository.removeAll(),
                        mContactRepository.getAll()
                                .flatMap(Observable::fromIterable)
                                .map(ContactEntity::getStepResultEntities)
                                .flatMap(Observable::fromIterable)
                                .map(ChoiceResultEntity::getFieldResultStepEntities)
                                .flatMap(Observable::fromIterable)
                                .filter(FieldResultStepEntity::isFile)
                                .filter(entity -> entity.getResult() != null)
                                .map(fieldResultModel -> new File(fieldResultModel.getResult()))
                                .filter(File::exists)
                                .map(File::delete),
                        mContactRepository.removeAll(),
                        mBreakRepository.removeAll(),
                        mNewsRepository.removeAll()
                ))
                .all(Boolean::booleanValue).toObservable();
    }
}
