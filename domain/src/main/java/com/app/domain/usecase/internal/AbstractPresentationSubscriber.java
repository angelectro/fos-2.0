package com.app.domain.usecase.internal;

import android.support.annotation.CallSuper;


import com.app.domain.model.ErrorModel;

import io.reactivex.observers.DisposableObserver;


public abstract class AbstractPresentationSubscriber<T> extends DisposableObserver<T> {

    @CallSuper
    @Override
    public void onComplete() {
        System.out.println();
    }

    @Override
    public void onNext(T t) {

    }

    @Override
    public final void onError(Throwable throwable) {
        log(throwable);
        handleError(throwable);
    }

    protected abstract void handleError(Throwable throwable);

    protected abstract void onError(ErrorModel errorTitle);

    protected abstract void log(Throwable throwable);
}