package com.app.domain.usecase;


import com.app.domain.model.news.NewsModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.NewsRepository;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetNewsUseCase {
    private final int mProjectId;
    private Scheduler mScheduler;
    private NewsRepository mNewsRepository;

    @Inject
    public GetNewsUseCase(Scheduler scheduler,
                          NewsRepository newsRepository,
                          ApplicationPreferences preferences) {
        mScheduler = scheduler;
        mNewsRepository = newsRepository;
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
    }

    public Observable<List<NewsModel>> getNews() {
        Calendar calendar = new GregorianCalendar();
        io.reactivex.functions.Predicate<NewsModel> newsModelPredicate = newsModel -> {
            Calendar date = new GregorianCalendar();
            date.setTime(newsModel.getDate());
            return
                    date.get(Calendar.YEAR) <= calendar.get(Calendar.YEAR) &&
                            date.get(Calendar.MONTH) <= calendar.get(Calendar.MONTH) &&
                            date.get(Calendar.DAY_OF_MONTH) <= calendar.get(Calendar.DAY_OF_MONTH);
        };
        return mNewsRepository.getNews(mProjectId)
                .flatMap(Observable::fromIterable)
                .filter(newsModelPredicate)
                .sorted()
                .toList()
                .toObservable()
                .subscribeOn(mScheduler);
    }
}
