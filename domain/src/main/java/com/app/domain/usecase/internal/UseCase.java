package com.app.domain.usecase.internal;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;


public abstract class UseCase<T> {
    private DisposableObserver subscriper;
    protected Scheduler taskScheduler;
    private int attemptCount = 0;

    public UseCase(Scheduler scheduler) {
        this.taskScheduler = scheduler;
    }

    protected abstract Observable<T> buildUseCaseObservable();

    @SuppressWarnings("unchecked")
    public void execute(AbstractPresentationSubscriber subscriber) {
        unsubscribe();
        this.subscriper = subscriber;
        buildUseCaseObservable()
                .subscribeOn(taskScheduler)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public void unsubscribe() {
        if (subscriper != null) {
            subscriper.dispose();
        }
    }

    public boolean isDisposed() {
        if (subscriper != null)
            return subscriper.isDisposed();
        return false;
    }
}