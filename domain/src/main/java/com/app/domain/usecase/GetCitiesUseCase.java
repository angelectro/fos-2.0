package com.app.domain.usecase;

import com.app.domain.model.CityModel;
import com.app.domain.model.ProjectModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.CityRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetCitiesUseCase extends UseCase<List<CityModel>> {

    private ProjectModel mProject;
    private CityRepository mCityRepository;

    @Inject
    public GetCitiesUseCase(Scheduler scheduler,
                            CityRepository cityRepository,
                            ApplicationPreferences preferences) {
        super(scheduler);
        mCityRepository = cityRepository;
        mProject = preferences.authCredentialsModel().get().getProjectModel();
    }

    @Override
    protected Observable<List<CityModel>> buildUseCaseObservable() {
        return mCityRepository.getCities(mProject.getId());
    }
}
