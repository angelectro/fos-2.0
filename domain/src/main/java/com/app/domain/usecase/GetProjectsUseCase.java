package com.app.domain.usecase;

import com.app.domain.model.ProjectModel;
import com.app.domain.repository.ProjectRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;


public class GetProjectsUseCase extends UseCase<List<ProjectModel>> {


    private ProjectRepository mProjectRepository;

    @Inject
    public GetProjectsUseCase(Scheduler scheduler,
                              ProjectRepository projectRepository) {
        super(scheduler);
        mProjectRepository = projectRepository;
    }

    @Override
    protected Observable<List<ProjectModel>> buildUseCaseObservable() {
        return mProjectRepository.projects();
    }
}
