package com.app.domain.usecase;

import android.text.TextUtils;

import com.app.domain.model.steps.ChoiceTempModel;
import com.app.domain.model.steps.StepModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.StepsRepository;
import com.app.domain.usecase.internal.UseCase;
import com.annimon.stream.Stream;

import java.io.File;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetStepUseCase extends UseCase<StepModel> {

    private int mProjectId;
    private ApplicationPreferences mPreferences;
    private StepsRepository mStepsRepository;
    private int mStepId = -1;

    @Inject
    public GetStepUseCase(Scheduler scheduler,
                          StepsRepository stepsRepository,
                          ApplicationPreferences preferences) {
        super(scheduler);
        mStepsRepository = stepsRepository;
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
        mPreferences = preferences;
    }

    public GetStepUseCase setStepId(int stepId) {
        mStepId = stepId;
        return this;
    }

    @Override
    public Observable<StepModel> buildUseCaseObservable() {
        return (mStepId == -1 ? mStepsRepository.getFirstStep(mProjectId)
                .doOnNext(stepModel -> cleanTempStory()) :
                mStepsRepository.getStepById(mStepId, mProjectId).toObservable());
    }

    private void cleanTempStory() {
        Set<ChoiceTempModel> choiceTempModels = mPreferences.tempContactData().get();
        Stream.of(choiceTempModels)
                .filter(value -> value.getFields() != null)
                .flatMap(stepModel -> Stream.of(stepModel.getFields()))
                .filter(value -> value.getTypeField().isFile())
                .filter(value -> !TextUtils.isEmpty(value.getResult()))
                .map(fieldModel -> new File(fieldModel.getResult()))
                .filter(File::exists)
                .forEach(File::delete);
        mPreferences.tempContactData().set(null);
    }
}
