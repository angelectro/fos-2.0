package com.app.domain.usecase;

import android.text.TextUtils;

import com.app.domain.model.FieldModel;
import com.app.domain.model.FieldResultModel;
import com.app.domain.model.QuestionnaireModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.FinishStepFieldsRepository;
import com.app.domain.repository.QuestionnaireRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetFinishStepUseCase extends UseCase<QuestionnaireModel> {

    private final int mProjectId;
    private FinishStepFieldsRepository finishStepFieldsRepository;
    private QuestionnaireRepository questionnaireRepository;
    private int order;

    @Inject
    public GetFinishStepUseCase(Scheduler scheduler,
                                QuestionnaireRepository questionnaireRepository,
                                ApplicationPreferences preferences,
                                FinishStepFieldsRepository finishStepFieldsRepository) {
        super(scheduler);
        this.questionnaireRepository = questionnaireRepository;
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
        this.finishStepFieldsRepository = finishStepFieldsRepository;
    }

    public GetFinishStepUseCase setOrder(int order) {
        this.order = order;
        return this;
    }

    @Override
    public Observable<QuestionnaireModel> buildUseCaseObservable() {
        return questionnaireRepository.getByOrder(order, mProjectId)
                .flatMap(stepModel -> {
                    return Observable.zip(Observable.just(stepModel),
                            finishStepFieldsRepository.getResultModels(stepModel.getId()),
                            (preparationWorkStepModel, fieldResultModels) -> {
                                if (!fieldResultModels.isEmpty()) {
                                    List<FieldModel> listFieldModels = preparationWorkStepModel.getListFieldModels();
                                    for (FieldModel listFieldModel : listFieldModels) {
                                        for (FieldResultModel fieldResultModel : fieldResultModels) {
                                            if (TextUtils.equals(fieldResultModel.getUuid(), listFieldModel.getUuid())) {
                                                listFieldModel.setResult(fieldResultModel.getResult());
                                                break;
                                            }
                                        }
                                    }
                                }
                                return preparationWorkStepModel;
                            });
                });
    }
}
