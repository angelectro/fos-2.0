package com.app.domain.usecase;

import com.app.domain.model.CityModel;
import com.app.domain.model.PromoterModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.PromoterRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;


public class GetPromotersUseCase extends UseCase<List<PromoterModel>> {

    private final int mProjectId;
    CityModel mCityModel;
    private PromoterRepository mPromoterRepository;

    @Inject
    public GetPromotersUseCase(Scheduler scheduler,
                               PromoterRepository promoterRepository,
                               ApplicationPreferences preferences) {
        super(scheduler);
        mPromoterRepository = promoterRepository;
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
    }

    public GetPromotersUseCase setCityModel(CityModel cityModel) {
        mCityModel = cityModel;
        return this;
    }

    @Override
    protected Observable<List<PromoterModel>> buildUseCaseObservable() {
        return mPromoterRepository.promotersByCity(mCityModel.getId(), mProjectId);
    }
}
