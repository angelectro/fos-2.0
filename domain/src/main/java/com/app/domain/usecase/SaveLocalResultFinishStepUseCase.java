package com.app.domain.usecase;

import com.app.domain.model.FieldResultModel;
import com.app.domain.model.FieldStatus;
import com.app.domain.model.QuestionnaireModel;
import com.app.domain.repository.FinishStepFieldsRepository;
import com.app.domain.usecase.internal.UseCase;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class SaveLocalResultFinishStepUseCase extends UseCase<Boolean> {

    private QuestionnaireModel questionnaireModel;
    private FinishStepFieldsRepository finishStepFieldsRepository;

    @Inject
    public SaveLocalResultFinishStepUseCase(Scheduler scheduler, FinishStepFieldsRepository initStepFieldsRepository) {
        super(scheduler);
        finishStepFieldsRepository = initStepFieldsRepository;
    }

    public SaveLocalResultFinishStepUseCase setQuestionnaireModel(QuestionnaireModel questionnaireModel) {
        this.questionnaireModel = questionnaireModel;
        return this;
    }

    @Override
    public Observable<Boolean> buildUseCaseObservable() {
        List<FieldResultModel> fieldModels = Stream.of(questionnaireModel.getListFieldModels())
                .map(fieldModel -> new FieldResultModel(
                        fieldModel.getUuid(),
                        fieldModel.getResult(),
                        questionnaireModel.getId(),
                        fieldModel.getTypeField(),
                        FieldStatus.NOTHING.toString(),
                        null)).collect(Collectors.toList());
        return finishStepFieldsRepository.saveFieldsToLocal(fieldModels);
    }
}
