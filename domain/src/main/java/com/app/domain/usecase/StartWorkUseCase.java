package com.app.domain.usecase;

import com.app.domain.model.ProgressState;
import com.app.domain.model.ViolationModel;
import com.app.domain.model.work.WorkStatus;
import com.app.domain.model.work.WorkStatusModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.repository.WorkRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.Date;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

import static com.app.domain.utils.ViolationUtils.checkDelayAtStart;

public class StartWorkUseCase extends UseCase<Boolean> {


    private final ApplicationPreferences mPreferences;
    @Inject
    WorkRepository mWorkRepository;

    private ViolationRepository mViolationRepository;

    @Inject
    public StartWorkUseCase(Scheduler scheduler,
                            ApplicationPreferences preferences,
                            ViolationRepository violationRepository) {
        super(scheduler);
        mPreferences = preferences;
        mViolationRepository = violationRepository;
    }

    @Override
    protected Observable<Boolean> buildUseCaseObservable() {
        Single<Boolean> checkDelayObservable = Single.fromCallable(() -> checkDelayAtStart(mPreferences))
                .flatMap(isDelay -> {
                    if (isDelay)
                        return mViolationRepository.createViolation(ViolationModel.Type.DELAY_AT_START.getId(), new Date())
                                .map(o -> isDelay);
                    else
                        return Single.just(isDelay);
                })
                .doOnSuccess(aBoolean -> mPreferences.progressState().set(ProgressState.COMPLETE_GO_WORK));

        mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.WORKS));
        return checkDelayObservable
                .toObservable();

    }
}
