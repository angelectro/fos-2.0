package com.app.domain.usecase;

import android.text.TextUtils;

import com.app.domain.model.FieldModel;
import com.app.domain.model.FieldResultModel;
import com.app.domain.model.PreparationWorkStepModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.InitStepFieldsRepository;
import com.app.domain.repository.PreparationRepository;
import com.app.domain.usecase.internal.UseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetPreparationStepUseCase extends UseCase<PreparationWorkStepModel> {

    private final int mProjectId;
    private InitStepFieldsRepository mInitStepFieldsRepository;
    private PreparationRepository mPreparationRepository;
    private int order;

    @Inject
    public GetPreparationStepUseCase(Scheduler scheduler,
                                     PreparationRepository preparationRepository,
                                     ApplicationPreferences preferences,
                                     InitStepFieldsRepository initStepFieldsRepository) {
        super(scheduler);
        mPreparationRepository = preparationRepository;
        mProjectId = preferences.authCredentialsModel().get().getProjectModel().getId();
        mInitStepFieldsRepository = initStepFieldsRepository;
    }

    public GetPreparationStepUseCase setOrder(int order) {
        this.order = order;
        return this;
    }

    @Override
    protected Observable<PreparationWorkStepModel> buildUseCaseObservable() {
        return mPreparationRepository.getByOrder(order, mProjectId)
                .flatMap(stepModel -> {
                    return Observable.zip(Observable.just(stepModel),
                            mInitStepFieldsRepository.getResultModels(stepModel.getId()),
                            (preparationWorkStepModel, fieldResultModels) -> {
                                if (!fieldResultModels.isEmpty()) {
                                    List<FieldModel> listFieldModels = preparationWorkStepModel.getListFieldModels();
                                    for (FieldModel listFieldModel : listFieldModels) {
                                        for (FieldResultModel fieldResultModel : fieldResultModels) {
                                            if (TextUtils.equals(fieldResultModel.getUuid(), listFieldModel.getUuid())) {
                                                listFieldModel.setResult(fieldResultModel.getResult());
                                                break;
                                            }
                                        }
                                    }
                                }
                                return preparationWorkStepModel;
                            });
                });
    }
}
