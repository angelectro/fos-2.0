package com.app.domain.usecase.internal;

import com.app.domain.model.ErrorModel;
import com.annimon.stream.Optional;
import com.google.gson.Gson;

import javax.inject.Inject;


public abstract class ErrorModelHandler {
    @Inject
    protected Gson gson = new Gson();

    public abstract Optional<ErrorModel> fetchError(Throwable throwable);
}
