package com.app.domain.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class FileUtils {


    public static boolean removeFile(@NonNull String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
            return true;
        }
        return true;
    }

    private static String getAudioFilePathFromUri(Uri uri, Context context) {
        Cursor cursor = context.getContentResolver()
                .query(uri, null, null, null, null);
        cursor.moveToFirst();
        int index = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
        return cursor.getString(index);
    }


    public static File getFileForProject(@NonNull Context context, String projectId, String fileName) {
        File storageDir = context.getExternalFilesDir(projectId);
        File file = new File(storageDir, fileName);
        return file;
    }

    public static File getProjectDirectoryPath(@NonNull Context context, String projectId) {
        return context.getExternalFilesDir(projectId);
    }

    public static String getFileNameFromUrl(URL url) {
        String[] split = url.getFile().split("/");
        String fileName = split[split.length - 1];
        return fileName;
    }

    public static String getFileNameFromUrl(String url) {
        try {
            return getFileNameFromUrl(new URL(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean removeDirectory(File file) {
        for (File file1 : file.listFiles()) {
            if (file1.exists())
                if (file1.isDirectory())
                    removeDirectory(file);
                else
                    file1.delete();
        }
       return file.delete();
    }
}
