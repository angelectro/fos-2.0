package com.app.domain.utils;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.annimon.stream.Optional;

import java.util.HashMap;
import java.util.Map;

public class EventBus {
    private static volatile EventBus Instance = null;
    private Map<String, Object> observers = new HashMap<>();

    public static EventBus getInstance() {
        EventBus localInstance = Instance;
        if (localInstance == null) {
            synchronized (EventBus.class) {
                localInstance = Instance;
                if (localInstance == null) {
                    Instance = localInstance = new EventBus();
                }
            }
        }
        return localInstance;
    }

    public void register(@NonNull Class tClass, @NonNull Object object) {
        String name = tClass.getCanonicalName();
        putIfAbsent(name, object);
    }

    void putIfAbsent(@NonNull String key, @NonNull Object value) {
        Object v = observers.get(key);
        if (v == null) {
            observers.put(key, value);
        }
    }

    public void unregister(@NonNull Class tClass) {
        observers.remove(tClass.getCanonicalName());
    }

    @SuppressWarnings("unchecked")
    @CheckResult
    public <T> Optional<T> getOptional(@NonNull Class<? extends T> tClass) {
        return Optional.ofNullable((T) observers.get(tClass.getCanonicalName()));
    }

    @SuppressWarnings("unchecked")
    @CheckResult
    public <T> T get(@NonNull Class<? extends T> tClass) {
        return (T) observers.get(tClass.getCanonicalName());
    }
}
