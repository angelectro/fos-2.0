package com.app.domain.utils;

import com.app.domain.model.AuthCredentialsModel;
import com.app.domain.model.ProjectModel;
import com.app.domain.model.ScheduleModel;
import com.app.domain.model.ViolationModel;
import com.app.domain.preferences.internal.ApplicationPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ViolationUtils {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");


    public static boolean checkDelayAtStart(ApplicationPreferences mPreferences) throws ParseException {
        if (!mPreferences.authCredentialsModel().get().getProjectModel().getTypeViolationModelMap().get(ViolationModel.Type.DELAY_AT_START).getActive())
            return false;
        ScheduleModel scheduleModel = mPreferences.scheduleModel().get();
        String timeForStartDay = String.format("%s %s", scheduleModel.getDate(), scheduleModel.getBeginAt());
        Date parsedDate = simpleDateFormat.parse(timeForStartDay);
        Date currentTime = new Date();
        int difference = (int) ((currentTime.getTime() - parsedDate.getTime()) / TimeUnit.MINUTES.toMillis(1));
        if (difference > 0) {
            return true;
        }
        return false;
    }

    public static boolean checkPrematureTermination(ApplicationPreferences preferences) {
        if (!preferences.authCredentialsModel()
                .getOptional()
                .map(AuthCredentialsModel::getProjectModel)
                .map(ProjectModel::getTypeViolationModelMap)
                .map(typeViolationModelMap -> typeViolationModelMap.get(ViolationModel.Type.PREMATURE_TERMINATION))
                .map(ViolationModel::getActive)
                .orElse(false)) {
            return false;
        }

        ScheduleModel scheduleModel = preferences.scheduleModel().get();
        String timeForStartDay = String.format("%s %s", scheduleModel.getDate(), scheduleModel.getEndAt());
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(timeForStartDay);
            Date currentTime = new Date();
            long difference = currentTime.getTime() - parsedDate.getTime();
            if (difference < 0) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkBreakInFirstHour(ApplicationPreferences preferences) {
        if (!preferences.authCredentialsModel().get().getProjectModel().getTypeViolationModelMap().get(ViolationModel.Type.BREAK_IN_THE_FIRST_HOUR).getActive())
            return false;
        ScheduleModel scheduleModel = preferences.scheduleModel().get();
        String timeForStartDay = String.format("%s %s", scheduleModel.getDate(), scheduleModel.getBeginAt());
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(timeForStartDay);
            Date currentTime = new Date();
            long difference = currentTime.getTime() - parsedDate.getTime();
            if (difference >= 0 && difference <= TimeUnit.HOURS.toMillis(1)) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkBreakInLastHour(ApplicationPreferences preferences) {
        if (!preferences.authCredentialsModel().get().getProjectModel().getTypeViolationModelMap().get(ViolationModel.Type.BREAK_IN_THE_LAST_HOUR).getActive())
            return false;
        ScheduleModel scheduleModel = preferences.scheduleModel().get();
        String timeForStartDay = String.format("%s %s", scheduleModel.getDate(), scheduleModel.getEndAt());
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(timeForStartDay);
            Date currentTime = new Date();
            long difference = parsedDate.getTime() - currentTime.getTime();
            if (difference >= 0 && difference <= TimeUnit.HOURS.toMillis(1)) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkMoreOneBreakInHour(ApplicationPreferences preferences) {
        if (!preferences.authCredentialsModel().get().getProjectModel().getTypeViolationModelMap().get(ViolationModel.Type.MORE_ON_BREAK_PER_HOUR).getActive())
            return false;
        Date lastBreakTime = preferences.breakTime().get();
        if (lastBreakTime == null)
            return false;
        Date currentTime = new Date();
        long difference = currentTime.getTime() - lastBreakTime.getTime();
        if (difference >= 0 && difference <= TimeUnit.HOURS.toMillis(1)) {
            return true;
        }
        return false;
    }

    public static boolean checkDelayOnBreak(ApplicationPreferences preferences) {
        ViolationModel violationModel = preferences.authCredentialsModel().get().getProjectModel().getTypeViolationModelMap().get(ViolationModel.Type.DELAY_ON_BREAK);
        if (!violationModel.getActive())
            return false;
        int minutesForBreak = violationModel.getValue().getMinutes();
        Date currentTime = new Date();
        long difference = currentTime.getTime() - preferences.breakTime().get().getTime();
        if (difference > TimeUnit.MINUTES.toMillis(minutesForBreak)) {
            return true;
        }
        return false;
    }

    public static String getDelayOnBreakMinutes(ApplicationPreferences preferences) {
        Map<ViolationModel.Type, ViolationModel> typeViolationModelMap = preferences.authCredentialsModel()
                .get()
                .getProjectModel()
                .getTypeViolationModelMap();
        String minute = typeViolationModelMap.get(ViolationModel.Type.DELAY_ON_BREAK)
                .getValue()
                .getMinutes()
                .toString();
        return minute;
    }


}
