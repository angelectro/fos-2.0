package com.app.domain.interactors;


import android.text.TextUtils;

import com.app.domain.model.BreakModel;
import com.app.domain.model.FieldModel;
import com.app.domain.model.ProgressState;
import com.app.domain.model.ViolationModel;
import com.app.domain.model.steps.ActionResult;
import com.app.domain.model.steps.ChoiceModel;
import com.app.domain.model.steps.ChoiceTempModel;
import com.app.domain.model.steps.StepModel;
import com.app.domain.model.work.WorkStatus;
import com.app.domain.model.work.WorkStatusModel;
import com.app.domain.preferences.internal.ApplicationPreferences;
import com.app.domain.repository.BreakRepository;
import com.app.domain.repository.ContactRepository;
import com.app.domain.repository.QuestionnaireRepository;
import com.app.domain.repository.ViolationRepository;
import com.app.domain.repository.WorkRepository;
import com.app.domain.usecase.CheckFalsificationsUseCase;
import com.app.domain.usecase.CreateViolationUseCase;
import com.app.domain.utils.ViolationUtils;
import com.annimon.stream.Stream;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import static com.app.domain.model.steps.ActionResult.Event.BREAK_IN_FIRST_HOUR_VIOLATION;
import static com.app.domain.model.steps.ActionResult.Event.BREAK_IN_FIRST_HOUR_WARNING;
import static com.app.domain.model.steps.ActionResult.Event.BREAK_IN_LAST_HOUR_VIOLATION;
import static com.app.domain.model.steps.ActionResult.Event.BREAK_IN_LAST_HOUR_WARNING;
import static com.app.domain.model.steps.ActionResult.Event.FALSIFICATION_OF_STATISTICS_VIOLATION;
import static com.app.domain.model.steps.ActionResult.Event.FALSIFICATION_OF_TIME_VIOLATION;
import static com.app.domain.model.steps.ActionResult.Event.MORE_ON_BREAK_PER_HOUR_VIOLATION;
import static com.app.domain.model.steps.ActionResult.Event.MORE_ON_BREAK_PER_HOUR_WARNING;
import static com.app.domain.model.steps.ActionResult.Event.PREMATURE_TERMINATION_VIOLATION;


public class StepsInteractor {

    @Inject
    protected BreakRepository mBreakRepository;
    @Inject
    protected WorkRepository mWorkRepository;
    @Inject
    protected ViolationRepository mViolationRepository;
    @Inject
    QuestionnaireRepository questionnaireRepository;
    private ApplicationPreferences mPreferences;
    private ContactRepository mContactRepository;
    private CreateViolationUseCase mCreateViolationUseCase;
    private CheckFalsificationsUseCase mCheckFalsificationsUseCase;

    @Inject
    public StepsInteractor(ApplicationPreferences applicationPreferences,
                           ContactRepository contactRepository,
                           CreateViolationUseCase createViolationUseCase,
                           CheckFalsificationsUseCase checkFalsificationsUseCase) {
        mPreferences = applicationPreferences;
        mContactRepository = contactRepository;
        mCreateViolationUseCase = createViolationUseCase;
        mCheckFalsificationsUseCase = checkFalsificationsUseCase;
    }

    public boolean checkRequiredFields(List<FieldModel> fieldModels) {
        if (fieldModels == null || fieldModels.isEmpty())
            return false;
        return Stream.of(fieldModels)
                .filter(FieldModel::isRequired)
                .filter(fieldModel -> TextUtils.isEmpty(fieldModel.getResult()))
                .map(fieldModel -> {
                    fieldModel.setEmptyField(true);
                    return fieldModel;
                })
                .map(FieldModel::isEmptyField)
                .reduce((value1, value2) -> value1 || value2)
                .orElse(false);
    }

    public Single<Boolean> saveStep(StepModel stepModel, ChoiceModel choiceModel) {
        return Single.fromCallable(() -> {
            Set<ChoiceTempModel> choiceTempModels = mPreferences.tempContactData().get();
            choiceTempModels.add(new ChoiceTempModel(choiceModel.getId(), stepModel.getFields(), choiceModel.isResultive()));
            mPreferences.tempContactData().set(choiceTempModels);
            return choiceModel.isResultive();
        }).flatMap(isLast -> {
            if (isLast) {
                return mContactRepository.create(mPreferences.tempContactData().get())
                        .doOnNext(aBoolean -> mPreferences.tempContactData().set(null))
                        .map(aBoolean -> isLast)
                        .singleOrError();
            }
            return Single.just(isLast);
        });

    }

    public Single<ActionResult> endWorkDay() {
        if (ViolationUtils.checkPrematureTermination(mPreferences))
            return Single.just(ActionResult.createResult(ActionResult.Event.PREMATURE_TERMINATION_WARNING));
        else
            return Single.just(ActionResult.createResult(ActionResult.Event.CONFIRMATION_END_DAY));
    }

    public Single<ActionResult> goBreak() {
        Single<ActionResult> actionResultSingle;
        if (ViolationUtils.checkBreakInFirstHour(mPreferences)) {
            actionResultSingle = Single.just(ActionResult.createResult(ActionResult.Event.BREAK_IN_FIRST_HOUR_WARNING));
        } else if (ViolationUtils.checkBreakInLastHour(mPreferences)) {
            actionResultSingle = Single.just(ActionResult.createResult(ActionResult.Event.BREAK_IN_LAST_HOUR_WARNING));
        } else if (ViolationUtils.checkMoreOneBreakInHour(mPreferences)) {
            actionResultSingle = Single.just(ActionResult.createResult(ActionResult.Event.MORE_ON_BREAK_PER_HOUR_WARNING));
        } else {
            mPreferences.breakTime().set(new Date());
            mPreferences.breakState().set(true);
            actionResultSingle = Completable.fromAction(() -> mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.IN_BREAK)))
                    .toSingle(ActionResult::createResultWithoutEvent);
        }
        return actionResultSingle;
    }

    public Single<ActionResult> endBreak() {
        mPreferences.breakState().set(false);

        Date dateBegin = mPreferences.breakTime().get();
        Date dateEnd = new Date();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        BreakModel breakModel = new BreakModel(format.format(dateBegin), format.format(dateEnd));

        Completable completableBreak = mBreakRepository.createBreak(breakModel);
        Completable completableWorkStatus = Completable.fromAction(() -> mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.WORKS)));
        Completable completable = Completable.concat(Arrays.asList(completableBreak, completableWorkStatus));
        ActionResult actionResultSingle;
        if (ViolationUtils.checkDelayOnBreak(mPreferences)) {
            String minute = ViolationUtils.getDelayOnBreakMinutes(mPreferences);
            actionResultSingle = ActionResult.createResult(ActionResult.Event.DELAY_ON_BREAK_VIOLATION, minute);
        } else
            actionResultSingle = ActionResult.createResultWithoutEvent();

        if (actionResultSingle.isContainsEvent()) {
            return createViolation(ViolationModel.Type.DELAY_ON_BREAK, actionResultSingle)
                    .flatMapCompletable(actionResult -> completable)
                    .toSingle(() -> actionResultSingle);
        } else {
            return completable.toSingle(() -> actionResultSingle);
        }
    }

    public void changedStateOnline(boolean isOnline) {
        mPreferences.onlineState().set(isOnline);
    }

    public Single<ActionResult> positiveAction(ActionResult.Event event) {
        switch (event) {
            case CONFIRMATION_END_DAY:
                mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.FINISHED_WORK));
                mPreferences.progressState().set(ProgressState.CHECK_QUESTIONNAIRE_AVAILABLE);
                break;
        }
        return Single.just(ActionResult.createResultWithoutEvent());
    }

    public Single<ActionResult> negativeAction(ActionResult.Event event) {
        Single<ActionResult> actionResultSingle;
        switch (event) {
            case PREMATURE_TERMINATION_WARNING:
                mPreferences.progressState().set(ProgressState.FINISHED_WORK_DAY);
                mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.FINISHED_WORK));
                actionResultSingle = createViolation(ViolationModel.Type.PREMATURE_TERMINATION,
                        PREMATURE_TERMINATION_VIOLATION);
                break;
            case BREAK_IN_FIRST_HOUR_WARNING:
                mPreferences.breakState().set(true);
                mPreferences.breakTime().set(new Date());
                actionResultSingle = createViolation(ViolationModel.Type.BREAK_IN_THE_FIRST_HOUR,
                        BREAK_IN_FIRST_HOUR_VIOLATION);
                break;
            case BREAK_IN_LAST_HOUR_WARNING:
                mPreferences.breakState().set(true);
                mPreferences.breakTime().set(new Date());
                actionResultSingle = createViolation(ViolationModel.Type.BREAK_IN_THE_LAST_HOUR,
                        BREAK_IN_LAST_HOUR_VIOLATION);
                break;
            case MORE_ON_BREAK_PER_HOUR_WARNING:
                mPreferences.breakState().set(true);
                mPreferences.breakTime().set(new Date());
                actionResultSingle = createViolation(ViolationModel.Type.MORE_ON_BREAK_PER_HOUR,
                        MORE_ON_BREAK_PER_HOUR_VIOLATION);
                break;
            default:
                actionResultSingle = Single.just(ActionResult.createResultWithoutEvent());
        }
        if (event == BREAK_IN_FIRST_HOUR_WARNING ||
                event == BREAK_IN_LAST_HOUR_WARNING ||
                event == MORE_ON_BREAK_PER_HOUR_WARNING) {
            mWorkRepository.addStatus(WorkStatusModel.create(WorkStatus.IN_BREAK));
            return actionResultSingle;
        } else {
            return actionResultSingle;
        }
    }

    private Single<ActionResult> createViolation(ViolationModel.Type type, ActionResult.Event event) {
        return mCreateViolationUseCase.setType(type)
                .buildUseCaseObservable()
                .map(aBoolean -> ActionResult.createResult(event))
                .singleOrError();
    }

    private Single<ActionResult> createViolation(ViolationModel.Type type, ActionResult event) {
        return mCreateViolationUseCase.setType(type)
                .buildUseCaseObservable()
                .map(aBoolean -> event)
                .singleOrError();
    }

    public Single<ActionResult> checkFalsifications() {
        return mCheckFalsificationsUseCase.buildUseCaseObservable()
                .flatMap(types -> {
                    if (!types.isEmpty()) {
                        return Observable.just(ActionResult.createResult(types.get(0) == ViolationModel.Type.FALSIFICATION_OF_STATISTICS ? FALSIFICATION_OF_STATISTICS_VIOLATION :
                                FALSIFICATION_OF_TIME_VIOLATION, false));
                    } else {
                        return Observable.just(ActionResult.createResultWithoutEvent());
                    }
                })
                .singleOrError();
    }

    public Single<Boolean> checkQuestionnaireAvailable() {
        int id = mPreferences.authCredentialsModel().get().getProjectModel().getId();
        return questionnaireRepository.getAll(id)
                .map(questionnaireModels -> {
                    if (questionnaireModels.isEmpty()) {
                        mPreferences.progressState().set(ProgressState.COMPLETE_FINISH_QUESTIONNAIRE);
                    } else {
                        mPreferences.progressState().set(ProgressState.FINISHED_WORK_DAY);
                    }
                    return !questionnaireModels.isEmpty();
                }).first(false);

    }
}
